using System;
using UnityEngine;

public class Input_Manager : MonoSingleton<Input_Manager>
{
    #region enum

    /// <summary>
    /// ボタンインデックス番号
    /// </summary>
    public enum ButtonIndex
    {
        A,
        B,
        X,
        Y,
        LB,
        RB,
        View,
        Menu,
        LStick,
        RStick,
    }

    /// <summary>
    /// 軸インデックス番号
    /// </summary>
    public enum AxisIndex
    {
        Left,
        Right,
        Direction,
    }

    /// <summary> ButtonIndexの要素数取得用 </summary>
    static int ButtonIndexNum { get => System.Enum.GetNames(typeof(ButtonIndex)).Length; }

    /// <summary> ButtonIndexの要素数取得用 </summary>
    static int AxisIndexNum { get => System.Enum.GetNames(typeof(AxisIndex)).Length; }

    #endregion

    /// <summary> Press入力か </summary>
    static BitFlag flagPress = new BitFlag(0);

    /// <summary> Trigger入力か </summary>
    static BitFlag flagTrigger = new BitFlag(1);

    /// <summary> Up入力か </summary>
    static BitFlag flagUp = new BitFlag(2);

    /// <summary> FixedUpdate内でのPress入力か </summary>
    static BitFlag flagFixedPress = new BitFlag(3);

    /// <summary> FixedUpdate内でのTrigger入力か </summary>
    static BitFlag flagFixedTrigger = new BitFlag(4);

    /// <summary> FixedUpdate内でのUp入力か </summary>
    static BitFlag flagFixedUp = new BitFlag(5);

    /// <summary> ボタン名群 </summary>
    [SerializeField] string[] buttonNames = new string[ButtonIndexNum];

    /// <summary> 横軸名群 </summary>
    [SerializeField] string[] horizontalAxisNames = new string[AxisIndexNum];

    /// <summary> 縦軸名群 </summary>
    [SerializeField] string[] verticalAxisNames = new string[AxisIndexNum];

    /// <summary> トリガーボタン名群 </summary>
    [SerializeField] string triggerName = "";

    /// <summary> ボタンの入力フラグ群 </summary>
    BitFlag[] buttonFlags = new BitFlag[ButtonIndexNum];

    /// <summary> 何らかのボタンの入力フラグ</summary>
    BitFlag anyButtonFlag = new BitFlag();

    /// <summary> 軸の入力群 </summary>
    Vector2[] axisValues = new Vector2[AxisIndexNum];

    /// <summary> 軸のRaw入力群 </summary>
    Vector2[] axisRawValues = new Vector2[AxisIndexNum];

    /// <summary> 軸ボタンの入力群 </summary>
    Vector2 triggerValue = new Vector2();

    /// <summary> 軸ボタンのRaw入力群 </summary>
    Vector2 triggerRawValue = new Vector2();

    #region 初期化

    /// <summary>
    /// 一回目のAwakeで呼ばれる関数
    /// </summary>
    protected override void FirstAwakeProcess()
    {
        for (int i = 0; i < buttonFlags.Length; ++i)
            buttonFlags[i] = new BitFlag();

        for (int i = 0; i < axisValues.Length; ++i)
            axisValues[i] = Vector2.zero;

        for (int i = 0; i < axisRawValues.Length; ++i)
            axisRawValues[i] = Vector2Int.zero;

        anyButtonFlag.Clear();

        triggerValue = Vector2Int.zero;
        triggerRawValue = Vector2Int.zero;
    }

    #endregion

    #region 更新

    void Update()
    {
        bool prevAnyPress = anyButtonFlag.CheckAnyPop(flagPress);
        bool currentAnyPress = Input.anyKey;

        for (int i = 0, iLength = ButtonIndexNum; i < iLength; ++i)
        {
            ref var buttonFlag = ref buttonFlags[i];
            var buttonName = buttonNames[i];

            bool prevPress = buttonFlag.CheckAnyPop(flagPress);
            bool currentPress = Input.GetButton(buttonName);

            if (currentPress)
                currentAnyPress = true;

            buttonFlag.PopOrUnPop(flagPress, currentPress);
            buttonFlag.PopOrUnPop(flagTrigger, currentPress && !prevPress);
            buttonFlag.PopOrUnPop(flagUp, !currentPress && prevPress);
        }

        anyButtonFlag.PopOrUnPop(flagPress, currentAnyPress);
        anyButtonFlag.PopOrUnPop(flagTrigger, currentAnyPress && !prevAnyPress);
        anyButtonFlag.PopOrUnPop(flagUp, !currentAnyPress && prevAnyPress);

        for (int i = 0, iLength = AxisIndexNum; i < iLength; ++i)
        {
            ref var axisValue = ref axisValues[i];
            ref var axisRawValue = ref axisRawValues[i];
            var horizontalAxisName = horizontalAxisNames[i];
            var virticalAxisName = verticalAxisNames[i];

            axisValue.x = Input.GetAxis(horizontalAxisName);
            axisValue.y = Input.GetAxis(virticalAxisName);

            axisRawValue.x = Input.GetAxisRaw(horizontalAxisName);
            axisRawValue.y = Input.GetAxisRaw(virticalAxisName);
        }

        triggerValue.x = Input.GetAxis(triggerName);
        triggerValue.y = Input.GetAxis(triggerName);

        triggerRawValue.x = Input.GetAxisRaw(triggerName);
        triggerRawValue.y = Input.GetAxisRaw(triggerName);
    }

    void FixedUpdate()
    {
        bool prevAnyPress = anyButtonFlag.CheckAnyPop(flagFixedPress);
        bool currentAnyPress = Input.anyKey;

        for (int i = 0, iLength = ButtonIndexNum; i < iLength; ++i)
        {
            ref var buttonFlag = ref buttonFlags[i];
            var buttonName = buttonNames[i];

            bool prevPress = buttonFlag.CheckAnyPop(flagFixedPress);
            bool currentPress = Input.GetButton(buttonName);

            if (currentPress)
                currentAnyPress = true;

            buttonFlag.PopOrUnPop(flagFixedPress, currentPress);
            buttonFlag.PopOrUnPop(flagFixedTrigger, currentPress && !prevPress);
            buttonFlag.PopOrUnPop(flagFixedUp, !currentPress && prevPress);
        }

        anyButtonFlag.PopOrUnPop(flagFixedPress, currentAnyPress);
        anyButtonFlag.PopOrUnPop(flagFixedTrigger, currentAnyPress && !prevAnyPress);
        anyButtonFlag.PopOrUnPop(flagFixedUp, !currentAnyPress && prevAnyPress);
    }

    #endregion

    #region 名前

    /// <summary>
    /// ボタン名取得
    /// </summary>
    /// <returns> ボタン名 </returns>
    public string[] GetButtonNames()
    {
        return buttonNames;
    }

    /// <summary>
    /// ボタン名設定
    /// </summary>
    /// <param name="names"> ボタン名 </param>
    public void SetButtonNames(string[] names)
    {
        if (names.Length != ButtonIndexNum)
            Array.Resize(ref names, AxisIndexNum);

        buttonNames = names;
    }

    /// <summary>
    /// 横軸名取得
    /// </summary>
    /// <returns> 横軸名 </returns>
    public string[] GetHorizontalAxisNames()
    {
        return horizontalAxisNames;
    }

    /// <summary>
    /// 横軸名設定
    /// </summary>
    /// <param name="names"> 横軸名 </param>
    public void SetHorizontalAxisNames(string[] names)
    {
        if (names.Length != AxisIndexNum)
            Array.Resize(ref names, AxisIndexNum);

        horizontalAxisNames = names;
    }

    /// <summary>
    /// 縦軸名取得
    /// </summary>
    /// <returns> 縦軸名 </returns>
    public string[] GetVerticalAxisNames()
    {
        return verticalAxisNames;
    }

    /// <summary>
    /// 縦軸名設定
    /// </summary>
    /// <param name="names"> 縦軸名 </param>
    public void SetVerticalAxisNames(string[] names)
    {
        if (names.Length != AxisIndexNum)
            Array.Resize(ref names, AxisIndexNum);

        verticalAxisNames = names;
    }

    /// <summary>
    /// トリガーボタン名取得
    /// </summary>
    /// <returns> トリガーボタン名 </returns>
    public string GetTriggerName()
    {
        return triggerName;
    }

    /// <summary>
    /// トリガーボタン名設定
    /// </summary>
    /// <param name="name"> トリガーボタン名 </param>
    public void SetTriggerName(string name)
    {
        triggerName = name;
    }

    #endregion

    #region 何らかのボタン入力

    /// <summary>
    /// Update時何らかのボタンのTrigger入力取得
    /// </summary>
    /// <returns> 入力 </returns>
    public bool GetAnyButtonTrigger()
    {
        return anyButtonFlag.CheckAnyPop(flagTrigger);
    }

    /// <summary>
    /// Update時何らかのボタンのPress入力取得
    /// </summary>
    /// <returns> 入力 </returns>
    public bool GetAnyButtonPress()
    {
        return anyButtonFlag.CheckAnyPop(flagPress);
    }

    /// <summary>
    /// Update時何らかのボタンのUp入力取得
    /// </summary>
    /// <returns> 入力 </returns>
    public bool GetAnyButtonUp()
    {
        return anyButtonFlag.CheckAnyPop(flagUp);
    }

    /// <summary>
    /// FixedUpdate時何らかのボタンのTrigger入力取得
    /// </summary>
    /// <returns> 入力 </returns>
    public bool GetAnyButtonFixedTrigger()
    {
        return anyButtonFlag.CheckAnyPop(flagFixedTrigger);
    }

    /// <summary>
    /// FixedUpdate時何らかのボタンのPress入力取得
    /// </summary>
    /// <returns> 入力 </returns>
    public bool GetAnyButtonFixedPress()
    {
        return anyButtonFlag.CheckAnyPop(flagFixedPress);
    }

    /// <summary>
    /// FixedUpdate時何らかのボタンのUp入力取得
    /// </summary>
    /// <returns> 入力 </returns>
    public bool GetAnyButtonFixedUp()
    {
        return anyButtonFlag.CheckAnyPop(flagFixedUp);
    }

    #endregion

    #region ボタン入力

    /// <summary>
    /// Update時ボタンのTrigger入力取得
    /// </summary>
    /// <param name="buttonIndex"> ボタン番号 </param>
    /// <returns> 入力 </returns>
    public bool GetButtonTrigger(ButtonIndex buttonIndex)
    {
        var index = (int)buttonIndex;

        if (index >= buttonFlags.Length)
            return false;

        return buttonFlags[index].CheckAnyPop(flagTrigger);
    }

    /// <summary>
    /// Update時ボタンのPress入力取得
    /// </summary>
    /// <param name="buttonIndex"> ボタン番号 </param>
    /// <returns> 入力 </returns>
    public bool GetButtonPress(ButtonIndex buttonIndex)
    {
        var index = (int)buttonIndex;

        if (index >= buttonFlags.Length)
            return false;

        return buttonFlags[index].CheckAnyPop(flagPress);
    }

    /// <summary>
    /// Update時ボタンのUp入力取得
    /// </summary>
    /// <param name="buttonIndex"> ボタン番号 </param>
    /// <returns> 入力 </returns>
    public bool GetButtonUp(ButtonIndex buttonIndex)
    {
        var index = (int)buttonIndex;

        if (index >= buttonFlags.Length)
            return false;

        return buttonFlags[index].CheckAnyPop(flagUp);
    }

    /// <summary>
    /// FixedUpdate時ボタンのTrigger入力取得
    /// </summary>
    /// <param name="buttonIndex"> ボタン番号 </param>
    /// <returns> 入力 </returns>
    public bool GetButtonFixedTrigger(ButtonIndex buttonIndex)
    {
        var index = (int)buttonIndex;

        if (index >= buttonFlags.Length)
            return false;

        return buttonFlags[index].CheckAnyPop(flagFixedTrigger);
    }

    /// <summary>
    /// FixedUpdate時ボタンのPress入力取得
    /// </summary>
    /// <param name="buttonIndex"> ボタン番号 </param>
    /// <returns> 入力 </returns>
    public bool GetButtonFixedPress(ButtonIndex buttonIndex)
    {
        var index = (int)buttonIndex;

        if (index >= buttonFlags.Length)
            return false;

        return buttonFlags[index].CheckAnyPop(flagFixedPress);
    }

    /// <summary>
    /// FixedUpdate時ボタンのUp入力取得
    /// </summary>
    /// <param name="buttonIndex"> ボタン番号 </param>
    /// <returns> 入力 </returns>
    public bool GetButtonFixedUp(ButtonIndex buttonIndex)
    {
        var index = (int)buttonIndex;

        if (index >= buttonFlags.Length)
            return false;

        return buttonFlags[index].CheckAnyPop(flagFixedUp);
    }

    #endregion

    #region 軸入力

    /// <summary>
    /// 軸入力取得
    /// </summary>
    /// <param name="axisIndex"></param>
    /// <returns> 軸入力 </returns>
    public Vector2 GetAxis(AxisIndex axisIndex)
    {
        var index = (int)axisIndex;

        if (index >= axisValues.Length)
            return Vector2.zero;

        return axisValues[index];
    }

    /// <summary>
    /// 軸Raw入力取得
    /// </summary>
    /// <param name="axisIndex"></param>
    /// <returns> 軸入力 </returns>
    public Vector2 GetAxisRaw(AxisIndex axisIndex)
    {
        var index = (int)axisIndex;

        if (index >= axisRawValues.Length)
            return Vector2.zero;

        return axisRawValues[index];
    }

    #endregion

    #region トリガーボタン入力

    /// <summary>
    /// トリガーボタン入力取得
    /// </summary>
    /// <returns> トリガーボタン入力 </returns>
    Vector2 GetTrigger()
    {
        return triggerValue;
    }

    /// <summary>
    /// トリガーボタンRaw入力取得
    /// </summary>
    /// <returns> トリガーボタン入力 </returns>
    Vector2 GetTriggerRaw()
    {
        return triggerRawValue;
    }

    #endregion
}
