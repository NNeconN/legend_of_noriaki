using System.Collections.Generic;
using UnityEngine;

public class StageCamera : MonoBehaviour
{
    #region enum

    /// <summary>
    /// 計算方式
    /// </summary>
    public enum CalculateType
    {
        TimeRate,
        Lerp,
        Slerp,
        Sine,
        InverseSine,
        Cosine,
        InverseCosine,
    }

    #endregion

    /// <summary> ワイプ用マテリアル </summary>
    [SerializeField] Material wipeMaterial = null;

    /// <summary> 自身のカメラスクリプト </summary>
    Camera myCamera = null;

    /// <summary> 次のXY位置 </summary>
    List<NextPositionXY> nextXYPositions = new List<NextPositionXY>();

    /// <summary> 次のZ位置 </summary>
    List<NextPositionZ> nextZPositions = new List<NextPositionZ>();

    /// <summary> 次の視野角 </summary>
    List<NextFieldOfViewParameter> nextFieldOfViewParameters = new List<NextFieldOfViewParameter>();

    /// <summary> 実際のカメラの位置 </summary>
    Vector3 originalPosition = Vector3.zero;

    /// <summary> 実際の視野角 </summary>
    float originalFieldOfView = 60.0f;

    /// <summary> 演出の現在の半径 </summary>
    float currentRadius = 1.0f;

    /// <summary> 演出の次回の半径 </summary>
    float nextRadius = 1.0f;

    /// <summary> 演出の時間 </summary>
    [SerializeField] float wipeTime = 1.0f;

    /// <summary> 自身のカメラスクリプト取得用 </summary>
    public Camera MyCamera { get => myCamera; }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        if (!wipeMaterial)
            return;

        Graphics.Blit(source, dest, wipeMaterial);
    }

    private void Awake()
    {
        if (!myCamera)
            Initialize();

        ForcedSetRadius(1.0f);
    }

    private void Update()
    {
        if (wipeTime <= 0.0f)
            currentRadius = nextRadius;
        else if (currentRadius < nextRadius)
            currentRadius = Mathf.Min(currentRadius + (Time.unscaledDeltaTime / wipeTime), nextRadius);
        else if (currentRadius > nextRadius)
            currentRadius = Mathf.Max(currentRadius - (Time.unscaledDeltaTime / wipeTime), nextRadius);

        wipeMaterial?.SetFloat("_Radius", currentRadius * 2.0f);
    }

    private void LateUpdate()
    {
        transform.eulerAngles = Vector3.zero;

        UpdateFieldOfView();
        UpdatePosition();
    }

    /// <summary>
    /// 初期化処理
    /// </summary>
    public void Initialize()
    {
        myCamera = GetComponent<Camera>();
        if (!myCamera)
            myCamera = gameObject.AddComponent<Camera>();

        originalPosition = transform.position;
        originalFieldOfView = myCamera.fieldOfView;
    }

    #region ワイプ演出関係

    /// <summary>
    /// 現在の半径を取得
    /// </summary>
    /// <returns> 半径 </returns>
    public float GetRadius()
    {
        return currentRadius;
    }

    /// <summary>
    /// 半径を設定
    /// </summary>
    /// <param name="radius"> 半径 </param>
    public void SetRadius(float radius)
    {
        nextRadius = Mathf.Clamp(radius, 0.0f, 1.0f);
    }

    /// <summary>
    /// 半径を強制的に設定
    /// </summary>
    /// <param name="radius"> 半径 </param>
    public void ForcedSetRadius(float radius)
    {
        nextRadius = Mathf.Clamp(radius, 0.0f, 1.0f);
        currentRadius = nextRadius;
        wipeMaterial?.SetFloat("_Radius", currentRadius * 2.0f);
    }

    /// <summary>
    /// ワイプにかかる時間設定
    /// </summary>
    /// <param name="time"> 時間 </param>
    public void SetWiprTime(float time)
    {
        wipeTime = time;
    }

    #endregion

    #region 位置関係

    /// <summary>
    /// 位置の更新
    /// </summary>
    public void UpdatePosition()
    {
        if (nextXYPositions.Count > 0)
        {
            var isFinish = nextXYPositions[0].UpdateTime(Time.deltaTime);

            var positionXY = nextXYPositions[0].GetPosition();
            originalPosition.x = positionXY.x;
            originalPosition.y = positionXY.y;

            if (isFinish)
                nextXYPositions.RemoveAt(0);
        }

        if (nextZPositions.Count > 0)
        {
            var isFinish = nextZPositions[0].UpdateTime(Time.deltaTime);

            originalPosition.z = nextZPositions[0].GetPosition();

            if (isFinish)
                nextZPositions.RemoveAt(0);
        }

        transform.position = originalPosition;
    }

    /// <summary>
    /// XY位置を強制的に変更する
    /// </summary>
    /// <param name="position"> XY位置 </param>
    public void ForcedChangePositionXY(Vector2 position)
    {
        DestroyNextPositionXY();

        originalPosition.x = position.x;
        originalPosition.y = position.y;
        transform.position = originalPosition;
    }

    /// <summary>
    /// Z位置を強制的に変更する
    /// </summary>
    /// <param name="position"> Z位置 </param>
    public void ForcedChangePositionZ(float position)
    {
        DestroyNextPositionZ();

        originalPosition.z = position;
        transform.position = originalPosition;
    }

    /// <summary>
    /// 次のXY位置を破棄する
    /// </summary>
    public void DestroyNextPositionXY()
    {
        nextXYPositions.Clear();
    }

    /// <summary>
    /// 次のZ位置を破棄する
    /// </summary>
    public void DestroyNextPositionZ()
    {
        nextZPositions.Clear();
    }

    /// <summary>
    /// XY位置設定
    /// </summary>
    /// <param name="position"> XY位置 </param>
    /// <param name="moveTime"> 遷移時間 </param>
    /// <param name="calculateType"> 遷移計算方式 </param>
    public void SetPositionXY(Vector2 position, float moveTime, CalculateType calculateType)
    {
        Vector2 prevPosition = new Vector2(originalPosition.x, originalPosition.y);

        if (nextXYPositions.Count > 0)
            prevPosition = nextXYPositions[nextXYPositions.Count - 1].NextPosition;

        nextXYPositions.Add(new NextPositionXY(prevPosition, position, moveTime, calculateType));
    }

    /// <summary>
    /// Z位置設定
    /// </summary>
    /// <param name="position"> Z位置 </param>
    /// <param name="moveTime"> 遷移時間 </param>
    /// <param name="calculateType"> 遷移計算方式 </param>
    public void SetPositionZ(float position, float moveTime, CalculateType calculateType)
    {
        float prevPosition = originalPosition.z;

        if (nextZPositions.Count > 0)
            prevPosition = nextZPositions[nextZPositions.Count - 1].NextPosition;

        nextZPositions.Add(new NextPositionZ(prevPosition, position, moveTime, calculateType));
    }

    #endregion

    #region 視野角関係

    /// <summary>
    /// 視野角の更新
    /// </summary>
    void UpdateFieldOfView()
    {
        if (nextFieldOfViewParameters.Count > 0)
        {
            var isFinish = nextFieldOfViewParameters[0].UpdateTime(Time.deltaTime);

            originalFieldOfView = nextFieldOfViewParameters[0].GetFieldOfView();

            if (isFinish)
                nextFieldOfViewParameters.RemoveAt(0);
        }

        myCamera.fieldOfView = originalFieldOfView;
    }

    /// <summary>
    /// 視野角を強制的に変更する
    /// </summary>
    /// <param name="fieldOfView"> 視野角 </param>
    public void ForcedChangeFieldOfView(float fieldOfView)
    {
        DestroyNextFieldOfView();

        myCamera.fieldOfView = originalFieldOfView = fieldOfView;
    }

    /// <summary>
    /// 次の視野角を破棄する
    /// </summary>
    public void DestroyNextFieldOfView()
    {
        nextFieldOfViewParameters.Clear();
    }

    /// <summary>
    /// 視野角設定
    /// </summary>
    /// <param name="fieldOfView"> 視野角 </param>
    /// <param name="moveTime"> 遷移時間 </param>
    /// <param name="calculateType"> 遷移計算方式 </param>
    public void SetFieldOfView(float fieldOfView, float moveTime, CalculateType calculateType)
    {
        var prevFieldOfView = originalFieldOfView;

        if (nextFieldOfViewParameters.Count > 0)
            prevFieldOfView = nextFieldOfViewParameters[nextFieldOfViewParameters.Count - 1].NextFieldOfView;

        nextFieldOfViewParameters.Add(new NextFieldOfViewParameter(prevFieldOfView, fieldOfView, moveTime, calculateType));
    }

    #endregion

    /// <summary>
    /// 次のXY位置管理用
    /// </summary>
    class NextPositionXY
    {
        /// <summary> 前回の位置 </summary>
        Vector2 prevPosition;

        /// <summary> 次回の位置 </summary>
        Vector2 nextPosition;

        /// <summary> 遷移用経過時間 </summary>
        float moveDeltaTime;

        /// <summary> 遷移時間 </summary>
        float moveTime;

        /// <summary> 遷移計算方式 </summary>
        CalculateType calculateType;

        /// <summary> 前回の位置取得用 </summary>
        public Vector2 PrevPosition { get => prevPosition; }

        /// <summary> 次回の位置取得用 </summary>
        public Vector2 NextPosition { get => nextPosition; }

        /// <summary> 遷移用経過時間取得用 </summary>
        public float MoveDeltaTime { get => moveDeltaTime; }

        /// <summary> 遷移時間取得用 </summary>
        public float MoveTime { get => moveTime; }

        /// <summary> 遷移計算方式取得用 </summary>
        public CalculateType CalculateType { get => calculateType; }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="prevPosition"> 前回の位置 </param>
        /// <param name="nextPosition"> 次回の位置 </param>
        /// <param name="moveTime"> 遷移時間 </param>
        /// <param name="calculateType"> 遷移計算方式 </param>
        public NextPositionXY(Vector2 prevPosition, Vector2 nextPosition, float moveTime, CalculateType calculateType)
        {
            this.prevPosition = prevPosition;
            this.nextPosition = nextPosition;
            this.moveDeltaTime = 0.0f;
            this.moveTime = moveTime;
            this.calculateType = calculateType;
        }

        /// <summary>
        /// 経過時間の更新
        /// </summary>
        /// <param name="deltaTime"> フレーム経過時間 </param>
        /// <returns> 遷移時間を超えたか </returns>
        public bool UpdateTime(float deltaTime)
        {
            return (moveDeltaTime += deltaTime) >= moveTime;
        }

        /// <summary>
        /// 位置取得
        /// </summary>
        /// <returns> 位置 </returns>
        public Vector2 GetPosition()
        {
            if (moveTime <= 0.0f)
                return nextPosition;

            var rate = Mathf.Min(moveDeltaTime / moveTime, 1.0f);

            switch (calculateType)
            {
                default:
                case CalculateType.TimeRate:
                    return (prevPosition * (1.0f - rate)) + (nextPosition * rate);
                case CalculateType.Lerp:
                    return Vector2.Lerp(prevPosition, nextPosition, rate);
                case CalculateType.Slerp:
                    return Vector2.Lerp(prevPosition, nextPosition, rate);
                case CalculateType.Sine:
                    {
                        var sin = Mathf.Sin(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - sin)) + (nextPosition * sin);
                    }
                case CalculateType.InverseSine:
                    {
                        var sin = 1.0f - Mathf.Sin(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - sin)) + (nextPosition * sin);
                    }
                case CalculateType.Cosine:
                    {
                        var cos = Mathf.Cos(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - cos)) + (nextPosition * cos);
                    }
                case CalculateType.InverseCosine:
                    {
                        var cos = 1.0f - Mathf.Cos(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - cos)) + (nextPosition * cos);
                    }
            }
        }
    }

    /// <summary>
    /// 次のZ位置管理用
    /// </summary>
    class NextPositionZ
    {
        /// <summary> 前回の位置 </summary>
        float prevPosition;

        /// <summary> 次回の位置 </summary>
        float nextPosition;

        /// <summary> 遷移用経過時間 </summary>
        float moveDeltaTime;

        /// <summary> 遷移時間 </summary>
        float moveTime;

        /// <summary> 遷移計算方式 </summary>
        CalculateType calculateType;

        /// <summary> 前回の位置取得用 </summary>
        public float PrevPosition { get { return prevPosition; } }

        /// <summary> 次回の位置取得用 </summary>
        public float NextPosition { get { return nextPosition; } }

        /// <summary> 遷移用経過時間取得用 </summary>
        public float MoveDeltaTime { get { return moveDeltaTime; } }

        /// <summary> 遷移時間取得用 </summary>
        public float MoveTime { get { return moveTime; } }

        /// <summary> 遷移計算方式取得用 </summary>
        public CalculateType CalculateType { get { return calculateType; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="prevPosition"> 前回の位置 </param>
        /// <param name="nextPosition"> 次回の位置 </param>
        /// <param name="moveTime"> 遷移時間 </param>
        /// <param name="calculateType"> 遷移計算方式 </param>
        public NextPositionZ(float prevPosition, float nextPosition, float moveTime, CalculateType calculateType)
        {
            this.prevPosition = prevPosition;
            this.nextPosition = nextPosition;
            this.moveDeltaTime = 0.0f;
            this.moveTime = moveTime;
            this.calculateType = calculateType;
        }

        /// <summary>
        /// 経過時間の更新
        /// </summary>
        /// <param name="deltaTime"> フレーム経過時間 </param>
        /// <returns> 遷移時間を超えたか </returns>
        public bool UpdateTime(float deltaTime)
        {
            return (moveDeltaTime += deltaTime) >= moveTime;
        }

        /// <summary>
        /// 位置取得
        /// </summary>
        /// <returns> 位置 </returns>
        public float GetPosition()
        {
            if (moveTime <= 0.0f)
                return nextPosition;

            var rate = Mathf.Min(moveDeltaTime / moveTime, 1.0f);

            switch (calculateType)
            {
                default:
                case CalculateType.TimeRate:
                    return (prevPosition * (1.0f - rate)) + (nextPosition * rate);
                case CalculateType.Lerp:
                    return Mathf.Lerp(prevPosition, nextPosition, rate);
                case CalculateType.Slerp:
                    return Mathf.Lerp(prevPosition, nextPosition, rate);
                case CalculateType.Sine:
                    {
                        var sin = Mathf.Sin(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - sin)) + (nextPosition * sin);
                    }
                case CalculateType.InverseSine:
                    {
                        var sin = 1.0f - Mathf.Sin(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - sin)) + (nextPosition * sin);
                    }
                case CalculateType.Cosine:
                    {
                        var cos = Mathf.Cos(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - cos)) + (nextPosition * cos);
                    }
                case CalculateType.InverseCosine:
                    {
                        var cos = 1.0f - Mathf.Cos(90.0f * rate * Mathf.Deg2Rad);
                        return (prevPosition * (1.0f - cos)) + (nextPosition * cos);
                    }
            }
        }
    }

    /// <summary>
    /// 次の視野角管理用
    /// </summary>
    class NextFieldOfViewParameter
    {
        /// <summary> 前回の視野角 </summary>
        float prevFieldOfView;

        /// <summary> 次回の視野角 </summary>
        float nextFieldOfView;

        /// <summary> 遷移用経過時間 </summary>
        float moveDeltaTime;

        /// <summary> 遷移時間 </summary>
        float moveTime;

        /// <summary> 遷移計算方式 </summary>
        CalculateType calculateType;

        /// <summary> 前回の視野角取得用 </summary>
        public float PrevFieldOfView { get { return prevFieldOfView; } }

        /// <summary> 次回の視野角取得用 </summary>
        public float NextFieldOfView { get { return nextFieldOfView; } }

        /// <summary> 遷移用経過時間取得用 </summary>
        public float MoveDeltaTime { get { return moveDeltaTime; } }

        /// <summary> 遷移時間取得用 </summary>
        public float MoveTime { get { return moveTime; } }

        /// <summary> 遷移計算方式取得用 </summary>
        public CalculateType CalculateType { get { return calculateType; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="prevFieldOfView"> 前回の視野角 </param>
        /// <param name="nextFieldOfView"> 次回の視野角 </param>
        /// <param name="moveTime"> 遷移時間 </param>
        /// <param name="calculateType"> 遷移計算方式 </param>
        public NextFieldOfViewParameter(float prevFieldOfView, float nextFieldOfView, float moveTime, CalculateType calculateType)
        {
            this.prevFieldOfView = prevFieldOfView;
            this.nextFieldOfView = nextFieldOfView;
            this.moveDeltaTime = 0.0f;
            this.moveTime = moveTime;
            this.calculateType = calculateType;
        }

        /// <summary>
        /// 経過時間の更新
        /// </summary>
        /// <param name="deltaTime"> フレーム経過時間 </param>
        /// <returns> 遷移時間を超えたか </returns>
        public bool UpdateTime(float deltaTime)
        {
            return (moveDeltaTime += deltaTime) >= moveTime;
        }

        /// <summary>
        /// 視野角取得
        /// </summary>
        /// <returns> 視野角 </returns>
        public float GetFieldOfView()
        {
            if (moveTime <= 0.0f)
                return nextFieldOfView;

            var rate = Mathf.Min(moveDeltaTime / moveTime, 1.0f);

            switch (calculateType)
            {
                default:
                case CalculateType.TimeRate:
                    return (prevFieldOfView * (1.0f - rate)) + (nextFieldOfView * rate);
                case CalculateType.Lerp:
                    return Mathf.Lerp(prevFieldOfView, nextFieldOfView, rate);
                case CalculateType.Slerp:
                    return Mathf.Lerp(prevFieldOfView, nextFieldOfView, rate);
                case CalculateType.Sine:
                    {
                        var sin = Mathf.Sin(90.0f * rate * Mathf.Deg2Rad);
                        return (prevFieldOfView * (1.0f - sin)) + (nextFieldOfView * sin);
                    }
                case CalculateType.InverseSine:
                    {
                        var sin = 1.0f - Mathf.Sin(90.0f * rate * Mathf.Deg2Rad);
                        return (prevFieldOfView * (1.0f - sin)) + (nextFieldOfView * sin);
                    }
                case CalculateType.Cosine:
                    {
                        var cos = Mathf.Cos(90.0f * rate * Mathf.Deg2Rad);
                        return (prevFieldOfView * (1.0f - cos)) + (nextFieldOfView * cos);
                    }
                case CalculateType.InverseCosine:
                    {
                        var cos = 1.0f - Mathf.Cos(90.0f * rate * Mathf.Deg2Rad);
                        return (prevFieldOfView * (1.0f - cos)) + (nextFieldOfView * cos);
                    }
            }
        }
    }
}
