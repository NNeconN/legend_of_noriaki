using UnityEngine;

public class ColorOutFade : Fade
{
    /// <summary> 自身のレンダラー </summary>
    MeshRenderer myRenderer = null;

    /// <summary> フェードカラー </summary>
    [SerializeField] Color color = Color.white;

    /// <summary> 初期のフェード時間 </summary>
    [SerializeField] float defaultFadeTime = 1.0f;

    void Awake()
    {
        myRenderer = GetComponent<MeshRenderer>();
    }

    /// <summary>
    /// 有効化時に呼び出される処理
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    public override void OnAwake(float fadeRate)
    {
        SetColor(fadeRate);
    }

    /// <summary>
    /// 無効化時に呼び出される処理
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    public override void OnSleep(float fadeRate)
    {
        SetColor(fadeRate);
    }

    /// <summary>
    /// フェードイン更新
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    /// <param name="deltaTime"> 経過時間 </param>
    public override void FadeIn(float fadeRate, float deltaTime)
    {
        SetColor(fadeRate);
    }

    /// <summary>
    /// フェードアウト更新
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    /// <param name="deltaTime"> 経過時間 </param>
    public override void FadeOut(float fadeRate, float deltaTime)
    {
        SetColor(fadeRate);
    }

    /// <summary>
    /// 色変更
    /// </summary>
    /// <param name="fadeRate"></param>
    void SetColor(float fadeRate)
    {
        color.a = fadeRate;

        if (!myRenderer)
        {
            myRenderer = GetComponent<MeshRenderer>();
        }

        if (myRenderer)
        {
            if (Application.isPlaying)
            {
                var material = myRenderer.material;
                material.color = color;
                myRenderer.sharedMaterial = material;
            }
            else
            {
                var material = myRenderer.sharedMaterial;
                material.color = color;
                myRenderer.sharedMaterial = material;
            }
        }
    }

    /// <summary>
    /// 初期のフェード時間を取得する
    /// </summary>
    /// <returns> 初期のフェード時間 </returns>
    public override float GetDefaultFadeTime()
    {
        return defaultFadeTime;
    }
}
