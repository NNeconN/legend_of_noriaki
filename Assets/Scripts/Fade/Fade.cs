using UnityEngine;

public class Fade : MonoBehaviour
{
    /// <summary>
    /// 有効化時に呼び出される処理
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    public virtual void OnAwake(float fadeRate)
    {
    }

    /// <summary>
    /// 無効化時に呼び出される処理
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    public virtual void OnSleep(float fadeRate)
    {
    }

    /// <summary>
    /// フェードイン更新
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    /// <param name="deltaTime"> 経過時間 </param>
    public virtual void FadeIn(float fadeRate, float deltaTime)
    {
    }

    /// <summary>
    /// フェードアウト更新
    /// </summary>
    /// <param name="fadeRate"> フェードの割合 </param>
    /// <param name="deltaTime"> 経過時間 </param>
    public virtual void FadeOut(float fadeRate, float deltaTime)
    {
    }

    /// <summary>
    /// 初期のフェード時間を取得する
    /// </summary>
    /// <returns> 初期のフェード時間 </returns>
    public virtual float GetDefaultFadeTime()
    {
        return 1.0f;
    }
}
