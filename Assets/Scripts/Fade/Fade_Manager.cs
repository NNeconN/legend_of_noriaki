using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// フェードマネージャー
/// </summary>
public class Fade_Manager : MonoSingleton<Fade_Manager>
{
    /// <summary>
    /// フェードステート
    /// </summary>
    enum FadeState
    {
        Idle,
        FadeIn,
        FadeOut,
    }

    /// <summary> 自身の保有するフェード群 </summary>
    Dictionary<string, Fade> fadeTable = new Dictionary<string, Fade>();

    /// <summary> 現在使用中のフェード群 </summary>
    [SerializeField] List<Fade> currentFadeList;

    /// <summary> フェード用カメラ </summary>
    Camera fadeCamera = null;

    /// <summary> フェードステート </summary>
    FadeState fadeState = FadeState.Idle;

    /// <summary> 処理時間 </summary>
    float processTime = 0.0f;

    /// <summary> フェードにかける時間 </summary>
    [SerializeField] float fadeTime = 1.0f;

    /// <summary> 最初のフレームか </summary>
    bool isFirstFrame = false;

    /// <summary> 処理割合 </summary>
    public float ProcessRate { get => Mathf.Clamp(processTime, 0.0f, 1.0f); }

    #region 初期化

    /// <summary>
    /// 一回目のAwakeで呼ばれる関数
    /// </summary>
    protected override void FirstAwakeProcess()
    {
        fadeCamera = GetComponentInChildren<Camera>();

        for (int i = 0; i < transform.childCount; ++i)
        {
            var fadesTransform = transform.GetChild(i);
            var fades = fadesTransform.GetComponentsInChildren<Fade>();

            for (int j = 0; j < fades.Length; ++j)
            {
                if (fadeTable.ContainsValue(fades[j]))
                    continue;

                fades[j].OnSleep(0.0f);
                fades[j].gameObject.SetActive(false);
                fadeTable.Add(fades[j].gameObject.name, fades[j]);
            }
        }

        for (int i = 0; i < currentFadeList.Count; i++)
        {
            if (!fadeTable.ContainsValue(currentFadeList[i]))
            {
                currentFadeList.RemoveAt(i);
                --i;
                continue;
            }

            currentFadeList[i].gameObject.SetActive(true);
            currentFadeList[i].OnAwake(1.0f);
        }

        if (currentFadeList.Count < 1)
        {
            processTime = 0.0f;
            fadeState = FadeState.Idle;
        }
        else
        {
            processTime = 1.0f;
            fadeState = FadeState.FadeIn;
        }
    }

    /// <summary>
    /// Awakeで呼ばれる関数
    /// </summary>
    protected override void AwakeProcess()
    {
        base.AwakeProcess();

        Debug.Log("Awake");

        isFirstFrame = true;
    }

    #endregion

    #region 更新

    void Update()
    {
        if (isFirstFrame)
        {
            isFirstFrame = false;
            return;
        }

        var deltaTime = Time.unscaledDeltaTime;

        switch (fadeState)
        {
            default:
            case FadeState.Idle:
                break;
            case FadeState.FadeIn:
                FadeInUpdate(deltaTime);
                break;
            case FadeState.FadeOut:
                FadeOutUpdate(deltaTime);
                break;
        }
    }

    /// <summary>
    /// フェードイン更新
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void FadeInUpdate(float deltaTime)
    {
        if (fadeTime <= 0.0f)
            processTime = 0.0f;
        else
            processTime = Mathf.Max(0.0f, processTime - (deltaTime / fadeTime));

        for (int i = 0; i < currentFadeList.Count; ++i)
            currentFadeList[i].FadeIn(processTime, deltaTime);

        if (processTime <= 0.0f)
            fadeState = FadeState.Idle;
    }

    /// <summary>
    /// フェードアウト更新
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void FadeOutUpdate(float deltaTime)
    {
        if (fadeTime <= 0.0f)
            processTime = 1.0f;
        else
            processTime = Mathf.Min(processTime + (deltaTime / fadeTime), 1.0f);

        for (int i = 0; i < currentFadeList.Count; ++i)
            currentFadeList[i].FadeOut(processTime, deltaTime);

        if (processTime >= 1.0f)
            fadeState = FadeState.Idle;
    }

    #endregion

    #region ステート開始

    /// <summary>
    /// フェードイン開始
    /// </summary>
    public void StartFadeIn()
    {
        fadeState = FadeState.FadeIn;
    }

    /// <summary>
    /// フェードイン開始
    /// </summary>
    /// <param name="fadeTime"> フェード時間 </param>
    public void StartFadeIn(float fadeTime)
    {
        this.fadeTime = fadeTime;
        fadeState = FadeState.FadeIn;
    }

    /// <summary>
    /// フェードアウト開始
    /// </summary>
    public void StartFadeOut()
    {
        fadeState = FadeState.FadeOut;
    }

    /// <summary>
    /// フェードアウト開始
    /// </summary>
    /// <param name="fadeTime"> フェード時間 </param>
    public void StartFadeOut(float fadeTime)
    {
        this.fadeTime = fadeTime;
        fadeState = FadeState.FadeOut;
    }

    #endregion

    /// <summary>
    /// 強制的にフェードインしたことにする
    /// </summary>
    public void ForcedSetFadeIn()
    {
        processTime = 0.0f;

        for (int i = 0; i < currentFadeList.Count; ++i)
            currentFadeList[i].FadeIn(processTime, 0.0f);
    }

    /// <summary>
    /// 強制的にフェードアウトしたことにする
    /// </summary>
    public void ForcedSetFadeOut()
    {
        processTime = 1.0f;

        for (int i = 0; i < currentFadeList.Count; ++i)
            currentFadeList[i].FadeIn(processTime, 0.0f);
    }

    #region ステートチェック

    /// <summary>
    /// 現在フェードイン中か
    /// </summary>
    /// <returns> フェードイン中か </returns>
    public bool CheckCurrentFadeIn()
    {
        return fadeState == FadeState.FadeIn;
    }

    /// <summary>
    /// 現在フェードアウト中か
    /// </summary>
    /// <returns> フェードアウト中か </returns>
    public bool CheckCurrentFadeOut()
    {
        return fadeState == FadeState.FadeOut;
    }

    /// <summary>
    /// 現在待機中か
    /// </summary>
    /// <returns> 待機中か </returns>
    public bool CheckCurrentIdle()
    {
        return fadeState == FadeState.Idle;
    }

    /// <summary>
    /// 現在フェードイン後の待機中か
    /// </summary>
    /// <returns> 待機中か </returns>
    public bool CheckCurrentIdleAfterFadeIn()
    {
        if (fadeState != FadeState.Idle)
            return false;

        return processTime <= 0.0f;
    }

    /// <summary>
    /// 現在フェードアウト後の待機中か
    /// </summary>
    /// <returns> 待機中か </returns>
    public bool CheckCurrentIdleAfterFadeOut()
    {
        if (fadeState != FadeState.Idle)
            return false;

        return processTime >= 1.0f;
    }

    /// <summary>
    /// 現在フェード途中後の待機中か
    /// </summary>
    /// <returns> 待機中か </returns>
    public bool CheckCurrentIdleAfterFade()
    {
        if (fadeState != FadeState.Idle)
            return false;

        return 0.0f < processTime && processTime < 1.0f;
    }

    #endregion
}
