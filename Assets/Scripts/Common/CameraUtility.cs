using UnityEngine;

public class CameraUtility : MonoBehaviour
{
    /// <summary>
    /// ����p�v�Z
    /// </summary>
    /// <param name="target"> �Ώ� </param>
    /// <param name="frustumWidth"> �`�扡�͈� </param>
    /// <param name="distance"> ���� </param>
    /// <returns> ����p </returns>
    public static float CalculateFieldOfViewFromFrustumWidth(Camera target, float frustumWidth, float distance)
    {
        var frustumHeight = frustumWidth / target.aspect;
        return CalculateFieldOfViewFromFrustumHeight(target, frustumHeight, distance);
    }

    /// <summary>
    /// ����p�v�Z
    /// </summary>
    /// <param name="target"> �Ώ� </param>
    /// <param name="frustumHeight"> �`��c�͈� </param>
    /// <param name="distance"> ���� </param>
    /// <returns> ����p </returns>
    public static float CalculateFieldOfViewFromFrustumHeight(Camera target, float frustumHeight, float distance)
    {
        return 2.0f * Mathf.Atan((frustumHeight * 0.5f) / distance) * Mathf.Rad2Deg;
    }
}
