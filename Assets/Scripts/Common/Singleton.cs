
/// <summary>
/// シングルトン用
/// </summary>
/// <typeparam name="T"> 対象Script </typeparam>
public abstract class Singleton<T> where T : new()
{
    /// <summary> インスタンス </summary>
    private static T instance;

    /// <summary>
    /// インスタンス取得用
    /// </summary>
    public static T Instance
    {
        get
        {
            if (instance == null)
                instance = new T();

            return instance;
        }
    }
}
