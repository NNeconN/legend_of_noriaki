using UnityEngine;

/// <summary>
/// 数学便利系
/// </summary>
public class MathUtility
{
    /// <summary>
    /// 最小値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最小値 </returns>
    public static Vector2Int Min(Vector2Int left, Vector2Int right)
    {
        var result = new Vector2Int();
        result.x = left.x < right.x ? left.x : right.x;
        result.y = left.y < right.y ? left.y : right.y;
        return result;
    }

    /// <summary>
    /// 最大値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最大値 </returns>
    public static Vector2Int Max(Vector2Int left, Vector2Int right)
    {
        var result = new Vector2Int();
        result.x = left.x > right.x ? left.x : right.x;
        result.y = left.y > right.y ? left.y : right.y;
        return result;
    }

    /// <summary>
    /// 範囲内に合わせる
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 範囲内に合わせた数 </returns>
    public static Vector2Int Clamp(Vector2Int target, Vector2Int min, Vector2Int max)
    {
        var result = Min(Max(target, min), max);
        return result;
    }

    /// <summary>
    /// 最小値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最小値 </returns>
    public static Vector3Int Min(Vector3Int left, Vector3Int right)
    {
        var result = new Vector3Int();
        result.x = left.x < right.x ? left.x : right.x;
        result.y = left.y < right.y ? left.y : right.y;
        result.z = left.z < right.z ? left.z : right.z;
        return result;
    }

    /// <summary>
    /// 最大値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最大値 </returns>
    public static Vector3Int Max(Vector3Int left, Vector3Int right)
    {
        var result = new Vector3Int();
        result.x = left.x > right.x ? left.x : right.x;
        result.y = left.y > right.y ? left.y : right.y;
        result.z = left.z > right.z ? left.z : right.z;
        return result;
    }

    /// <summary>
    /// 範囲内に合わせる
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 範囲内に合わせた数 </returns>
    public static Vector3Int Clamp(Vector3Int target, Vector3Int min, Vector3Int max)
    {
        var result = Min(Max(target, min), max);
        return result;
    }

    /// <summary>
    /// 最小値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最小値 </returns>
    public static Vector2 Min(Vector2 left, Vector2 right)
    {
        var result = new Vector2();
        result.x = left.x < right.x ? left.x : right.x;
        result.y = left.y < right.y ? left.y : right.y;
        return result;
    }

    /// <summary>
    /// 最大値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最大値 </returns>
    public static Vector2 Max(Vector2 left, Vector2 right)
    {
        var result = new Vector2();
        result.x = left.x > right.x ? left.x : right.x;
        result.y = left.y > right.y ? left.y : right.y;
        return result;
    }

    /// <summary>
    /// 範囲内に合わせる
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 範囲内に合わせた数 </returns>
    public static Vector2 Clamp(Vector2 target, Vector2 min, Vector2 max)
    {
        var result = Min(Max(target, min), max);
        return result;
    }

    /// <summary>
    /// 最小値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最小値 </returns>
    public static Vector3 Min(Vector3 left, Vector3 right)
    {
        var result = new Vector3();
        result.x = left.x < right.x ? left.x : right.x;
        result.y = left.y < right.y ? left.y : right.y;
        result.z = left.z < right.z ? left.z : right.z;
        return result;
    }

    /// <summary>
    /// 最大値を取得
    /// </summary>
    /// <param name="left"> 比較対象一つ目 </param>
    /// <param name="right"> 比較対象二つ目 </param>
    /// <returns> 最大値 </returns>
    public static Vector3 Max(Vector3 left, Vector3 right)
    {
        var result = new Vector3();
        result.x = left.x > right.x ? left.x : right.x;
        result.y = left.y > right.y ? left.y : right.y;
        result.z = left.z > right.z ? left.z : right.z;
        return result;
    }

    /// <summary>
    /// 範囲内に合わせる
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 範囲内に合わせた数 </returns>
    public static Vector3 Clamp(Vector3 target, Vector3 min, Vector3 max)
    {
        var result = Min(Max(target, min), max);
        return result;
    }

    /// <summary>
    /// 矩形と矩形の当たり判定
    /// </summary>
    /// <param name="mainRect"> 当たる方の矩形 </param>
    /// <param name="otherRect"> 当たられる方の矩形 </param>
    /// <param name="distance"> 押し戻し距離 </param>
    /// <returns></returns>
    public static bool CheckHitRectToRect(Rect mainRect, Rect otherRect, out Vector2 distance)
    {
        distance = Vector2.zero;

        float distanceX = Mathf.Abs(otherRect.x - mainRect.x);
        float distanceY = Mathf.Abs(otherRect.y - mainRect.y);
        float halfTotalLengthX = (mainRect.width + otherRect.width) * 0.5f;
        float halfTotalLengthY = (mainRect.height + otherRect.height) * 0.5f;

        if (!(distanceX < halfTotalLengthX && distanceY < halfTotalLengthY))
            return false;

        var mainColliderHalfSizeX = mainRect.width * 0.5f;
        var mainColliderHalfSizeY = mainRect.height * 0.5f;
        var otherColliderHalfSizeX = otherRect.width * 0.5f;
        var otherColliderHalfSizeY = otherRect.height * 0.5f;

        float distanceX1 = (otherRect.x - otherColliderHalfSizeX) - (mainRect.x + mainColliderHalfSizeX);
        float distanceX2 = (otherRect.x + otherColliderHalfSizeX) - (mainRect.x - mainColliderHalfSizeX);
        float distanceY1 = (otherRect.y - otherColliderHalfSizeY) - (mainRect.y + mainColliderHalfSizeY);
        float distanceY2 = (otherRect.y + otherColliderHalfSizeY) - (mainRect.y - mainColliderHalfSizeY);

        distanceX = Mathf.Abs(distanceX1) < Mathf.Abs(distanceX2) ? distanceX1 : distanceX2;
        distanceY = Mathf.Abs(distanceY1) < Mathf.Abs(distanceY2) ? distanceY1 : distanceY2;

        if (Mathf.Abs(distanceX) < Mathf.Abs(distanceY))
        {
            distance.x = distanceX;
            distance.y = 0.0f;
        }
        else
        {
            distance.x = 0.0f;
            distance.y = distanceY;
        }

        return true;
    }
}
