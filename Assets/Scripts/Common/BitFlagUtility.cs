/// <summary>
/// ビットフラグ
/// </summary>
public class BitFlag
{
    /// <summary> 保有しているフラグ </summary>
    int flag = 0;

    #region 初期化

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public BitFlag()
    {
        Clear();
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="bit"> 立てるビット位置 </param>
    public BitFlag(int bit)
    {
        Initialize(bit);
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="bitFlag"> 立てるビット位置 </param>
    public BitFlag(BitFlag bitFlag)
    {
        Initialize(bitFlag);
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="bitFlags"> ビットフラグ群 </param>
    public BitFlag(int[] bitFlags)
    {
        Initialize(bitFlags);
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="bitFlags"> ビットフラグ群 </param>
    public BitFlag(BitFlag[] bitFlags)
    {
        Initialize(bitFlags);
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="bit"> 立てるビット位置 </param>
    public void Initialize(int bit)
    {
        flag = BitFlagUtility.CalculateFlag(bit);
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="bitFlag"> 立てるビット位置 </param>
    public void Initialize(BitFlag bitFlag)
    {
        flag = bitFlag.flag;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="bitFlags"> ビットフラグ群 </param>
    public void Initialize(int[] bitFlags)
    {
        flag = BitFlagUtility.CalculateMask(bitFlags);
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="bitFlags"> ビットフラグ群 </param>
    public void Initialize(BitFlag[] bitFlags)
    {
        Clear();

        for (int i = 0; i < bitFlags.Length; ++i)
            flag |= bitFlags[i].flag;
    }

    /// <summary>
    /// 全てのフラグを折る
    /// </summary>
    public void Clear()
    {
        flag = 0;
    }

    #endregion

    #region フラグ操作

    /// <summary>
    /// フラグを立てる
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    public void Pop(int bitMask)
    {
        flag |= bitMask;
    }

    /// <summary>
    /// フラグを立てる
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    public void Pop(BitFlag bitMask)
    {
        flag |= bitMask.flag;
    }

    /// <summary>
    /// フラグを折る
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    public void UnPop(int bitMask)
    {
        flag &= ~bitMask;
    }

    /// <summary>
    /// フラグを折る
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    public void UnPop(BitFlag bitMask)
    {
        flag &= ~bitMask.flag;
    }

    /// <summary>
    /// フラグをbool値によって操作
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <param name="isPop"> フラグを立てるかどうか </param>
    public void PopOrUnPop(int bitMask, bool isPop)
    {
        if (isPop)
            Pop(bitMask);
        else
            UnPop(bitMask);
    }

    /// <summary>
    /// フラグをbool値によって操作
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <param name="isPop"> フラグを立てるかどうか </param>
    /// <returns> ビットフラグ </returns>
    public void PopOrUnPop(BitFlag bitMask, bool isPop)
    {
        if (isPop)
            Pop(bitMask);
        else
            UnPop(bitMask);
    }

    /// <summary>
    /// フラグを立てたものを取得
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> ビットフラグ </returns>
    public int GetPop(int bitMask)
    {
        return flag | bitMask;
    }

    /// <summary>
    /// フラグを立てたものを取得
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> ビットフラグ </returns>
    public int GetPop(BitFlag bitMask)
    {
        return flag | bitMask.flag;
    }

    /// <summary>
    /// フラグを折ったモノを取得
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> ビットフラグ </returns>
    public int GetUnPop(int bitMask)
    {
        return flag & ~bitMask;
    }

    /// <summary>
    /// フラグを折ったモノを取得
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> ビットフラグ </returns>
    public int GetUnPop(BitFlag bitMask)
    {
        return flag & ~bitMask.flag;
    }

    /// <summary>
    /// フラグをbool値によって操作したものを取得
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <param name="isPop"> フラグを立てるかどうか </param>
    /// <returns> ビットフラグ </returns>
    public int GetPopOrUnPop(int bitMask, bool isPop)
    {
        return isPop ? GetPop(bitMask) : GetUnPop(bitMask);
    }

    /// <summary>
    /// フラグをbool値によって操作したものを取得
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <param name="isPop"> フラグを立てるかどうか </param>
    /// <returns> ビットフラグ </returns>
    public int GetPopOrUnPop(BitFlag bitMask, bool isPop)
    {
        return isPop ? GetPop(bitMask) : GetUnPop(bitMask.flag);
    }

    #endregion

    #region フラグチェック

    /// <summary>
    /// 指定したマスクビットのうちどこか一つ以上が立っているか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 一つ以上立っているか </returns>
    public bool CheckAnyPop(int bitMask)
    {
        return BitFlagUtility.CheckAnyPop(flag, bitMask);
    }

    /// <summary>
    /// 指定したマスクビットのうちどこか一つ以上が立っているか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 一つ以上立っているか </returns>
    public bool CheckAnyPop(BitFlag bitMask)
    {
        return BitFlagUtility.CheckAnyPop(flag, bitMask.flag);
    }

    /// <summary>
    /// 指定したマスクビットの全てが立っているか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 全て立っているか </returns>
    public bool CheckAllPop(int bitMask)
    {
        return BitFlagUtility.CheckAllPop(flag, bitMask);
    }

    /// <summary>
    /// 指定したマスクビットの全てが立っているか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 全て立っているか </returns>
    public bool CheckAllPop(BitFlag bitMask)
    {
        return BitFlagUtility.CheckAllPop(flag, bitMask.flag);
    }

    /// <summary>
    /// 指定したマスクビットのうちどこか一つ以上が立っていないか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 一つ以上立っていないか </returns>
    public bool CheckAnyUnPop(int bitMask)
    {
        return BitFlagUtility.CheckAnyUnPop(flag, bitMask);
    }

    /// <summary>
    /// 指定したマスクビットのうちどこか一つ以上が立っていないか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 一つ以上立っていないか </returns>
    public bool CheckAnyUnPop(BitFlag bitMask)
    {
        return BitFlagUtility.CheckAnyUnPop(flag, bitMask.flag);
    }

    /// <summary>
    /// 指定したマスクビットの全てが立っていないか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 全て立っていないか </returns>
    public bool CheckAllUnPop(int bitMask)
    {
        return BitFlagUtility.CheckAnyUnPop(flag, bitMask);
    }

    /// <summary>
    /// 指定したマスクビットの全てが立っていないか
    /// </summary>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 全て立っていないか </returns>
    public bool CheckAllUnPop(BitFlag bitMask)
    {
        return BitFlagUtility.CheckAnyUnPop(flag, bitMask.flag);
    }

    #endregion
}

/// <summary>
/// ビットフラグ便利系
/// </summary>
public class BitFlagUtility
{
    #region フラグ生成

    /// <summary>
    /// ビットフラグを取得
    /// </summary>
    /// <param name="bit"> 立てるビット位置 </param>
    /// <returns> ビットフラグ </returns>
    public static int CalculateFlag(int bit)
    {
        return 1 << bit;
    }

    /// <summary>
    /// ビットマスクを取得
    /// </summary>
    /// <param name="bitFlags"> ビットフラグ群 </param>
    /// <returns> ビットマスク </returns>
    public static int CalculateMask(int[] bitFlags)
    {
        int mask = 0;

        for (int i = 0; i < bitFlags.Length; ++i)
            mask |= bitFlags[i];

        return mask;
    }


    #endregion

    #region フラグ操作

    /// <summary>
    /// フラグを立てる
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> ビットフラグ </returns>
    public static int GetPop(int bitFlag, int bitMask)
    {
        return bitFlag | bitMask;
    }

    /// <summary>
    /// フラグを折る
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> ビットフラグ </returns>
    public static int GetUnPop(int bitFlag, int bitMask)
    {
        return bitFlag & ~bitMask;
    }

    /// <summary>
    /// フラグをbool値によって操作
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <param name="isPop"> フラグを立てるかどうか </param>
    /// <returns> ビットフラグ </returns>
    public static int GetPopOrUnPop(int bitFlag, int bitMask, bool isPop)
    {
        return isPop ? GetPop(bitFlag, bitMask) : GetUnPop(bitFlag, bitMask);
    }

    #endregion

    #region フラグチェック

    /// <summary>
    /// 指定したマスクビットのうちどこか一つ以上が立っているか
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 一つ以上立っているか </returns>
    public static bool CheckAnyPop(int bitFlag, int bitMask)
    {
        return (bitFlag & bitMask) != 0;
    }

    /// <summary>
    /// 指定したマスクビットの全てが立っているか
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 全て立っているか </returns>
    public static bool CheckAllPop(int bitFlag, int bitMask)
    {
        return (bitFlag & bitMask) == bitMask;
    }

    /// <summary>
    /// 指定したマスクビットのうちどこか一つ以上が立っていないか
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 一つ以上立っていないか </returns>
    public static bool CheckAnyUnPop(int bitFlag, int bitMask)
    {
        return !CheckAllPop(bitFlag, bitMask);
    }

    /// <summary>
    /// 指定したマスクビットの全てが立っていないか
    /// </summary>
    /// <param name="bitFlag"> 対象ビットフラグ </param>
    /// <param name="bitMask"> 対象ビットマスク </param>
    /// <returns> 全て立っていないか </returns>
    public static bool CheckAllUnPop(int bitFlag, int bitMask)
    {
        return !CheckAnyPop(bitFlag, bitMask);
    }

    #endregion
}
