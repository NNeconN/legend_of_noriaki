using UnityEngine;
using System;

/// <summary>
/// シングルトン用
/// </summary>
/// <typeparam name="T"> 対象Script </typeparam>
public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    #region static

    /// <summary> インスタンス </summary>
    private static T instance;

    /// <summary> 一回目のAwakeで呼ばれる </summary>
    static BitFlag flagFirstAwake = new BitFlag(0);

    /// <summary> Awakeで呼ばれる </summary>
    static BitFlag flagAwake = new BitFlag(1);

    /// <summary> 一回目のStartで呼ばれる </summary>
    static BitFlag flagFirstStart = new BitFlag(2);

    /// <summary> Startで呼ばれる </summary>
    static BitFlag flagStart = new BitFlag(3);

    /// <summary> インスタンス取得用 </summary>
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                Type t = typeof(T);

                instance = (T)FindObjectOfType(t);

                if (instance == null)
                {
                    Debug.LogError(t + " をアタッチしているものはありません");
                }
                else
                {
                    DontDestroyOnLoad(instance.gameObject);
                    instance.SystemCallFirstAwakeProcess();
                    instance.SystemCallAwakeProcess();
                }
            }

            return instance;
        }
    }

    #endregion

    /// <summary> ビットフラグ </summary>
    BitFlag systemBitFlag = new BitFlag();

    void Awake()
    {
        if (instance != null)
        {
            instance.SystemCallFirstAwakeProcess();
            instance.SystemCallAwakeProcess();

            if (instance != this)
                Destroy(gameObject);

            return;
        }

        instance = this as T;
        DontDestroyOnLoad(instance.gameObject);
        instance.SystemCallFirstAwakeProcess();
        instance.SystemCallAwakeProcess();
    }

    void Start()
    {
        if (instance != null)
        {
            instance.SystemCallFirstStartProcess();
            instance.SystemCallStartProcess();

            if (instance != this)
                Destroy(this.gameObject);

            return;
        }

        instance = this as T;
        DontDestroyOnLoad(instance.gameObject);
        instance.SystemCallFirstStartProcess();
        instance.SystemCallStartProcess();
    }

    private void LateUpdate()
    {
        systemBitFlag.UnPop(flagAwake);
        systemBitFlag.UnPop(flagStart);
    }

    /// <summary>
    /// システム上の一回目のAwakeで呼ばれる関数
    /// </summary>
    void SystemCallFirstAwakeProcess()
    {
        if (systemBitFlag.CheckAnyPop(flagFirstAwake))
            return;

        var target = this as T;

        if (target)
            target.FirstAwakeProcess();
        else
            FirstAwakeProcess();

        systemBitFlag.Pop(flagFirstAwake);
    }

    /// <summary>
    /// システム上のAwakeで呼ばれる関数
    /// </summary>
    void SystemCallAwakeProcess()
    {
        if (systemBitFlag.CheckAnyPop(flagAwake))
            return;

        var target = this as T;

        if (target)
            target.AwakeProcess();
        else
            AwakeProcess();

        systemBitFlag.Pop(flagAwake);
    }

    /// <summary>
    /// システム上の一回目のStartで呼ばれる関数
    /// </summary>
    void SystemCallFirstStartProcess()
    {
        if (systemBitFlag.CheckAnyPop(flagFirstStart))
            return;

        var target = this as T;

        if (target)
            target.FirstStartProcess();
        else
            FirstStartProcess();

        systemBitFlag.Pop(flagFirstStart);
    }

    /// <summary>
    /// システム上のStartで呼ばれる関数
    /// </summary>
    void SystemCallStartProcess()
    {
        if (systemBitFlag.CheckAnyPop(flagStart))
            return;

        var target = this as T;

        if (target)
            target.StartProcess();
        else
            StartProcess();

        systemBitFlag.Pop(flagStart);
    }

    /// <summary>
    /// 一回目のAwakeで呼ばれる関数
    /// </summary>
    protected virtual void FirstAwakeProcess()
    {
    }

    /// <summary>
    /// Awakeで呼ばれる関数
    /// </summary>
    protected virtual void AwakeProcess()
    {

    }

    /// <summary>
    /// 一回目のStartで呼ばれる関数
    /// </summary>
    protected virtual void FirstStartProcess()
    {
    }

    /// <summary>
    /// Startで呼ばれる関数
    /// </summary>
    protected virtual void StartProcess()
    {

    }
}
