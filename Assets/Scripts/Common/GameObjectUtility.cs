using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GameObject関係の便利系
/// </summary>
public class GameObjectUtility
{
    /// <summary>
    /// ゲームオブジェクトの破棄
    /// </summary>
    /// <param name="gameObject"> ゲームオブジェクト </param>
    public static void DestroyGameObject(GameObject gameObject)
    {
        if (Application.isPlaying)
            Object.Destroy(gameObject);
        else
            Object.DestroyImmediate(gameObject);
    }

    /// <summary>
    /// 対象クラスのゲームオブジェクトの破棄
    /// </summary>
    /// <typeparam name="T"> MonoBehaviour継承クラス </typeparam>
    /// <param name="monoBehaviour"> 対象クラス </param>
    public static void DestroyGameObject<T>(T monoBehaviour) where T : MonoBehaviour
    {
        if (Application.isPlaying)
            Object.Destroy(monoBehaviour.gameObject);
        else
            Object.DestroyImmediate(monoBehaviour.gameObject);
    }

    /// <summary>
    /// ゲームオブジェクト群の破棄
    /// </summary>
    /// <param name="gameObjects"> ゲームオブジェクト群 </param>
    public static void DestroyGameObject(ref GameObject[] gameObjects)
    {
        if (Application.isPlaying)
            for (int i = 0, iLength = gameObjects.Length; i < iLength; ++i)
            {
                Object.Destroy(gameObjects[i]);
                gameObjects[i] = null;
            }
        else
            for (int i = 0, iLength = gameObjects.Length; i < iLength; ++i)
            {
                Object.DestroyImmediate(gameObjects[i]);
                gameObjects[i] = null;
            }
    }

    /// <summary>
    /// 対象クラスのゲームオブジェクト群の破棄
    /// </summary>
    /// <typeparam name="T"> MonoBehaviour継承クラス </typeparam>
    /// <param name="monoBehaviours"> 対象クラス群 </param>
    public static void DestroyGameObject<T>(ref T[] monoBehaviours) where T : MonoBehaviour
    {
        if (Application.isPlaying)
            for (int i = 0, iLength = monoBehaviours.Length; i < iLength; ++i)
            {
                Object.Destroy(monoBehaviours[i].gameObject);
                monoBehaviours[i] = null;
            }
        else
            for (int i = 0, iLength = monoBehaviours.Length; i < iLength; ++i)
            {
                Object.DestroyImmediate(monoBehaviours[i].gameObject);
                monoBehaviours[i] = null;
            }
    }

    /// <summary>
    /// ゲームオブジェクト群の破棄
    /// </summary>
    /// <param name="gameObjects"> ゲームオブジェクト群 </param>
    public static void DestroyGameObject(ref List<GameObject> gameObjects)
    {
        if (Application.isPlaying)
            for (int i = 0, iLength = gameObjects.Count; i < iLength; ++i)
                Object.Destroy(gameObjects[i]);
        else
            for (int i = 0, iLength = gameObjects.Count; i < iLength; ++i)
                Object.DestroyImmediate(gameObjects[i]);

        gameObjects.Clear();
    }

    /// <summary>
    /// 対象クラスのゲームオブジェクト群の破棄
    /// </summary>
    /// <typeparam name="T"> MonoBehaviour継承クラス </typeparam>
    /// <param name="monoBehaviours"> 対象クラス群 </param>
    public static void DestroyGameObject<T>(ref List<T> monoBehaviours) where T : MonoBehaviour
    {
        if (Application.isPlaying)
            for (int i = 0, iLength = monoBehaviours.Count; i < iLength; ++i)
                Object.Destroy(monoBehaviours[i].gameObject);
        else
            for (int i = 0, iLength = monoBehaviours.Count; i < iLength; ++i)
                Object.DestroyImmediate(monoBehaviours[i].gameObject);

        monoBehaviours.Clear();
    }

    /// <summary>
    /// 対象コンポーネントがnullならFindObjectOfType関数を呼ぶ
    /// </summary>
    /// <typeparam name="T"> 対象コンポーネント </typeparam>
    /// <param name="component"> コンポーネント参照 </param>
    /// <returns> コンポーネントを保持しているか </returns>
    public static bool NullCheckAndFind<T>(ref T component) where T : MonoBehaviour
    {
        if (!component)
            component = Object.FindObjectOfType<T>();

        return component;
    }

    /// <summary>
    /// 対象コンポーネントがnullならGetComponent関数を呼ぶ
    /// </summary>
    /// <typeparam name="T"> 対象コンポーネント </typeparam>
    /// <param name="component"> コンポーネント参照 </param>
    /// <param name="transform"> コンポーネントを持っていると思われるトランスフォーム </param>
    /// <returns> コンポーネントを保持しているか </returns>
    public static bool NullCheckAndGet<T>(ref T component, Transform transform) where T : MonoBehaviour
    {
        if (!component && transform)
            component = transform.GetComponent<T>();

        return component;
    }

    /// <summary>
    /// 対象コンポーネントがnullならAddComponent関数を呼ぶ
    /// </summary>
    /// <typeparam name="T"> 対象コンポーネント </typeparam>
    /// <param name="component"> コンポーネント参照 </param>
    /// <param name="transform"> コンポーネントを追加したいトランスフォーム </param>
    /// <returns> コンポーネントを保持しているか </returns>
    public static bool NullCheckAndAdd<T>(ref T component, Transform transform) where T : MonoBehaviour
    {
        if (!component && transform)
            component = transform.gameObject.AddComponent<T>();

        return component;
    }
}
