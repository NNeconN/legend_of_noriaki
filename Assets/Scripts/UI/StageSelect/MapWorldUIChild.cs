using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapWorldUIChild : MonoBehaviour
{
    /// <summary> 自身の親 </summary>
    MapWorldUIParent uiParent = null;

    /// <summary> ステージUI群 </summary>
    List<SectionStageUI> sectionStageUIChildren = new List<SectionStageUI>();

    /// <summary> 自身のワールド番号 </summary>
    [SerializeField] int worldNum = 1;

    /// <summary> 現在選択中 </summary>
    int currentSelectedIndex = 0;

    /// <summary> 自身のワールド番号取得用 </summary>
    public int WorldNum { get=> worldNum; }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="uiParent"> 自身の親 </param>
    /// <returns> 成功したか </returns>
    public bool Initialize(MapWorldUIParent uiParent)
    {
        this.uiParent = uiParent;

        var children = transform.GetComponentsInChildren<SectionStageUI>();

        sectionStageUIChildren = new List<SectionStageUI>(children);

        for (int i = 0; i < sectionStageUIChildren.Count; ++i)
        {
            if (sectionStageUIChildren[i].Initialize(uiParent, this))
                continue;

            Destroy(sectionStageUIChildren[i].gameObject);
            sectionStageUIChildren.RemoveAt(i);
            --i;
        }

        if (sectionStageUIChildren.Count < 1)
            return false;

        return true;
    }

    #region インデックス番号

    /// <summary>
    /// ゲームマネージャーに選択されているか
    /// </summary>
    /// <returns> 選択されている </returns>
    public bool CheckGameManagerSelected()
    {
        if (Game_Manager.Instance.WorldNum != worldNum)
            return false;

        for (int i = 0; i < sectionStageUIChildren.Count; ++i)
        {
            if (!sectionStageUIChildren[i].CheckGameManagerSelected())
                continue;

            currentSelectedIndex = i;

            return true;
        }

        return false;
    }

    /// <summary>
    /// ワールド番号を設定
    /// </summary>
    public void SetNumberToGameManager()
    {
        Game_Manager.Instance.WorldNum = worldNum;
        sectionStageUIChildren[currentSelectedIndex].SetNumberToGameManager();
    }

    /// <summary>
    /// 選択する
    /// </summary>
    /// <param name="index"> 選択するインデックス番号 </param>
    public void Select(int index)
    {
        sectionStageUIChildren[currentSelectedIndex].DeSelect();

        currentSelectedIndex = Mathf.Clamp(index, 0, sectionStageUIChildren.Count - 1);

        sectionStageUIChildren[currentSelectedIndex].Select();
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    /// <param name="index"> 選択しないインデックス番号 </param>
    public void DeSelect(int index)
    {
        sectionStageUIChildren[currentSelectedIndex].DeSelect();
    }

    /// <summary>
    /// 選択中のインデックス番号を取得
    /// </summary>
    /// <returns> 選択中のインデックス番号 </returns>
    public int GetSelectedIndex()
    {
        return currentSelectedIndex;
    }

    /// <summary>
    /// ひとつ前のインデックス番号を取得
    /// </summary>
    /// <returns> インデックス番号 </returns>
    public int GetPrevIndex()
    {
        return currentSelectedIndex - 1;
    }

    /// <summary>
    /// ひとつ後のインデックス番号を取得
    /// </summary>
    /// <returns> インデックス番号 </returns>
    public int GetNextIndex()
    {
        var next = currentSelectedIndex + 1;

        return next >= sectionStageUIChildren.Count ? -1 : next;
    }

    /// <summary>
    /// 最初のインデックス番号を取得
    /// </summary>
    /// <returns> インデックス番号 </returns>
    public int GetBeginIndex()
    {
        return 0;
    }

    /// <summary>
    /// 最後のインデックス番号を取得
    /// </summary>
    /// <returns> インデックス番号 </returns>
    public int GetEndIndex()
    {
        return sectionStageUIChildren.Count - 1;
    }

    /// <summary>
    /// ひとつ前への向き
    /// </summary>
    /// <returns> 向き </returns>
    public Vector2 GetPrevDiretion()
    {
        var index = GetPrevIndex();

        if (index < 0)
            return Vector2.left;

        return (sectionStageUIChildren[index].transform.position - sectionStageUIChildren[currentSelectedIndex].transform.position).normalized;
    }

    /// <summary>
    /// ひとつ後への向き
    /// </summary>
    /// <returns> 向き </returns>
    public Vector2 GetNextDiretion()
    {
        var index = GetNextIndex();

        if (index < 0)
            return Vector2.right;

        return (sectionStageUIChildren[index].transform.position - sectionStageUIChildren[currentSelectedIndex].transform.position).normalized;
    }

    #endregion
}
