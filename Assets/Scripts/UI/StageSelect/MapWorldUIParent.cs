using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapWorldUIParent : MonoBehaviour
{
    /// <summary>
    /// UIの状態
    /// </summary>
    enum UICondition
    {
        Idle,
        PrevSlide,
        NextSlide,
    }

    /// <summary> 入力間の待ち </summary>
    static float defaultInputWait = 0.5f;

    /// <summary> 横の空白 </summary>
    static float widthSpace = 25.0f;

    /// <summary> スプライト群 </summary>
    [SerializeField] SpritesParent[] spritesParents = new SpritesParent[0];

    /// <summary> ワールドUI群 </summary>
    List<MapWorldUIChild> mapWorldUIChildren = new List<MapWorldUIChild>();

    /// <summary> UIの状態 </summary>
    UICondition uiCondition = UICondition.Idle;

    /// <summary> 現在選択中 </summary>
    int currentSelectedIndex = 0;

    /// <summary> 次に選択するインデックス番号 </summary>
    int nextSelectedIndex = 0;

    /// <summary> 入力間の待ち </summary>
    float inputWait = 0.0f;

    /// <summary> スライド時間 </summary>
    [SerializeField] float slideTime = 1.0f;

    /// <summary> 実行時間 </summary>
    float processTime = 0.0f;

    private void Awake()
    {
        var children = transform.GetComponentsInChildren<MapWorldUIChild>();
        uiCondition = UICondition.Idle;

        mapWorldUIChildren = new List<MapWorldUIChild>(children);

        for (int i = 0; i < mapWorldUIChildren.Count; ++i)
        {
            if (mapWorldUIChildren[i].Initialize(this))
            {
                mapWorldUIChildren[i].transform.localEulerAngles = new Vector3(10.0f, 0.0f, 0.0f);
                mapWorldUIChildren[i].transform.localPosition = GetPositionOutSide();
                continue;
            }

            Destroy(mapWorldUIChildren[i].gameObject);
            mapWorldUIChildren.RemoveAt(i);
            --i;
        }

        if (mapWorldUIChildren.Count < 1)
        {
            Destroy(this.gameObject);

            return;
        }

        if (!CheckGameManagerSelected())
            currentSelectedIndex = 0;

        mapWorldUIChildren[currentSelectedIndex].transform.localPosition = GetPositionCenter();
        mapWorldUIChildren[currentSelectedIndex].Select(mapWorldUIChildren[currentSelectedIndex].GetSelectedIndex());
    }

    private void Update()
    {
        switch (uiCondition)
        {
            default:
            case UICondition.Idle:
                if (UISelectUpdate())
                {
                    processTime = 0.0f;
                    inputWait = defaultInputWait;
                    return;
                }

                if (Input.GetButtonDown("Submit"))
                {
                    mapWorldUIChildren[currentSelectedIndex].SetNumberToGameManager();
                    Scene_Manager.Instance.LoadScene("StageGame");
                }
                else if (Input.GetButtonDown("Cancel"))
                    Scene_Manager.Instance.LoadScene("Title");
                break;
            case UICondition.PrevSlide:
            case UICondition.NextSlide:
                UISlideUpdate();
                break;
        }
    }

    #region 位置計算

    /// <summary>
    /// 外側位置取得
    /// </summary>
    /// <returns> 位置 </returns>
    Vector3 GetPositionOutSide()
    {
        return new Vector3(0.0f, -100.0f, 0.0f);
    }

    /// <summary>
    /// 左側位置取得
    /// </summary>
    /// <returns> 位置 </returns>
    Vector3 GetPositionLeft()
    {
        return new Vector3(-widthSpace, 0.0f, 0.0f);
    }

    /// <summary>
    /// 右側位置取得
    /// </summary>
    /// <returns> 位置 </returns>
    Vector3 GetPositionRight()
    {
        return new Vector3(widthSpace, 0.0f, 0.0f);
    }

    /// <summary>
    /// 中央位置取得
    /// </summary>
    /// <returns> 位置 </returns>
    Vector3 GetPositionCenter()
    {
        return new Vector3(0.0f, 0.0f, 0.0f);
    }

    #endregion

    /// <summary>
    /// ゲームマネージャーに選択されているか
    /// </summary>
    /// <returns> 選択されている </returns>
    public bool CheckGameManagerSelected()
    {
        for (int i = 0; i < mapWorldUIChildren.Count; ++i)
        {
            if (!mapWorldUIChildren[i].CheckGameManagerSelected())
                continue;

            currentSelectedIndex = i;
            return true;
        }

        return false;
    }

    /// <summary>
    /// スプライト取得
    /// </summary>
    /// <param name="worldNum"> ワールド番号 </param>
    /// <param name="stageNum"> ステージ番号 </param>
    /// <returns>  </returns>
    public Sprite GetSprite(int worldNum, int stageNum)
    {
        if (worldNum > spritesParents.Length)
            return null;

        var spritesChildren = spritesParents[worldNum - 1].sprites;

        if (stageNum > spritesChildren.Length)
            return null;

        return spritesChildren[stageNum - 1];
    }

    bool UISelectUpdate()
    {
        if (inputWait > 0.0f)
        {
            inputWait -= Time.deltaTime;
            return false;
        }

        var inputDirection = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        //var inputMagnitude = inputDirection.magnitude;

        //if (inputMagnitude <= 0.0f)
        //    return false;

        //var prevDirection = mapWorldUIChildren[currentSelectedIndex].GetPrevDiretion();
        //var nextDirection = mapWorldUIChildren[currentSelectedIndex].GetNextDiretion();

        //var prevAngle = Mathf.Abs(Mathf.Acos(Vector2.Dot(prevDirection, inputDirection) / (prevDirection.magnitude * inputMagnitude)));
        //var nextAngle = Mathf.Abs(Mathf.Acos(Vector2.Dot(nextDirection, inputDirection) / (nextDirection.magnitude * inputMagnitude)));

        //var isPrevSlide = prevAngle < 90.0f;
        //var isNextSlide = nextAngle < 90.0f;

        //if (isPrevSlide && isNextSlide)
        //{
        //    if (prevAngle < nextAngle)
        //        isNextSlide = false;
        //    else
        //        isPrevSlide = false;
        //}

        if (inputDirection.x > 0)
        {
            var next = mapWorldUIChildren[currentSelectedIndex].GetNextIndex();

            if (next < 0)
            {
                if (mapWorldUIChildren.Count < 2)
                    mapWorldUIChildren[currentSelectedIndex].Select(mapWorldUIChildren[currentSelectedIndex].GetBeginIndex());
                else
                {
                    mapWorldUIChildren[currentSelectedIndex].DeSelect(mapWorldUIChildren[currentSelectedIndex].GetSelectedIndex());
                    uiCondition = UICondition.NextSlide;
                    nextSelectedIndex = currentSelectedIndex + 1;
                    if (nextSelectedIndex >= mapWorldUIChildren.Count)
                        nextSelectedIndex = 0;
                }
            }
            else
                mapWorldUIChildren[currentSelectedIndex].Select(next);

            return true;
        }
        if (inputDirection.x < 0)
        {
            var prev = mapWorldUIChildren[currentSelectedIndex].GetPrevIndex();

            if (prev < 0)
            {
                if (mapWorldUIChildren.Count < 2)
                    mapWorldUIChildren[currentSelectedIndex].Select(mapWorldUIChildren[currentSelectedIndex].GetEndIndex());
                else
                {
                    mapWorldUIChildren[currentSelectedIndex].DeSelect(mapWorldUIChildren[currentSelectedIndex].GetSelectedIndex());
                    uiCondition = UICondition.PrevSlide;
                    nextSelectedIndex = currentSelectedIndex - 1;
                    if (nextSelectedIndex < 0)
                        nextSelectedIndex = mapWorldUIChildren.Count - 1;
                }
            }
            else
                mapWorldUIChildren[currentSelectedIndex].Select(prev);

            return true;
        }

        return false;
    }

    void UISlideUpdate()
    {
        var leftPosition = GetPositionLeft();
        var centerPosition = GetPositionCenter();
        var rightPosition = GetPositionRight();

        if (slideTime > 0.0f)
            processTime = Mathf.Min(processTime + (Time.deltaTime / slideTime), 1.0f);
        else
            processTime = 1.0f;

        if (uiCondition == UICondition.NextSlide)
        {
            mapWorldUIChildren[currentSelectedIndex].transform.localPosition = Vector3.Lerp(centerPosition, leftPosition, processTime);
            mapWorldUIChildren[nextSelectedIndex].transform.localPosition = Vector3.Lerp(rightPosition, centerPosition, processTime);

            if (processTime >= 1.0f)
            {
                mapWorldUIChildren[currentSelectedIndex].transform.localPosition = GetPositionOutSide();
                currentSelectedIndex = nextSelectedIndex;
                uiCondition = UICondition.Idle;
                mapWorldUIChildren[currentSelectedIndex].Select(mapWorldUIChildren[currentSelectedIndex].GetBeginIndex());
            }
        }
        else
        {
            mapWorldUIChildren[currentSelectedIndex].transform.localPosition = Vector3.Lerp(centerPosition, rightPosition, processTime);
            mapWorldUIChildren[nextSelectedIndex].transform.localPosition = Vector3.Lerp(leftPosition, centerPosition, processTime);

            if (processTime >= 1.0f)
            {
                mapWorldUIChildren[currentSelectedIndex].transform.localPosition = GetPositionOutSide();
                currentSelectedIndex = nextSelectedIndex;
                uiCondition = UICondition.Idle;
                mapWorldUIChildren[currentSelectedIndex].Select(mapWorldUIChildren[currentSelectedIndex].GetEndIndex());
            }
        }
    }

    /// <summary>
    /// スプライト群
    /// </summary>
    [System.Serializable]
    class SpritesParent
    {
        public Sprite[] sprites = new Sprite[0];
    }
}
