using UnityEngine;

public class SectionStageUI : MonoBehaviour
{
    /// <summary> 自身の一番の親 </summary>
    MapWorldUIParent uiMostParent = null;

    /// <summary> 自身の親 </summary>
    MapWorldUIChild uiParent = null;

    /// <summary> 自身のレンダラー </summary>
    SpriteRenderer myRenderer = null;

    /// <summary> 自身のアニメーター </summary>
    Animator myAnimator = null;

    /// <summary> 自身のステージ番号 </summary>
    [SerializeField] int stageNum = 1;

    /// <summary> 自身のステージ番号取得用 </summary>
    public int StageNum { get => stageNum; }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="uiMostParent"> 自身の一番の親 </param>
    /// <param name="uiParent"> 自身の親 </param>
    /// <returns> 成功したか </returns>
    public bool Initialize(MapWorldUIParent uiMostParent, MapWorldUIChild uiParent)
    {
        this.uiMostParent = uiMostParent;
        this.uiParent = uiParent;

        myAnimator = transform.GetChild(0)?.GetComponent<Animator>();
        if (!myAnimator)
            return false;

        var sprite = this.uiMostParent.GetSprite(this.uiParent.WorldNum, stageNum);
        if (!sprite)
            return false;

        myRenderer = transform.GetChild(0)?.GetChild(0)?.GetChild(1)?.GetComponent<SpriteRenderer>();
        if (!myRenderer)
            return false;

        myRenderer.sprite = sprite;

        return true;
    }

    /// <summary>
    /// ゲームマネージャーに選択されているか
    /// </summary>
    /// <returns> 選択されている </returns>
    public bool CheckGameManagerSelected()
    {
        return Game_Manager.Instance.StageNum == stageNum;
    }

    /// <summary>
    /// ワールド番号を設定
    /// </summary>
    public void SetNumberToGameManager()
    {
        Game_Manager.Instance.StageNum = stageNum;
    }

    /// <summary>
    /// 選択する
    /// </summary>
    public void Select()
    {
        myAnimator.Play("SelectIcon", 0, Mathf.Clamp(myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime, 0.0f, 1.0f));
        myAnimator.SetFloat("animationSpeed", 1.0f);
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    public void DeSelect()
    {
        myAnimator.Play("SelectIcon", 0, Mathf.Clamp(myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime, 0.0f, 1.0f));
        myAnimator.SetFloat("animationSpeed", -1.0f);
    }
}
