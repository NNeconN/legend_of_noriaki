using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    /// <summary> ステージマネージャー </summary>
    Stage_Manager stageManager = null;

    /// <summary> ステージBGM </summary>
    StageBGM stageBGM = null;

    /// <summary> 自身のCanvasGroup </summary>
    CanvasGroup canvasGroup = null;

    /// <summary> 自身のAudioSource </summary>
    AudioSource audioSource = null;

    /// <summary> 最初に選択されるボタン </summary>
    [SerializeField] Button firstSelectButton = null;

    /// <summary> フェードする速さ </summary>
    [SerializeField] float fadeSpeed = 1.0f;

    /// <summary> ロック中か </summary>
    bool isLock = false;

    /// <summary> 開く処理を行うか </summary>
    bool isOpenProcess = false;

    /// <summary> SE再生を行うか </summary>
    bool isPlaySE = false;

    private void Awake()
    {
        stageManager = FindObjectOfType<Stage_Manager>();
        if (!stageManager)
        {
            Destroy(gameObject);
            return;
        }

        stageBGM = FindObjectOfType<StageBGM>();
        audioSource = GetComponent<AudioSource>();

        canvasGroup = GetComponent<CanvasGroup>();
        if (!canvasGroup)
            canvasGroup = gameObject.AddComponent<CanvasGroup>();

        canvasGroup.alpha = 0.0f;
        isLock = false;
        isOpenProcess = false;
    }

    private void Update()
    {
        var currentSpeed = fadeSpeed * Time.unscaledDeltaTime;

        if (isOpenProcess)
            canvasGroup.alpha = Mathf.Min(canvasGroup.alpha + currentSpeed, 1.0f);
        else
            canvasGroup.alpha = Mathf.Max(canvasGroup.alpha - currentSpeed, 0.0f);

        if (isPlaySE && CheckIsOpen())
        {
            audioSource?.Play();
            isPlaySE = false;
        }
    }

    #region 開閉関係

    /// <summary>
    /// メニューを開いているか
    /// </summary>
    /// <returns> 開いているか </returns>
    public bool CheckIsOpen()
    {
        return canvasGroup.alpha >= 1.0f;
    }

    /// <summary>
    /// メニューを閉じているか
    /// </summary>
    /// <returns> 閉じているか </returns>
    public bool CheckIsClose()
    {
        return canvasGroup.alpha <= 0.0f;
    }

    /// <summary>
    /// 開く
    /// </summary>
    public void Open()
    {
        stageBGM?.SetVolume(0.0f);
        isOpenProcess = true;
        firstSelectButton?.Select();
        isPlaySE = true;
    }

    /// <summary>
    /// 閉じる
    /// </summary>
    public void Close()
    {
        stageBGM?.SetVolume(1.0f);
        isOpenProcess = false;
    }

    /// <summary>
    /// 強制的に開く
    /// </summary>
    public void ForcedOpen()
    {
        isOpenProcess = true;

        canvasGroup.alpha = 1.0f;

        isLock = false;
    }

    /// <summary>
    /// 強制的に閉じる
    /// </summary>
    public void ForcedClose()
    {
        isOpenProcess = false;

        if (!canvasGroup)
            Awake();

        canvasGroup.alpha = 0.0f;

        isLock = false;
    }

    #endregion

    #region ボタン動作

    /// <summary>
    /// タイトルシーンを読み込む
    /// </summary>
    public void LoadTitleScene()
    {
        if (!CheckIsOpen() || isLock)
            return;

        Scene_Manager.Instance.LoadScene("Title");

        isLock = true;
    }

    /// <summary>
    /// ステージ選択シーンを読み込む
    /// </summary>
    public void LoadSelectScene()
    {
        if (!CheckIsOpen() || isLock)
            return;

        Scene_Manager.Instance.LoadScene("StageSelect");

        isLock = true;
    }

    /// <summary>
    /// ステージ再挑戦
    /// </summary>
    public void RetryStage()
    {
        if (!CheckIsOpen() || isLock)
            return;

        stageManager.ReSetStage();

        isLock = true;
    }

    #endregion
}
