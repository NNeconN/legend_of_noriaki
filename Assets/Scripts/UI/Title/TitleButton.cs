using UnityEngine;

/// <summary>
/// タイトルボタン
/// </summary>
public class TitleButton : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input_Manager.Instance.GetAnyButtonTrigger())
            Scene_Manager.Instance.LoadScene("StageSelect");
    }
}
