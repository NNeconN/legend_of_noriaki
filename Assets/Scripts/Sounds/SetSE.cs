using UnityEngine;

public class SetSE : MonoBehaviour
{
    // Start is called before the first frame update
    /// <summary> SEのソース </summary>
    [SerializeField] AudioClip[] audioClips = new AudioClip[0];

    /// <summary> 自身のAudioSource </summary>
    AudioSource myAudioSource = null;


    private void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
        if (!myAudioSource)
            myAudioSource = gameObject.AddComponent<AudioSource>();

    }

    public void JumpSe()
    {
        myAudioSource?.PlayOneShot(audioClips[0]);
    }

    public void Grab()
    {
        myAudioSource?.PlayOneShot(audioClips[1]);
    }

    public void KnockBack()
    {
        myAudioSource?.PlayOneShot(audioClips[2]);
    }

    public void Fall()
    {
        myAudioSource?.PlayOneShot(audioClips[3]);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
