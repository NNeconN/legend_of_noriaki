using UnityEngine;

public class CommonBGM : MonoBehaviour
{
    /// <summary> BGMのソース </summary>
    [SerializeField] AudioClip audioClips;

    /// <summary> 自身のAudioSource </summary>
    AudioSource myAudioSource = null;

    /// <summary> 音の割合 </summary>
    [SerializeField] float volumeScale = 1.0f;

    /// <summary> 前回の音の大きさ </summary>
    float prevVolume = 1.0f;

    /// <summary> 次回の音の大きさ </summary>
    float nextVolume = 1.0f;

    private void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
        if (!myAudioSource)
            myAudioSource = gameObject.AddComponent<AudioSource>();

        prevVolume = 0.0f;
        myAudioSource.volume = 0.0f;
        myAudioSource.loop = true;
        myAudioSource.playOnAwake = false;

        var index = Game_Manager.Instance.WorldNum - 1;

        if (!audioClips)
        {
            Destroy(gameObject);
            return;
        }

        myAudioSource.clip = audioClips;
        myAudioSource.volume = 0.0f;
        myAudioSource.Play();
    }

    private void Update()
    {
        var fadeRate = 1.0f - Fade_Manager.Instance.ProcessRate;

        prevVolume = Mathf.Lerp(prevVolume, nextVolume, Time.unscaledDeltaTime * 5.0f);

        myAudioSource.volume = prevVolume * fadeRate * volumeScale;
    }

    /// <summary>
    /// 音量設定
    /// </summary>
    /// <param name="volume"> 音量 </param>
    public void SetVolume(float volume)
    {
        nextVolume = Mathf.Clamp(volume, 0.0f, 1.0f);
    }
}
