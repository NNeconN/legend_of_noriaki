using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEtest : MonoBehaviour
{
    AudioSource SeAudio;
    public AudioClip audioClip = null;

    private void Start()
    {
        SeAudio = GetComponent<AudioSource>();
    }
    public void CancelSE()
    {
        SeAudio.PlayOneShot(audioClip);
    }  
}
