using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SE_kakusyuku : MonoBehaviour
{
    [SerializeField] AudioClip kakusyuku_se;
    AudioSource kakusyuku;

    // Start is called before the first frame update
    void Start()
    {
        kakusyuku = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnceSE()
    {
        kakusyuku.PlayOneShot(kakusyuku_se);
    }

    public void StopSE()
    {
        kakusyuku.Stop();
    }
}
