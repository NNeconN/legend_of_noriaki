using System;
using System.Collections.Generic;

public class BlockLayerData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        MyBlockType,
        ChildDatas,
    }

    #endregion

    /// <summary> ブロックデータ群 </summary>
    List<BlockData> blockDatas = new List<BlockData>();

    /// <summary> ブロックの種類 </summary>
    BlockData.BlockType myBlockType = BlockData.BlockType.Fixed;

    /// <summary> ブロックデータ群 </summary>
    public List<BlockData> BlockDatas { get => blockDatas; }

    /// <summary> ブロックの種類設定・取得用 </summary>
    public BlockData.BlockType MyBlockType { get => myBlockType; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public BlockLayerData()
    {
        blockDatas.Clear();
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public BlockLayerData(EditorBlockLayer target)
    {
        myBlockType = target.GetBlockType();

        var blockObjects = target.GetMyBlockObjects();

        for (int i = 0; i < blockObjects.Count; i++)
            blockDatas.Add(new BlockData(blockObjects[i]));
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(EditorBlockLayer target)
    {
        for (int i = 0; i < blockDatas.Count; ++i)
            target.AddBlockObject(blockDatas[i].GridPosition);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        while (index < csvDatas.Count)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][0], out dataID))
                break;

            var dataIsNone = false;

            switch (dataID)
            {
                case DataID.MyBlockType:
                    myBlockType = Enum.Parse<BlockData.BlockType>(csvDatas[index][1]);
                    ++index;
                    break;
                case DataID.ChildDatas:
                    ++index;
                    while (index < csvDatas.Count)
                    {
                        StageData.ObjectType objectType;
                        if (!Enum.TryParse(csvDatas[index][0], out objectType))
                            break;

                        var isFailed = false;

                        switch (objectType)
                        {
                            case StageData.ObjectType.Block:
                                ++index;
                                {
                                    var newData = new BlockData();
                                    newData.SetData(csvDatas, ref index);
                                    blockDatas.Add(newData);
                                }
                                break;
                            default:
                                isFailed = true;
                                break;
                        }

                        if (isFailed)
                            break;
                    }
                    break;
                default:
                    dataIsNone = true;
                    break;
            }

            if (dataIsNone)
                break;
        }
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.BlockLayer.ToString() });

        csvDatas.Add(new string[] { DataID.MyBlockType.ToString(), myBlockType.ToString() });

        csvDatas.Add(new string[1] { DataID.ChildDatas.ToString() });

        for (int i = 0; i < blockDatas.Count; ++i)
            blockDatas[i].GetData(ref csvDatas);
    }
}
