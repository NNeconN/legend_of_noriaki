using System.Collections.Generic;
using UnityEngine;

public class EditorBlockLayer : EditorObject
{
    /// <summary> ブロックオブジェクト群 </summary>
    [SerializeField] List<BlockEditorObject> blockObjects = new List<BlockEditorObject>();

    /// <summary> ブロックの種類 </summary>
    [SerializeField] BlockData.BlockType blockType = BlockData.BlockType.Fixed;

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        UnInitialize();

        SetGridSize(Vector2Int.one);

        ChangeObjectName();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        RemoveAllBlockObject();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        manager.RemoveBlockLayer(this);
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public override void ChangeObjectName()
    {
        gameObject.name = GetType().Name + "_" + blockType.ToString();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        for (int i = 0; i < blockObjects.Count; ++i)
            blockObjects[i].ResetGridAllObjects();

        return false;
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int ReTouchEditorObjects()
    {
        Vector2Int maxGridSize = Vector2Int.one;

        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref blockObjects));

        return maxGridSize;
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <typeparam name="T"> 対象 </typeparam>
    /// <param name="myList"> 入れ物 </param>
    /// <returns> グリッドの大きさ </returns>
    Vector2Int ReTouchEditorObjects<T>(ref List<T> myList) where T : EditorObject
    {
        Vector2Int maxGridSize = Vector2Int.one;

        myList.Clear();

        var targets = GetComponentsInChildren<T>();

        for (int i = 0; i < targets.Length; ++i)
        {
            myList.Add(targets[i]);

            var gridPosition = targets[i].GetGridPosition();
            var gridSize = targets[i].GetGridSize();

            maxGridSize.x = Mathf.Max(maxGridSize.x, gridPosition.x + gridSize.x);
            maxGridSize.y = Mathf.Max(maxGridSize.y, gridPosition.y + gridSize.y);
        }

        return maxGridSize;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        var managerGridSize = manager.GetGridSize();
        SetGridPosition(gridPosition);
        SetGridSize(gridSize);

        for (int i = 0; i < blockObjects.Count; ++i)
        {
            if (!blockObjects[i].ReSizeGrid())
                continue;

            if (RemoveBlockObject(blockObjects[i]))
                --i;
        }

        return false;
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - Vector2Int.one);
    }

    /// <summary>
    /// グリッド上の大きさ設定
    /// </summary>
    /// <param name="position"> 大きさ </param>
    public override void SetGridSize(Vector2Int size)
    {
        var managerGridSize = manager.GetGridSize();

        gridSize = MathUtility.Clamp(size, Vector2Int.one, managerGridSize - gridPosition);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        return CheckHasBlockObjects();
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        for (int i = 0; i < blockObjects.Count; ++i)
            blockObjects[i].SetGridPosition(blockObjects[i].GetGridPosition() + direction);

        ResetGridAllObjects();
    }

    #endregion

    #region ブロックオブジェクト

    /// <summary>
    /// ブロックが追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddBlockObject(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + BlockData.DEFAULT_GRID_SIZE.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + BlockData.DEFAULT_GRID_SIZE.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// ブロックオブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加したオブジェクト </returns>
    public BlockEditorObject AddBlockObject(Vector2Int position)
    {
        var newItem = objectFactory.InstantiateBlockObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.SetBlockType(blockType);
        newItem.DestroyOther();

        blockObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// ブロックオブジェクトをまとめて追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加したオブジェクト </returns>
    public List<BlockEditorObject> AddRangeBlockObject(Vector2Int position, Vector2Int size)
    {
        var newItems = new List<BlockEditorObject>();

        for (int x = 0; x < size.x; ++x)
        {
            for (int y = 0; y < size.y; ++y)
            {
                var newItem = AddBlockObject(new Vector2Int(position.x + x, position.y + y));

                if (!newItem)
                    continue;

                if (newItems.Contains(newItem))
                    continue;

                newItems.Add(newItem);
            }
        }

        return newItems;
    }

    /// <summary>
    /// ブロックオブジェクトを枠状に追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加したオブジェクト </returns>
    public List<BlockEditorObject> AddFrameBlockObject(Vector2Int position, Vector2Int size)
    {
        var newItems = new List<BlockEditorObject>();

        var leftX = position.x;
        var bottomY = position.y;
        var rightX = leftX + size.x - 1;
        var topY = bottomY + size.y - 1;

        for (int x = leftX; x <= rightX; ++x)
        {
            var bottomNewItem = AddBlockObject(new Vector2Int(x, bottomY));
            var topNewItem = AddBlockObject(new Vector2Int(x, topY));

            if (!bottomNewItem && !topNewItem)
                continue;

            if (!newItems.Contains(bottomNewItem))
                newItems.Add(bottomNewItem);

            if (!newItems.Contains(topNewItem))
                newItems.Add(topNewItem);
        }

        for (int y = bottomY + 1; y <= topY - 1; ++y)
        {
            var leftNewItem = AddBlockObject(new Vector2Int(leftX, y));
            var rightNewItem = AddBlockObject(new Vector2Int(rightX, y));

            if (!leftNewItem && !rightNewItem)
                continue;

            if (!newItems.Contains(leftNewItem))
                newItems.Add(leftNewItem);

            if (!newItems.Contains(rightNewItem))
                newItems.Add(rightNewItem);
        }

        return newItems;
    }

    /// <summary>
    /// ブロックオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveBlockObject(BlockEditorObject item)
    {
        objectFactory.DestroyBlockObject(item);

        if (!blockObjects.Contains(item))
            return false;

        blockObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtBlockObject(Vector2Int position)
    {
        var targets = GetAtMyBlockObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveBlockObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeBlockObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyBlockObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveBlockObject(targets[i]);
        }
    }

    /// <summary>
    /// 全てのブロックオブジェクトを取り除く
    /// </summary>
    public void RemoveAllBlockObject()
    {
        while (blockObjects.Count > 0)
            blockObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身のブロックオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<BlockEditorObject> GetMyBlockObjects()
    {
        return blockObjects;
    }

    /// <summary>
    /// 指定位置の自身のブロックオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<BlockEditorObject> GetAtMyBlockObjects(Vector2Int position)
    {
        var targets = new List<BlockEditorObject>();
        var stageObjects = manager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as BlockEditorObject;

            if (!target)
                continue;

            if (!blockObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身のブロックオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<BlockEditorObject> GetRangeMyBlockObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<BlockEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as BlockEditorObject;

            if (!target)
                continue;

            if (!blockObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// ブロックオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasBlockObjects()
    {
        return GetMyBlockObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtBlockObjects(Vector2Int position)
    {
        return GetAtMyBlockObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeBlockObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyBlockObjects(position, size).Count > 0;
    }

    #endregion

    #region ブロックの種類

    /// <summary>
    /// ブロックの種類取得
    /// </summary>
    /// <returns> ブロックの種類 </returns>
    public BlockData.BlockType GetBlockType()
    {
        return blockType;
    }

    /// <summary>
    /// ブロックの種類取得
    /// </summary>
    /// <param name="blockType"> ブロックの種類 </param>
    public void SetBlockType(BlockData.BlockType blockType)
    {
        this.blockType = blockType;

        ChangeObjectName();

        for (int i = 0; i < blockObjects.Count; ++i)
            blockObjects[i].SetBlockType(this.blockType);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.BlockLayer;
    }

    #endregion
}
