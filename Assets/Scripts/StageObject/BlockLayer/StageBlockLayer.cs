#if true

using System.Collections.Generic;
using UnityEngine;

public class StageBlockLayer : StageObject
{
    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = new StageObject_Manager();

    /// <summary> 拡大縮小マネージャー </summary>
    ScaleChange_Manager scaleChangeManager = new ScaleChange_Manager();

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        var isFailed = !objectManager.Initialize(this);
        isFailed = isFailed || !scaleChangeManager.Initialize(this);

        isFailed = isFailed || !base.Initialize(stageManager, objectFactory);

        systemBitFlag.Pop(bitFlagEnable);

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        objectManager.UnInitialize();
        scaleChangeManager.UnInitialize();

        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager.RemoveBlockLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 成功したか </returns>
    public bool SetData(BlockLayerData data)
    {
        scaleChangeManager.MyBlockType = data.MyBlockType;

        CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        if (objectManager == null)
            return false;

        for (int i = 0; i < data.BlockDatas.Count; ++i)
            objectManager.AddBlockObject(data.BlockDatas[i]);

        CreateSelectInformationAll();

        return true;
    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        objectManager.UpdateProcess();
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        objectManager.FixedUpdateProcess();
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        objectManager.LateUpdateProcess();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.BlockLayer;
    }

    #endregion

    #region ブロックオブジェクト

    /// <summary>
    /// ブロックオブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public BlockStageObject AddBlockObject(BlockData data)
    {
        return objectManager.AddBlockObject(data);
    }

    /// <summary>
    /// ブロックオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBlockObject(BlockStageObject item)
    {
        objectManager.RemoveBlockObject(item);
    }

    /// <summary>
    /// 全てのブロックオブジェクトを取り除く
    /// </summary>
    public void RemoveAllBlockObject()
    {
        objectManager.RemoveAllBlockObject();
    }

    /// <summary>
    /// 自身のブロックオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<BlockStageObject> GetMyBlockObjects()
    {
        return objectManager.GetMyBlockObjects();
    }

    /// <summary>
    /// 指定位置の自身のブロックオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<BlockStageObject> GetAtMyBlockObjects(Vector2Int position)
    {
        return objectManager.GetAtMyBlockObjects(position);
    }

    /// <summary>
    /// 指定位置の自身のブロックオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<BlockStageObject> GetRangeMyBlockObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyBlockObjects(position, size);
    }

    /// <summary>
    /// ブロックオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasBlockObjects()
    {
        return objectManager.CheckHasBlockObjects();
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtBlockObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtBlockObjects(position);
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeBlockObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeBlockObjects(position, size);
    }

    #endregion

    #region 選択関係

    public void CreateSelectInformationAll()
    {
        CreateBlockGroup();
    }

    /// <summary>
    /// ブロックグループを生成
    /// </summary>
    public void CreateBlockGroup()
    {
        scaleChangeManager.CreateBlockGroup();
    }

    /// <summary>
    /// 選択する
    /// </summary>
    /// <param name="information"> 選択情報 </param>
    public void Select(Stage_Manager.ScaleChange_Manager.SelectInformation information)
    {
        scaleChangeManager.Select(information);
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    /// <param name="information"> 選択情報 </param>
    public void UnSelect(Stage_Manager.ScaleChange_Manager.SelectInformation information)
    {
        scaleChangeManager.UnSelect(information);
    }

    #endregion

    #region 拡大縮小チェック

    /// <summary>
    /// 移動予定状態にする
    /// </summary>
    /// <param name="moveDirection"> 移動方向 </param>
    /// <param name="information"> 選択情報 </param>
    public void MoveLock(StageData.GridDirection moveDirection, Stage_Manager.ScaleChange_Manager.SelectInformation information)
    {
        scaleChangeManager.MoveLock(moveDirection, information);
    }

    /// <summary>
    /// 掴みロック状態にする
    /// </summary>
    public void GrabLock()
    {
        scaleChangeManager.GrabLock();
    }

    /// <summary>
    /// ロックを外す
    /// </summary>
    public void UnLock()
    {
        scaleChangeManager.UnLock();
    }

    /// <summary>
    /// 縮小可能か
    /// </summary>
    /// <param name="checkDistance"> 縮小距離 </param>
    /// <returns> 縮小可能か </returns>
    public int CheckCanScaleDown(int checkDistance)
    {
        return scaleChangeManager.CheckCanScaleDown(checkDistance);
    }

    #endregion

    #region 拡大縮小関係

    /// <summary>
    /// 拡大縮小を始める
    /// </summary>
    public void ScaleChangeStartProcess()
    {
        scaleChangeManager.StartProcess();
    }

    /// <summary>
    /// 拡大終了の終了
    /// </summary>
    public void ScaleChangeFinishProcess()
    {
        scaleChangeManager.FinishProcess();
    }

    /// <summary>
    /// 拡大縮小更新
    /// </summary>
    /// <param name="isFailed"> 失敗状態か </param>
    /// <param name="processRate"> 移動割合 </param>
    /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
    /// <param name="distance"> 拡大縮小距離 </param>
    public void ScaleChangeUpdateProcess(bool isFailed, float processRate, int currentDistance, int distance)
    {
        scaleChangeManager.UpdateProcess(isFailed, processRate, currentDistance, distance);
    }

    #endregion

    #region 子クラス

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> ブロックオブジェクト </summary>
        public List<BlockStageObject> blockObjects = new List<BlockStageObject>();
    }

    /// <summary>
    /// 拡大縮小マネージャー
    /// </summary>
    public class ScaleChange_Manager
    {
        /// <summary> ブロックレイヤー </summary>
        StageBlockLayer blockLayer = null;

        /// <summary> 選択情報 </summary>
        SelectInformation selectInformation = new SelectInformation();

        /// <summary> 拡大縮小情報 </summary>
        ScaleChangeInformation scaleChangeInformation = new ScaleChangeInformation();

        /// <summary> ブロックの種類 </summary>
        BlockData.BlockType myBlockType = BlockData.BlockType.Fixed;

        /// <summary> ブロックの種類 設定・取得用 </summary>
        public BlockData.BlockType MyBlockType { get => myBlockType; set => myBlockType = value; }

        #region 初期化・解放処理

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <param name="blockLayer"> ブロックレイヤー </param>
        /// <returns> 成功した </returns>
        public bool Initialize(StageBlockLayer blockLayer)
        {
            if (blockLayer)
                this.blockLayer = blockLayer;

            if (!this.blockLayer)
                return false;

            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            selectInformation.leftGroupBlockObjects.Clear();
            selectInformation.rightGroupBlockObjects.Clear();
            selectInformation.upGroupBlockObjects.Clear();
            selectInformation.downGroupBlockObjects.Clear();

            scaleChangeInformation.lockedBlockObjects.Clear();
        }

        #endregion

        #region 選択関係

        /// <summary>
        /// ブロックグループを生成
        /// </summary>
        public void CreateBlockGroup()
        {
            selectInformation.leftGroupBlockObjects.Clear();
            selectInformation.rightGroupBlockObjects.Clear();
            selectInformation.upGroupBlockObjects.Clear();
            selectInformation.downGroupBlockObjects.Clear();

            var objectManager = blockLayer.objectManager;
            var blockObjects = objectManager.GetMyBlockObjects();

            for (int i = 0; i < blockObjects.Count; ++i)
            {
                var gridPosition = blockObjects[i].GetGridPosition();
                var left = new Vector2Int(gridPosition.x - 1, gridPosition.y);
                var right = new Vector2Int(gridPosition.x + 1, gridPosition.y);
                var up = new Vector2Int(gridPosition.x, gridPosition.y + 1);
                var down = new Vector2Int(gridPosition.x, gridPosition.y - 1);

                if (!objectManager.CheckHasAtBlockObjects(left))
                    selectInformation.leftGroupBlockObjects.Add(blockObjects[i]);

                if (!objectManager.CheckHasAtBlockObjects(right))
                    selectInformation.rightGroupBlockObjects.Add(blockObjects[i]);

                if (!objectManager.CheckHasAtBlockObjects(up))
                    selectInformation.upGroupBlockObjects.Add(blockObjects[i]);

                if (!objectManager.CheckHasAtBlockObjects(down))
                    selectInformation.downGroupBlockObjects.Add(blockObjects[i]);
            }
        }

        /// <summary>
        /// 選択する
        /// </summary>
        /// <param name="information"> 選択情報 </param>
        public void Select(Stage_Manager.ScaleChange_Manager.SelectInformation information)
        {
            List<HandleStageObject> targetHandles;

            switch (myBlockType)
            {
                default:
                    return;
                case BlockData.BlockType.Red:
                    targetHandles = information.redHandleType;
                    break;
                case BlockData.BlockType.Green:
                    targetHandles = information.greenHandleType;
                    break;
                case BlockData.BlockType.Blue:
                    targetHandles = information.blueHandleType;
                    break;
            }

            for (int i = 0; i < targetHandles.Count; ++i)
            {
                var handleType = targetHandles[i].GetHandleType();

                switch (handleType)
                {
                    case HandleData.HandleType.LeftHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupBlockObjects.Count; ++j)
                            selectInformation.leftGroupBlockObjects[j].Select();
                        break;
                    case HandleData.HandleType.MiddleHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupBlockObjects.Count; ++j)
                            selectInformation.leftGroupBlockObjects[j].Select();

                        for (int j = 0; j < selectInformation.rightGroupBlockObjects.Count; ++j)
                            selectInformation.rightGroupBlockObjects[j].Select();
                        break;
                    case HandleData.HandleType.RightHorizontal:
                        for (int j = 0; j < selectInformation.rightGroupBlockObjects.Count; ++j)
                            selectInformation.rightGroupBlockObjects[j].Select();
                        break;
                    case HandleData.HandleType.BottomVertical:
                        for (int j = 0; j < selectInformation.downGroupBlockObjects.Count; ++j)
                            selectInformation.downGroupBlockObjects[j].Select();
                        break;
                    case HandleData.HandleType.MiddleVertical:
                        for (int j = 0; j < selectInformation.downGroupBlockObjects.Count; ++j)
                            selectInformation.downGroupBlockObjects[j].Select();

                        for (int j = 0; j < selectInformation.upGroupBlockObjects.Count; ++j)
                            selectInformation.upGroupBlockObjects[j].Select();
                        break;
                    case HandleData.HandleType.TopVertical:
                        for (int j = 0; j < selectInformation.upGroupBlockObjects.Count; ++j)
                            selectInformation.upGroupBlockObjects[j].Select();
                        break;
                }
            }
        }

        /// <summary>
        /// 選択しない
        /// </summary>
        /// <param name="information"> 選択情報 </param>
        public void UnSelect(Stage_Manager.ScaleChange_Manager.SelectInformation information)
        {
            List<HandleStageObject> targetHandles;

            switch (myBlockType)
            {
                default:
                    return;
                case BlockData.BlockType.Red:
                    targetHandles = information.redHandleType;
                    break;
                case BlockData.BlockType.Green:
                    targetHandles = information.greenHandleType;
                    break;
                case BlockData.BlockType.Blue:
                    targetHandles = information.blueHandleType;
                    break;
            }

            for (int i = 0; i < targetHandles.Count; ++i)
            {
                var handleType = targetHandles[i].GetHandleType();

                switch (handleType)
                {
                    case HandleData.HandleType.LeftHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupBlockObjects.Count; ++j)
                            selectInformation.leftGroupBlockObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.MiddleHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupBlockObjects.Count; ++j)
                            selectInformation.leftGroupBlockObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.rightGroupBlockObjects.Count; ++j)
                            selectInformation.rightGroupBlockObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.RightHorizontal:
                        for (int j = 0; j < selectInformation.rightGroupBlockObjects.Count; ++j)
                            selectInformation.rightGroupBlockObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.BottomVertical:
                        for (int j = 0; j < selectInformation.downGroupBlockObjects.Count; ++j)
                            selectInformation.downGroupBlockObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.MiddleVertical:
                        for (int j = 0; j < selectInformation.downGroupBlockObjects.Count; ++j)
                            selectInformation.downGroupBlockObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.upGroupBlockObjects.Count; ++j)
                            selectInformation.upGroupBlockObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.TopVertical:
                        for (int j = 0; j < selectInformation.upGroupBlockObjects.Count; ++j)
                            selectInformation.upGroupBlockObjects[j].UnSelect();
                        break;
                }
            }
        }

        #endregion

        #region 拡大縮小チェック

        /// <summary>
        /// 拡大フラグを設定し移動予定にする
        /// </summary>
        /// <typeparam name="T"> 対象ステージオブジェクト </typeparam>
        /// <param name="locked"> フラグ設定後に登録するリスト </param>
        /// <param name="targets"> 対象 </param>
        void SetFlagScaleUpAndMoveLock<T>(ref List<T> locked, List<T> targets) where T : StageObject
        {
            for (int j = 0; j < targets.Count; ++j)
            {
                targets[j].SetFlagScaleUp(true);

                if (locked.Contains(targets[j]))
                    continue;

                targets[j].SetFlagMoveLock(true);
                locked.Add(targets[j]);
            }
        }

        /// <summary>
        /// 縮小フラグを設定し移動予定にする
        /// </summary>
        /// <typeparam name="T"> 対象ステージオブジェクト </typeparam>
        /// <param name="locked"> フラグ設定後に登録するリスト </param>
        /// <param name="targets"> 対象 </param>
        void SetFlagScaleDownAndMoveLock<T>(ref List<T> locked, List<T> targets) where T : StageObject
        {
            for (int j = 0; j < targets.Count; ++j)
            {
                targets[j].SetFlagScaleDown(true);

                if (locked.Contains(targets[j]))
                    continue;

                targets[j].SetFlagMoveLock(true);
                locked.Add(targets[j]);
            }
        }

        /// <summary>
        /// 移動予定状態にする
        /// </summary>
        /// <param name="moveDirection"> 移動方向 </param>
        /// <param name="information"> 選択情報 </param>
        public void MoveLock(StageData.GridDirection moveDirection, Stage_Manager.ScaleChange_Manager.SelectInformation information)
        {
            UnLock();

            List<HandleStageObject> targetHandles;

            switch (myBlockType)
            {
                default:
                    return;
                case BlockData.BlockType.Red:
                    targetHandles = information.redHandleType;
                    break;
                case BlockData.BlockType.Green:
                    targetHandles = information.greenHandleType;
                    break;
                case BlockData.BlockType.Blue:
                    targetHandles = information.blueHandleType;
                    break;
            }

            scaleChangeInformation.moveDirection = moveDirection;

            for (int i = 0; i < targetHandles.Count; ++i)
            {
                var handleType = targetHandles[i].GetHandleType();

                switch (moveDirection)
                {
                    case StageData.GridDirection.Left:
                        switch (handleType)
                        {
                            case HandleData.HandleType.LeftHorizontal:
                            case HandleData.HandleType.MiddleHorizontal:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.leftGroupBlockObjects);
                                break;
                            case HandleData.HandleType.RightHorizontal:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.rightGroupBlockObjects);
                                break;
                        }
                        break;
                    case StageData.GridDirection.Right:
                        switch (handleType)
                        {
                            case HandleData.HandleType.RightHorizontal:
                            case HandleData.HandleType.MiddleHorizontal:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.rightGroupBlockObjects);
                                break;
                            case HandleData.HandleType.LeftHorizontal:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.leftGroupBlockObjects);
                                break;
                        }
                        break;
                    case StageData.GridDirection.Up:
                        switch (handleType)
                        {
                            case HandleData.HandleType.TopVertical:
                            case HandleData.HandleType.MiddleVertical:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.upGroupBlockObjects);
                                break;
                            case HandleData.HandleType.BottomVertical:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.downGroupBlockObjects);
                                break;
                        }
                        break;
                    case StageData.GridDirection.Down:
                        switch (handleType)
                        {
                            case HandleData.HandleType.BottomVertical:
                            case HandleData.HandleType.MiddleVertical:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.downGroupBlockObjects);
                                break;
                            case HandleData.HandleType.TopVertical:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedBlockObjects, selectInformation.upGroupBlockObjects);
                                break;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// 掴みロック状態にする
        /// </summary>
        public void GrabLock()
        {
            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
                scaleChangeInformation.lockedBlockObjects[i].SetFlagGrabLock(true);
        }

        /// <summary>
        /// ロックを外す
        /// </summary>
        public void UnLock()
        {
            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                scaleChangeInformation.lockedBlockObjects[i].SetFlagGrabLock(false);
                scaleChangeInformation.lockedBlockObjects[i].SetFlagMoveLock(false);
                scaleChangeInformation.lockedBlockObjects[i].SetFlagScaleUp(false);
                scaleChangeInformation.lockedBlockObjects[i].SetFlagScaleDown(false);
            }

            scaleChangeInformation.lockedBlockObjects.Clear();
        }

        /// <summary>
        /// 縮小可能か
        /// </summary>
        /// <param name="checkDistance"> 縮小距離 </param>
        /// <returns> 縮小可能か </returns>
        public int CheckCanScaleDown(int checkDistance)
        {
            var distanceDirection = Vector2Int.zero;

            switch (scaleChangeInformation.moveDirection)
            {
                case StageData.GridDirection.Left:
                    distanceDirection.x = -1;
                    break;
                case StageData.GridDirection.Right:
                    distanceDirection.x = 1;
                    break;
                case StageData.GridDirection.Up:
                    distanceDirection.y = 1;
                    break;
                case StageData.GridDirection.Down:
                    distanceDirection.y = -1;
                    break;
            }

            int distance = checkDistance;

            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedBlockObjects[i].GetGridPosition();

                var isScaleUp = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleDown();

                if (isScaleUp || !isScaleDown)
                    continue;

                var tempDistance = distance;

                for (int j = 0; j < checkDistance; ++j)
                {
                    var targetPosition = gridPosition + (distanceDirection * (j + 1));

                    var myObjects = blockLayer.GetAtMyBlockObjects(targetPosition);
                    var isSlide = false;

                    if (myObjects.Count > 0)
                    {
                        for (int k = 0; k < myObjects.Count; ++k)
                        {
                            if (!myObjects[k].GetFlagScaleUp())
                                continue;

                            isSlide = true;
                            break;
                        }

                        if (isSlide)
                        {
                            distance = tempDistance;
                            break;
                        }

                        continue;
                    }

                    distance = j < distance ? j : distance;
                }
            }

            return distance;
        }

        #endregion

        #region 拡大縮小関係

        /// <summary>
        /// 拡大縮小を始める
        /// </summary>
        public void StartProcess()
        {
            scaleChangeInformation.distance = 0;

            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                var isScaleUp = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleDown();

                if (isScaleUp && !isScaleDown)
                {
                    var targetGridPosition = scaleChangeInformation.lockedBlockObjects[i].GetGridPosition();

                    BlockData blockData = new BlockData();
                    blockData.GridPosition = targetGridPosition;

                    var newItem = blockLayer.AddBlockObject(blockData);
                    if (!newItem)
                        continue;

                    if (!scaleChangeInformation.newBlockObjects.Contains(newItem))
                        scaleChangeInformation.newBlockObjects.Add(newItem);
                }
            }
        }

        /// <summary>
        /// 拡大終了の終了
        /// </summary>
        public void FinishProcess()
        {
            scaleChangeInformation.newBlockObjects.Clear();

            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                var rect = scaleChangeInformation.lockedBlockObjects[i].GetGridColliderRect();
                scaleChangeInformation.lockedBlockObjects[i].CalculateGridPosition(rect.x, rect.y);
                scaleChangeInformation.lockedBlockObjects[i].CalculatePosition(scaleChangeInformation.lockedBlockObjects[i].GetGridPosition());
            }
        }

        /// <summary>
        /// 拡大縮小更新
        /// </summary>
        /// <param name="isFailed"> 失敗状態か </param>
        /// <param name="processRate"> 移動割合 </param>
        /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        public void UpdateProcess(bool isFailed, float processRate, int currentDistance, int distance)
        {
            var distanceDirection = Vector2Int.zero;

            switch (scaleChangeInformation.moveDirection)
            {
                case StageData.GridDirection.Left:
                    distanceDirection.x = -1;
                    break;
                case StageData.GridDirection.Right:
                    distanceDirection.x = 1;
                    break;
                case StageData.GridDirection.Up:
                    distanceDirection.y = 1;
                    break;
                case StageData.GridDirection.Down:
                    distanceDirection.y = -1;
                    break;
            }

            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedBlockObjects[i].GetGridPosition();
                var nextGridPosition = gridPosition;

                nextGridPosition += distanceDirection * distance;

                var prevPosition = scaleChangeInformation.lockedBlockObjects[i].GetCalculatePosition(gridPosition);
                var nextPosition = scaleChangeInformation.lockedBlockObjects[i].GetCalculatePosition(nextGridPosition);

                var position = scaleChangeInformation.lockedBlockObjects[i].GetCalculatePosition(gridPosition);

                prevPosition.z = position.z - 0.0005f;
                nextPosition.z = position.z - 0.0005f;

                scaleChangeInformation.lockedBlockObjects[i].transform.position = Vector3.Lerp(prevPosition, nextPosition, processRate);

                scaleChangeInformation.lockedBlockObjects[i].CheckFailedScaleChange();
            }

            if (isFailed)
                FailedUpdateProcess(processRate, currentDistance, distance, distanceDirection);
            else
                SuccessUpdateProcess(processRate, currentDistance, distance, distanceDirection);

            scaleChangeInformation.distance = currentDistance;
        }

        /// <summary>
        /// 成功時の拡大縮小更新
        /// </summary>
        /// <param name="processRate"> 移動割合 </param>
        /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        /// <param name="distanceDirection"> 判定方向 </param>
        void SuccessUpdateProcess(float processRate, int currentDistance, int distance, Vector2Int distanceDirection)
        {
            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedBlockObjects[i].GetGridPosition();

                var isScaleUp = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleDown();

                if (isScaleUp && !isScaleDown && scaleChangeInformation.distance + 1 < distance)
                {
                    for (int j = scaleChangeInformation.distance + 1; j <= currentDistance; ++j)
                    {
                        var targetGridPosition = gridPosition;
                        targetGridPosition += distanceDirection * j;

                        BlockData blockData = new BlockData();
                        blockData.GridPosition = targetGridPosition;

                        var newItem = blockLayer.AddBlockObject(blockData);
                        if (!newItem)
                            continue;

                        if (!scaleChangeInformation.newBlockObjects.Contains(newItem))
                            scaleChangeInformation.newBlockObjects.Add(newItem);
                    }
                }
                else if (!isScaleUp && isScaleDown)
                {
                    for (int j = scaleChangeInformation.distance + 1; j <= currentDistance; ++j)
                    {
                        var targetGridPosition = gridPosition;
                        targetGridPosition += distanceDirection * j;

                        var stageObjects = blockLayer.stageManager.GetAtGrid(targetGridPosition);
                        for (int k = 0; k < stageObjects.Count; ++k)
                        {
                            if (stageObjects[k].GetObjectType() != StageData.ObjectType.Block)
                                continue;

                            var targetBlock = stageObjects[k] as BlockStageObject;
                            if (!targetBlock)
                                continue;

                            if (!targetBlock.CheckParent(blockLayer))
                                continue;

                            targetBlock.DestroyThis();

                            if (scaleChangeInformation.newBlockObjects.Contains(targetBlock))
                                scaleChangeInformation.newBlockObjects.Remove(targetBlock);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 失敗時の拡大縮小更新
        /// </summary>
        /// <param name="processRate"> 移動割合 </param>
        /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        /// <param name="distanceDirection"> 判定方向 </param>
        void FailedUpdateProcess(float processRate, int currentDistance, int distance, Vector2Int distanceDirection)
        {
            for (int i = 0; i < scaleChangeInformation.lockedBlockObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedBlockObjects[i].GetGridPosition();

                var isScaleUp = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedBlockObjects[i].GetFlagScaleDown();

                if (isScaleUp && !isScaleDown)
                {
                    for (int j = currentDistance; j <= scaleChangeInformation.distance; ++j)
                    {
                        var targetGridPosition = gridPosition;
                        targetGridPosition += distanceDirection * j;

                        var stageObjects = blockLayer.stageManager.GetAtGrid(targetGridPosition);
                        for (int k = 0; k < stageObjects.Count; ++k)
                        {
                            if (stageObjects[k].GetObjectType() != StageData.ObjectType.Block)
                                continue;

                            var targetBlock = stageObjects[k] as BlockStageObject;
                            if (!targetBlock)
                                continue;

                            if (!scaleChangeInformation.newBlockObjects.Contains(targetBlock))
                                continue;

                            stageObjects[k].DestroyThis();

                            scaleChangeInformation.newBlockObjects.Remove(targetBlock);
                        }
                    }
                }
            }
        }

        #endregion

        #region 子クラス

        /// <summary>
        /// 選択情報
        /// </summary>
        public class SelectInformation
        {
            /// <summary> 左側ブロックオブジェクト </summary>
            public List<BlockStageObject> leftGroupBlockObjects = new List<BlockStageObject>();

            /// <summary> 右側ブロックオブジェクト </summary>
            public List<BlockStageObject> rightGroupBlockObjects = new List<BlockStageObject>();

            /// <summary> 上側ブロックオブジェクト </summary>
            public List<BlockStageObject> upGroupBlockObjects = new List<BlockStageObject>();

            /// <summary> 下側ブロックオブジェクト </summary>
            public List<BlockStageObject> downGroupBlockObjects = new List<BlockStageObject>();
        }

        /// <summary>
        /// 拡大縮小情報
        /// </summary>
        class ScaleChangeInformation
        {
            /// <summary> 拡大縮小距離 </summary>
            public int distance = 0;

            /// <summary> 移動方向 </summary>
            public StageData.GridDirection moveDirection = StageData.GridDirection.None;

            /// <summary> ロック中のブロック群 </summary>
            public List<BlockStageObject> lockedBlockObjects = new List<BlockStageObject>();

            /// <summary> 新しいブロック群 </summary>
            public List<BlockStageObject> newBlockObjects = new List<BlockStageObject>();
        }

        #endregion
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> ブロックレイヤー </summary>
        StageBlockLayer blockLayer = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = new StageObjects();

        #region 初期化・解放処理

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <param name="blockLayer"> ブロックレイヤー </param>
        /// <returns> 成功した </returns>
        public bool Initialize(StageBlockLayer blockLayer)
        {
            if (blockLayer)
                this.blockLayer = blockLayer;

            if (!this.blockLayer)
                return false;

            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAllBlockObject();
        }

        #endregion

        #region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.blockObjects.Count; ++i)
                stageObjects.blockObjects[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.blockObjects.Count; ++i)
                stageObjects.blockObjects[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.blockObjects.Count; ++i)
                stageObjects.blockObjects[i].LateUpdateProcess();
        }

        #endregion

        #region ブロックオブジェクト

        /// <summary>
        /// ブロックオブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public BlockStageObject AddBlockObject(BlockData data)
        {
            var stageManager = blockLayer.stageManager;
            var objectFactory = blockLayer.objectFactory;

            var newItem = objectFactory.InstantiateBlockObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, blockLayer.scaleChangeManager.MyBlockType, blockLayer))
            {
                objectFactory.DestroyBlockObject(newItem);

                return null;
            }

            if (!stageObjects.blockObjects.Contains(newItem))
                stageObjects.blockObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// ブロックオブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveBlockObject(BlockStageObject item)
        {
            var objectFactory = blockLayer.objectFactory;

            if (!item)
                return;

            if (stageObjects.blockObjects.Contains(item))
                stageObjects.blockObjects.Remove(item);

            objectFactory.DestroyBlockObject(item);
        }

        /// <summary>
        /// 全てのブロックオブジェクトを取り除く
        /// </summary>
        public void RemoveAllBlockObject()
        {
            while (stageObjects.blockObjects.Count > 0)
                stageObjects.blockObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のブロックオブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<BlockStageObject> GetMyBlockObjects()
        {
            return stageObjects.blockObjects;
        }

        /// <summary>
        /// 指定位置の自身のブロックオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<BlockStageObject> GetAtMyBlockObjects(Vector2Int position)
        {
            var targets = new List<BlockStageObject>();
            var stageObjects = blockLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BlockStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.blockObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のブロックオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<BlockStageObject> GetRangeMyBlockObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<BlockStageObject>();
            var stageObjects = blockLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BlockStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.blockObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// ブロックオブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasBlockObjects()
        {
            return GetMyBlockObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のブロックオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtBlockObjects(Vector2Int position)
        {
            return GetAtMyBlockObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のブロックオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeBlockObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyBlockObjects(position, size).Count > 0;
        }

        #endregion
    }

    #endregion
}

#elif false

using System.Collections.Generic;
using UnityEngine;
using static BlockData;

public class StageBlockLayer : StageObject
{
    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = null;

    /// <summary> ブロックの種類 </summary>
    BlockData.BlockType myBlockType = BlockData.BlockType.Fixed;

    /// <summary> オブジェクトマネージャー </summary>
    public StageObject_Manager ObjectManager { get => objectManager; }

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        if (objectManager == null)
            objectManager = new StageObject_Manager(this);

        UnInitialize();

        objectManager.Initialize();

        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        objectManager.UnInitialize();

        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager?.ObjectManager?.RemoveBlockLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 成功したか </returns>
    public bool SetData(BlockLayerData data)
    {
        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        myBlockType = data.MyBlockType;

        transform.position = CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        if (objectManager == null)
            return false;

        for (int i = 0; i < data.BlockDatas.Count; ++i)
            objectManager.AddBlockObject(data.BlockDatas[i]);

        return true;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        objectManager.UpdateProcess();
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        objectManager.FixedUpdateProcess();
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        objectManager.LateUpdateProcess();
    }

#endregion

#region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.BlockLayer;
    }

#endregion

#region 子クラス

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> ブロックオブジェクト </summary>
        public List<BlockStageObject> blockObjects = new List<BlockStageObject>();
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> ブロックレイヤー </summary>
        StageBlockLayer blockLayer = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = null;

#region 初期化・解放処理関係

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="blockLayer"> ブロックレイヤー </param>
        public StageObject_Manager(StageBlockLayer blockLayer)
        {
            this.blockLayer = blockLayer;
            stageObjects = new StageObjects();
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Initialize()
        {
            UnInitialize();
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAllBlockObject();
        }

#endregion

#region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.blockObjects.Count; ++i)
                stageObjects.blockObjects[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.blockObjects.Count; ++i)
                stageObjects.blockObjects[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.blockObjects.Count; ++i)
                stageObjects.blockObjects[i].LateUpdateProcess();
        }

#endregion

#region ブロックオブジェクト

        /// <summary>
        /// ブロックオブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public BlockStageObject AddBlockObject(BlockData data)
        {
            if (!blockLayer)
                return null;

            var stageManager = blockLayer.stageManager;
            var objectFactory = blockLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateBlockObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, blockLayer.myBlockType, blockLayer))
            {
                objectFactory.DestroyBlockObject(newItem);

                return null;
            }

            if (!stageObjects.blockObjects.Contains(newItem))
                stageObjects.blockObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// ブロックオブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveBlockObject(BlockStageObject item)
        {
            var objectFactory = blockLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (!item)
                return;

            if (stageObjects.blockObjects.Contains(item))
                stageObjects.blockObjects.Remove(item);

            objectFactory.DestroyBlockObject(item);
        }

        /// <summary>
        /// 全てのブロックオブジェクトを取り除く
        /// </summary>
        public void RemoveAllBlockObject()
        {
            while (stageObjects.blockObjects.Count > 0)
                stageObjects.blockObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のブロックオブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<BlockStageObject> GetMyBlockObjects()
        {
            return stageObjects.blockObjects;
        }

        /// <summary>
        /// 指定位置の自身のブロックオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<BlockStageObject> GetAtMyBlockObjects(Vector2Int position)
        {
            var targets = new List<BlockStageObject>();
            var stageObjects = blockLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BlockStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.blockObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のブロックオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<BlockStageObject> GetRangeMyBlockObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<BlockStageObject>();
            var stageObjects = blockLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BlockStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.blockObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// ブロックオブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasBlockObjects()
        {
            return GetMyBlockObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のブロックオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtBlockObjects(Vector2Int position)
        {
            return GetAtMyBlockObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のブロックオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeBlockObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyBlockObjects(position, size).Count > 0;
        }

#endregion
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StageBlockLayer : StageObject
{
    /// <summary> ブロックオブジェクト </summary>
    List<BlockStageObject> blockObjects = new List<BlockStageObject>();

    /// <summary> ブロックの種類 </summary>
    BlockData.BlockType myBlockType = BlockData.BlockType.Fixed;

    /// <summary> 左端のブロックオブジェクト </summary>
    List<BlockStageObject> leftBlockObjects = new List<BlockStageObject>();

    /// <summary> 右端のブロックオブジェクト </summary>
    List<BlockStageObject> rightBlockObjects = new List<BlockStageObject>();

    /// <summary> 上端のブロックオブジェクト </summary>
    List<BlockStageObject> upBlockObjects = new List<BlockStageObject>();

    /// <summary> 下端のブロックオブジェクト </summary>
    List<BlockStageObject> downBlockObjects = new List<BlockStageObject>();

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        RemoveAllBlockObject();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager.RemoveBlockLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    public void SetData(BlockLayerData data)
    {
        systemBitFlag.Pop(bitFlagEnable);

        transform.position = CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        myBlockType = data.MyBlockType;

        for (int i = 0; i < data.BlockDatas.Count; ++i)
            AddBlockObject(data.BlockDatas[i]);

        CheckBlockGrabGroup();
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < blockObjects.Count; ++i)
            blockObjects[i].UpdateProcess(managerCondition);
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < blockObjects.Count; ++i)
            blockObjects[i].FixedUpdateProcess(managerCondition);
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < blockObjects.Count; ++i)
            blockObjects[i].LateUpdateProcess(managerCondition);
    }

#endregion

#region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region ブロックオブジェクト

    /// <summary>
    /// ブロックオブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public BlockStageObject AddBlockObject(BlockData data)
    {
        var newItem = objectFactory.InstantiateBlockObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, myBlockType, this);

        if (!blockObjects.Contains(newItem))
            blockObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// ブロックオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBlockObject(BlockStageObject item)
    {
        if (!item)
            return;

        if (blockObjects.Contains(item))
            blockObjects.Remove(item);

        objectFactory.DestroyBlockObject(item);
    }

    /// <summary>
    /// 全てのブロックオブジェクトを取り除く
    /// </summary>
    public void RemoveAllBlockObject()
    {
        while (blockObjects.Count > 0)
            blockObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身のブロックオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<BlockStageObject> GetMyBlockObjects()
    {
        return blockObjects;
    }

    /// <summary>
    /// 指定位置の自身のブロックオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<BlockStageObject> GetAtMyBlockObjects(Vector2Int position)
    {
        var targets = new List<BlockStageObject>();
        var stageObjects = stageManager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as BlockStageObject;

            if (!target)
                continue;

            if (!blockObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身のブロックオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<BlockStageObject> GetRangeMyBlockObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<BlockStageObject>();
        var stageObjects = stageManager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as BlockStageObject;

            if (!target)
                continue;

            if (!blockObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// ブロックオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasBlockObjects()
    {
        return GetMyBlockObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtBlockObjects(Vector2Int position)
    {
        return GetAtMyBlockObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置のブロックオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeBlockObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyBlockObjects(position, size).Count > 0;
    }

    /// <summary>
    /// ブロックの掴み移動グループを調べる
    /// </summary>
    public void CheckBlockGrabGroup()
    {
        leftBlockObjects.Clear();
        rightBlockObjects.Clear();
        upBlockObjects.Clear();
        downBlockObjects.Clear();

        for (int i = 0; i < blockObjects.Count; ++i)
        {
            var gridPosition = blockObjects[i].GetGridPosition();
            var left = new Vector2Int(gridPosition.x - 1, gridPosition.y);
            var right = new Vector2Int(gridPosition.x + 1, gridPosition.y);
            var up = new Vector2Int(gridPosition.x, gridPosition.y + 1);
            var down = new Vector2Int(gridPosition.x, gridPosition.y - 1);

            if (stageManager.CheckPositionInGrid(left))
                if (!CheckHasAtBlockObjects(left))
                    leftBlockObjects.Add(blockObjects[i]);

            if (stageManager.CheckPositionInGrid(right))
                if (!CheckHasAtBlockObjects(right))
                    rightBlockObjects.Add(blockObjects[i]);

            if (stageManager.CheckPositionInGrid(up))
                if (!CheckHasAtBlockObjects(up))
                    upBlockObjects.Add(blockObjects[i]);

            if (stageManager.CheckPositionInGrid(down))
                if (!CheckHasAtBlockObjects(down))
                    downBlockObjects.Add(blockObjects[i]);
        }
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.BlockLayer;
    }

#endregion
}

#endif