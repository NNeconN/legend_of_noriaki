using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleEditorObject : EditorObject
{
    /// <summary> 自身のモデル操作用 </summary>
    [SerializeField] HandleModelController controller = null;

    /// <summary> 領域レイヤー </summary>
    [SerializeField] EditorAreaLayer areaLayer = null;

    /// <summary> 領域の種類 </summary>
    [SerializeField] BlockData.BlockType areaType = BlockData.BlockType.Fixed;

    /// <summary> 取っ手の種類 </summary>
    [SerializeField] HandleData.HandleType handleType = HandleData.HandleType.MiddleHorizontal;

    /// <summary> リンクする取っ手のグループ番号 </summary>
    [SerializeField] int linkGroupNumber = -1;

    /// <summary> 領域レイヤー設定・取得用 </summary>
    public EditorAreaLayer AreaLayer { get => areaLayer; set => areaLayer = value; }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        if (!GameObjectUtility.NullCheckAndGet(ref controller, transform))
            return false;

        gridSize = HandleData.DEFAULT_GRID_SIZE;

        SetHandleType(HandleData.HandleType.MiddleHorizontal);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        areaLayer.RemoveHandleObject(this);
    }

    /// <summary>
    /// 他を破壊する
    /// </summary>
    public override void DestroyOther()
    {
        var gridHasObjects = manager.GetRangeStageObjects(gridPosition, gridSize);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            if (gridHasObjects[i] == this)
                continue;

            var tag = gridHasObjects[i].GetObjectType();

            switch (tag)
            {
                case StageData.ObjectType.Block:
                case StageData.ObjectType.Handle:
                case StageData.ObjectType.Goal:
                case StageData.ObjectType.Player:
                case StageData.ObjectType.Enemy1:
                    gridHasObjects[i]?.DestroyThis();
                    break;
            }
        }
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public override void ChangeObjectName()
    {
        if (linkGroupNumber < 0)
        {
            gameObject.name =
                GetType().Name + "_" +
                areaType.ToString() + "_" +
                handleType.ToString();
        }
        else
        {
            gameObject.name =
                "Link_No_" +
                linkGroupNumber.ToString() + "_" +
                GetType().Name + "_" +
                areaType.ToString() + "_" +
                handleType.ToString();
        }
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        if (gridPosition.x < 0)
            return true;

        if (gridPosition.y < 0)
            return true;

        if (gridPosition.x + gridSize.x - 1 >= managerGridSize.x)
            return true;

        if (gridPosition.y + gridSize.y - 1 >= managerGridSize.y)
            return true;

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        DestroyOther();

        return false;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        return ResetGridAllObjects();
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - gridSize);

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        transform.position = CalculatePosition(gridPosition);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        var leftBottom = gridPosition + direction;
        var rightTop = leftBottom + gridSize - Vector2Int.one;

        return manager.CheckPositionInGrid(leftBottom) && manager.CheckPositionInGrid(rightTop);
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        var targetPosition = gridPosition + direction;

        SetGridPosition(targetPosition);

        DestroyOther();
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!manager)
            return Vector3.zero;

        var position = HandleData.DEFAULT_POSITION_OFFSET;

        position.x += manager.GetLeftPivotX() + gridPosition.x;
        position.y += manager.GetBottomPivotY() + gridPosition.y;
        position.z += manager.transform.position.z;

        return position;
    }

    #endregion

    #region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return areaType;
    }

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    public void SetAreaType(BlockData.BlockType areaType)
    {
        this.areaType = areaType;

        ChangeObjectName();
    }

    #endregion

    #region 取っ手の種類

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <returns> 取っ手の種類 </returns>
    public HandleData.HandleType GetHandleType()
    {
        return handleType;
    }

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    public void SetHandleType(HandleData.HandleType handleType)
    {
        this.handleType = handleType;

        ChangeObjectName();

        if (HandleData.CheckHorizontalHandleType(handleType))
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
        else
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
    }

    #endregion

    #region リンク番号

    /// <summary>
    /// リンクするグループ番号を取得
    /// </summary>
    /// <returns> リンクするグループ番号 </returns>
    public int GetLinkGroupNumber()
    {
        return linkGroupNumber;
    }

    /// <summary>
    /// リンクするグループ番号を設定
    /// </summary>
    /// <param name="linkGroupNumber"> リンクするグループ番号 </param>
    public void SetLinkGroupNumber(int linkGroupNumber)
    {
        this.linkGroupNumber = Mathf.Max(linkGroupNumber, -1);

        ChangeObjectName();
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Handle;
    }

    #endregion
}
