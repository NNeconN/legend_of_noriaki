#if true

using UnityEngine;

public class HandleStageObject : StageObject
{
    /// <summary> 領域レイヤー </summary>
    StageAreaLayer areaLayer = null;

    /// <summary> モデルコントローラー </summary>
    HandleModelController controller = null;

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

    /// <summary> 取っ手の種類 </summary>
    HandleData.HandleType myHandleType = HandleData.HandleType.MiddleHorizontal;

    /// <summary> リンクする取っ手のグループ番号 </summary>
    int linkGroupNumber = -1;

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        var isFailed = !base.Initialize(stageManager, objectFactory);
        isFailed = isFailed || !controller;

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGrid(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        areaLayer.RemoveHandleObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="areaLayer"> 領域レイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(HandleData data, StageAreaLayer areaLayer)
    {
        this.areaLayer = areaLayer;

        controller.ForcedFinishBright();

        CalculatePosition(data.GridPosition);
        transform.parent = this.areaLayer.transform;

        myAreaType = data.MyAreaType;
        linkGroupNumber = data.LinkGroupNumber;

        SetHandleType(data.MyHandleType);

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        CalculateGridPosition(xMin, yMin);
        CalculateGridSize(xMin, xMax, yMin, yMax);

        stageManager.AddRangeGrid(this, gridPosition, gridSize);

        return true;
    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        controller.BrightUpdate(Time.deltaTime);
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + HandleData.DEFAULT_COMMON_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + HandleData.DEFAULT_COMMON_GRID_COLLIDER_OFFSET.y;

        if (HandleData.CheckHorizontalHandleType(myHandleType))
        {
            rect.width = HandleData.DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE.x;
            rect.height = HandleData.DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE.y;
        }
        else
        {
            rect.width = HandleData.DEFAULT_VERTICAL_GRID_COLLIDER_SIZE.x;
            rect.height = HandleData.DEFAULT_VERTICAL_GRID_COLLIDER_SIZE.y;
        }

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = HandleData.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

    #endregion

    #region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return myAreaType;
    }

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    public void SetAreaType(BlockData.BlockType areaType)
    {
        myAreaType = areaType;
    }

    /// <summary>
    /// 親レイヤーが同じか
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <returns> 同じか </returns>
    public bool CheckParent(StageAreaLayer target)
    {
        return areaLayer == target;
    }

    #endregion

    #region 取っ手の種類

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <returns> 取っ手の種類 </returns>
    public HandleData.HandleType GetHandleType()
    {
        return myHandleType;
    }

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    public void SetHandleType(HandleData.HandleType handleType)
    {
        myHandleType = handleType;

        if (HandleData.CheckHorizontalHandleType(handleType))
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
        else
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
    }

    #endregion

    #region リンク番号

    /// <summary>
    /// リンクするグループ番号を取得
    /// </summary>
    /// <returns> リンクするグループ番号 </returns>
    public int GetLinkGroupNumber()
    {
        return linkGroupNumber;
    }

    /// <summary>
    /// リンクするグループ番号を設定
    /// </summary>
    /// <param name="linkGroupNumber"> リンクするグループ番号 </param>
    public void SetLinkGroupNumber(int linkGroupNumber)
    {
        this.linkGroupNumber = Mathf.Max(linkGroupNumber, -1);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Handle;
    }

    #endregion

    #region 選択関係

    /// <summary>
    /// 選択する
    /// </summary>
    public override void Select()
    {
        controller.SetBrightTime(stageManager.SelectBrightTime);
        controller.SetBrightColor(stageManager.SelectColor);
        controller.StartBright();
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    public override void UnSelect()
    {
        controller.FinishBright();
    }

    #endregion

    #region 拡大縮小関係

    /// <summary>
    /// 拡大縮小失敗か確認
    /// </summary>
    public override void CheckFailedScaleChange()
    {
        var hitObjects = stageManager.HitCheckGridActive(this);

        for (int i = 0; i < hitObjects.Count; ++i)
        {
            var objectType = hitObjects[i].GetObjectType();

            switch (objectType)
            {
                case StageData.ObjectType.Goal:
                case StageData.ObjectType.Handle:
                case StageData.ObjectType.Block:
                    stageManager.SetFailedObject(this);
                    return;
            }
        }

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        var leftBottom = GetCalculateGridPosition(xMin, yMin);
        var gridSize = GetCalculateGridSize(xMin, xMax, yMin, yMax);
        var rightTop = leftBottom + gridSize - Vector2Int.one;

        if (!stageManager.CheckInPosition(leftBottom) || !stageManager.CheckInPosition(rightTop))
            stageManager.SetFailedObject(this);
    }

    /// <summary>
    /// 失敗演出
    /// </summary>
    public override void FailedEffect()
    {
        controller.SetBrightTime(stageManager.FailedBrightTime);
        controller.SetBrightColor(stageManager.FailedColor);
        controller.StartBrightOnce();
    }

    #endregion
}

#elif false

using UnityEngine;

public class HandleStageObject : StageObject
{
    /// <summary> 領域レイヤー </summary>
    StageAreaLayer areaLayer = null;

    /// <summary> モデルコントローラー </summary>
    HandleModelController controller = null;

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

    /// <summary> 取っ手の種類 </summary>
    HandleData.HandleType myHandleType = HandleData.HandleType.MiddleHorizontal;

    /// <summary> リンクする取っ手のグループ番号 </summary>
    int linkGroupNumber = -1;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager?.GridHasObjects?.RemoveRange(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        areaLayer?.ObjectManager?.RemoveHandleObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="areaLayer"> 領域レイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(HandleData data, StageAreaLayer areaLayer)
    {
        this.areaLayer = areaLayer;

        transform.position = CalculatePosition(data.GridPosition);
        transform.parent = this.areaLayer.transform;

        myAreaType = data.MyAreaType;
        linkGroupNumber = data.LinkGroupNumber;

        SetHandleType(data.MyHandleType);

        var rect = GetGridColliderRect();

        CalculateGridPosition(rect.xMin, rect.yMin);
        CalculateGridSize(rect.xMin, rect.xMax, rect.yMin, rect.yMax);

        stageManager?.GridHasObjects?.AddRange(this, gridPosition, gridSize);

        return true;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        switch (stageManager.MyMainCondition)
        {
            case Stage_Manager.MainCondition.PlayNormal:
                UpdateGridParameter();
                break;
        }
    }



#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + HandleData.DEFAULT_COMMON_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + HandleData.DEFAULT_COMMON_GRID_COLLIDER_OFFSET.y;

        if (HandleData.CheckHorizontalHandleType(myHandleType))
        {
            rect.width = HandleData.DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE.x;
            rect.height = HandleData.DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE.y;
        }
        else
        {
            rect.width = HandleData.DEFAULT_VERTICAL_GRID_COLLIDER_SIZE.x;
            rect.height = HandleData.DEFAULT_VERTICAL_GRID_COLLIDER_SIZE.y;
        }

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = HandleData.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

#endregion

#region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return myAreaType;
    }

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    public void SetAreaType(BlockData.BlockType areaType)
    {
        myAreaType = areaType;

        ChangeObjectName();
    }

#endregion

#region 取っ手の種類

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <returns> 取っ手の種類 </returns>
    public HandleData.HandleType GetHandleType()
    {
        return myHandleType;
    }

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    public void SetHandleType(HandleData.HandleType handleType)
    {
        myHandleType = handleType;

        ChangeObjectName();

        if (HandleData.CheckHorizontalHandleType(handleType))
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
        else
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
    }

#endregion

#region リンク番号

    /// <summary>
    /// リンクするグループ番号を取得
    /// </summary>
    /// <returns> リンクするグループ番号 </returns>
    public int GetLinkGroupNumber()
    {
        return linkGroupNumber;
    }

    /// <summary>
    /// リンクするグループ番号を設定
    /// </summary>
    /// <param name="linkGroupNumber"> リンクするグループ番号 </param>
    public void SetLinkGroupNumber(int linkGroupNumber)
    {
        this.linkGroupNumber = Mathf.Max(linkGroupNumber, -1);

        ChangeObjectName();
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Handle;
    }

#endregion
}

#else

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleStageObject : StageObject
{
    /// <summary> 領域レイヤー </summary>
    StageAreaLayer areaLayer = null;

    /// <summary> モデルコントローラー </summary>
    HandleModelController controller = null;

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

    /// <summary> 取っ手の種類 </summary>
    HandleData.HandleType myHandleType = HandleData.HandleType.MiddleHorizontal;

    /// <summary> リンクする取っ手のグループ番号 </summary>
    int linkGroupNumber = -1;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        areaLayer.RemoveHandleObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="areaLayer"> 領域レイヤー </param>
    public void SetData(HandleData data, StageAreaLayer areaLayer)
    {
        this.areaLayer = areaLayer;

        transform.position = CalculatePosition(data.GridPosition);
        transform.parent = this.areaLayer.transform;

        myAreaType = data.MyAreaType;
        linkGroupNumber = data.LinkGroupNumber;

        SetHandleType(data.MyHandleType);

        var rect = GetGridColliderRect();

        CalculateGridPosition(rect.xMin, rect.yMin);
        CalculateGridSize(rect.xMin, rect.xMax, rect.yMin, rect.yMax);

        stageManager.AddRangeGridHasObject(this, gridPosition, gridSize);
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        switch (managerCondition)
        {
            case Stage_Manager.MainCondition.PlayNormal:
                UpdateGridParameter();
                break;
        }
    }



#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + HandleData.DEFAULT_COMMON_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + HandleData.DEFAULT_COMMON_GRID_COLLIDER_OFFSET.y;

        if (HandleData.CheckHorizontalHandleType(myHandleType))
        {
            rect.width = HandleData.DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE.x;
            rect.height = HandleData.DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE.y;
        }
        else
        {
            rect.width = HandleData.DEFAULT_VERTICAL_GRID_COLLIDER_SIZE.x;
            rect.height = HandleData.DEFAULT_VERTICAL_GRID_COLLIDER_SIZE.y;
        }

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = HandleData.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

#endregion

#region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return myAreaType;
    }

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    public void SetAreaType(BlockData.BlockType areaType)
    {
        myAreaType = areaType;

        ChangeObjectName();
    }

#endregion

#region 取っ手の種類

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <returns> 取っ手の種類 </returns>
    public HandleData.HandleType GetHandleType()
    {
        return myHandleType;
    }

    /// <summary>
    /// 取っ手の種類取得
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    public void SetHandleType(HandleData.HandleType handleType)
    {
        myHandleType = handleType;

        ChangeObjectName();

        if (HandleData.CheckHorizontalHandleType(handleType))
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
        else
            transform.eulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
    }

#endregion

#region リンク番号

    /// <summary>
    /// リンクするグループ番号を取得
    /// </summary>
    /// <returns> リンクするグループ番号 </returns>
    public int GetLinkGroupNumber()
    {
        return linkGroupNumber;
    }

    /// <summary>
    /// リンクするグループ番号を設定
    /// </summary>
    /// <param name="linkGroupNumber"> リンクするグループ番号 </param>
    public void SetLinkGroupNumber(int linkGroupNumber)
    {
        this.linkGroupNumber = Mathf.Max(linkGroupNumber, -1);

        ChangeObjectName();
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Handle;
    }

#endregion
}

#endif