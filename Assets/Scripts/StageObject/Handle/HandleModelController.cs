using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 取っ手モデル操作用
/// </summary>
public class HandleModelController : MonoBehaviour
{
    /// <summary>
    /// 光の種類
    /// </summary>
    enum BrightType
    {
        None,
        Once,
        Loop,
    }

    /// <summary>
    /// マテリアルの種類
    /// </summary>
    enum MaterialType
    {
        Normal,
        Shield
    }

    /// <summary> 自身の補助コントローラー </summary>
    SubHandleModelController[] subHandleControllers = new SubHandleModelController[0];

    /// <summary> 光らせる色 </summary>
    [SerializeField] Color emissionColor = Color.white;

    /// <summary> 通常時のマテリアル </summary>
    [SerializeField] Material normalMaterial = null;

    /// <summary> シールド時のマテリアル </summary>
    [SerializeField] Material shieldMaterial = null;
    
    /// <summary> 光の種類 </summary>
    BrightType brightType = BrightType.None;

    /// <summary> 光の種類 </summary>
    MaterialType materialType = MaterialType.Normal;

    /// <summary> 点灯時間 </summary>
    [SerializeField] float brightTime = 1.0f;

    /// <summary> 光の割合 </summary>
    float brightRate = 0.0f;

    /// <summary> 光の割合取得用 </summary>
    public float BrightRate { get { return brightRate; } }

    void Awake()
    {
        transform.localScale = Vector3.one;

        subHandleControllers = GetComponentsInChildren<SubHandleModelController>();

        for (int i = 0, iLength = subHandleControllers.Length; i < iLength; ++i)
        {
            subHandleControllers[i].Initialize();
            subHandleControllers[i].SetMaterial(normalMaterial);
            subHandleControllers[i].UpdateMeshVertices(1);
        }
    }

    /// <summary>
    /// 通常マテリアルを設定する
    /// </summary>
    public void SetNormalMaterial()
    {
        materialType = MaterialType.Normal;

        for (int i = 0, iLength = subHandleControllers.Length; i < iLength; ++i)
            subHandleControllers[i].SetMaterial(normalMaterial);
    }

    /// <summary>
    /// シールドマテリアルを設定する
    /// </summary>
    public void SetShieldMaterial()
    {
        materialType = MaterialType.Shield;

        for (int i = 0, iLength = subHandleControllers.Length; i < iLength; ++i)
            subHandleControllers[i].SetMaterial(shieldMaterial);
    }

    /// <summary>
    /// 強制的に光らせるのをやめる
    /// </summary>
    public void ForcedFinishBright()
    {
        brightType = BrightType.None;
        brightRate = 0.0f;

        if (materialType != MaterialType.Normal)
            return;

        for (int i = 0, iLength = subHandleControllers.Length; i < iLength; ++i)
            subHandleControllers[i].SetEmmisionColor(Color.black);
    }

    /// <summary>
    /// 光らせるのをやめる
    /// </summary>
    public void FinishBright()
    {
        switch (brightType)
        {
            case BrightType.Loop:
                break;
            default:
                return;
        }

        brightType = BrightType.Once;
    }

    /// <summary>
    /// 光らせる
    /// </summary>
    public void StartBright()
    {
        brightRate = 0.0f;
        brightType = BrightType.Loop;
    }

    /// <summary>
    /// 一度だけ光らせる
    /// </summary>
    public void StartBrightOnce()
    {
        brightRate = 0.0f;
        brightType = BrightType.Once;
    }

    /// <summary>
    /// 点灯時間設定
    /// </summary>
    /// <param name="time"> 点灯時間 </param>
    public void SetBrightTime(float time)
    {
        brightTime = time;
    }

    /// <summary>
    /// 光の更新
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    public void BrightUpdate(float deltaTime)
    {
        if (brightType == BrightType.None)
            return;

        if (subHandleControllers.Length < 1 || brightTime <= 0.0f)
        {
            brightType = BrightType.None;
            return;
        }

        brightRate += deltaTime / brightTime;

        if (brightRate >= 1.0f)
        {
            if (brightType == BrightType.Loop)
                brightRate = brightRate % 1.0f;
            else
            {
                brightType = BrightType.None;
                brightRate = 1.0f;
            }
        }

        var rate = 1.0f - Mathf.Sin(brightRate * 180.0f * Mathf.Deg2Rad);
        var color = (Color.black * rate) + (emissionColor * (1.0f - rate));

        if (materialType != MaterialType.Normal)
            return;

        for (int i = 0, iLength = subHandleControllers.Length; i < iLength; ++i)
            subHandleControllers[i].SetEmmisionColor(color);
    }

    /// <summary>
    /// 光の色設定
    /// </summary>
    /// <param name="color"> 光の色 </param>
    public void SetBrightColor(Color color)
    {
        emissionColor = color;
    }
}
