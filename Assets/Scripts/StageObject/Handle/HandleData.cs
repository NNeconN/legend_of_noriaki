using System;
using System.Collections.Generic;
using UnityEngine;

public class HandleData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        GridPosition = -1,
        MyAreaType = -2,
        MyHandleType = -3,
        LinkGroupNumber = -4,
    }

    /// <summary>
    /// 取っ手の種類
    /// </summary>
    public enum HandleType
    {
        LeftHorizontal,
        MiddleHorizontal,
        RightHorizontal,
        BottomVertical,
        MiddleVertical,
        TopVertical,
    }

    /// <summary>
    /// enum : HandleTypeの要素数取得
    /// </summary>
    public static int HandleTypeNum { get { return Enum.GetNames(typeof(HandleType)).Length; } }

    /// <summary>
    /// 取っ手の種類が横向きか
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    /// <returns> 横向きか </returns>
    public static bool CheckHorizontalHandleType(HandleType handleType)
    {
        switch (handleType)
        {
            case HandleType.LeftHorizontal:
            case HandleType.MiddleHorizontal:
            case HandleType.RightHorizontal:
                return true;
            default:
                return false;
        }
    }

    /// <summary>
    /// 取っ手の種類が縦向きか
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    /// <returns> 横向きか </returns>
    public static bool CheckVerticalHandleType(HandleType handleType)
    {
        switch (handleType)
        {
            case HandleType.BottomVertical:
            case HandleType.MiddleVertical:
            case HandleType.TopVertical:
                return true;
            default:
                return false;
        }
    }

    #endregion

    #region static変数

    /// <summary>
    /// グリッド上の大きさ
    /// </summary>
    static Vector2Int defaultGridSize = new Vector2Int(1, 1);

    /// <summary>
    /// 縦横共通のマス目判定用コライダーのずれ
    /// </summary>
    static Vector3 defaultCommonGridColliderOffset = new Vector3(0.0f, 0.0f, -0.1f);

    /// <summary>
    /// 横移動取っ手のマス目判定用コライダーの大きさ
    /// </summary>
    static Vector3 defaultHorizontalGridColliderSize = new Vector3(0.15f, 0.95f, 0.2f);

    /// <summary>
    /// 縦移動取っ手のマス目判定用コライダーの大きさ
    /// </summary>
    static Vector3 defaultVerticalGridColliderSize = new Vector3(0.95f, 0.15f, 0.2f);

    /// <summary>
    /// 位置のずれ
    /// </summary>
    static Vector3 defaultPositionOffset = new Vector3(0.5f, 0.5f, 0.0f);

    /// <summary>
    /// グリッド上の大きさ取得用
    /// </summary>
    public static Vector2Int DEFAULT_GRID_SIZE { get => defaultGridSize; }

    /// <summary>
    /// 縦横共通のマス目判定用コライダーのずれ取得用
    /// </summary>
    public static Vector3 DEFAULT_COMMON_GRID_COLLIDER_OFFSET { get => defaultCommonGridColliderOffset; }

    /// <summary>
    /// 横移動取っ手のマス目判定用コライダーの大きさ取得用
    /// </summary>
    public static Vector3 DEFAULT_HORIZONTAL_GRID_COLLIDER_SIZE { get => defaultHorizontalGridColliderSize; }

    /// <summary>
    /// 縦移動取っ手のマス目判定用コライダーの大きさ取得用
    /// </summary>
    public static Vector3 DEFAULT_VERTICAL_GRID_COLLIDER_SIZE { get => defaultVerticalGridColliderSize; }

    /// <summary>
    /// 位置のずれ取得用
    /// </summary>
    public static Vector3 DEFAULT_POSITION_OFFSET { get => defaultPositionOffset; }

    #endregion

    /// <summary> グリッド上の位置 </summary>
    Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

    /// <summary> 取っ手の種類 </summary>
    HandleType myHandleType = HandleType.MiddleHorizontal;

    /// <summary> リンクする取っ手のグループ番号 </summary>
    int linkGroupNumber = -1;

    /// <summary> グリッド上の位置設定・取得用 </summary>
    public Vector2Int GridPosition { get => gridPosition; set => gridPosition = value; }

    /// <summary> 領域の種類設定・取得用 </summary>
    public BlockData.BlockType MyAreaType { get => myAreaType; set => myAreaType = value; }

    /// <summary> 取っ手の種類設定・取得用 </summary>
    public HandleType MyHandleType { get => myHandleType; set => myHandleType = value; }

    /// <summary> リンクする取っ手のグループ番号設定・取得用 </summary>
    public int LinkGroupNumber { get => linkGroupNumber; set => linkGroupNumber = value; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public HandleData()
    {
        gridPosition = Vector2Int.zero;
        myAreaType = BlockData.BlockType.Fixed;
        myHandleType = HandleType.MiddleHorizontal;
        linkGroupNumber = -1;
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public HandleData(HandleEditorObject target)
    {
        gridPosition = target.GetGridPosition();
        myAreaType = target.GetAreaType();
        myHandleType = target.GetHandleType();
        linkGroupNumber = target.GetLinkGroupNumber();
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(HandleEditorObject target)
    {
        target.SetGridPosition(gridPosition);
        target.SetAreaType(myAreaType);
        target.SetHandleType(myHandleType);
        target.SetLinkGroupNumber(linkGroupNumber);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        for (int indexX = 0; indexX < csvDatas[index].Length;)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][indexX], out dataID))
                break;

            switch (dataID)
            {
                case DataID.GridPosition:
                    gridPosition.x = int.Parse(csvDatas[index][indexX + 1]);
                    gridPosition.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                case DataID.MyAreaType:
                    myAreaType = Enum.Parse<BlockData.BlockType>(csvDatas[index][indexX + 1]);
                    indexX += 2;
                    break;
                case DataID.MyHandleType:
                    myHandleType = Enum.Parse<HandleType>(csvDatas[index][indexX + 1]);
                    indexX += 2;
                    break;
                case DataID.LinkGroupNumber:
                    linkGroupNumber = int.Parse(csvDatas[index][indexX + 1]);
                    indexX += 2;
                    break;
                default:
                    ++indexX;
                    break;
            }
        }

        ++index;
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.Handle.ToString() });

        csvDatas.Add(new string[] { 
            DataID.GridPosition.ToString(),
            gridPosition.x.ToString(), 
            gridPosition.y.ToString(), 
            DataID.MyAreaType.ToString(),
            myAreaType.ToString(), 
            DataID.MyHandleType.ToString(),
            MyHandleType.ToString(),
            DataID.LinkGroupNumber.ToString(),
            linkGroupNumber.ToString()
        });
    }
}
