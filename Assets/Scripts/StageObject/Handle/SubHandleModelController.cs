using UnityEngine;

/// <summary>
/// 取っ手モデル操作補助用
/// </summary>
public class SubHandleModelController : MonoBehaviour
{
    /// <summary> 自身のメッシュレンダラー </summary>
    MeshRenderer myMeshRenderer = null;

    /// <summary> 自身のMesgFilter </summary>
    MeshFilter myMeshFilter = null;

    /// <summary> 参照頂点 </summary>
    Vector3[] vertices = null;

    /// <summary>
    /// 初期化
    /// </summary>
    public void Initialize()
    {
        myMeshRenderer = GetComponent<MeshRenderer>();
        myMeshFilter = GetComponent<MeshFilter>();
        vertices = myMeshFilter?.mesh.vertices;
    }

    /// <summary>
    /// 頂点位置の補正
    /// </summary>
    /// <param name="height"> 希望するモデルの高さ </param>
    public void UpdateMeshVertices(float height)
    {
        if (!myMeshFilter)
            return;

        if (transform.localScale.y == 0.0f)
            return;

        var mesh = myMeshFilter.mesh;
        var vertices = this.vertices;
        var minY = 1000.0f;
        var maxY = -1000.0f;

        for (int i = 0, iLength = vertices.Length; i < iLength; ++i)
        {
            var currentY = vertices[i].y;

            if (currentY < minY) minY = currentY;
            if (currentY > maxY) maxY = currentY;
        }

        var convertHeight = height / transform.localScale.y;
        var halfHeight = convertHeight * 0.5f;
        var distanceUpY = maxY - halfHeight;
        var distanceDownY = minY + halfHeight;

        for (int i = 0, iLength = vertices.Length; i < iLength; ++i)
        {
            if (vertices[i].y > 0.0f)
            {
                vertices[i].y -= distanceUpY;
                if (vertices[i].y < 0.0f) vertices[i].y = 0.0f;
            }
            else if (vertices[i].y < 0.0f)
            {
                vertices[i].y -= distanceDownY;
                if (vertices[i].y > 0.0f) vertices[i].y = 0.0f;
            }
        }

        mesh.vertices = vertices;
        myMeshFilter.mesh = mesh;
    }

    /// <summary>
    /// マテリアルの設定
    /// </summary>
    /// <param name="material"> 設定するマテリアル </param>
    public void SetMaterial(Material material)
    {
        myMeshRenderer.material = material;
    }

    /// <summary>
    /// エミッションカラー設定
    /// </summary>
    /// <param name="color"> エミッションカラー </param>
    public void SetEmmisionColor(Color color)
    {
        var material = myMeshRenderer.material;
        material.EnableKeyword("_EMISSION");
        material.SetColor("_EmissionColor", color);
        myMeshRenderer.material = material;
    }
}
