#if true

using UnityEngine;

public class BackStageObject : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> モデルコントローラー </summary>
    BackModelController controller = null;

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        var isFailed = !base.Initialize(stageManager, objectFactory);
        isFailed = isFailed || !controller;

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGrid(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveBackObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(BackData data, StageObjectsLayer objectsLayer)
    {
        if (!stageManager)
            return false;

        this.objectsLayer = objectsLayer;

        controller.SetMaterialIndex((int)data.MaterialIndex);

        var managerGridSize = stageManager.GetGridSize();

        gridPosition = MathUtility.Clamp(data.GridPosition, Vector2Int.zero, managerGridSize - Vector2Int.one);
        gridSize = MathUtility.Clamp(data.GridSize, Vector2Int.zero, managerGridSize - gridPosition);

        CalculatePosition(data.GridPosition);
        transform.localScale = CalculateScale(data.GridSize);
        transform.parent = this.objectsLayer.transform;

        stageManager.AddRangeGrid(this, gridPosition, gridSize);

        return true;
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + BackData.DEFAULT_GRID_COLLIDER_OFFSET.x * (gridSize.x + 1);
        rect.y = transform.position.y + BackData.DEFAULT_GRID_COLLIDER_OFFSET.y * (gridSize.y + 1);

        rect.width = BackData.DEFAULT_GRID_COLLIDER_SIZE.x + (gridSize.x - 1);
        rect.height = BackData.DEFAULT_GRID_COLLIDER_SIZE.y + (gridSize.y - 1);

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = BackData.DEFAULT_POSITION_OFFSET;

        position.x *= gridSize.x;
        position.y *= gridSize.y;
        position.x += gridPosition.x + stageManager.GetLeftPivotX();
        position.y += gridPosition.y + stageManager.GetBottomPivotY();
        position.z += stageManager.transform.position.z;

        return position;
    }

    /// <summary>
    /// マス目上の大きさからワールドスケールを計算する
    /// </summary>
    /// <param name="gridSize"> マス目上の大きさ </param>
    /// <returns> ワールドスケール </returns>
    public Vector3 CalculateScale(Vector2Int gridSize)
    {
        return new Vector3(gridSize.x, gridSize.y, 1.0f);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Back;
    }

    #endregion
}

#elif false

using UnityEngine;

public class BackStageObject : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> モデルコントローラー </summary>
    BackModelController controller = null;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager?.GridHasObjects?.RemoveRange(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer?.ObjectManager?.RemoveBackObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(BackData data, StageObjectsLayer objectsLayer)
    {
        if (!stageManager)
            return false;

        this.objectsLayer = objectsLayer;

        controller.SetMaterialIndex((int)data.MaterialIndex);

        var managerGridSize = stageManager.GridSize;

        gridPosition = MathUtility.Clamp(data.GridPosition, Vector2Int.zero, managerGridSize - Vector2Int.one);
        gridSize = MathUtility.Clamp(data.GridSize, Vector2Int.zero, managerGridSize - gridPosition);

        transform.position = CalculatePosition(data.GridPosition);
        transform.localScale = CalculateScale(data.GridSize);
        transform.parent = this.objectsLayer.transform;

        stageManager.GridHasObjects?.AddRange(this, gridPosition, gridSize);

        return true;
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + BackData.DEFAULT_GRID_COLLIDER_OFFSET.x * (gridSize.x + 1);
        rect.y = transform.position.y + BackData.DEFAULT_GRID_COLLIDER_OFFSET.y * (gridSize.y + 1);

        rect.width = BackData.DEFAULT_GRID_COLLIDER_SIZE.x + (gridSize.x - 1);
        rect.height = BackData.DEFAULT_GRID_COLLIDER_SIZE.y + (gridSize.y - 1);

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = BackData.DEFAULT_POSITION_OFFSET;

        position.x *= gridSize.x;
        position.y *= gridSize.y;
        position.x += gridPosition.x + stageManager.GetLeftPivotX();
        position.y += gridPosition.y + stageManager.GetBottomPivotY();
        position.z += stageManager.transform.position.z;

        return position;
    }

    /// <summary>
    /// マス目上の大きさからワールドスケールを計算する
    /// </summary>
    /// <param name="gridSize"> マス目上の大きさ </param>
    /// <returns> ワールドスケール </returns>
    public Vector3 CalculateScale(Vector2Int gridSize)
    {
        return new Vector3(gridSize.x, gridSize.y, 1.0f);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Back;
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.UIElements;

public class BackStageObject : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> モデルコントローラー </summary>
    BackModelController controller = null;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveBackObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    public void SetData(BackData data, StageObjectsLayer objectsLayer)
    {
        this.objectsLayer = objectsLayer;

        controller.SetMaterialIndex((int)data.MaterialIndex);

        var managerGridSize = stageManager.GetGridSize();

        gridPosition = MathUtility.Clamp(data.GridPosition, Vector2Int.zero, managerGridSize - Vector2Int.one);
        gridSize = MathUtility.Clamp(data.GridSize, Vector2Int.zero, managerGridSize - gridPosition);

        transform.position = CalculatePosition(data.GridPosition);
        transform.localScale = CalculateScale(data.GridSize);
        transform.parent = this.objectsLayer.transform;

        stageManager.AddRangeGridHasObject(this, gridPosition, gridSize);
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + BackData.DEFAULT_GRID_COLLIDER_OFFSET.x * (gridSize.x + 1);
        rect.y = transform.position.y + BackData.DEFAULT_GRID_COLLIDER_OFFSET.y * (gridSize.y + 1);

        rect.width = BackData.DEFAULT_GRID_COLLIDER_SIZE.x + (gridSize.x - 1);
        rect.height = BackData.DEFAULT_GRID_COLLIDER_SIZE.y + (gridSize.y - 1);

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = BackData.DEFAULT_POSITION_OFFSET;

        position.x *= gridSize.x;
        position.y *= gridSize.y;
        position.x += gridPosition.x + stageManager.GetLeftPivotX();
        position.y += gridPosition.y + stageManager.GetBottomPivotY();
        position.z += stageManager.transform.position.z;

        return position;
    }

    /// <summary>
    /// マス目上の大きさからワールドスケールを計算する
    /// </summary>
    /// <param name="gridSize"> マス目上の大きさ </param>
    /// <returns> ワールドスケール </returns>
    public Vector3 CalculateScale(Vector2Int gridSize)
    {
        return new Vector3(gridSize.x, gridSize.y, 1.0f);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Back;
    }

#endregion
}

#endif