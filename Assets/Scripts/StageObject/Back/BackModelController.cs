using UnityEngine;

/// <summary>
/// 背景モデル操作用
/// </summary>
public class BackModelController : MonoBehaviour
{
    /// <summary> 自身のメッシュレンダラー </summary>
    [SerializeField] MeshRenderer myMeshRenderer = null;

    /// <summary> マテリアル群 </summary>
    [SerializeField] Material[] materials = new Material[0];

    /// <summary>
    /// マテリアルを設定
    /// </summary>
    /// <param name="index"> マテリアル番号 </param>
    /// <returns> 実際の番号 </returns>
    public int SetMaterialIndex(int index)
    {
        if (!myMeshRenderer)
            return 0;

        if (materials.Length < 1)
            return 0;

        index = Mathf.Clamp(index, 0, materials.Length - 1);

        myMeshRenderer.material = materials[index];

        return index;
    }

    /// <summary>
    /// レンダラー表示切替
    /// </summary>
    /// <param name="enabled"> 表示するか </param>
    public void SetEnableRenderer(bool enabled)
    {
        myMeshRenderer.enabled = enabled;
    }
}
