using UnityEngine;

public class BackEditorObject : EditorObject
{
    /// <summary> 自身のモデル操作用 </summary>
    [SerializeField] BackModelController controller = null;

    /// <summary> オブジェクトレイヤー </summary>
    [SerializeField] EditorObjectsLayer objectsLayer = null;

    /// <summary> マテリアルのインデックス番号 </summary>
    [SerializeField] int materialIndex = 0;

    /// <summary> オブジェクトレイヤー設定・取得用 </summary>
    public EditorObjectsLayer ObjectsLayer { get => objectsLayer; set => objectsLayer = value; }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        if (!GameObjectUtility.NullCheckAndGet(ref controller, transform))
            return false;

        SetMaterialIndex(0);
        gridPosition = Vector2Int.zero;
        gridSize = Vector2Int.one;

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveBackObject(this);
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        if (gridPosition.x < 0)
            return true;

        if (gridPosition.y < 0)
            return true;

        if (gridPosition.x >= managerGridSize.x)
            return true;

        if (gridPosition.y >= managerGridSize.y)
            return true;

        SetGridSize(gridSize);

        DestroyOther();

        return false;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        return ResetGridAllObjects();
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - Vector2Int.one);

        SetGridSize(gridSize);
    }

    /// <summary>
    /// グリッド上の大きさ設定
    /// </summary>
    /// <param name="position"> 大きさ </param>
    public override void SetGridSize(Vector2Int size)
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        gridSize = MathUtility.Clamp(size, Vector2Int.zero, managerGridSize - gridPosition);

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        transform.position = CalculatePosition(gridPosition);
        transform.localScale = CalculateScale(gridSize);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        var targetPosition = gridPosition + direction;

        return manager.CheckPositionInGrid(targetPosition);
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        var targetPosition = gridPosition + direction;

        SetGridPosition(targetPosition);

        DestroyOther();
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!manager)
            return Vector3.zero;

        var position = BackData.DEFAULT_POSITION_OFFSET;

        position.x *= gridSize.x;
        position.y *= gridSize.y;
        position.x += gridPosition.x + manager.GetLeftPivotX();
        position.y += gridPosition.y + manager.GetBottomPivotY();
        position.z += manager.transform.position.z;

        return position;
    }

    /// <summary>
    /// マス目上の大きさからワールドスケールを計算する
    /// </summary>
    /// <param name="gridSize"> マス目上の大きさ </param>
    /// <returns> ワールドスケール </returns>
    public Vector3 CalculateScale(Vector2Int gridSize)
    {
        return new Vector3(gridSize.x, gridSize.y, 1.0f);
    }

    #endregion

    #region マテリアル設定

    /// <summary>
    /// マテリアルのインデックス番号取得
    /// </summary>
    /// <returns> インデックス番号 </returns>
    public int GetMaterialIndex()
    {
        return materialIndex;
    }

    /// <summary>
    /// マテリアルのインデックス番号設定
    /// </summary>
    /// <param name="index"> インデックス番号 </param>
    public void SetMaterialIndex(int index)
    {
        materialIndex = controller.SetMaterialIndex(index);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Back;
    }

    #endregion
}
