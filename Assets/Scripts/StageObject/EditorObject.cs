using UnityEngine;

/// <summary>
/// エディター上のオブジェクト
/// </summary>
public class EditorObject : MonoBehaviour
{
    /// <summary> エディターマネージャー </summary>
    [SerializeField] protected StageEditor_Manager manager = null;

    /// <summary> オブジェクトファクトリー </summary>
    [SerializeField] protected EditorObjectFactory objectFactory = null;

    /// <summary> グリッド上の位置 </summary>
    [SerializeField] protected Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> グリッド上の大きさ </summary>
    [SerializeField] protected Vector2Int gridSize = Vector2Int.one;

    /// <summary> エディターマネージャー取得用 </summary>
    public StageEditor_Manager Manager { get => manager; set => manager = value; }

    /// <summary> オブジェクトファクトリー取得用 </summary>
    public EditorObjectFactory Factory { get => objectFactory; set => objectFactory = value; }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public virtual bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        this.manager = manager;
        this.objectFactory = objectFactory;

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public virtual void UnInitialize()
    {

    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public virtual void DestroyThis()
    {

    }

    /// <summary>
    /// 他を破壊する
    /// </summary>
    public virtual void DestroyOther()
    {

    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public virtual void ChangeObjectName()
    {
        gameObject.name = GetType().Name;
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public virtual bool ResetGridAllObjects()
    {
        return false;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public virtual bool ReSizeGrid()
    {
        return false;
    }
        
    /// <summary>
    /// グリッド上の位置取得
    /// </summary>
    /// <returns> グリッド上の位置 </returns>
    public Vector2Int GetGridPosition()
    {
        return gridPosition;
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public virtual void SetGridPosition(Vector2Int position)
    {
    }

    /// <summary>
    /// グリッド上の大きさ取得
    /// </summary>
    /// <returns> グリッド上の大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return gridSize;
    }

    /// <summary>
    /// グリッド上の大きさ設定
    /// </summary>
    /// <param name="size"> 大きさ </param>
    public virtual void SetGridSize(Vector2Int size)
    {
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public virtual bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        return true;
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public virtual void SlideGridPosition(Vector2Int direction)
    {

    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public virtual Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!manager)
            return Vector3.zero;

        var position = new Vector3(manager.GetLeftPivotX(), manager.GetBottomPivotY(), manager.transform.position.z);

        position.x += gridPosition.x;
        position.y += gridPosition.y;

        return position;
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public virtual StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.None;
    }

    #endregion
}
