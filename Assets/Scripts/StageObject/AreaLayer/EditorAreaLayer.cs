using System.Collections.Generic;
using UnityEngine;

public class EditorAreaLayer : EditorObject
{
    /// <summary> 領域オブジェクト群 </summary>
    [SerializeField] List<AreaEditorObject> areaObjects = new List<AreaEditorObject>();

    /// <summary> 取っ手オブジェクト群 </summary>
    [SerializeField] List<HandleEditorObject> handleObjects = new List<HandleEditorObject>();

    /// <summary> 領域の種類 </summary>
    [SerializeField] BlockData.BlockType areaType = BlockData.BlockType.Fixed;

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        UnInitialize();

        SetGridSize(Vector2Int.one);

        ChangeObjectName();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        RemoveAllAreaObject();

        RemoveAllHandleObject();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        manager.RemoveAreaLayer(this);
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public override void ChangeObjectName()
    {
        gameObject.name = GetType().Name + "_" + areaType.ToString();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        for (int i = 0; i < areaObjects.Count; ++i)
            areaObjects[i].ResetGridAllObjects();

        for (int i = 0; i < handleObjects.Count; ++i)
            handleObjects[i].ResetGridAllObjects();

        return false;
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int ReTouchEditorObjects()
    {
        Vector2Int maxGridSize = Vector2Int.one;

        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref areaObjects));
        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref handleObjects));

        return maxGridSize;
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <typeparam name="T"> 対象 </typeparam>
    /// <param name="myList"> 入れ物 </param>
    /// <returns> グリッドの大きさ </returns>
    Vector2Int ReTouchEditorObjects<T>(ref List<T> myList) where T : EditorObject
    {
        Vector2Int maxGridSize = Vector2Int.one;

        myList.Clear();

        var targets = GetComponentsInChildren<T>();

        for (int i = 0; i < targets.Length; ++i)
        {
            myList.Add(targets[i]);

            var gridPosition = targets[i].GetGridPosition();
            var gridSize = targets[i].GetGridSize();

            maxGridSize.x = Mathf.Max(maxGridSize.x, gridPosition.x + gridSize.x);
            maxGridSize.y = Mathf.Max(maxGridSize.y, gridPosition.y + gridSize.y);
        }

        return maxGridSize;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        var managerGridSize = manager.GetGridSize();
        SetGridPosition(gridPosition);
        SetGridSize(gridSize);

        for (int i = 0; i < areaObjects.Count; ++i)
        {
            if (!areaObjects[i].ReSizeGrid())
                continue;

            if (RemoveAreaObject(areaObjects[i]))
                --i;
        }

        for (int i = 0; i < handleObjects.Count; ++i)
        {
            if (!handleObjects[i].ReSizeGrid())
                continue;

            if (RemoveHandleObject(handleObjects[i]))
                --i;
        }

        return false;
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - Vector2Int.one);
    }

    /// <summary>
    /// グリッド上の大きさ設定
    /// </summary>
    /// <param name="position"> 大きさ </param>
    public override void SetGridSize(Vector2Int size)
    {
        var managerGridSize = manager.GetGridSize();

        gridSize = MathUtility.Clamp(size, Vector2Int.one, managerGridSize - gridPosition);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        return CheckHasAreaObjects() || CheckHasHandleObjects();
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        for (int i = 0; i < areaObjects.Count; ++i)
            areaObjects[i].SetGridPosition(areaObjects[i].GetGridPosition() + direction);

        for (int i = 0; i < handleObjects.Count; ++i)
            handleObjects[i].SetGridPosition(handleObjects[i].GetGridPosition() + direction);

        ResetGridAllObjects();
    }

    #endregion

    #region 領域オブジェクト

    /// <summary>
    /// 領域が追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddAreaObject(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + AreaData.DEFAULT_GRID_SIZE.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + AreaData.DEFAULT_GRID_SIZE.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// 領域オブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加したオブジェクト </returns>
    public AreaEditorObject AddAreaObject(Vector2Int position)
    {
        var newItem = objectFactory.InstantiateAreaObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.SetAreaType(areaType);
        newItem.DestroyOther();

        areaObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 領域オブジェクトをまとめて追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加したオブジェクト </returns>
    public List<AreaEditorObject> AddRangeAreaObject(Vector2Int position, Vector2Int size)
    {
        var newItems = new List<AreaEditorObject>();

        for (int x = 0; x < size.x; ++x)
        {
            for (int y = 0; y < size.y; ++y)
            {
                var newItem = AddAreaObject(new Vector2Int(position.x + x, position.y + y));

                if (!newItem)
                    continue;

                if (newItems.Contains(newItem))
                    continue;

                newItems.Add(newItem);
            }
        }

        return newItems;
    }

    /// <summary>
    /// 領域オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveAreaObject(AreaEditorObject item)
    {
        objectFactory.DestroyAreaObject(item);

        if (!areaObjects.Contains(item))
            return false;

        areaObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtAreaObject(Vector2Int position)
    {
        var targets = GetAtMyAreaObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveAreaObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeAreaObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyAreaObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveAreaObject(targets[i]);
        }
    }

    /// <summary>
    /// 全ての領域オブジェクトを取り除く
    /// </summary>
    public void RemoveAllAreaObject()
    {
        while (areaObjects.Count > 0)
            areaObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の領域オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<AreaEditorObject> GetMyAreaObjects()
    {
        return areaObjects;
    }

    /// <summary>
    /// 指定位置の自身の領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<AreaEditorObject> GetAtMyAreaObjects(Vector2Int position)
    {
        var targets = new List<AreaEditorObject>();
        var stageObjects = manager.GetStageObjects(gridPosition);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as AreaEditorObject;

            if (!target)
                continue;

            if (!areaObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身の領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<AreaEditorObject> GetRangeMyAreaObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<AreaEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(gridPosition, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as AreaEditorObject;

            if (!target)
                continue;

            if (!areaObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 領域オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAreaObjects()
    {
        return GetMyAreaObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtAreaObjects(Vector2Int position)
    {
        return GetAtMyAreaObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeAreaObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyAreaObjects(position, size).Count > 0;
    }

    #endregion

    #region 取っ手オブジェクト

    /// <summary>
    /// 取っ手が追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddHandleObject(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + HandleData.DEFAULT_GRID_SIZE.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + HandleData.DEFAULT_GRID_SIZE.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// 取っ手オブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="handleType"> 取っ手の種類 </param>
    /// <param name="areaType"> 領域の種類 </param>
    /// <returns> 追加したオブジェクト </returns>
    public HandleEditorObject AddHandleObject(Vector2Int position, HandleData.HandleType handleType, BlockData.BlockType areaType)
    {
        var newItem = objectFactory.InstantiateHandleObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.SetHandleType(handleType);
        newItem.SetAreaType(areaType);
        newItem.DestroyOther();

        handleObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 取っ手オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveHandleObject(HandleEditorObject item)
    {
        objectFactory.DestroyHandleObject(item);

        if (!handleObjects.Contains(item))
            return false;

        handleObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置の取っ手オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtHandleObject(Vector2Int position)
    {
        var targets = GetAtMyHandleObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveHandleObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置の取っ手オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeHandleObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyHandleObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveHandleObject(targets[i]);
        }
    }

    /// <summary>
    /// 全ての取っ手オブジェクトを取り除く
    /// </summary>
    public void RemoveAllHandleObject()
    {
        while (handleObjects.Count > 0)
            handleObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<HandleEditorObject> GetMyHandleObjects()
    {
        return handleObjects;
    }

    /// <summary>
    /// 指定位置の自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<HandleEditorObject> GetAtMyHandleObjects(Vector2Int position)
    {
        var targets = new List<HandleEditorObject>();
        var stageObjects = manager.GetStageObjects(gridPosition);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as HandleEditorObject;

            if (!target)
                continue;

            if (!handleObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<HandleEditorObject> GetRangeMyHandleObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<HandleEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(gridPosition, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as HandleEditorObject;

            if (!target)
                continue;

            if (!handleObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 取っ手オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasHandleObjects()
    {
        return GetMyHandleObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置の取っ手オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtHandleObjects(Vector2Int position)
    {
        return GetAtMyHandleObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置の取っ手オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeHandleObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyHandleObjects(position, size).Count > 0;
    }

    #endregion

    #region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return areaType;
    }

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    public void SetAreaType(BlockData.BlockType areaType)
    {
        this.areaType = areaType;

        ChangeObjectName();

        for (int i = 0; i < areaObjects.Count; ++i)
            areaObjects[i].SetAreaType(this.areaType);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.AreaLayer;
    }

    #endregion
}
