using System;
using System.Collections.Generic;

public class AreaLayerData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        MyAreaType,
        ChildDatas,
    }

    #endregion

    /// <summary> 領域データ群 </summary>
    List<AreaData> areaDatas = new List<AreaData>();

    /// <summary> 取っ手データ群 </summary>
    List<HandleData> handleDatas = new List<HandleData>();

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

    /// <summary> 領域データ群取得用 </summary>
    public List<AreaData> AreaDatas { get => areaDatas; }

    /// <summary> 取っ手データ群取得用 </summary>
    public List<HandleData> HandleDatas { get => handleDatas; }

    /// <summary> 領域の種類取得用 </summary>
    public BlockData.BlockType MyAreaType { get => myAreaType; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public AreaLayerData()
    {
        areaDatas.Clear();
        handleDatas.Clear();
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public AreaLayerData(EditorAreaLayer target)
    {
        myAreaType = target.GetAreaType();

        var areaObjects = target.GetMyAreaObjects();
        var handleObjects = target.GetMyHandleObjects();

        for (int i = 0; i < areaObjects.Count; i++)
            areaDatas.Add(new AreaData(areaObjects[i]));

        for (int i = 0; i < handleObjects.Count; i++)
            handleDatas.Add(new HandleData(handleObjects[i]));
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(EditorAreaLayer target)
    {
        target.SetAreaType(myAreaType);

        for (int i = 0; i < areaDatas.Count; ++i)
            target.AddAreaObject(areaDatas[i].GridPosition);

        for (int i = 0; i < handleDatas.Count; ++i)
        {
            var newItem = target.AddHandleObject(handleDatas[i].GridPosition, handleDatas[i].MyHandleType, handleDatas[i].MyAreaType);
            if (newItem)
                newItem.SetLinkGroupNumber(handleDatas[i].LinkGroupNumber);
        }
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        while (index < csvDatas.Count)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][0], out dataID))
                break;

            var dataIsNone = false;

            switch (dataID)
            {
                case DataID.MyAreaType:
                    myAreaType = Enum.Parse<BlockData.BlockType>(csvDatas[index][1]);
                    ++index;
                    break;
                case DataID.ChildDatas:
                    ++index;
                    while (index < csvDatas.Count)
                    {
                        StageData.ObjectType objectType;
                        if (!Enum.TryParse(csvDatas[index][0], out objectType))
                            break;

                        var isFailed = false;

                        switch (objectType)
                        {
                            case StageData.ObjectType.Area:
                                ++index;
                                {
                                    var newData = new AreaData();
                                    newData.SetData(csvDatas, ref index);
                                    areaDatas.Add(newData);
                                }
                                break;
                            case StageData.ObjectType.Handle:
                                ++index;
                                {
                                    var newData = new HandleData();
                                    newData.SetData(csvDatas, ref index);
                                    handleDatas.Add(newData);
                                }
                                break;
                            default:
                                isFailed = true;
                                break;
                        }

                        if (isFailed)
                            break;
                    }
                    break;
                default:
                    dataIsNone = true;
                    break;
            }

            if (dataIsNone)
                break;
        }
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.AreaLayer.ToString() });

        csvDatas.Add(new string[] { DataID.MyAreaType.ToString(), myAreaType.ToString() });

        csvDatas.Add(new string[1] { DataID.ChildDatas.ToString() });

        for (int i = 0; i < areaDatas.Count; ++i)
            areaDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < handleDatas.Count; ++i)
            handleDatas[i].GetData(ref csvDatas);
    }
}
