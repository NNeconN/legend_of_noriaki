#if true

using System.Collections.Generic;
using UnityEngine;

public class StageAreaLayer : StageObject
{
    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = new StageObject_Manager();

    /// <summary> 拡大縮小マネージャー </summary>
    ScaleChange_Manager scaleChangeManager = new ScaleChange_Manager();

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        var isFailed = !base.Initialize(stageManager, objectFactory);

        isFailed = isFailed || !objectManager.Initialize(this);
        isFailed = isFailed || !scaleChangeManager.Initialize(this);

        systemBitFlag.Pop(bitFlagEnable);

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        objectManager.UnInitialize();

        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager.RemoveAreaLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 成功したか </returns>
    public bool SetData(AreaLayerData data)
    {
        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        scaleChangeManager.MyAreaType = data.MyAreaType;

        CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        if (objectManager == null)
            return false;

        for (int i = 0; i < data.AreaDatas.Count; ++i)
            objectManager.AddAreaObject(data.AreaDatas[i]);

        for (int i = 0; i < data.HandleDatas.Count; ++i)
            objectManager.AddHandleObject(data.HandleDatas[i]);

        CreateSelectInformationAll();

        return true;
    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        objectManager.UpdateProcess();
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        objectManager.FixedUpdateProcess();
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        objectManager.LateUpdateProcess();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

    #endregion

    #region 領域オブジェクト

    /// <summary>
    /// 領域オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public AreaStageObject AddAreaObject(AreaData data)
    {
        return objectManager.AddAreaObject(data);
    }

    /// <summary>
    /// 領域オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveAreaObject(AreaStageObject item)
    {
        objectManager.RemoveAreaObject(item);
    }

    /// <summary>
    /// 全ての領域オブジェクトを取り除く
    /// </summary>
    public void RemoveAllAreaObject()
    {
        objectManager.RemoveAllAreaObject();
    }

    /// <summary>
    /// 自身の領域オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<AreaStageObject> GetMyAreaObjects()
    {
        return objectManager.GetMyAreaObjects();
    }

    /// <summary>
    /// 指定位置の自身の領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<AreaStageObject> GetAtMyAreaObjects(Vector2Int position)
    {
        return objectManager.GetAtMyAreaObjects(position);
    }

    /// <summary>
    /// 指定位置の自身の領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<AreaStageObject> GetRangeMyAreaObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyAreaObjects(position, size);
    }

    /// <summary>
    /// 領域オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAreaObjects()
    {
        return objectManager.CheckHasAreaObjects();
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtAreaObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtAreaObjects(position);
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeAreaObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeAreaObjects(position, size);
    }

    #endregion

    #region 取っ手オブジェクト

    /// <summary>
    /// 取っ手オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public HandleStageObject AddHandleObject(HandleData data)
    {
        return objectManager.AddHandleObject(data);
    }

    /// <summary>
    /// 取っ手オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveHandleObject(HandleStageObject item)
    {
        objectManager.RemoveHandleObject(item);
    }

    /// <summary>
    /// 全ての取っ手オブジェクトを取り除く
    /// </summary>
    public void RemoveAllHandleObject()
    {
        objectManager.RemoveAllHandleObject();
    }

    /// <summary>
    /// 自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<HandleStageObject> GetMyHandleObjects()
    {
        return objectManager.GetMyHandleObjects();
    }

    /// <summary>
    /// 指定位置の自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<HandleStageObject> GetAtMyHandleObjects(Vector2Int position)
    {
        return objectManager.GetAtMyHandleObjects(position);
    }

    /// <summary>
    /// 指定位置の自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<HandleStageObject> GetRangeMyHandleObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyHandleObjects(position, size);
    }

    /// <summary>
    /// 取っ手オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasHandleObjects()
    {
        return objectManager.CheckHasHandleObjects();
    }

    /// <summary>
    /// 指定位置の取っ手オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtHandleObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtHandleObjects(position);
    }

    /// <summary>
    /// 指定位置の取っ手オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeHandleObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeHandleObjects(position, size);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.AreaLayer;
    }

    /// <summary>
    /// 領域の種類の取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return scaleChangeManager.MyAreaType;
    }

    #endregion

    #region 選択関係

    public void CreateSelectInformationAll()
    {
        CreateCenter();
        CreateHandleType();
        CreateAreaGroup();
        CreateHandleGroup();
    }

    /// <summary>
    /// 中心位置生成
    /// </summary>
    public void CreateCenter()
    {
        scaleChangeManager.CreateCenter();
    }

    /// <summary>
    /// 取っ手の種類を生成する
    /// </summary>
    public void CreateHandleType()
    {
        scaleChangeManager.CreateHandleType();
    }

    /// <summary>
    /// 領域グループを生成
    /// </summary>
    public void CreateAreaGroup()
    {
        scaleChangeManager.CreateAreaGroup();
    }

    /// <summary>
    /// 取っ手グループを生成
    /// </summary>
    public void CreateHandleGroup()
    {
        scaleChangeManager.CreateHandleGroup();
    }

    /// <summary>
    /// 選択する
    /// </summary>
    /// <param name="information"> 選択情報 </param>
    public void Select(Stage_Manager.ScaleChange_Manager.SelectInformation information)
    {
        scaleChangeManager.Select(information);
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    /// <param name="information"> 選択情報 </param>
    public void UnSelect(Stage_Manager.ScaleChange_Manager.SelectInformation information)
    {
        scaleChangeManager.UnSelect(information);
    }

    #endregion

    #region 拡大縮小チェック

    /// <summary>
    /// 移動予定状態にする
    /// </summary>
    /// <param name="moveDirection"> 移動方向 </param>
    /// <param name="information"> 選択情報 </param>
    public void MoveLock(StageData.GridDirection moveDirection, Stage_Manager.ScaleChange_Manager.SelectInformation information)
    {
        scaleChangeManager.MoveLock(moveDirection, information);
    }

    /// <summary>
    /// 掴みロック状態にする
    /// </summary>
    public void GrabLock()
    {
        scaleChangeManager.GrabLock();
    }

    /// <summary>
    /// ロックを外す
    /// </summary>
    public void UnLock()
    {
        scaleChangeManager.UnLock();
    }

    /// <summary>
    /// 縮小可能か
    /// </summary>
    /// <param name="checkDistance"> 縮小距離 </param>
    /// <returns> 縮小可能か </returns>
    public int CheckCanScaleDown(int checkDistance)
    {
        return scaleChangeManager.CheckCanScaleDown(checkDistance);
    }

    #endregion

    #region 拡大縮小関係

    /// <summary>
    /// 拡大縮小を始める
    /// </summary>
    public void ScaleChangeStartProcess()
    {
        scaleChangeManager.StartProcess();
    }

    /// <summary>
    /// 拡大終了の終了
    /// </summary>
    public void ScaleChangeFinishProcess()
    {
        scaleChangeManager.FinishProcess();
    }

    /// <summary>
    /// 拡大縮小更新
    /// </summary>
    /// <param name="isFailed"> 失敗状態か </param>
    /// <param name="processRate"> 移動割合 </param>
    /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
    /// <param name="distance"> 拡大縮小距離 </param>
    public void ScaleChangeUpdateProcess(bool isFailed, float processRate, int currentDistance, int distance)
    {
        scaleChangeManager.UpdateProcess(isFailed, processRate, currentDistance, distance);
    }

    #endregion

    #region 子クラス

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> 領域オブジェクト </summary>
        public List<AreaStageObject> areaObjects = new List<AreaStageObject>();

        /// <summary> 取っ手オブジェクト群 </summary>
        public List<HandleStageObject> handleObjects = new List<HandleStageObject>();
    }

    /// <summary>
    /// 拡大縮小マネージャー
    /// </summary>
    public class ScaleChange_Manager
    {
        /// <summary> 領域レイヤー </summary>
        StageAreaLayer areaLayer = null;

        /// <summary> 選択情報 </summary>
        SelectInformation selectInformation = new SelectInformation();

        /// <summary> 拡大縮小情報 </summary>
        ScaleChangeInformation scaleChangeInformation = new ScaleChangeInformation();

        /// <summary> 領域の種類 </summary>
        BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

        /// <summary> 中心位置 </summary>
        Vector2 center = Vector2.zero;

        /// <summary> 領域の種類 設定・取得用 </summary>
        public BlockData.BlockType MyAreaType { get => myAreaType; set => myAreaType = value; }

        #region 初期化・解放処理

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <param name="areaLayer"> 領域レイヤー </param>
        /// <returns> 成功した </returns>
        public bool Initialize(StageAreaLayer areaLayer)
        {
            if (areaLayer)
                this.areaLayer = areaLayer;

            if (!this.areaLayer)
                return false;

            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            selectInformation.leftGroupAreaObjects.Clear();
            selectInformation.rightGroupAreaObjects.Clear();
            selectInformation.upGroupAreaObjects.Clear();
            selectInformation.downGroupAreaObjects.Clear();

            selectInformation.leftGroupHandleObjects.Clear();
            selectInformation.rightGroupHandleObjects.Clear();
            selectInformation.upGroupHandleObjects.Clear();
            selectInformation.downGroupHandleObjects.Clear();

            scaleChangeInformation.lockedAreaObjects.Clear();
            scaleChangeInformation.newAreaObjects.Clear();
            scaleChangeInformation.lockedHandleObjects.Clear();
        }

        #endregion

        #region 選択関係

        /// <summary>
        /// 中心位置生成
        /// </summary>
        public void CreateCenter()
        {
            Vector2 min = areaLayer.stageManager.GetGridSize();
            Vector2 max = Vector2.zero;

            var areaObjects = areaLayer.objectManager.GetMyAreaObjects();


            for (int i = 0; i < areaObjects.Count; ++i)
            {
                var leftBottom = areaObjects[i].GetGridPosition();
                var rightTop = leftBottom + areaObjects[i].GetGridSize() - Vector2Int.one;

                if (min.x > leftBottom.x) min.x = leftBottom.x;
                if (min.y > leftBottom.y) min.y = leftBottom.y;
                if (max.x < rightTop.x) max.x = rightTop.x;
                if (max.y < rightTop.y) max.y = rightTop.y;
            }

            min.x += 0.5f;
            min.y += 0.5f;
            max.x += 0.5f;
            max.y += 0.5f;

            center = (min + max) * 0.5f;
        }

        /// <summary>
        /// 取っ手の種類を生成する
        /// </summary>
        public void CreateHandleType()
        {
            var objectManager = areaLayer.objectManager;
            var handleObjects = objectManager.GetMyHandleObjects();
            var centerX = areaLayer.stageManager.GetLeftPivotX() + center.x;
            var centerY = areaLayer.stageManager.GetBottomPivotY() + center.y;

            Debug.Log("取っ手タイプ : " + centerX + " : " + centerY);

            for (int i = 0; i < handleObjects.Count; ++i)
            {
                if (HandleData.CheckHorizontalHandleType(handleObjects[i].GetHandleType()))
                {
                    if (handleObjects[i].transform.position.x < centerX)
                        handleObjects[i].SetHandleType(HandleData.HandleType.LeftHorizontal);
                    else if (handleObjects[i].transform.position.x > centerX)
                        handleObjects[i].SetHandleType(HandleData.HandleType.RightHorizontal);
                    else
                        handleObjects[i].SetHandleType(HandleData.HandleType.MiddleHorizontal);
                }
                else
                {
                    if (handleObjects[i].transform.position.y < centerY)
                        handleObjects[i].SetHandleType(HandleData.HandleType.BottomVertical);
                    else if (handleObjects[i].transform.position.y > centerY)
                        handleObjects[i].SetHandleType(HandleData.HandleType.TopVertical);
                    else
                        handleObjects[i].SetHandleType(HandleData.HandleType.MiddleVertical);
                }
            }
        }

        /// <summary>
        /// 領域グループを生成
        /// </summary>
        public void CreateAreaGroup()
        {
            selectInformation.leftGroupAreaObjects.Clear();
            selectInformation.rightGroupAreaObjects.Clear();
            selectInformation.upGroupAreaObjects.Clear();
            selectInformation.downGroupAreaObjects.Clear();

            var objectManager = areaLayer.objectManager;
            var areaObjects = objectManager.GetMyAreaObjects();

            for (int i = 0; i < areaObjects.Count; ++i)
            {
                var gridPosition = areaObjects[i].GetGridPosition();
                var left = new Vector2Int(gridPosition.x - 1, gridPosition.y);
                var right = new Vector2Int(gridPosition.x + 1, gridPosition.y);
                var up = new Vector2Int(gridPosition.x, gridPosition.y + 1);
                var down = new Vector2Int(gridPosition.x, gridPosition.y - 1);

                if (!objectManager.CheckHasAtAreaObjects(left))
                    selectInformation.leftGroupAreaObjects.Add(areaObjects[i]);

                if (!objectManager.CheckHasAtAreaObjects(right))
                    selectInformation.rightGroupAreaObjects.Add(areaObjects[i]);

                if (!objectManager.CheckHasAtAreaObjects(up))
                    selectInformation.upGroupAreaObjects.Add(areaObjects[i]);

                if (!objectManager.CheckHasAtAreaObjects(down))
                    selectInformation.downGroupAreaObjects.Add(areaObjects[i]);
            }
        }

        /// <summary>
        /// 取っ手グループを生成
        /// </summary>
        public void CreateHandleGroup()
        {
            selectInformation.leftGroupHandleObjects.Clear();
            selectInformation.rightGroupHandleObjects.Clear();
            selectInformation.upGroupHandleObjects.Clear();
            selectInformation.downGroupHandleObjects.Clear();

            var objectManager = areaLayer.objectManager;
            var handleObjects = objectManager.GetMyHandleObjects();

            for (int i = 0; i < handleObjects.Count; ++i)
            {
                if (handleObjects[i].transform.position.x < center.x)
                    selectInformation.leftGroupHandleObjects.Add(handleObjects[i]);
                else if (handleObjects[i].transform.position.x > center.x)
                    selectInformation.rightGroupHandleObjects.Add(handleObjects[i]);
                else
                {
                    selectInformation.leftGroupHandleObjects.Add(handleObjects[i]);
                    selectInformation.rightGroupHandleObjects.Add(handleObjects[i]);
                }

                if (handleObjects[i].transform.position.y < center.y)
                    selectInformation.downGroupHandleObjects.Add(handleObjects[i]);
                else if (handleObjects[i].transform.position.y > center.y)
                    selectInformation.upGroupHandleObjects.Add(handleObjects[i]);
                else
                {
                    selectInformation.downGroupHandleObjects.Add(handleObjects[i]);
                    selectInformation.upGroupHandleObjects.Add(handleObjects[i]);
                }
            }
        }

        /// <summary>
        /// 選択する
        /// </summary>
        /// <param name="information"> 選択情報 </param>
        public void Select(Stage_Manager.ScaleChange_Manager.SelectInformation information)
        {
            List<HandleStageObject> targetHandles;

            switch (myAreaType)
            {
                default:
                    return;
                case BlockData.BlockType.Red:
                    targetHandles = information.redHandleType;
                    break;
                case BlockData.BlockType.Green:
                    targetHandles = information.greenHandleType;
                    break;
                case BlockData.BlockType.Blue:
                    targetHandles = information.blueHandleType;
                    break;
            }

            for (int i = 0; i < targetHandles.Count; ++i)
            {
                if (!targetHandles[i].CheckParent(areaLayer))
                    continue;

                var handleType = targetHandles[i].GetHandleType();

                switch (handleType)
                {
                    case HandleData.HandleType.LeftHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupAreaObjects.Count; ++j)
                            selectInformation.leftGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.leftGroupHandleObjects.Count; ++j)
                            selectInformation.leftGroupHandleObjects[j].Select();
                        break;
                    case HandleData.HandleType.MiddleHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupAreaObjects.Count; ++j)
                            selectInformation.leftGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.rightGroupAreaObjects.Count; ++j)
                            selectInformation.rightGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.leftGroupHandleObjects.Count; ++j)
                            selectInformation.leftGroupHandleObjects[j].Select();

                        for (int j = 0; j < selectInformation.rightGroupHandleObjects.Count; ++j)
                            selectInformation.rightGroupHandleObjects[j].Select();
                        break;
                    case HandleData.HandleType.RightHorizontal:
                        for (int j = 0; j < selectInformation.rightGroupAreaObjects.Count; ++j)
                            selectInformation.rightGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.rightGroupHandleObjects.Count; ++j)
                            selectInformation.rightGroupHandleObjects[j].Select();
                        break;
                    case HandleData.HandleType.BottomVertical:
                        for (int j = 0; j < selectInformation.downGroupAreaObjects.Count; ++j)
                            selectInformation.downGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.downGroupHandleObjects.Count; ++j)
                            selectInformation.downGroupHandleObjects[j].Select();
                        break;
                    case HandleData.HandleType.MiddleVertical:
                        for (int j = 0; j < selectInformation.downGroupAreaObjects.Count; ++j)
                            selectInformation.downGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.upGroupAreaObjects.Count; ++j)
                            selectInformation.upGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.downGroupHandleObjects.Count; ++j)
                            selectInformation.downGroupHandleObjects[j].Select();

                        for (int j = 0; j < selectInformation.upGroupHandleObjects.Count; ++j)
                            selectInformation.upGroupHandleObjects[j].Select();
                        break;
                    case HandleData.HandleType.TopVertical:
                        for (int j = 0; j < selectInformation.upGroupAreaObjects.Count; ++j)
                            selectInformation.upGroupAreaObjects[j].Select();

                        for (int j = 0; j < selectInformation.upGroupHandleObjects.Count; ++j)
                            selectInformation.upGroupHandleObjects[j].Select();
                        break;
                }
            }
        }

        /// <summary>
        /// 選択しない
        /// </summary>
        /// <param name="information"> 選択情報 </param>
        public void UnSelect(Stage_Manager.ScaleChange_Manager.SelectInformation information)
        {
            List<HandleStageObject> targetHandles;

            switch (myAreaType)
            {
                default:
                    return;
                case BlockData.BlockType.Red:
                    targetHandles = information.redHandleType;
                    break;
                case BlockData.BlockType.Green:
                    targetHandles = information.greenHandleType;
                    break;
                case BlockData.BlockType.Blue:
                    targetHandles = information.blueHandleType;
                    break;
            }

            for (int i = 0; i < targetHandles.Count; ++i)
            {
                if (!targetHandles[i].CheckParent(areaLayer))
                    continue;

                var handleType = targetHandles[i].GetHandleType();

                switch (handleType)
                {
                    case HandleData.HandleType.LeftHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupAreaObjects.Count; ++j)
                            selectInformation.leftGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.leftGroupHandleObjects.Count; ++j)
                            selectInformation.leftGroupHandleObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.MiddleHorizontal:
                        for (int j = 0; j < selectInformation.leftGroupAreaObjects.Count; ++j)
                            selectInformation.leftGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.rightGroupAreaObjects.Count; ++j)
                            selectInformation.rightGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.leftGroupHandleObjects.Count; ++j)
                            selectInformation.leftGroupHandleObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.rightGroupHandleObjects.Count; ++j)
                            selectInformation.rightGroupHandleObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.RightHorizontal:
                        for (int j = 0; j < selectInformation.rightGroupAreaObjects.Count; ++j)
                            selectInformation.rightGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.rightGroupHandleObjects.Count; ++j)
                            selectInformation.rightGroupHandleObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.BottomVertical:
                        for (int j = 0; j < selectInformation.downGroupAreaObjects.Count; ++j)
                            selectInformation.downGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.downGroupHandleObjects.Count; ++j)
                            selectInformation.downGroupHandleObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.MiddleVertical:
                        for (int j = 0; j < selectInformation.downGroupAreaObjects.Count; ++j)
                            selectInformation.downGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.upGroupAreaObjects.Count; ++j)
                            selectInformation.upGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.downGroupHandleObjects.Count; ++j)
                            selectInformation.downGroupHandleObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.upGroupHandleObjects.Count; ++j)
                            selectInformation.upGroupHandleObjects[j].UnSelect();
                        break;
                    case HandleData.HandleType.TopVertical:
                        for (int j = 0; j < selectInformation.upGroupAreaObjects.Count; ++j)
                            selectInformation.upGroupAreaObjects[j].UnSelect();

                        for (int j = 0; j < selectInformation.upGroupHandleObjects.Count; ++j)
                            selectInformation.upGroupHandleObjects[j].UnSelect();
                        break;
                }
            }
        }

        #endregion

        #region 拡大縮小チェック

        /// <summary>
        /// 拡大フラグを設定し移動予定にする
        /// </summary>
        /// <typeparam name="T"> 対象ステージオブジェクト </typeparam>
        /// <param name="locked"> フラグ設定後に登録するリスト </param>
        /// <param name="targets"> 対象 </param>
        void SetFlagScaleUpAndMoveLock<T>(ref List<T> locked, List<T> targets) where T : StageObject
        {
            for (int j = 0; j < targets.Count; ++j)
            {
                targets[j].SetFlagScaleUp(true);

                if (locked.Contains(targets[j]))
                    continue;

                targets[j].SetFlagMoveLock(true);
                locked.Add(targets[j]);
            }
        }

        /// <summary>
        /// 縮小フラグを設定し移動予定にする
        /// </summary>
        /// <typeparam name="T"> 対象ステージオブジェクト </typeparam>
        /// <param name="locked"> フラグ設定後に登録するリスト </param>
        /// <param name="targets"> 対象 </param>
        void SetFlagScaleDownAndMoveLock<T>(ref List<T> locked, List<T> targets) where T : StageObject
        {
            for (int j = 0; j < targets.Count; ++j)
            {
                targets[j].SetFlagScaleDown(true);

                if (locked.Contains(targets[j]))
                    continue;

                targets[j].SetFlagMoveLock(true);
                locked.Add(targets[j]);
            }
        }

        /// <summary>
        /// 移動予定状態にする
        /// </summary>
        /// <param name="moveDirection"> 移動方向 </param>
        /// <param name="information"> 選択情報 </param>
        public void MoveLock(StageData.GridDirection moveDirection, Stage_Manager.ScaleChange_Manager.SelectInformation information)
        {
            UnLock();

            List<HandleStageObject> targetHandles;

            switch (myAreaType)
            {
                default:
                    return;
                case BlockData.BlockType.Red:
                    targetHandles = information.redHandleType;
                    break;
                case BlockData.BlockType.Green:
                    targetHandles = information.greenHandleType;
                    break;
                case BlockData.BlockType.Blue:
                    targetHandles = information.blueHandleType;
                    break;
            }

            scaleChangeInformation.moveDirection = moveDirection;

            for (int i = 0; i < targetHandles.Count; ++i)
            {
                if (!targetHandles[i].CheckParent(areaLayer))
                    continue;

                var handleType = targetHandles[i].GetHandleType();

                switch (moveDirection)
                {
                    case StageData.GridDirection.Left:
                        switch (handleType)
                        {
                            case HandleData.HandleType.LeftHorizontal:
                            case HandleData.HandleType.MiddleHorizontal:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.leftGroupAreaObjects);
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.leftGroupHandleObjects);
                                break;
                            case HandleData.HandleType.RightHorizontal:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.rightGroupAreaObjects);
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.rightGroupHandleObjects);
                                break;
                        }
                        break;
                    case StageData.GridDirection.Right:
                        switch (handleType)
                        {
                            case HandleData.HandleType.RightHorizontal:
                            case HandleData.HandleType.MiddleHorizontal:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.rightGroupAreaObjects);
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.rightGroupHandleObjects);
                                break;
                            case HandleData.HandleType.LeftHorizontal:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.leftGroupAreaObjects);
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.leftGroupHandleObjects);
                                break;
                        }
                        break;
                    case StageData.GridDirection.Up:
                        switch (handleType)
                        {
                            case HandleData.HandleType.TopVertical:
                            case HandleData.HandleType.MiddleVertical:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.upGroupAreaObjects);
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.upGroupHandleObjects);
                                break;
                            case HandleData.HandleType.BottomVertical:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.downGroupAreaObjects);
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.downGroupHandleObjects);
                                break;
                        }
                        break;
                    case StageData.GridDirection.Down:
                        switch (handleType)
                        {
                            case HandleData.HandleType.BottomVertical:
                            case HandleData.HandleType.MiddleVertical:
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.downGroupAreaObjects);
                                SetFlagScaleUpAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.downGroupHandleObjects);
                                break;
                            case HandleData.HandleType.TopVertical:
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedAreaObjects, selectInformation.upGroupAreaObjects);
                                SetFlagScaleDownAndMoveLock(ref scaleChangeInformation.lockedHandleObjects, selectInformation.upGroupHandleObjects);
                                break;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// 掴みロック状態にする
        /// </summary>
        public void GrabLock()
        {
            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
                scaleChangeInformation.lockedAreaObjects[i].SetFlagGrabLock(true);

            for (int i = 0; i < scaleChangeInformation.lockedHandleObjects.Count; ++i)
                scaleChangeInformation.lockedHandleObjects[i].SetFlagGrabLock(true);
        }

        /// <summary>
        /// ロックを外す
        /// </summary>
        public void UnLock()
        {
            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                scaleChangeInformation.lockedAreaObjects[i].SetFlagGrabLock(false);
                scaleChangeInformation.lockedAreaObjects[i].SetFlagMoveLock(false);
                scaleChangeInformation.lockedAreaObjects[i].SetFlagScaleUp(false);
                scaleChangeInformation.lockedAreaObjects[i].SetFlagScaleDown(false);
            }

            for (int i = 0; i < scaleChangeInformation.lockedHandleObjects.Count; ++i)
            {
                scaleChangeInformation.lockedHandleObjects[i].SetFlagGrabLock(false);
                scaleChangeInformation.lockedHandleObjects[i].SetFlagMoveLock(false);
                scaleChangeInformation.lockedHandleObjects[i].SetFlagScaleUp(false);
                scaleChangeInformation.lockedHandleObjects[i].SetFlagScaleDown(false);
            }

            scaleChangeInformation.lockedAreaObjects.Clear();
            scaleChangeInformation.lockedHandleObjects.Clear();
        }

        /// <summary>
        /// 縮小可能か
        /// </summary>
        /// <param name="checkDistance"> 縮小距離 </param>
        /// <returns> 縮小可能か </returns>
        public int CheckCanScaleDown(int checkDistance)
        {
            var distanceDirection = Vector2Int.zero;

            switch (scaleChangeInformation.moveDirection)
            {
                case StageData.GridDirection.Left:
                    distanceDirection.x = -1;
                    break;
                case StageData.GridDirection.Right:
                    distanceDirection.x = 1;
                    break;
                case StageData.GridDirection.Up:
                    distanceDirection.y = 1;
                    break;
                case StageData.GridDirection.Down:
                    distanceDirection.y = -1;
                    break;
            }

            int distance = checkDistance;

            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedAreaObjects[i].GetGridPosition();

                var isScaleUp = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleDown();

                if (isScaleUp || !isScaleDown)
                    continue;

                var tempDistance = distance;

                for (int j = 0; j < checkDistance; ++j)
                {
                    var targetPosition = gridPosition + (distanceDirection * (j + 1));

                    var myObjects = areaLayer.GetAtMyAreaObjects(targetPosition);
                    var isSlide = false;

                    if (myObjects.Count > 0)
                    {
                        for (int k = 0; k < myObjects.Count; ++k)
                        {
                            if (!myObjects[k].GetFlagScaleUp())
                                continue;

                            isSlide = true;
                            break;
                        }

                        if (isSlide)
                        {
                            distance = tempDistance;
                            break;
                        }

                        continue;
                    }

                    distance = j < distance ? j : distance;
                }
            }

            return distance;
        }

        #endregion

        #region 拡大縮小関係

        /// <summary>
        /// 拡大縮小を始める
        /// </summary>
        public void StartProcess()
        {
            scaleChangeInformation.distance = 0;

            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                var isScaleUp = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleDown();

                if (isScaleUp && !isScaleDown)
                {
                    var targetGridPosition = scaleChangeInformation.lockedAreaObjects[i].GetGridPosition();

                    AreaData areaData = new AreaData();
                    areaData.GridPosition = targetGridPosition;
                    areaData.MyAreaType = myAreaType;

                    var newItem = areaLayer.AddAreaObject(areaData);
                    if (!newItem)
                        continue;

                    if (!scaleChangeInformation.newAreaObjects.Contains(newItem))
                        scaleChangeInformation.newAreaObjects.Add(newItem);
                }
            }
        }

        /// <summary>
        /// 拡大終了の終了
        /// </summary>
        public void FinishProcess()
        {
            scaleChangeInformation.newAreaObjects.Clear();

            for (int i = 0; i < scaleChangeInformation.lockedHandleObjects.Count; ++i)
            {
                var rect = scaleChangeInformation.lockedHandleObjects[i].GetGridColliderRect();
                scaleChangeInformation.lockedHandleObjects[i].CalculateGridPosition(rect.x, rect.y);
                scaleChangeInformation.lockedHandleObjects[i].CalculatePosition(scaleChangeInformation.lockedHandleObjects[i].GetGridPosition());
            }

            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                var rect = scaleChangeInformation.lockedAreaObjects[i].GetGridColliderRect();
                scaleChangeInformation.lockedAreaObjects[i].CalculateGridPosition(rect.x, rect.y);
                scaleChangeInformation.lockedAreaObjects[i].CalculatePosition(scaleChangeInformation.lockedAreaObjects[i].GetGridPosition());
            }
        }

        /// <summary>
        /// 拡大縮小更新
        /// </summary>
        /// <param name="isFailed"> 失敗状態か </param>
        /// <param name="processRate"> 移動割合 </param>
        /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        public void UpdateProcess(bool isFailed, float processRate, int currentDistance, int distance)
        {
            var distanceDirection = Vector2Int.zero;

            switch (scaleChangeInformation.moveDirection)
            {
                case StageData.GridDirection.Left:
                    distanceDirection.x = -1;
                    break;
                case StageData.GridDirection.Right:
                    distanceDirection.x = 1;
                    break;
                case StageData.GridDirection.Up:
                    distanceDirection.y = 1;
                    break;
                case StageData.GridDirection.Down:
                    distanceDirection.y = -1;
                    break;
            }

            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedAreaObjects[i].GetGridPosition();
                var nextGridPosition = gridPosition;

                nextGridPosition += distanceDirection * distance;

                var prevPosition = scaleChangeInformation.lockedAreaObjects[i].GetCalculatePosition(gridPosition);
                var nextPosition = scaleChangeInformation.lockedAreaObjects[i].GetCalculatePosition(nextGridPosition);

                var position = scaleChangeInformation.lockedAreaObjects[i].GetCalculatePosition(gridPosition);

                prevPosition.z = position.z - 0.0005f;
                nextPosition.z = position.z - 0.0005f;

                scaleChangeInformation.lockedAreaObjects[i].transform.position = Vector3.Lerp(prevPosition, nextPosition, processRate);

                scaleChangeInformation.lockedAreaObjects[i].CheckFailedScaleChange();
            }

            for (int i = 0; i < scaleChangeInformation.lockedHandleObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedHandleObjects[i].GetGridPosition();
                var nextGridPosition = gridPosition;

                nextGridPosition += distanceDirection * distance;

                var prevPosition = scaleChangeInformation.lockedHandleObjects[i].GetCalculatePosition(gridPosition);
                var nextPosition = scaleChangeInformation.lockedHandleObjects[i].GetCalculatePosition(nextGridPosition);

                var position = scaleChangeInformation.lockedHandleObjects[i].GetCalculatePosition(gridPosition);

                prevPosition.z = position.z - 0.0005f;
                nextPosition.z = position.z - 0.0005f;

                scaleChangeInformation.lockedHandleObjects[i].transform.position = Vector3.Lerp(prevPosition, nextPosition, processRate);

                scaleChangeInformation.lockedHandleObjects[i].CheckFailedScaleChange();
            }

            if (isFailed)
                FailedUpdateProcess(processRate, currentDistance, distance, distanceDirection);
            else
                SuccessUpdateProcess(processRate, currentDistance, distance, distanceDirection);

            scaleChangeInformation.distance = currentDistance;
        }

        /// <summary>
        /// 成功時の拡大縮小更新
        /// </summary>
        /// <param name="processRate"> 移動割合 </param>
        /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        /// <param name="distanceDirection"> 判定方向 </param>
        void SuccessUpdateProcess(float processRate, int currentDistance, int distance, Vector2Int distanceDirection)
        {
            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedAreaObjects[i].GetGridPosition();

                var isScaleUp = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleDown();

                if (isScaleUp && !isScaleDown && scaleChangeInformation.distance + 1 < distance)
                {
                    for (int j = scaleChangeInformation.distance + 1; j <= currentDistance; ++j)
                    {
                        var targetGridPosition = gridPosition;
                        targetGridPosition += distanceDirection * j;

                        AreaData areaData = new AreaData();
                        areaData.GridPosition = targetGridPosition;
                        areaData.MyAreaType = myAreaType;

                        var newItem = areaLayer.AddAreaObject(areaData);
                        if (!newItem)
                            continue;

                        if (!scaleChangeInformation.newAreaObjects.Contains(newItem))
                            scaleChangeInformation.newAreaObjects.Add(newItem);
                    }
                }
                else if (!isScaleUp && isScaleDown)
                {
                    for (int j = scaleChangeInformation.distance + 1; j <= currentDistance; ++j)
                    {
                        var targetGridPosition = gridPosition;
                        targetGridPosition += distanceDirection * j;

                        var stageObjects = areaLayer.stageManager.GetAtGrid(targetGridPosition);
                        for (int k = 0; k < stageObjects.Count; ++k)
                        {
                            if (stageObjects[k].GetObjectType() != StageData.ObjectType.Area)
                                continue;

                            var targetArea = stageObjects[k] as AreaStageObject;
                            if (!targetArea)
                                continue;

                            if (!targetArea.CheckParent(areaLayer))
                                continue;

                            targetArea.DestroyThis();

                            if (scaleChangeInformation.newAreaObjects.Contains(targetArea))
                                scaleChangeInformation.newAreaObjects.Remove(targetArea);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 失敗時の拡大縮小更新
        /// </summary>
        /// <param name="processRate"> 移動割合 </param>
        /// <param name="currentDistance"> 現在の拡大縮小距離 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        /// <param name="distanceDirection"> 判定方向 </param>
        void FailedUpdateProcess(float processRate, int currentDistance, int distance, Vector2Int distanceDirection)
        {
            for (int i = 0; i < scaleChangeInformation.lockedAreaObjects.Count; ++i)
            {
                var gridPosition = scaleChangeInformation.lockedAreaObjects[i].GetGridPosition();

                var isScaleUp = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleUp();
                var isScaleDown = scaleChangeInformation.lockedAreaObjects[i].GetFlagScaleDown();

                if (isScaleUp && !isScaleDown)
                {
                    for (int j = currentDistance; j <= scaleChangeInformation.distance; ++j)
                    {
                        var targetGridPosition = gridPosition;
                        targetGridPosition += distanceDirection * j;

                        var stageObjects = areaLayer.stageManager.GetAtGrid(targetGridPosition);
                        for (int k = 0; k < stageObjects.Count; ++k)
                        {
                            if (stageObjects[k].GetObjectType() != StageData.ObjectType.Area)
                                continue;

                            var targetArea = stageObjects[k] as AreaStageObject;
                            if (!targetArea)
                                continue;

                            if (!scaleChangeInformation.newAreaObjects.Contains(targetArea))
                                continue;

                            stageObjects[k].DestroyThis();

                            scaleChangeInformation.newAreaObjects.Remove(targetArea);
                        }
                    }
                }
            }
        }

        #endregion

        #region 子クラス

        /// <summary>
        /// 選択情報
        /// </summary>
        public class SelectInformation
        {
            /// <summary> 左側領域オブジェクト </summary>
            public List<AreaStageObject> leftGroupAreaObjects = new List<AreaStageObject>();

            /// <summary> 右側領域オブジェクト </summary>
            public List<AreaStageObject> rightGroupAreaObjects = new List<AreaStageObject>();

            /// <summary> 上側領域オブジェクト </summary>
            public List<AreaStageObject> upGroupAreaObjects = new List<AreaStageObject>();

            /// <summary> 下側領域オブジェクト </summary>
            public List<AreaStageObject> downGroupAreaObjects = new List<AreaStageObject>();

            /// <summary> 左側取っ手オブジェクト </summary>
            public List<HandleStageObject> leftGroupHandleObjects = new List<HandleStageObject>();

            /// <summary> 右側取っ手オブジェクト </summary>
            public List<HandleStageObject> rightGroupHandleObjects = new List<HandleStageObject>();

            /// <summary> 上側取っ手オブジェクト </summary>
            public List<HandleStageObject> upGroupHandleObjects = new List<HandleStageObject>();

            /// <summary> 下側取っ手オブジェクト </summary>
            public List<HandleStageObject> downGroupHandleObjects = new List<HandleStageObject>();
        }

        /// <summary>
        /// 拡大縮小情報
        /// </summary>
        public class ScaleChangeInformation
        {
            /// <summary> 拡大縮小距離 </summary>
            public int distance = 0;

            /// <summary> 移動方向 </summary>
            public StageData.GridDirection moveDirection = StageData.GridDirection.None;

            /// <summary> ロック中の領域群 </summary>
            public List<AreaStageObject> lockedAreaObjects = new List<AreaStageObject>();

            /// <summary> 新しい領域群 </summary>
            public List<AreaStageObject> newAreaObjects = new List<AreaStageObject>();

            /// <summary> ロック中の取っ手群 </summary>
            public List<HandleStageObject> lockedHandleObjects = new List<HandleStageObject>();
        }

        #endregion
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> 領域レイヤー </summary>
        StageAreaLayer areaLayer = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = new StageObjects();

        #region 初期化・解放処理関係

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <param name="areaLayer"> 領域レイヤー </param>
        /// <returns> 成功した </returns>
        public bool Initialize(StageAreaLayer areaLayer)
        {
            if (areaLayer)
                this.areaLayer = areaLayer;

            if (!this.areaLayer)
                return false;

            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAllAreaObject();
            RemoveAllHandleObject();
        }

        #endregion

        #region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.areaObjects.Count; ++i)
                stageObjects.areaObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.handleObjects.Count; ++i)
                stageObjects.handleObjects[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.areaObjects.Count; ++i)
                stageObjects.areaObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.handleObjects.Count; ++i)
                stageObjects.handleObjects[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.areaObjects.Count; ++i)
                stageObjects.areaObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.handleObjects.Count; ++i)
                stageObjects.handleObjects[i].LateUpdateProcess();
        }

        #endregion

        #region 領域オブジェクト

        /// <summary>
        /// 領域オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public AreaStageObject AddAreaObject(AreaData data)
        {
            var stageManager = areaLayer.stageManager;
            var objectFactory = areaLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateAreaObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, areaLayer))
            {
                objectFactory.DestroyAreaObject(newItem);

                return null;
            }

            if (!stageObjects.areaObjects.Contains(newItem))
                stageObjects.areaObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 領域オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveAreaObject(AreaStageObject item)
        {
            var objectFactory = areaLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.areaObjects.Contains(item))
                stageObjects.areaObjects.Remove(item);

            objectFactory.DestroyAreaObject(item);
        }

        /// <summary>
        /// 全ての領域オブジェクトを取り除く
        /// </summary>
        public void RemoveAllAreaObject()
        {
            while (stageObjects.areaObjects.Count > 0)
                stageObjects.areaObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身の領域オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<AreaStageObject> GetMyAreaObjects()
        {
            return stageObjects.areaObjects;
        }

        /// <summary>
        /// 指定位置の自身の領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<AreaStageObject> GetAtMyAreaObjects(Vector2Int position)
        {
            var targets = new List<AreaStageObject>();
            var stageObjects = areaLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as AreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.areaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<AreaStageObject> GetRangeMyAreaObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<AreaStageObject>();
            var stageObjects = areaLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as AreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.areaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 領域オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAreaObjects()
        {
            return GetMyAreaObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置の領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtAreaObjects(Vector2Int position)
        {
            return GetAtMyAreaObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeAreaObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyAreaObjects(position, size).Count > 0;
        }

        #endregion

        #region 取っ手オブジェクト

        /// <summary>
        /// 取っ手オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public HandleStageObject AddHandleObject(HandleData data)
        {
            var stageManager = areaLayer.stageManager;
            var objectFactory = areaLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateHandleObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, areaLayer))
            {
                objectFactory.DestroyHandleObject(newItem);

                return null;
            }

            if (!stageObjects.handleObjects.Contains(newItem))
                stageObjects.handleObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 取っ手オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveHandleObject(HandleStageObject item)
        {
            var objectFactory = areaLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.handleObjects.Contains(item))
                stageObjects.handleObjects.Remove(item);

            objectFactory.DestroyHandleObject(item);
        }

        /// <summary>
        /// 全ての取っ手オブジェクトを取り除く
        /// </summary>
        public void RemoveAllHandleObject()
        {
            while (stageObjects.handleObjects.Count > 0)
                stageObjects.handleObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身の取っ手オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<HandleStageObject> GetMyHandleObjects()
        {
            return stageObjects.handleObjects;
        }

        /// <summary>
        /// 指定位置の自身の取っ手オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<HandleStageObject> GetAtMyHandleObjects(Vector2Int position)
        {
            var targets = new List<HandleStageObject>();
            var stageObjects = areaLayer.stageManager?.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as HandleStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.handleObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の取っ手オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<HandleStageObject> GetRangeMyHandleObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<HandleStageObject>();
            var stageObjects = areaLayer.stageManager?.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as HandleStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.handleObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 取っ手オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasHandleObjects()
        {
            return GetMyHandleObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置の取っ手オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtHandleObjects(Vector2Int position)
        {
            return GetAtMyHandleObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の取っ手オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeHandleObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyHandleObjects(position, size).Count > 0;
        }

        #endregion
    }

    #endregion
}

#elif false

using System.Collections.Generic;
using UnityEngine;

public class StageAreaLayer : StageObject
{
    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = null;

    /// <summary> オブジェクトマネージャー </summary>
    public StageObject_Manager ObjectManager { get => objectManager; }

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        if (objectManager == null)
            objectManager = new StageObject_Manager(this);

        UnInitialize();

        objectManager.Initialize();

        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        objectManager.UnInitialize();

        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager?.ObjectManager?.RemoveAreaLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 成功したか </returns>
    public bool SetData(AreaLayerData data)
    {
        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        transform.position = CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        if (objectManager == null)
            return false;

        for (int i = 0; i < data.AreaDatas.Count; ++i)
            objectManager.AddAreaObject(data.AreaDatas[i]);

        for (int i = 0; i < data.HandleDatas.Count; ++i)
            objectManager.AddHandleObject(data.HandleDatas[i]);

        return true;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        objectManager.UpdateProcess();
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        objectManager.FixedUpdateProcess();
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        objectManager.LateUpdateProcess();
    }

#endregion

#region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.AreaLayer;
    }

#endregion

#region 子クラス

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> 領域オブジェクト </summary>
        public List<AreaStageObject> areaObjects = new List<AreaStageObject>();

        /// <summary> 取っ手オブジェクト群 </summary>
        public List<HandleStageObject> handleObjects = new List<HandleStageObject>();
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> 領域レイヤー </summary>
        StageAreaLayer areaLayer = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = null;

#region 初期化・解放処理関係

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="areaLayer"> 領域レイヤー </param>
        public StageObject_Manager(StageAreaLayer areaLayer)
        {
            this.areaLayer = areaLayer;
            stageObjects = new StageObjects();
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Initialize()
        {
            UnInitialize();
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAllAreaObject();
            RemoveAllHandleObject();
        }

#endregion

#region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.areaObjects.Count; ++i)
                stageObjects.areaObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.handleObjects.Count; ++i)
                stageObjects.handleObjects[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.areaObjects.Count; ++i)
                stageObjects.areaObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.handleObjects.Count; ++i)
                stageObjects.handleObjects[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.areaObjects.Count; ++i)
                stageObjects.areaObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.handleObjects.Count; ++i)
                stageObjects.handleObjects[i].LateUpdateProcess();
        }

#endregion

#region 領域オブジェクト

        /// <summary>
        /// 領域オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public AreaStageObject AddAreaObject(AreaData data)
        {
            if (!areaLayer)
                return null;

            var stageManager = areaLayer.stageManager;
            var objectFactory = areaLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateAreaObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, areaLayer))
            {
                objectFactory.DestroyAreaObject(newItem);

                return null;
            }

            if (!stageObjects.areaObjects.Contains(newItem))
                stageObjects.areaObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 領域オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveAreaObject(AreaStageObject item)
        {
            var objectFactory = areaLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (!item)
                return;

            if (stageObjects.areaObjects.Contains(item))
                stageObjects.areaObjects.Remove(item);

            objectFactory.DestroyAreaObject(item);
        }

        /// <summary>
        /// 全ての領域オブジェクトを取り除く
        /// </summary>
        public void RemoveAllAreaObject()
        {
            while (stageObjects.areaObjects.Count > 0)
                stageObjects.areaObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身の領域オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<AreaStageObject> GetMyAreaObjects()
        {
            return stageObjects.areaObjects;
        }

        /// <summary>
        /// 指定位置の自身の領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<AreaStageObject> GetAtMyAreaObjects(Vector2Int position)
        {
            var targets = new List<AreaStageObject>();
            var stageObjects = areaLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as AreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.areaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<AreaStageObject> GetRangeMyAreaObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<AreaStageObject>();
            var stageObjects = areaLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as AreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.areaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 領域オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAreaObjects()
        {
            return GetMyAreaObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置の領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtAreaObjects(Vector2Int position)
        {
            return GetAtMyAreaObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeAreaObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyAreaObjects(position, size).Count > 0;
        }

#endregion

#region 取っ手オブジェクト

        /// <summary>
        /// 取っ手オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public HandleStageObject AddHandleObject(HandleData data)
        {
            if (!areaLayer)
                return null;

            var stageManager = areaLayer.stageManager;
            var objectFactory = areaLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateHandleObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, areaLayer))
            {
                objectFactory.DestroyHandleObject(newItem);

                return null;
            }

            if (!stageObjects.handleObjects.Contains(newItem))
                stageObjects.handleObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 取っ手オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveHandleObject(HandleStageObject item)
        {
            var objectFactory = areaLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (!item)
                return;

            if (stageObjects.handleObjects.Contains(item))
                stageObjects.handleObjects.Remove(item);

            objectFactory.DestroyHandleObject(item);
        }

        /// <summary>
        /// 全ての取っ手オブジェクトを取り除く
        /// </summary>
        public void RemoveAllHandleObject()
        {
            while (stageObjects.handleObjects.Count > 0)
                stageObjects.handleObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身の取っ手オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<HandleStageObject> GetMyHandleObjects()
        {
            return stageObjects.handleObjects;
        }

        /// <summary>
        /// 指定位置の自身の取っ手オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<HandleStageObject> GetAtMyHandleObjects(Vector2Int position)
        {
            var targets = new List<HandleStageObject>();
            var stageObjects = areaLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as HandleStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.handleObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の取っ手オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<HandleStageObject> GetRangeMyHandleObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<HandleStageObject>();
            var stageObjects = areaLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as HandleStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.handleObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 取っ手オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasHandleObjects()
        {
            return GetMyHandleObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置の取っ手オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtHandleObjects(Vector2Int position)
        {
            return GetAtMyHandleObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の取っ手オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeHandleObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyHandleObjects(position, size).Count > 0;
        }

#endregion
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StageAreaLayer : StageObject
{
    /// <summary> 領域オブジェクト </summary>
    List<AreaStageObject> areaObjects = new List<AreaStageObject>();

    /// <summary> 取っ手オブジェクト群 </summary>
    List<HandleStageObject> handleObjects = new List<HandleStageObject>();

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType myAreaType = BlockData.BlockType.Fixed;

    /// <summary> 中心位置 </summary>
    Vector2 center = Vector2.zero;

    /// <summary> 左端の領域オブジェクト </summary>
    List<AreaStageObject> leftAreaObjects = new List<AreaStageObject>();

    /// <summary> 右端の領域オブジェクト </summary>
    List<AreaStageObject> rightAreaObjects = new List<AreaStageObject>();

    /// <summary> 上端の領域オブジェクト </summary>
    List<AreaStageObject> upAreaObjects = new List<AreaStageObject>();

    /// <summary> 下端の領域オブジェクト </summary>
    List<AreaStageObject> downAreaObjects = new List<AreaStageObject>();

    /// <summary> 左端の取っ手オブジェクト </summary>
    List<HandleStageObject> leftHandleObjects = new List<HandleStageObject>();

    /// <summary> 右端の取っ手オブジェクト </summary>
    List<HandleStageObject> rightHandleObjects = new List<HandleStageObject>();

    /// <summary> 上端の取っ手オブジェクト </summary>
    List<HandleStageObject> upHandleObjects = new List<HandleStageObject>();

    /// <summary> 下端の取っ手オブジェクト </summary>
    List<HandleStageObject> downHandleObjects = new List<HandleStageObject>();

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        RemoveAllAreaObject();
        RemoveAllHandleObject();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager.RemoveAreaLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    public void SetData(AreaLayerData data)
    {
        systemBitFlag.Pop(bitFlagEnable);

        transform.position = CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        myAreaType = data.MyAreaType;

        for (int i = 0; i < data.AreaDatas.Count; ++i)
            AddAreaObject(data.AreaDatas[i]);

        for (int i = 0; i < data.HandleDatas.Count; ++i)
            AddHandleObject(data.HandleDatas[i]);

        UpdateCenter();
        CheckHandleType();
        CheckAreaGrabGroup();
        CheckHandleGrabGroup();
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < areaObjects.Count; ++i)
            areaObjects[i].UpdateProcess(managerCondition);

        for (int i = 0; i < handleObjects.Count; ++i)
            handleObjects[i].UpdateProcess(managerCondition);
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < areaObjects.Count; ++i)
            areaObjects[i].FixedUpdateProcess(managerCondition);

        for (int i = 0; i < handleObjects.Count; ++i)
            handleObjects[i].FixedUpdateProcess(managerCondition);
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < areaObjects.Count; ++i)
            areaObjects[i].LateUpdateProcess(managerCondition);

        for (int i = 0; i < handleObjects.Count; ++i)
            handleObjects[i].LateUpdateProcess(managerCondition);
    }

#endregion

#region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 領域オブジェクト

    /// <summary>
    /// 領域オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public AreaStageObject AddAreaObject(AreaData data)
    {
        var newItem = objectFactory.InstantiateAreaObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, myAreaType, this);

        if (!areaObjects.Contains(newItem))
            areaObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 領域オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveAreaObject(AreaStageObject item)
    {
        if (!item)
            return;

        if (areaObjects.Contains(item))
            areaObjects.Remove(item);

        objectFactory.DestroyAreaObject(item);
    }

    /// <summary>
    /// 全ての領域オブジェクトを取り除く
    /// </summary>
    public void RemoveAllAreaObject()
    {
        while (areaObjects.Count > 0)
            areaObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の領域オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<AreaStageObject> GetMyAreaObjects()
    {
        return areaObjects;
    }

    /// <summary>
    /// 指定位置の自身の領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<AreaStageObject> GetAtMyAreaObjects(Vector2Int position)
    {
        var targets = new List<AreaStageObject>();
        var stageObjects = stageManager.GetStageObjects(gridPosition);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as AreaStageObject;

            if (!target)
                continue;

            if (!areaObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身の領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<AreaStageObject> GetRangeMyAreaObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<AreaStageObject>();
        var stageObjects = stageManager.GetRangeStageObjects(gridPosition, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as AreaStageObject;

            if (!target)
                continue;

            if (!areaObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 領域オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAreaObjects()
    {
        return GetMyAreaObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtAreaObjects(Vector2Int position)
    {
        return GetAtMyAreaObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置の領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeAreaObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyAreaObjects(position, size).Count > 0;
    }

    /// <summary>
    /// 領域の掴み移動グループを調べる
    /// </summary>
    public void CheckAreaGrabGroup()
    {
        leftAreaObjects.Clear();
        rightAreaObjects.Clear();
        upAreaObjects.Clear();
        downAreaObjects.Clear();

        for (int i = 0; i < areaObjects.Count; ++i)
        {
            var gridPosition = areaObjects[i].GetGridPosition();
            var left = new Vector2Int(gridPosition.x - 1, gridPosition.y);
            var right = new Vector2Int(gridPosition.x + 1, gridPosition.y);
            var up = new Vector2Int(gridPosition.x, gridPosition.y + 1);
            var down = new Vector2Int(gridPosition.x, gridPosition.y - 1);

            if (stageManager.CheckPositionInGrid(left))
                if (!CheckHasAtAreaObjects(left))
                    leftAreaObjects.Add(areaObjects[i]);

            if (stageManager.CheckPositionInGrid(right))
                if (!CheckHasAtAreaObjects(right))
                    rightAreaObjects.Add(areaObjects[i]);

            if (stageManager.CheckPositionInGrid(up))
                if (!CheckHasAtAreaObjects(up))
                    upAreaObjects.Add(areaObjects[i]);

            if (stageManager.CheckPositionInGrid(down))
                if (!CheckHasAtAreaObjects(down))
                    downAreaObjects.Add(areaObjects[i]);
        }
    }

#endregion

#region 取っ手オブジェクト

    /// <summary>
    /// 取っ手オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public HandleStageObject AddHandleObject(HandleData data)
    {
        var newItem = objectFactory.InstantiateHandleObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, this);

        if (!handleObjects.Contains(newItem))
            handleObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 取っ手オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveHandleObject(HandleStageObject item)
    {
        if (!item)
            return;

        if (handleObjects.Contains(item))
            handleObjects.Remove(item);

        objectFactory.DestroyHandleObject(item);
    }

    /// <summary>
    /// 全ての取っ手オブジェクトを取り除く
    /// </summary>
    public void RemoveAllHandleObject()
    {
        while (handleObjects.Count > 0)
            handleObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の取っ手オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<HandleStageObject> GetMyHandleObjects()
    {
        return handleObjects;
    }

    /// <summary>
    /// 取っ手オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasHandleObjects()
    {
        return GetMyHandleObjects().Count > 0;
    }

    /// <summary>
    /// 取っ手の種類を調べる
    /// </summary>
    public void CheckHandleType()
    {
        for (int i = 0; i < handleObjects.Count; ++i)
        {
            if (HandleData.CheckHorizontalHandleType(handleObjects[i].GetHandleType()))
            {
                if (handleObjects[i].transform.position.x < center.x)
                    handleObjects[i].SetHandleType(HandleData.HandleType.LeftHorizontal);
                else if (handleObjects[i].transform.position.x > center.x)
                    handleObjects[i].SetHandleType(HandleData.HandleType.RightHorizontal);
                else
                {
                    handleObjects[i].SetHandleType(HandleData.HandleType.LeftHorizontal);
                    handleObjects[i].SetHandleType(HandleData.HandleType.RightHorizontal);
                }
            }
            else
            {
                if (handleObjects[i].transform.position.y < center.y)
                    handleObjects[i].SetHandleType(HandleData.HandleType.BottomVertical);
                else if (handleObjects[i].transform.position.y > center.y)
                    handleObjects[i].SetHandleType(HandleData.HandleType.TopVertical);
                else
                {
                    handleObjects[i].SetHandleType(HandleData.HandleType.BottomVertical);
                    handleObjects[i].SetHandleType(HandleData.HandleType.TopVertical);
                }
            }
        }
    }

    /// <summary>
    /// 取っ手の掴み移動グループを調べる
    /// </summary>
    public void CheckHandleGrabGroup()
    {
        leftHandleObjects.Clear();
        rightHandleObjects.Clear();
        upHandleObjects.Clear();
        downHandleObjects.Clear();

        for (int i = 0; i < handleObjects.Count; ++i)
        {
            if (handleObjects[i].transform.position.x < center.x)
                leftHandleObjects.Add(handleObjects[i]);
            else if (handleObjects[i].transform.position.x > center.x)
                rightHandleObjects.Add(handleObjects[i]);
            else
            {
                leftHandleObjects.Add(handleObjects[i]);
                rightHandleObjects.Add(handleObjects[i]);
            }

            if (handleObjects[i].transform.position.y < center.y)
                downHandleObjects.Add(handleObjects[i]);
            else if (handleObjects[i].transform.position.y > center.y)
                upHandleObjects.Add(handleObjects[i]);
            else
            {
                downHandleObjects.Add(handleObjects[i]);
                upHandleObjects.Add(handleObjects[i]);
            }
        }
    }

#endregion

#region 中心位置関係

    /// <summary>
    /// 中心位置更新
    /// </summary>
    public void UpdateCenter()
    {
        Vector2 min = stageManager.GetGridSize();
        Vector2 max = Vector2.zero;

        for (int i = 0; i < areaObjects.Count; ++i)
        {
            var leftBottom = areaObjects[i].GetGridPosition();
            var rightTop = leftBottom + areaObjects[i].GetGridSize() - Vector2Int.one;

            if (min.x > leftBottom.x) min.x = leftBottom.x;
            if (min.y > leftBottom.y) min.y = leftBottom.y;
            if (max.x < rightTop.x) max.x = rightTop.x;
            if (max.y < rightTop.y) max.y = rightTop.y;
        }

        min.x += 0.5f;
        min.y += 0.5f;
        max.x += 0.5f;
        max.y += 0.5f;

        center = (min + max) * 0.5f;
    }

    /// <summary>
    /// 中心位置取得
    /// </summary>
    /// <returns> 位置 </returns>
    Vector2 GetCenter()
    {
        return center;
    }

#endregion

#region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return myAreaType;
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.AreaLayer;
    }

#endregion
}

#endif