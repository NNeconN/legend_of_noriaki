#if true

using UnityEngine;

public class CameraAreaStageObject : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> カメラ領域の位置 </summary>
    Vector2Int cameraAreaOffset = Vector2Int.zero;

    /// <summary> カメラ領域の大きさ </summary>
    Vector2Int cameraAreaSize = Vector2Int.one;

    /// <summary> カメラ領域の位置 取得用 </summary>
    public Vector2Int CameraAreaOffset { get => cameraAreaOffset; }

    /// <summary> カメラ領域の大きさ 取得用 </summary>
    public Vector2Int CameraAreaSize { get => cameraAreaSize; }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        var isFailed = !base.Initialize(stageManager, objectFactory);

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGrid(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveCameraAreaObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(CameraAreaData data, StageObjectsLayer objectsLayer)
    {
        if (!stageManager)
            return false;

        this.objectsLayer = objectsLayer;

        gridPosition = data.GridPosition;
        gridSize = data.GridSize;

        cameraAreaOffset = data.CameraAreaOffset;
        cameraAreaSize = data.CameraAreaSize;

        CalculatePosition(gridPosition);
        transform.parent = this.objectsLayer.transform;

        stageManager.AddRangeGrid(this, gridPosition, gridSize);

        return true;
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + gridPosition.x + (gridSize.x * 0.5f);
        rect.y = transform.position.y + gridPosition.y + (gridSize.y * 0.5f);

        rect.width = gridSize.x;
        rect.height = gridSize.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.CameraArea;
    }

    #endregion
}

#elif false

using UnityEditor;
using UnityEngine;

public class CameraAreaStageObject : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> カメラ領域の位置 </summary>
    Vector2Int cameraAreaOffset = Vector2Int.zero;

    /// <summary> カメラ領域の大きさ </summary>
    Vector2Int cameraAreaSize = Vector2Int.one;

    /// <summary> カメラ領域の位置 取得用 </summary>
    public Vector2Int CameraAreaOffset { get => cameraAreaOffset; }

    /// <summary> カメラ領域の大きさ 取得用 </summary>
    public Vector2Int CameraAreaSize { get => cameraAreaSize; }

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.GridHasObjects?.RemoveRange(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer?.ObjectManager?.RemoveCameraAreaObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(CameraAreaData data, StageObjectsLayer objectsLayer)
    {
        if (!stageManager)
            return false;

        this.objectsLayer = objectsLayer;

        gridPosition = data.GridPosition;
        gridSize = data.GridSize;

        cameraAreaOffset = data.CameraAreaOffset;
        cameraAreaSize = data.CameraAreaSize;

        transform.position = CalculatePosition(gridPosition);
        transform.parent = this.objectsLayer.transform;

        stageManager.GridHasObjects?.AddRange(this, gridPosition, gridSize);

        return true;
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + gridPosition.x + (gridSize.x * 0.5f);
        rect.y = transform.position.y + gridPosition.y + (gridSize.y * 0.5f);

        rect.width = gridSize.x;
        rect.height = gridSize.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.CameraArea;
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessAreaStageObject : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> 処理領域の位置 </summary>
    Vector2Int processGridAreaOffset = Vector2Int.zero;

    /// <summary> 処理領域の大きさ </summary>
    Vector2Int processGridAreaSize = Vector2Int.one;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveProcessAreaObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    public void SetData(ProcessAreaData data, StageObjectsLayer objectsLayer)
    {
        this.objectsLayer = objectsLayer;

        gridPosition = data.GridPosition;
        gridSize = data.GridSize;

        processGridAreaOffset = data.ProcessGridAreaOffset;
        processGridAreaSize = data.ProcessGridAreaSize;

        transform.position = CalculatePosition(gridPosition);
        transform.parent = this.objectsLayer.transform;

        stageManager.AddRangeGridHasObject(this, gridPosition, gridSize);
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + gridPosition.x + (gridSize.x * 0.5f);
        rect.y = transform.position.y + gridPosition.y + (gridSize.y * 0.5f);

        rect.width = gridSize.x;
        rect.height = gridSize.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 処理領域関係

    /// <summary>
    /// 処理領域の位置取得
    /// </summary>
    /// <returns> 位置 </returns>
    public Vector2Int GetProcessGridAreaOffset()
    {
        return processGridAreaOffset;
    }

    /// <summary>
    /// 処理領域の大きさ取得
    /// </summary>
    /// <returns> 大きさ </returns>
    public Vector2Int GetProcessGridAreaSize()
    {
        return processGridAreaSize;
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.ProcessArea;
    }

#endregion
}

#endif