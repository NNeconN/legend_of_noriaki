using System;
using System.Collections.Generic;
using UnityEngine;

public class CameraAreaData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        GridPosition = -1,
        GridSize = -2,
        CameraAreaOffset = -3,
        CameraAreaSize = -4,
    }

    #endregion

    /// <summary> グリッド上の位置 </summary>
    Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> グリッド上の大きさ </summary>
    Vector2Int gridSize = Vector2Int.one;

    /// <summary> カメラ領域の位置 </summary>
    Vector2Int cameraAreaOffset = Vector2Int.zero;

    /// <summary> カメラ領域の大きさ </summary>
    Vector2Int cameraAreaSize = Vector2Int.one;

    /// <summary> グリッド上の位置設定・取得用 </summary>
    public Vector2Int GridPosition { get => gridPosition; set => gridPosition = value; }

    /// <summary> グリッド上の大きさ設定・取得用 </summary>
    public Vector2Int GridSize { get => gridSize; set => gridSize = value; }

    /// <summary> カメラ領域の位置 設定・取得用 </summary>
    public Vector2Int CameraAreaOffset { get => cameraAreaOffset; set => cameraAreaOffset = value; }

    /// <summary> カメラ領域の大きさ 設定・取得用 </summary>
    public Vector2Int CameraAreaSize { get => cameraAreaSize; set => cameraAreaSize = value; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public CameraAreaData()
    {
        gridPosition = Vector2Int.zero;
        gridSize = Vector2Int.zero;
        cameraAreaOffset = Vector2Int.zero;
        cameraAreaSize = Vector2Int.zero;
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public CameraAreaData(CameraAreaEditorObject target)
    {
        gridPosition = target.GetGridPosition();
        gridSize = target.GetGridSize();
        cameraAreaOffset = target.GetProcessGridAreaOffset();
        cameraAreaSize = target.GetProcessGridAreaSize();
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(CameraAreaEditorObject target)
    {
        target.SetGridPosition(gridPosition);
        target.SetGridSize(gridSize);
        target.SetProcessGridAreaOffset(cameraAreaOffset);
        target.SetProcessGridAreaSize(cameraAreaSize);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        for (int indexX = 0; indexX < csvDatas[index].Length;)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][indexX], out dataID))
                break;

            switch (dataID)
            {
                case DataID.GridPosition:
                    gridPosition.x = int.Parse(csvDatas[index][indexX + 1]);
                    gridPosition.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                case DataID.GridSize:
                    gridSize.x = int.Parse(csvDatas[index][indexX + 1]);
                    gridSize.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                case DataID.CameraAreaOffset:
                    cameraAreaOffset.x = int.Parse(csvDatas[index][indexX + 1]);
                    cameraAreaOffset.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                case DataID.CameraAreaSize:
                    cameraAreaSize.x = int.Parse(csvDatas[index][indexX + 1]);
                    cameraAreaSize.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                default:
                    ++indexX;
                    break;
            }
        }

        ++index;
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.CameraArea.ToString() });

        csvDatas.Add(new string[] {
            DataID.GridPosition.ToString(),
            gridPosition.x.ToString(),
            gridPosition.y.ToString(),
            DataID.GridSize.ToString(),
            gridSize.x.ToString(),
            gridSize.y.ToString(),
            DataID.CameraAreaOffset.ToString(),
            cameraAreaOffset.x.ToString(),
            cameraAreaOffset.y.ToString(),
            DataID.CameraAreaSize.ToString(),
            cameraAreaSize.x.ToString(),
            cameraAreaSize.y.ToString(),
        });
    }
}
