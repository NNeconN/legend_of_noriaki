using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゴールモデル制御関係
/// </summary>
public class GoalModelController : MonoBehaviour
{
    [SerializeField, Header("左ドア")]
    GameObject leftDoor = null;

    [SerializeField, Header("右ドア")]
    GameObject rightDoor = null;

    /// <summary>
    /// ドアを初期化
    /// </summary>
    public void InitializeDoor()
    {
        leftDoor.transform.eulerAngles = Vector3.zero;
        rightDoor.transform.eulerAngles = Vector3.zero;
    }

    /// <summary>
    /// ドアの回転
    /// </summary>
    /// <param name="leftPivot"> 左計算位置 </param>
    /// <param name="rightPivot"> 右計算位置 </param>
    public void DoorRotate(Vector3 leftPivot, Vector3 rightPivot)
    {
        Vector2 leftDirection = new Vector2(
            leftPivot.x - leftDoor.transform.position.x,
            leftPivot.z - leftDoor.transform.position.z
            );

        Vector2 rightDirection = new Vector2(
            rightPivot.x - rightDoor.transform.position.x,
            rightPivot.z - rightDoor.transform.position.z
            );

        var leftMagnitude = leftDirection.magnitude;
        var rightMagnitude = rightDirection.magnitude;

        if (leftMagnitude > 0)
        {
            var angle = leftDoor.transform.eulerAngles;

            angle.y = Mathf.Acos(leftDirection.x / leftMagnitude) * Mathf.Rad2Deg;

            if (leftPivot.z > leftDoor.transform.position.z)
                angle.y *= -1.0f;

            leftDoor.transform.eulerAngles = angle;
        }

        if (rightMagnitude > 0)
        {
            var angle = rightDoor.transform.eulerAngles;

            angle.y = Mathf.Acos(rightDirection.x / rightMagnitude) * Mathf.Rad2Deg;

            if (rightPivot.z > rightDoor.transform.position.z)
                angle.y *= -1.0f;

            angle.y += 180.0f;

            rightDoor.transform.eulerAngles = angle;
        }
    }
}
