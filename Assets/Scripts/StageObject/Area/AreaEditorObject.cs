using UnityEngine;

public class AreaEditorObject : EditorObject
{
    /// <summary> 自身のモデル操作用 </summary>
    [SerializeField] AreaModelController controller = null;

    /// <summary> 領域レイヤー </summary>
    [SerializeField] EditorAreaLayer areaLayer = null;

    /// <summary> 領域の種類 </summary>
    [SerializeField] BlockData.BlockType areaType = BlockData.BlockType.Fixed;

    /// <summary> 領域レイヤー設定・取得用 </summary>
    public EditorAreaLayer AreaLayer { get => areaLayer; set => areaLayer = value; }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        if (!GameObjectUtility.NullCheckAndGet(ref controller, transform))
            return false;

        gridSize = AreaData.DEFAULT_GRID_SIZE;

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        areaLayer.RemoveAreaObject(this);
    }

    /// <summary>
    /// 他を破壊する
    /// </summary>
    public override void DestroyOther()
    {
        var gridHasObjects = manager.GetRangeStageObjects(gridPosition, gridSize);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            if (gridHasObjects[i] == this)
                continue;

            var tag = gridHasObjects[i].GetObjectType();

            switch (tag)
            {
                case StageData.ObjectType.Area:
                    {
                        var target = gridHasObjects[i] as AreaEditorObject;

                        if (!target)
                            break;

                        if (target.GetAreaType() != areaType)
                            break;

                        target.DestroyThis();
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public override void ChangeObjectName()
    {
        gameObject.name = GetType().Name + "_" + areaType.ToString();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        if (gridPosition.x < 0)
            return true;

        if (gridPosition.y < 0)
            return true;

        if (gridPosition.x + gridSize.x - 1 >= managerGridSize.x)
            return true;

        if (gridPosition.y + gridSize.y - 1 >= managerGridSize.y)
            return true;


        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        DestroyOther();

        return false;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        return ResetGridAllObjects();
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - gridSize);

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        transform.position = CalculatePosition(gridPosition);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        var leftBottom = gridPosition + direction;
        var rightTop = leftBottom + gridSize - Vector2Int.one;

        return manager.CheckPositionInGrid(leftBottom) && manager.CheckPositionInGrid(rightTop);
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        var targetPosition = gridPosition + direction;

        SetGridPosition(targetPosition);

        DestroyOther();
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!manager)
            return Vector3.zero;

        var position = BlockData.DEFAULT_POSITION_OFFSET;

        position.x += manager.GetLeftPivotX() + gridPosition.x;
        position.y += manager.GetBottomPivotY() + gridPosition.y;
        position.z += manager.transform.position.z - (((int)areaType + 1) * 0.001f);

        return position;
    }

    #endregion

    #region 領域の種類

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <returns> 領域の種類 </returns>
    public BlockData.BlockType GetAreaType()
    {
        return areaType;
    }

    /// <summary>
    /// 領域の種類取得
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    public void SetAreaType(BlockData.BlockType areaType)
    {
        this.areaType = areaType;

        ChangeObjectName();
        transform.position = CalculatePosition(gridPosition);

        controller.SetMaterialIndex((int)this.areaType);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Area;
    }

    #endregion
}
