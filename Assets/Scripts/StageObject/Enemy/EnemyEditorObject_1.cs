using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEditorObject_1 : EditorObject
{
    /// <summary> 自身のモデル操作用 </summary>
    [SerializeField] EnemyModelController_1 controller = null;

    /// <summary> オブジェクトレイヤー </summary>
    [SerializeField] EditorObjectsLayer objectsLayer = null;

    /// <summary> オブジェクトレイヤー設定・取得用 </summary>
    public EditorObjectsLayer ObjectsLayer { get => objectsLayer; set => objectsLayer = value; }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        if (!GameObjectUtility.NullCheckAndGet(ref controller, transform))
            return false;

        gridSize = EnemyData_1.DEFAULT_GRID_SIZE;

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveEnemyObject_1(this);
    }

    /// <summary>
    /// 他を破壊する
    /// </summary>
    public override void DestroyOther()
    {
        var gridHasObjects = manager.GetRangeStageObjects(gridPosition, gridSize);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            if (gridHasObjects[i] == this)
                continue;

            var tag = gridHasObjects[i].GetObjectType();

            Debug.Log(tag);

            switch (tag)
            {
                case StageData.ObjectType.Block:
                case StageData.ObjectType.Handle:
                case StageData.ObjectType.Goal:
                case StageData.ObjectType.Player:
                case StageData.ObjectType.Enemy1:
                    gridHasObjects[i]?.DestroyThis();
                    break;
            }
        }
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        if (gridPosition.x < 0)
            return true;

        if (gridPosition.y < 0)
            return true;

        if (gridPosition.x + gridSize.x - 1 >= managerGridSize.x)
            return true;

        if (gridPosition.y + gridSize.y - 1 >= managerGridSize.y)
            return true;

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        DestroyOther();

        return false;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        return ResetGridAllObjects();
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - gridSize);

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);

        transform.position = CalculatePosition(gridPosition);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        var leftBottom = gridPosition + direction;
        var rightTop = leftBottom + gridSize - Vector2Int.one;

        return manager.CheckPositionInGrid(leftBottom) && manager.CheckPositionInGrid(rightTop);
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        var targetPosition = gridPosition + direction;

        SetGridPosition(targetPosition);

        DestroyOther();
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!manager)
            return Vector3.zero;

        var position = EnemyData_1.DEFAULT_POSITION_OFFSET;

        position.x += manager.GetLeftPivotX() + gridPosition.x;
        position.y += manager.GetBottomPivotY() + gridPosition.y;
        position.z += manager.transform.position.z;

        return position;
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Enemy1;
    }

    #endregion
}
