#if true

using UnityEngine;

public class EnemyStageObject_1 : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> モデルコントローラー </summary>
    EnemyModelController_1 controller = null;

    /// <summary> ノックバックの強さ </summary>
    [SerializeField] Vector2 knockBackPower = Vector2.one;

    /// <summary> 地面レイヤー </summary>
    [SerializeField] LayerMask groundLayers = 0;

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        var isFailed = !base.Initialize(stageManager, objectFactory);
        isFailed = isFailed || !controller;

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGrid(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveEnemyObject_1(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(EnemyData_1 data, StageObjectsLayer objectsLayer)
    {
        this.objectsLayer = objectsLayer;

        CalculatePosition(data.GridPosition);
        transform.parent = this.objectsLayer.transform;

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        CalculateGridPosition(xMin, yMin);
        CalculateGridSize(xMin, xMax, yMin, yMax);

        stageManager.AddRangeGrid(this, gridPosition, gridSize);

        return true;
    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        var players = stageManager.HitCheckGridActive<PlayerStageObject>(this);

        for (int i = 0; i < players.Count; ++i)
        {
            var currentKnockBackPower = knockBackPower;

            if (players[i].transform.position.x < transform.position.x)
                currentKnockBackPower.x = -knockBackPower.x;
            else if (players[i].transform.position.x > transform.position.x)
                currentKnockBackPower.x = knockBackPower.x;
            else
                continue;

            players[i].KnockBack(currentKnockBackPower);
        }
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        if (stageManager.CheckScaleChangeLock())
            if (CheckIsStuckGround(groundLayers))
                stageManager.SetFailedObject(this);

        SetFallOutState();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + EnemyData_1.DEFAULT_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + EnemyData_1.DEFAULT_GRID_COLLIDER_OFFSET.y;

        rect.width = EnemyData_1.DEFAULT_GRID_COLLIDER_SIZE.x;
        rect.height = EnemyData_1.DEFAULT_GRID_COLLIDER_SIZE.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = EnemyData_1.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

    #endregion

    #region 落下状態

    /// <summary>
    /// 落下
    /// </summary>
    void SetFallOutState()
    {
        var positionY = transform.position.y + GetGridColliderRect().height;
        var borderBottom = stageManager.GetBottomPivotY();

        if (positionY > borderBottom)
            return;

        DestroyThis();
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Enemy1;
    }

    #endregion
}

#elif false

using System.Runtime.Serialization;
using UnityEngine;

public class EnemyStageObject_1 : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> モデルコントローラー </summary>
    EnemyModelController_1 controller = null;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager?.GridHasObjects?.RemoveRange(this, gridPosition, gridSize);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer?.ObjectManager?.RemoveEnemyObject_1(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 成功したか </returns>
    public bool SetData(EnemyData_1 data, StageObjectsLayer objectsLayer)
    {
        this.objectsLayer = objectsLayer;

        transform.position = CalculatePosition(data.GridPosition);
        transform.parent = this.objectsLayer.transform;

        var rect = GetGridColliderRect();

        CalculateGridPosition(rect.xMin, rect.yMin);
        CalculateGridSize(rect.xMin, rect.xMax, rect.yMin, rect.yMax);

        stageManager?.GridHasObjects?.AddRange(this, gridPosition, gridSize);

        return true;
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + EnemyData_1.DEFAULT_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + EnemyData_1.DEFAULT_GRID_COLLIDER_OFFSET.y;

        rect.width = EnemyData_1.DEFAULT_GRID_COLLIDER_SIZE.x;
        rect.height = EnemyData_1.DEFAULT_GRID_COLLIDER_SIZE.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = EnemyData_1.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Enemy1;
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStageObject_1 : StageObject
{
    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> モデルコントローラー </summary>
    EnemyModelController_1 controller = null;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        return true;
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemoveEnemyObject_1(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    public void SetData(EnemyData_1 data, StageObjectsLayer objectsLayer)
    {
        this.objectsLayer = objectsLayer;

        transform.position = CalculatePosition(data.GridPosition);
        transform.parent = this.objectsLayer.transform;

        var rect = GetGridColliderRect();

        CalculateGridPosition(rect.xMin, rect.yMin);
        CalculateGridSize(rect.xMin, rect.xMax, rect.yMin, rect.yMax);

        stageManager.AddRangeGridHasObject(this, gridPosition, gridSize);
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        switch (managerCondition)
        {
            case Stage_Manager.MainCondition.PlayNormal:
                UpdateGridParameter();
                break;
        }
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + EnemyData_1.DEFAULT_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + EnemyData_1.DEFAULT_GRID_COLLIDER_OFFSET.y;

        rect.width = EnemyData_1.DEFAULT_GRID_COLLIDER_SIZE.x;
        rect.height = EnemyData_1.DEFAULT_GRID_COLLIDER_SIZE.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = EnemyData_1.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Enemy1;
    }

#endregion
}

#endif