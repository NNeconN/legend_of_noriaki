using System;
using System.Collections.Generic;

public class ObjectsLayerData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        ChildDatas,
    }

    #endregion

    /// <summary> 背景データ群 </summary>
    List<BackData> backDatas = new List<BackData>();

    /// <summary> 処理領域データ群 </summary>
    List<CameraAreaData> processAreaDatas = new List<CameraAreaData>();

    /// <summary> プレイヤーデータ群 </summary>
    List<PlayerData> playerDatas = new List<PlayerData>();

    /// <summary> ゴールデータ群 </summary>
    List<GoalData> goalDatas = new List<GoalData>();

    /// <summary> 敵 1 データ群 </summary>
    List<EnemyData_1> enemyDatas_1 = new List<EnemyData_1>();

    /// <summary> 背景データ群取得用 </summary>
    public List<BackData> BackDatas { get => backDatas; }

    /// <summary> 処理領域データ群取得用 </summary>
    public List<CameraAreaData> ProcessAreaDatas { get => processAreaDatas; }

    /// <summary> プレイヤーデータ群取得用 </summary>
    public List<PlayerData> PlayerDatas { get => playerDatas; }

    /// <summary> ゴールデータ群取得用 </summary>
    public List<GoalData> GoalDatas { get => goalDatas; }

    /// <summary> 敵 1 データ群取得用 </summary>
    public List<EnemyData_1> EnemyDatas_1 { get => enemyDatas_1; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public ObjectsLayerData()
    {
        backDatas.Clear();
        processAreaDatas.Clear();
        playerDatas.Clear();
        goalDatas.Clear();
        enemyDatas_1.Clear();
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public ObjectsLayerData(EditorObjectsLayer target)
    {
        var backObjects = target.GetMyBackObjects();
        var processAreaObjects = target.GetMyProcessAreaObjects();
        var playerObjects = target.GetMyPlayerObjects();
        var goalObjects = target.GetMyGoalObjects();
        var enemyObjects_1 = target.GetMyEnemyObjects_1();

        for (int i = 0; i < backObjects.Count; i++)
            backDatas.Add(new BackData(backObjects[i]));

        for (int i = 0; i < processAreaObjects.Count; i++)
            processAreaDatas.Add(new CameraAreaData(processAreaObjects[i]));

        for (int i = 0; i < playerObjects.Count; i++)
            playerDatas.Add(new PlayerData(playerObjects[i]));

        for (int i = 0; i < goalObjects.Count; i++)
            goalDatas.Add(new GoalData(goalObjects[i]));

        for (int i = 0; i < enemyObjects_1.Count; i++)
            enemyDatas_1.Add(new EnemyData_1(enemyObjects_1[i]));
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(EditorObjectsLayer target)
    {
        for (int i = 0; i < backDatas.Count; ++i)
            target.AddBackObject(backDatas[i].GridPosition, backDatas[i].GridSize, backDatas[i].MaterialIndex);

        for (int i = 0; i < processAreaDatas.Count; ++i)
        {
            var newItem = target.AddProcessAreaObject(processAreaDatas[i].GridPosition, processAreaDatas[i].GridSize);

            if (!newItem)
                continue;

            newItem.SetProcessGridAreaOffset(processAreaDatas[i].CameraAreaOffset);
            newItem.SetProcessGridAreaSize(processAreaDatas[i].CameraAreaSize);
        }

        for (int i = 0; i < playerDatas.Count; ++i)
            target.AddPlayerObject(playerDatas[i].GridPosition, playerDatas[i].MyMoveDirection);

        for (int i = 0; i < goalDatas.Count; ++i)
            target.AddGoalObject(goalDatas[i].GridPosition);

        for (int i = 0; i < enemyDatas_1.Count; ++i)
            target.AddEnemyObject_1(enemyDatas_1[i].GridPosition);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        while (index < csvDatas.Count)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][0], out dataID))
                break;

            var dataIsNone = false;

            switch (dataID)
            {
                case DataID.ChildDatas:
                    ++index;
                    while (index < csvDatas.Count)
                    {
                        StageData.ObjectType objectType;
                        if (!Enum.TryParse(csvDatas[index][0], out objectType))
                            break;

                        var isFailed = false;

                        switch (objectType)
                        {
                            case StageData.ObjectType.Back:
                                ++index;
                                {
                                    var newData = new BackData();
                                    newData.SetData(csvDatas, ref index);
                                    backDatas.Add(newData);
                                }
                                break;
                            case StageData.ObjectType.CameraArea:
                                ++index;
                                {
                                    var newData = new CameraAreaData();
                                    newData.SetData(csvDatas, ref index);
                                    processAreaDatas.Add(newData);
                                }
                                break;
                            case StageData.ObjectType.Player:
                                ++index;
                                {
                                    var newData = new PlayerData();
                                    newData.SetData(csvDatas, ref index);
                                    playerDatas.Add(newData);
                                }
                                break;
                            case StageData.ObjectType.Goal:
                                ++index;
                                {
                                    var newData = new GoalData();
                                    newData.SetData(csvDatas, ref index);
                                    goalDatas.Add(newData);
                                }
                                break;
                            case StageData.ObjectType.Enemy1:
                                ++index;
                                {
                                    var newData = new EnemyData_1();
                                    newData.SetData(csvDatas, ref index);
                                    enemyDatas_1.Add(newData);
                                }
                                break;
                            default:
                                isFailed = true;
                                break;
                        }

                        if (isFailed)
                            break;
                    }
                    break;
                default:
                    dataIsNone = true;
                    break;
            }

            if (dataIsNone)
                break;
        }
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.ObjectsLayer.ToString() });

        csvDatas.Add(new string[1] { DataID.ChildDatas.ToString() });

        for (int i = 0; i < backDatas.Count; ++i)
            backDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < processAreaDatas.Count; ++i)
            processAreaDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < playerDatas.Count; ++i)
            playerDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < goalDatas.Count; ++i)
            goalDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < enemyDatas_1.Count; ++i)
            enemyDatas_1[i].GetData(ref csvDatas);
    }
}
