using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorObjectsLayer : EditorObject
{
    /// <summary> 背景オブジェクト群 </summary>
    [SerializeField] List<BackEditorObject> backObjects = new List<BackEditorObject>();

    /// <summary> 処理領域オブジェクト群 </summary>
    [SerializeField] List<CameraAreaEditorObject> processAreaObjects = new List<CameraAreaEditorObject>();

    /// <summary> プレイヤーオブジェクト群 </summary>
    [SerializeField] List<PlayerEditorObject> playerObjects = new List<PlayerEditorObject>();

    /// <summary> ゴールオブジェクト群 </summary>
    [SerializeField] List<GoalEditorObject> goalObjects = new List<GoalEditorObject>();

    /// <summary> 敵 1 オブジェクト群 </summary>
    [SerializeField] List<EnemyEditorObject_1> enemyObjects_1 = new List<EnemyEditorObject_1>();

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(StageEditor_Manager manager, EditorObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        UnInitialize();

        SetGridSize(Vector2Int.one);

        ChangeObjectName();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        RemoveAllBackObject();

        RemoveAllProcessAreaObject();

        RemoveAllPlayerObject();

        RemoveAllGoalObject();

        RemoveAllEnemyObject_1();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        manager.RemoveObjectsLayer(this);
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ResetGridAllObjects()
    {
        for (int i = 0; i < backObjects.Count; ++i)
            backObjects[i].ResetGridAllObjects();

        for (int i = 0; i < processAreaObjects.Count; ++i)
            processAreaObjects[i].ResetGridAllObjects();

        for (int i = 0; i < playerObjects.Count; ++i)
            playerObjects[i].ResetGridAllObjects();

        for (int i = 0; i < goalObjects.Count; ++i)
            goalObjects[i].ResetGridAllObjects();

        for (int i = 0; i < enemyObjects_1.Count; ++i)
            enemyObjects_1[i].ResetGridAllObjects();

        return false;
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int ReTouchEditorObjects()
    {
        Vector2Int maxGridSize = Vector2Int.one;

        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref backObjects));
        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref processAreaObjects));
        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref playerObjects));
        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref goalObjects));
        maxGridSize = MathUtility.Max(maxGridSize, ReTouchEditorObjects(ref enemyObjects_1));

        return maxGridSize;
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <typeparam name="T"> 対象 </typeparam>
    /// <param name="myList"> 入れ物 </param>
    /// <returns> グリッドの大きさ </returns>
    Vector2Int ReTouchEditorObjects<T>(ref List<T> myList) where T : EditorObject
    {
        Vector2Int maxGridSize = Vector2Int.one;

        myList.Clear();

        var targets = GetComponentsInChildren<T>();

        for (int i = 0; i < targets.Length; ++i)
        {
            myList.Add(targets[i]);

            var gridPosition = targets[i].GetGridPosition();
            var gridSize = targets[i].GetGridSize();

            maxGridSize.x = Mathf.Max(maxGridSize.x, gridPosition.x + gridSize.x);
            maxGridSize.y = Mathf.Max(maxGridSize.y, gridPosition.y + gridSize.y);
        }

        return maxGridSize;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <returns> 削除するか </returns>
    public override bool ReSizeGrid()
    {
        var managerGridSize = manager.GetGridSize();
        SetGridPosition(gridPosition);
        SetGridSize(gridSize);

        for (int i = 0; i < backObjects.Count; ++i)
        {
            if (!backObjects[i].ReSizeGrid())
                continue;

            if (RemoveBackObject(backObjects[i]))
                --i;
        }

        for (int i = 0; i < processAreaObjects.Count; ++i)
        {
            if (!processAreaObjects[i].ReSizeGrid())
                continue;

            if (RemoveProcessAreaObject(processAreaObjects[i]))
                --i;
        }

        for (int i = 0; i < playerObjects.Count; ++i)
        {
            if (!playerObjects[i].ReSizeGrid())
                continue;

            if (RemovePlayerObject(playerObjects[i]))
                --i;
        }

        for (int i = 0; i < goalObjects.Count; ++i)
        {
            if (!goalObjects[i].ReSizeGrid())
                continue;

            if (RemoveGoalObject(goalObjects[i]))
                --i;
        }

        for (int i = 0; i < enemyObjects_1.Count; ++i)
        {
            if (!enemyObjects_1[i].ReSizeGrid())
                continue;

            if (RemoveEnemyObject_1(enemyObjects_1[i]))
                --i;
        }

        return false;
    }

    /// <summary>
    /// グリッド上の位置設定
    /// </summary>
    /// <param name="position"> 位置 </param>
    public override void SetGridPosition(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        gridPosition = MathUtility.Clamp(position, Vector2Int.zero, managerGridSize - Vector2Int.one);
    }

    /// <summary>
    /// グリッド上の大きさ設定
    /// </summary>
    /// <param name="position"> 大きさ </param>
    public override void SetGridSize(Vector2Int size)
    {
        var managerGridSize = manager.GetGridSize();

        gridSize = MathUtility.Clamp(size, Vector2Int.one, managerGridSize - gridPosition);
    }

    /// <summary>
    /// スライド出来るか
    /// </summary>
    /// <param name="direction"> 向き </param>
    /// <returns> スライド出来るか </returns>
    public override bool CheckCanSlideGridPosition(Vector2Int direction)
    {
        return CheckHasBackObjects() || CheckHasPlayerObjects() || CheckHasGoalObjects() || CheckHasEnemyObjects_1();
    }

    /// <summary>
    /// スライドする
    /// </summary>
    /// <param name="direction"> 向き </param>
    public override void SlideGridPosition(Vector2Int direction)
    {
        for (int i = 0; i < backObjects.Count; ++i)
            backObjects[i].SetGridPosition(backObjects[i].GetGridPosition() + direction);

        for (int i = 0; i < playerObjects.Count; ++i)
            playerObjects[i].SetGridPosition(playerObjects[i].GetGridPosition() + direction);

        for (int i = 0; i < goalObjects.Count; ++i)
            goalObjects[i].SetGridPosition(goalObjects[i].GetGridPosition() + direction);

        for (int i = 0; i < enemyObjects_1.Count; ++i)
            enemyObjects_1[i].SetGridPosition(enemyObjects_1[i].GetGridPosition() + direction);

        ResetGridAllObjects();
    }

    #endregion

    #region 背景オブジェクト

    /// <summary>
    /// 背景が追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddBackObject(Vector2Int position, Vector2Int size)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + size.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + size.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// 背景オブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <param name="materialIndex"> マテリアルのインデックス番号 </param>
    /// <returns> 追加したオブジェクト </returns>
    public BackEditorObject AddBackObject(Vector2Int position, Vector2Int size, int materialIndex)
    {
        var newItem = objectFactory.InstantiateBackObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.SetGridSize(size);
        newItem.SetMaterialIndex(materialIndex);
        newItem.DestroyOther();

        backObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 背景オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveBackObject(BackEditorObject item)
    {
        objectFactory.DestroyBackObject(item);

        if (!backObjects.Contains(item))
            return false;

        backObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置の背景オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtBackObject(Vector2Int position)
    {
        var targets = GetAtMyBackObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveBackObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置の背景オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeBackObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyBackObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveBackObject(targets[i]);
        }
    }

    /// <summary>
    /// 全ての背景オブジェクトを取り除く
    /// </summary>
    public void RemoveAllBackObject()
    {
        while (backObjects.Count > 0)
            backObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の背景オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<BackEditorObject> GetMyBackObjects()
    {
        return backObjects;
    }

    /// <summary>
    /// 指定位置の自身の背景オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<BackEditorObject> GetAtMyBackObjects(Vector2Int position)
    {
        var targets = new List<BackEditorObject>();
        var stageObjects = manager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as BackEditorObject;

            if (!target)
                continue;

            if (!backObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身の背景オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<BackEditorObject> GetRangeMyBackObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<BackEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as BackEditorObject;

            if (!target)
                continue;

            if (!backObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 背景オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasBackObjects()
    {
        return GetMyBackObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置の背景オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtBackObjects(Vector2Int position)
    {
        return GetAtMyBackObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置の背景オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeBackObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyBackObjects(position, size).Count > 0;
    }

    #endregion

    #region 処理領域オブジェクト

    /// <summary>
    /// 処理領域が追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddProcessAreaObject(Vector2Int position, Vector2Int size)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + size.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + size.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// 処理領域オブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="size"> 追加サイズ </param>
    /// <returns> 追加したオブジェクト </returns>
    public CameraAreaEditorObject AddProcessAreaObject(Vector2Int position, Vector2Int size)
    {
        var newItem = objectFactory.InstantiateProcessAreaObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.SetGridSize(size);
        newItem.SetProcessGridAreaOffset(position);
        newItem.SetProcessGridAreaSize(size);
        newItem.DestroyOther();

        processAreaObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 処理領域オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveProcessAreaObject(CameraAreaEditorObject item)
    {
        objectFactory.DestroyProcessAreaObject(item);

        if (!processAreaObjects.Contains(item))
            return false;

        processAreaObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置の処理領域オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtProcessAreaObject(Vector2Int position)
    {
        var targets = GetAtMyProcessAreaObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveProcessAreaObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置の処理領域オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeProcessAreaObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyProcessAreaObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveProcessAreaObject(targets[i]);
        }
    }

    /// <summary>
    /// 全ての処理領域オブジェクトを取り除く
    /// </summary>
    public void RemoveAllProcessAreaObject()
    {
        while (processAreaObjects.Count > 0)
            processAreaObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の処理領域オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<CameraAreaEditorObject> GetMyProcessAreaObjects()
    {
        return processAreaObjects;
    }

    /// <summary>
    /// 指定位置の自身の処理領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<CameraAreaEditorObject> GetAtMyProcessAreaObjects(Vector2Int position)
    {
        var targets = new List<CameraAreaEditorObject>();
        var stageObjects = manager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as CameraAreaEditorObject;

            if (!target)
                continue;

            if (!processAreaObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身の処理領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<CameraAreaEditorObject> GetRangeMyProcessAreaObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<CameraAreaEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as CameraAreaEditorObject;

            if (!target)
                continue;

            if (!processAreaObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 処理領域オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasProcessAreaObjects()
    {
        return GetMyProcessAreaObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置の処理領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtProcessAreaObjects(Vector2Int position)
    {
        return GetAtMyProcessAreaObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置の処理領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeProcessAreaObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyBackObjects(position, size).Count > 0;
    }

    #endregion

    #region プレイヤーオブジェクト

    /// <summary>
    /// プレイヤーが追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddPlayerObject(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + PlayerData.DEFAULT_GRID_SIZE.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + PlayerData.DEFAULT_GRID_SIZE.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// プレイヤーオブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <param name="moveDirection"> 移動方向 </param>
    /// <returns> 追加したオブジェクト </returns>
    public PlayerEditorObject AddPlayerObject(Vector2Int position, PlayerData.MoveDirection moveDirection)
    {
        var newItem = objectFactory.InstantiatePlayerObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.SetMoveDirection(moveDirection);
        newItem.DestroyOther();

        playerObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// プレイヤーオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemovePlayerObject(PlayerEditorObject item)
    {
        objectFactory.DestroyPlayerObject(item);

        if (!playerObjects.Contains(item))
            return false;

        playerObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置のプレイヤーオブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtPlayerObject(Vector2Int position)
    {
        var targets = GetAtMyPlayerObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemovePlayerObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置のプレイヤーオブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangePlayerObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyPlayerObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemovePlayerObject(targets[i]);
        }
    }

    /// <summary>
    /// 全てのプレイヤーオブジェクトを取り除く
    /// </summary>
    public void RemoveAllPlayerObject()
    {
        while (playerObjects.Count > 0)
            playerObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerEditorObject> GetMyPlayerObjects()
    {
        return playerObjects;
    }

    /// <summary>
    /// 指定位置の自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerEditorObject> GetAtMyPlayerObjects(Vector2Int position)
    {
        var targets = new List<PlayerEditorObject>();
        var stageObjects = manager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as PlayerEditorObject;

            if (!target)
                continue;

            if (!playerObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerEditorObject> GetRangeMyPlayerObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<PlayerEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as PlayerEditorObject;

            if (!target)
                continue;

            if (!playerObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// プレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasPlayerObjects()
    {
        return GetMyPlayerObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置のプレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtPlayerObjects(Vector2Int position)
    {
        return GetAtMyPlayerObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置のプレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangePlayerObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyPlayerObjects(position, size).Count > 0;
    }

    #endregion

    #region ゴールオブジェクト

    /// <summary>
    /// ゴールが追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddGoalObject(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + GoalData.DEFAULT_GRID_SIZE.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + GoalData.DEFAULT_GRID_SIZE.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// ゴールオブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加したオブジェクト </returns>
    public GoalEditorObject AddGoalObject(Vector2Int position)
    {
        var newItem = objectFactory.InstantiateGoalObject(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.DestroyOther();

        goalObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// ゴールオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveGoalObject(GoalEditorObject item)
    {
        objectFactory.DestroyGoalObject(item);

        if (!goalObjects.Contains(item))
            return false;

        goalObjects.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置のゴールオブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtGoalObject(Vector2Int position)
    {
        var targets = GetAtMyGoalObjects(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveGoalObject(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置のゴールオブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeGoalObjects(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyGoalObjects(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveGoalObject(targets[i]);
        }
    }

    /// <summary>
    /// 全てのゴールオブジェクトを取り除く
    /// </summary>
    public void RemoveAllGoalObject()
    {
        while (goalObjects.Count > 0)
            goalObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身のゴールオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<GoalEditorObject> GetMyGoalObjects()
    {
        return goalObjects;
    }

    /// <summary>
    /// 指定位置の自身のゴールオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<GoalEditorObject> GetAtMyGoalObjects(Vector2Int position)
    {
        var targets = new List<GoalEditorObject>();
        var stageObjects = manager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as GoalEditorObject;

            if (!target)
                continue;

            if (!goalObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身のゴールオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<GoalEditorObject> GetRangeMyGoalObjects(Vector2Int position, Vector2Int size)
    {
        var targets = new List<GoalEditorObject>();
        var stageObjects = manager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as GoalEditorObject;

            if (!target)
                continue;

            if (!goalObjects.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// ゴールオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasGoalObjects()
    {
        return GetMyGoalObjects().Count > 0;
    }

    /// <summary>
    /// 指定位置のゴールオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtGoalObjects(Vector2Int position)
    {
        return GetAtMyGoalObjects(position).Count > 0;
    }

    /// <summary>
    /// 指定位置のゴールオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeGoalObjects(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyGoalObjects(position, size).Count > 0;
    }

    #endregion

    #region 敵 1 オブジェクト

    /// <summary>
    /// 敵 1 が追加できるか
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加できるか </returns>
    public bool CheckCanAddEnemyObject_1(Vector2Int position)
    {
        var managerGridSize = manager.GetGridSize();

        if (position.x < 0)
            return false;

        if (position.y < 0)
            return false;

        if (position.x + EnemyData_1.DEFAULT_GRID_SIZE.x - 1 >= managerGridSize.x)
            return false;

        if (position.y + EnemyData_1.DEFAULT_GRID_SIZE.y - 1 >= managerGridSize.y)
            return false;

        return true;
    }

    /// <summary>
    /// 敵 1 オブジェクト追加
    /// </summary>
    /// <param name="position"> 追加位置 </param>
    /// <returns> 追加したオブジェクト </returns>
    public EnemyEditorObject_1 AddEnemyObject_1(Vector2Int position)
    {
        var newItem = objectFactory.InstantiateEnemyObject_1(manager, this);
        if (!newItem)
            return null;

        newItem.SetGridPosition(position);
        newItem.DestroyOther();

        enemyObjects_1.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 敵 1 オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    /// <returns> 自身から削除したか </returns>
    public bool RemoveEnemyObject_1(EnemyEditorObject_1 item)
    {
        objectFactory.DestroyEnemyObject_1(item);

        if (!enemyObjects_1.Contains(item))
            return false;

        enemyObjects_1.Remove(item);
        return true;
    }

    /// <summary>
    /// 指定位置の敵 1 オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    public void RemoveAtEnemyObject_1(Vector2Int position)
    {
        var targets = GetAtMyEnemyObjects_1(position);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveEnemyObject_1(targets[i]);
        }
    }

    /// <summary>
    /// 指定位置の敵 1 オブジェクトを取り除く
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    public void RemoveRangeEnemyObject_1(Vector2Int position, Vector2Int size)
    {
        var targets = GetRangeMyEnemyObjects_1(position, size);

        for (int i = 0; i < targets.Count; ++i)
        {
            RemoveEnemyObject_1(targets[i]);
        }
    }

    /// <summary>
    /// 全ての敵 1 オブジェクトを取り除く
    /// </summary>
    public void RemoveAllEnemyObject_1()
    {
        while (enemyObjects_1.Count > 0)
            enemyObjects_1[0].DestroyThis();
    }

    /// <summary>
    /// 自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyEditorObject_1> GetMyEnemyObjects_1()
    {
        return enemyObjects_1;
    }

    /// <summary>
    /// 指定位置の自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyEditorObject_1> GetAtMyEnemyObjects_1(Vector2Int position)
    {
        var targets = new List<EnemyEditorObject_1>();
        var stageObjects = manager.GetStageObjects(position);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as EnemyEditorObject_1;

            if (!target)
                continue;

            if (!enemyObjects_1.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 指定位置の自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyEditorObject_1> GetRangeMyEnemyObjects_1(Vector2Int position, Vector2Int size)
    {
        var targets = new List<EnemyEditorObject_1>();
        var stageObjects = manager.GetRangeStageObjects(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var target = stageObjects[i] as EnemyEditorObject_1;

            if (!target)
                continue;

            if (!enemyObjects_1.Contains(target))
                continue;

            if (targets.Contains(target))
                continue;

            targets.Add(target);
        }

        return targets;
    }

    /// <summary>
    /// 敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasEnemyObjects_1()
    {
        return GetMyEnemyObjects_1().Count > 0;
    }

    /// <summary>
    /// 指定位置の敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtEnemyObjects_1(Vector2Int position)
    {
        return GetAtMyEnemyObjects_1(position).Count > 0;
    }

    /// <summary>
    /// 指定位置の敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeEnemyObjects_1(Vector2Int position, Vector2Int size)
    {
        return GetRangeMyEnemyObjects_1(position, size).Count > 0;
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.ObjectsLayer;
    }

    #endregion
}
