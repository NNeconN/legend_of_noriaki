#if true

using System.Collections.Generic;
using UnityEngine;

public class StageObjectsLayer : StageObject
{
    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = new StageObject_Manager();

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        var isFailed = !objectManager.Initialize(this);

        isFailed = isFailed || !base.Initialize(stageManager, objectFactory);

        systemBitFlag.Pop(bitFlagEnable);

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        objectManager.UnInitialize();

        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager.RemoveObjectsLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 成功したか </returns>
    public bool SetData(ObjectsLayerData data)
    {
        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        if (objectManager == null)
            return false;

        for (int i = 0; i < data.BackDatas.Count; ++i)
            objectManager.AddBackObject(data.BackDatas[i]);

        for (int i = 0; i < data.ProcessAreaDatas.Count; ++i)
            objectManager.AddCameraAreaObject(data.ProcessAreaDatas[i]);

        for (int i = 0; i < data.PlayerDatas.Count; ++i)
            objectManager.AddPlayerObject(data.PlayerDatas[i]);

        for (int i = 0; i < data.GoalDatas.Count; ++i)
            objectManager.AddGoalObject(data.GoalDatas[i]);

        for (int i = 0; i < data.EnemyDatas_1.Count; ++i)
            objectManager.AddEnemyObject_1(data.EnemyDatas_1[i]);

        return true;
    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        objectManager.UpdateProcess();
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        objectManager.FixedUpdateProcess();
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        objectManager.LateUpdateProcess();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

    #endregion

    #region 背景オブジェクト

    /// <summary>
    /// 背景オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public BackStageObject AddBackObject(BackData data)
    {
        return objectManager.AddBackObject(data);
    }

    /// <summary>
    /// 背景オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBackObject(BackStageObject item)
    {
        objectManager.RemoveBackObject(item);
    }

    /// <summary>
    /// 全ての背景オブジェクトを取り除く
    /// </summary>
    public void RemoveAllBackObject()
    {
        objectManager.RemoveAllBackObject();
    }

    /// <summary>
    /// 自身の背景オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<BackStageObject> GetMyBackObjects()
    {
        return objectManager.GetMyBackObjects();
    }

    /// <summary>
    /// 指定位置の自身の背景オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<BackStageObject> GetAtMyBackObjects(Vector2Int position)
    {
        return objectManager.GetAtMyBackObjects(position);
    }

    /// <summary>
    /// 指定位置の自身の背景オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<BackStageObject> GetRangeMyBackObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyBackObjects(position, size);
    }

    /// <summary>
    /// 背景オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasBackObjects()
    {
        return objectManager.CheckHasBackObjects();
    }

    /// <summary>
    /// 指定位置の背景オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtBackObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtBackObjects(position);
    }

    /// <summary>
    /// 指定位置の背景オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeBackObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeBackObjects(position, size);
    }

    #endregion

    #region カメラ領域オブジェクト

    /// <summary>
    /// カメラ領域オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public CameraAreaStageObject AddCameraAreaObject(CameraAreaData data)
    {
        return objectManager.AddCameraAreaObject(data);
    }

    /// <summary>
    /// カメラ領域オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveCameraAreaObject(CameraAreaStageObject item)
    {
        objectManager.RemoveCameraAreaObject(item);
    }

    /// <summary>
    /// 全てのカメラ領域オブジェクトを取り除く
    /// </summary>
    public void RemoveAllCameraAreaObject()
    {
        objectManager.RemoveAllCameraAreaObject();
    }

    /// <summary>
    /// 自身のカメラ領域オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<CameraAreaStageObject> GetMyCameraAreaObjects()
    {
        return objectManager.GetMyCameraAreaObjects();
    }

    /// <summary>
    /// 指定位置の自身のカメラ領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<CameraAreaStageObject> GetAtMyCameraAreaObjects(Vector2Int position)
    {
        return objectManager.GetAtMyCameraAreaObjects(position);
    }

    /// <summary>
    /// 指定位置の自身のカメラ領域オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<CameraAreaStageObject> GetRangeMyCameraAreaObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyCameraAreaObjects(position, size);
    }

    /// <summary>
    /// カメラ領域オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasCameraAreaObjects()
    {
        return objectManager.CheckHasCameraAreaObjects();
    }

    /// <summary>
    /// 指定位置のカメラ領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtCameraAreaObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtCameraAreaObjects(position);
    }

    /// <summary>
    /// 指定位置のカメラ領域オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeCameraAreaObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeCameraAreaObjects(position, size);
    }

    #endregion

    #region プレイヤーオブジェクト

    /// <summary>
    /// プレイヤーオブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public PlayerStageObject AddPlayerObject(PlayerData data)
    {
        return objectManager.AddPlayerObject(data);
    }

    /// <summary>
    /// プレイヤーオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemovePlayerObject(PlayerStageObject item)
    {
        objectManager.RemovePlayerObject(item);
    }

    /// <summary>
    /// 全てのプレイヤーオブジェクトを取り除く
    /// </summary>
    public void RemoveAllPlayerObject()
    {
        objectManager.RemoveAllPlayerObject();
    }

    /// <summary>
    /// 自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerStageObject> GetMyPlayerObjects()
    {
        return objectManager.GetMyPlayerObjects();
    }

    /// <summary>
    /// 指定位置の自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerStageObject> GetAtMyPlayerObjects(Vector2Int position)
    {
        return objectManager.GetAtMyPlayerObjects(position);
    }

    /// <summary>
    /// 指定位置の自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerStageObject> GetRangeMyPlayerObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyPlayerObjects(position, size);
    }

    /// <summary>
    /// プレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasPlayerObjects()
    {
        return objectManager.CheckHasPlayerObjects();
    }

    /// <summary>
    /// 指定位置のプレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtPlayerObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtPlayerObjects(position);
    }

    /// <summary>
    /// 指定位置のプレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangePlayerObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangePlayerObjects(position, size);
    }

    #endregion

    #region ゴールオブジェクト

    /// <summary>
    /// ゴールオブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public GoalStageObject AddGoalObject(GoalData data)
    {
        return objectManager.AddGoalObject(data);
    }

    /// <summary>
    /// ゴールオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveGoalObject(GoalStageObject item)
    {
        objectManager.RemoveGoalObject(item);
    }

    /// <summary>
    /// 全てのゴールオブジェクトを取り除く
    /// </summary>
    public void RemoveAllGoalObject()
    {
        objectManager.RemoveAllGoalObject();
    }

    /// <summary>
    /// 自身のゴールオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<GoalStageObject> GetMyGoalObjects()
    {
        return objectManager.GetMyGoalObjects();
    }

    /// <summary>
    /// 指定位置の自身のゴールオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<GoalStageObject> GetAtMyGoalObjects(Vector2Int position)
    {
        return objectManager.GetAtMyGoalObjects(position);
    }

    /// <summary>
    /// 指定位置の自身のゴールオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<GoalStageObject> GetRangeMyGoalObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyGoalObjects(position, size);
    }

    /// <summary>
    /// ゴールオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasGoalObjects()
    {
        return objectManager.CheckHasGoalObjects();
    }

    /// <summary>
    /// 指定位置のゴールオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtGoalObjects(Vector2Int position)
    {
        return objectManager.CheckHasAtGoalObjects(position);
    }

    /// <summary>
    /// 指定位置のゴールオブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeGoalObjects(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeGoalObjects(position, size);
    }

    #endregion

    #region 敵 1 オブジェクト

    /// <summary>
    /// 敵 1 オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public EnemyStageObject_1 AddEnemyObject_1(EnemyData_1 data)
    {
        return objectManager.AddEnemyObject_1(data);
    }

    /// <summary>
    /// 敵 1 オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveEnemyObject_1(EnemyStageObject_1 item)
    {
        objectManager.RemoveEnemyObject_1(item);
    }

    /// <summary>
    /// 全ての敵 1 オブジェクトを取り除く
    /// </summary>
    public void RemoveAllEnemyObject_1()
    {
        objectManager.RemoveAllEnemyObject_1();
    }

    /// <summary>
    /// 自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyStageObject_1> GetMyEnemyObjects_1()
    {
        return objectManager.GetMyEnemyObjects_1();
    }

    /// <summary>
    /// 指定位置の自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyStageObject_1> GetAtMyEnemyObjects_1(Vector2Int position)
    {
        return objectManager.GetAtMyEnemyObjects_1(position);
    }

    /// <summary>
    /// 指定位置の自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyStageObject_1> GetRangeMyEnemyObjects_1(Vector2Int position, Vector2Int size)
    {
        return objectManager.GetRangeMyEnemyObjects_1(position, size);
    }

    /// <summary>
    /// 敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasEnemyObjects_1()
    {
        return objectManager.CheckHasEnemyObjects_1();
    }

    /// <summary>
    /// 指定位置の敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasAtEnemyObjects_1(Vector2Int position)
    {
        return objectManager.CheckHasAtEnemyObjects_1(position);
    }

    /// <summary>
    /// 指定位置の敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <param name="position"> 指定位置 </param>
    /// <param name="size"> 指定サイズ </param>
    /// <returns> 所持しているか </returns>
    public bool CheckHasRangeEnemyObjects_1(Vector2Int position, Vector2Int size)
    {
        return objectManager.CheckHasRangeEnemyObjects_1(position, size);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.ObjectsLayer;
    }

    #endregion

    #region 子クラス

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> 背景オブジェクト </summary>
        public List<BackStageObject> backObjects = new List<BackStageObject>();

        /// <summary> カメラ領域オブジェクト群 </summary>
        public List<CameraAreaStageObject> cameraAreaObjects = new List<CameraAreaStageObject>();

        /// <summary> プレイヤーオブジェクト群 </summary>
        public List<PlayerStageObject> playerObjects = new List<PlayerStageObject>();

        /// <summary> ゴールオブジェクト群 </summary>
        public List<GoalStageObject> goalObjects = new List<GoalStageObject>();

        /// <summary> 敵 1 オブジェクト群 </summary>
        public List<EnemyStageObject_1> enemyObjects_1 = new List<EnemyStageObject_1>();
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> オブジェクトレイヤー </summary>
        StageObjectsLayer objectsLayer = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = new StageObjects();

        #region 初期化・解放処理関係

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <param name="objectsLayer"> ブロックレイヤー </param>
        /// <returns> 成功した </returns>
        public bool Initialize(StageObjectsLayer objectsLayer)
        {
            if (objectsLayer)
                this.objectsLayer = objectsLayer;

            if (!this.objectsLayer)
                return false;

            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAllBackObject();
            RemoveAllCameraAreaObject();
            RemoveAllPlayerObject();
            RemoveAllGoalObject();
            RemoveAllEnemyObject_1();
        }

        #endregion

        #region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.cameraAreaObjects.Count; ++i)
                stageObjects.cameraAreaObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.backObjects.Count; ++i)
                stageObjects.backObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.playerObjects.Count; ++i)
                stageObjects.playerObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.goalObjects.Count; ++i)
                stageObjects.goalObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.enemyObjects_1.Count; ++i)
                stageObjects.enemyObjects_1[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.cameraAreaObjects.Count; ++i)
                stageObjects.cameraAreaObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.backObjects.Count; ++i)
                stageObjects.backObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.playerObjects.Count; ++i)
                stageObjects.playerObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.goalObjects.Count; ++i)
                stageObjects.goalObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.enemyObjects_1.Count; ++i)
                stageObjects.enemyObjects_1[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.cameraAreaObjects.Count; ++i)
                stageObjects.cameraAreaObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.backObjects.Count; ++i)
                stageObjects.backObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.playerObjects.Count; ++i)
                stageObjects.playerObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.goalObjects.Count; ++i)
                stageObjects.goalObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.enemyObjects_1.Count; ++i)
                stageObjects.enemyObjects_1[i].LateUpdateProcess();
        }

        #endregion

        #region 背景オブジェクト

        /// <summary>
        /// 背景オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public BackStageObject AddBackObject(BackData data)
        {
            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateBackObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyBackObject(newItem);

                return null;
            }

            if (!stageObjects.backObjects.Contains(newItem))
                stageObjects.backObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 背景オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveBackObject(BackStageObject item)
        {
            var objectFactory = objectsLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.backObjects.Contains(item))
                stageObjects.backObjects.Remove(item);

            objectFactory.DestroyBackObject(item);
        }

        /// <summary>
        /// 全ての背景オブジェクトを取り除く
        /// </summary>
        public void RemoveAllBackObject()
        {
            while (stageObjects.backObjects.Count > 0)
                stageObjects.backObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身の背景オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<BackStageObject> GetMyBackObjects()
        {
            return stageObjects.backObjects;
        }

        /// <summary>
        /// 指定位置の自身の背景オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<BackStageObject> GetAtMyBackObjects(Vector2Int position)
        {
            var targets = new List<BackStageObject>();
            var stageObjects = objectsLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BackStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.backObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の背景オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<BackStageObject> GetRangeMyBackObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<BackStageObject>();
            var stageObjects = objectsLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BackStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.backObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 背景オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasBackObjects()
        {
            return GetMyBackObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置の背景オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtBackObjects(Vector2Int position)
        {
            return GetAtMyBackObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の背景オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeBackObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyBackObjects(position, size).Count > 0;
        }

        #endregion

        #region カメラ領域オブジェクト

        /// <summary>
        /// カメラ領域オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public CameraAreaStageObject AddCameraAreaObject(CameraAreaData data)
        {
            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateProcessAreaObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyProcessAreaObject(newItem);

                return null;
            }

            if (!stageObjects.cameraAreaObjects.Contains(newItem))
                stageObjects.cameraAreaObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// カメラ領域オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveCameraAreaObject(CameraAreaStageObject item)
        {
            var objectFactory = objectsLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.cameraAreaObjects.Contains(item))
                stageObjects.cameraAreaObjects.Remove(item);

            objectFactory.DestroyProcessAreaObject(item);
        }

        /// <summary>
        /// 全てのカメラ領域オブジェクトを取り除く
        /// </summary>
        public void RemoveAllCameraAreaObject()
        {
            while (stageObjects.cameraAreaObjects.Count > 0)
                stageObjects.cameraAreaObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のカメラ領域オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<CameraAreaStageObject> GetMyCameraAreaObjects()
        {
            return stageObjects.cameraAreaObjects;
        }

        /// <summary>
        /// 指定位置の自身のカメラ領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<CameraAreaStageObject> GetAtMyCameraAreaObjects(Vector2Int position)
        {
            var targets = new List<CameraAreaStageObject>();
            var stageObjects = objectsLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as CameraAreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.cameraAreaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のカメラ領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<CameraAreaStageObject> GetRangeMyCameraAreaObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<CameraAreaStageObject>();
            var stageObjects = objectsLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as CameraAreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.cameraAreaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// カメラ領域オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasCameraAreaObjects()
        {
            return GetMyCameraAreaObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のカメラ領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtCameraAreaObjects(Vector2Int position)
        {
            return GetAtMyCameraAreaObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のカメラ領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeCameraAreaObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyCameraAreaObjects(position, size).Count > 0;
        }

        #endregion

        #region プレイヤーオブジェクト

        /// <summary>
        /// プレイヤーオブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public PlayerStageObject AddPlayerObject(PlayerData data)
        {
            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiatePlayerObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyPlayerObject(newItem);

                return null;
            }

            if (!stageObjects.playerObjects.Contains(newItem))
                stageObjects.playerObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// プレイヤーオブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemovePlayerObject(PlayerStageObject item)
        {
            var objectFactory = objectsLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.playerObjects.Contains(item))
                stageObjects.playerObjects.Remove(item);

            objectFactory.DestroyPlayerObject(item);
        }

        /// <summary>
        /// 全てのプレイヤーオブジェクトを取り除く
        /// </summary>
        public void RemoveAllPlayerObject()
        {
            while (stageObjects.playerObjects.Count > 0)
                stageObjects.playerObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のプレイヤーオブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<PlayerStageObject> GetMyPlayerObjects()
        {
            return stageObjects.playerObjects;
        }

        /// <summary>
        /// 指定位置の自身のプレイヤーオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<PlayerStageObject> GetAtMyPlayerObjects(Vector2Int position)
        {
            var targets = new List<PlayerStageObject>();
            var stageObjects = objectsLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as PlayerStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.playerObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のプレイヤーオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<PlayerStageObject> GetRangeMyPlayerObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<PlayerStageObject>();
            var stageObjects = objectsLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as PlayerStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.playerObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// プレイヤーオブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasPlayerObjects()
        {
            return GetMyPlayerObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のプレイヤーオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtPlayerObjects(Vector2Int position)
        {
            return GetAtMyPlayerObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のプレイヤーオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangePlayerObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyPlayerObjects(position, size).Count > 0;
        }

        #endregion

        #region ゴールオブジェクト

        /// <summary>
        /// ゴールオブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public GoalStageObject AddGoalObject(GoalData data)
        {
            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateGoalObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyGoalObject(newItem);

                return null;
            }

            if (!stageObjects.goalObjects.Contains(newItem))
                stageObjects.goalObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// ゴールオブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveGoalObject(GoalStageObject item)
        {
            var objectFactory = objectsLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.goalObjects.Contains(item))
                stageObjects.goalObjects.Remove(item);

            objectFactory.DestroyGoalObject(item);
        }

        /// <summary>
        /// 全てのゴールオブジェクトを取り除く
        /// </summary>
        public void RemoveAllGoalObject()
        {
            while (stageObjects.goalObjects.Count > 0)
                stageObjects.goalObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のゴールオブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<GoalStageObject> GetMyGoalObjects()
        {
            return stageObjects.goalObjects;
        }

        /// <summary>
        /// 指定位置の自身のゴールオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<GoalStageObject> GetAtMyGoalObjects(Vector2Int position)
        {
            var targets = new List<GoalStageObject>();
            var stageObjects = objectsLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as GoalStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.goalObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のゴールオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<GoalStageObject> GetRangeMyGoalObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<GoalStageObject>();
            var stageObjects = objectsLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as GoalStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.goalObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// ゴールオブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasGoalObjects()
        {
            return GetMyGoalObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のゴールオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtGoalObjects(Vector2Int position)
        {
            return GetAtMyGoalObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のゴールオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeGoalObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyGoalObjects(position, size).Count > 0;
        }

        #endregion

        #region 敵 1 オブジェクト

        /// <summary>
        /// 敵 1 オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public EnemyStageObject_1 AddEnemyObject_1(EnemyData_1 data)
        {
            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateEnemyObject_1(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyEnemyObject_1(newItem);

                return null;
            }

            if (!stageObjects.enemyObjects_1.Contains(newItem))
                stageObjects.enemyObjects_1.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 敵 1 オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveEnemyObject_1(EnemyStageObject_1 item)
        {
            var objectFactory = objectsLayer.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.enemyObjects_1.Contains(item))
                stageObjects.enemyObjects_1.Remove(item);

            objectFactory.DestroyEnemyObject_1(item);
        }

        /// <summary>
        /// 全ての敵 1 オブジェクトを取り除く
        /// </summary>
        public void RemoveAllEnemyObject_1()
        {
            while (stageObjects.enemyObjects_1.Count > 0)
                stageObjects.enemyObjects_1[0].DestroyThis();
        }

        /// <summary>
        /// 自身の敵 1 オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<EnemyStageObject_1> GetMyEnemyObjects_1()
        {
            return stageObjects.enemyObjects_1;
        }

        /// <summary>
        /// 指定位置の自身の敵 1 オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<EnemyStageObject_1> GetAtMyEnemyObjects_1(Vector2Int position)
        {
            var targets = new List<EnemyStageObject_1>();
            var stageObjects = objectsLayer.stageManager.GetAtGrid(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as EnemyStageObject_1;

                if (!target)
                    continue;

                if (!this.stageObjects.enemyObjects_1.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の敵 1 オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<EnemyStageObject_1> GetRangeMyEnemyObjects_1(Vector2Int position, Vector2Int size)
        {
            var targets = new List<EnemyStageObject_1>();
            var stageObjects = objectsLayer.stageManager.GetRangeGrid(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as EnemyStageObject_1;

                if (!target)
                    continue;

                if (!this.stageObjects.enemyObjects_1.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 敵 1 オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasEnemyObjects_1()
        {
            return GetMyEnemyObjects_1().Count > 0;
        }

        /// <summary>
        /// 指定位置の敵 1 オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtEnemyObjects_1(Vector2Int position)
        {
            return GetAtMyEnemyObjects_1(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の敵 1 オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeEnemyObjects_1(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyEnemyObjects_1(position, size).Count > 0;
        }

        #endregion
    }

    #endregion
}

#elif false

using System.Collections.Generic;
using UnityEngine;

public class StageObjectsLayer : StageObject
{
    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = null;

    /// <summary> オブジェクトマネージャー </summary>
    public StageObject_Manager ObjectManager { get => objectManager; }

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        if (objectManager == null)
            objectManager = new StageObject_Manager(this);

        UnInitialize();

        objectManager.Initialize();

        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        objectManager.UnInitialize();

        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager?.ObjectManager?.RemoveObjectsLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 成功したか </returns>
    public bool SetData(ObjectsLayerData data)
    {
        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        transform.position = CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        if (objectManager == null)
            return false;

        for (int i = 0; i < data.BackDatas.Count; ++i)
            objectManager.AddBackObject(data.BackDatas[i]);

        for (int i = 0; i < data.ProcessAreaDatas.Count; ++i)
            objectManager.AddCameraAreaObject(data.ProcessAreaDatas[i]);

        for (int i = 0; i < data.PlayerDatas.Count; ++i)
            objectManager.AddPlayerObject(data.PlayerDatas[i]);

        for (int i = 0; i < data.GoalDatas.Count; ++i)
            objectManager.AddGoalObject(data.GoalDatas[i]);

        for (int i = 0; i < data.EnemyDatas_1.Count; ++i)
            objectManager.AddEnemyObject_1(data.EnemyDatas_1[i]);

        return true;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        objectManager.UpdateProcess();
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        objectManager.FixedUpdateProcess();
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        objectManager.LateUpdateProcess();
    }

#endregion

#region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.ObjectsLayer;
    }

#endregion

#region 子クラス

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> 背景オブジェクト </summary>
        public List<BackStageObject> backObjects = new List<BackStageObject>();

        /// <summary> カメラ領域オブジェクト群 </summary>
        public List<CameraAreaStageObject> cameraAreaObjects = new List<CameraAreaStageObject>();

        /// <summary> プレイヤーオブジェクト群 </summary>
        public List<PlayerStageObject> playerObjects = new List<PlayerStageObject>();

        /// <summary> ゴールオブジェクト群 </summary>
        public List<GoalStageObject> goalObjects = new List<GoalStageObject>();

        /// <summary> 敵 1 オブジェクト群 </summary>
        public List<EnemyStageObject_1> enemyObjects_1 = new List<EnemyStageObject_1>();
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> オブジェクトレイヤー </summary>
        StageObjectsLayer objectsLayer = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = null;

#region 初期化・解放処理関係

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="objectsLayer"> オブジェクトレイヤー </param>
        public StageObject_Manager(StageObjectsLayer objectsLayer)
        {
            this.objectsLayer = objectsLayer;
            stageObjects = new StageObjects();
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Initialize()
        {
            UnInitialize();
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAllBackObject();
            RemoveAllCameraAreaObject();
            RemoveAllPlayerObject();
            RemoveAllGoalObject();
            RemoveAllEnemyObject_1();
        }

#endregion

#region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.cameraAreaObjects.Count; ++i)
                stageObjects.cameraAreaObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.backObjects.Count; ++i)
                stageObjects.backObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.playerObjects.Count; ++i)
                stageObjects.playerObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.goalObjects.Count; ++i)
                stageObjects.goalObjects[i].UpdateProcess();

            for (int i = 0; i < stageObjects.enemyObjects_1.Count; ++i)
                stageObjects.enemyObjects_1[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.cameraAreaObjects.Count; ++i)
                stageObjects.cameraAreaObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.backObjects.Count; ++i)
                stageObjects.backObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.playerObjects.Count; ++i)
                stageObjects.playerObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.goalObjects.Count; ++i)
                stageObjects.goalObjects[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.enemyObjects_1.Count; ++i)
                stageObjects.enemyObjects_1[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.cameraAreaObjects.Count; ++i)
                stageObjects.cameraAreaObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.backObjects.Count; ++i)
                stageObjects.backObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.playerObjects.Count; ++i)
                stageObjects.playerObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.goalObjects.Count; ++i)
                stageObjects.goalObjects[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.enemyObjects_1.Count; ++i)
                stageObjects.enemyObjects_1[i].LateUpdateProcess();
        }

#endregion

#region 背景オブジェクト

        /// <summary>
        /// 背景オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public BackStageObject AddBackObject(BackData data)
        {
            if (!objectsLayer)
                return null;

            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateBackObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyBackObject(newItem);

                return null;
            }

            if (!stageObjects.backObjects.Contains(newItem))
                stageObjects.backObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 背景オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveBackObject(BackStageObject item)
        {
            var objectFactory = objectsLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.backObjects.Contains(item))
                stageObjects.backObjects.Remove(item);

            objectFactory.DestroyBackObject(item);
        }

        /// <summary>
        /// 全ての背景オブジェクトを取り除く
        /// </summary>
        public void RemoveAllBackObject()
        {
            while (stageObjects.backObjects.Count > 0)
                stageObjects.backObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身の背景オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<BackStageObject> GetMyBackObjects()
        {
            return stageObjects.backObjects;
        }

        /// <summary>
        /// 指定位置の自身の背景オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<BackStageObject> GetAtMyBackObjects(Vector2Int position)
        {
            var targets = new List<BackStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BackStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.backObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の背景オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<BackStageObject> GetRangeMyBackObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<BackStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as BackStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.backObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 背景オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasBackObjects()
        {
            return GetMyBackObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置の背景オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtBackObjects(Vector2Int position)
        {
            return GetAtMyBackObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の背景オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeBackObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyBackObjects(position, size).Count > 0;
        }

#endregion

#region カメラ領域オブジェクト

        /// <summary>
        /// カメラ領域オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public CameraAreaStageObject AddCameraAreaObject(CameraAreaData data)
        {
            if (!objectsLayer)
                return null;

            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateProcessAreaObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyProcessAreaObject(newItem);

                return null;
            }

            if (!stageObjects.cameraAreaObjects.Contains(newItem))
                stageObjects.cameraAreaObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// カメラ領域オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveCameraAreaObject(CameraAreaStageObject item)
        {
            var objectFactory = objectsLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.cameraAreaObjects.Contains(item))
                stageObjects.cameraAreaObjects.Remove(item);

            objectFactory.DestroyProcessAreaObject(item);
        }

        /// <summary>
        /// 全てのカメラ領域オブジェクトを取り除く
        /// </summary>
        public void RemoveAllCameraAreaObject()
        {
            while (stageObjects.cameraAreaObjects.Count > 0)
                stageObjects.cameraAreaObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のカメラ領域オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<CameraAreaStageObject> GetMyProcessAreaObjects()
        {
            return stageObjects.cameraAreaObjects;
        }

        /// <summary>
        /// 指定位置の自身のカメラ領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<CameraAreaStageObject> GetAtMyProcessAreaObjects(Vector2Int position)
        {
            var targets = new List<CameraAreaStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as CameraAreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.cameraAreaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のカメラ領域オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<CameraAreaStageObject> GetRangeMyProcessAreaObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<CameraAreaStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as CameraAreaStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.cameraAreaObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// カメラ領域オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasProcessAreaObjects()
        {
            return GetMyProcessAreaObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のカメラ領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtProcessAreaObjects(Vector2Int position)
        {
            return GetAtMyProcessAreaObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のカメラ領域オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeProcessAreaObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyProcessAreaObjects(position, size).Count > 0;
        }

#endregion

#region プレイヤーオブジェクト

        /// <summary>
        /// プレイヤーオブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public PlayerStageObject AddPlayerObject(PlayerData data)
        {
            if (!objectsLayer)
                return null;

            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiatePlayerObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyPlayerObject(newItem);

                return null;
            }

            if (!stageObjects.playerObjects.Contains(newItem))
                stageObjects.playerObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// プレイヤーオブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemovePlayerObject(PlayerStageObject item)
        {
            var objectFactory = objectsLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.playerObjects.Contains(item))
                stageObjects.playerObjects.Remove(item);

            objectFactory.DestroyPlayerObject(item);
        }

        /// <summary>
        /// 全てのプレイヤーオブジェクトを取り除く
        /// </summary>
        public void RemoveAllPlayerObject()
        {
            while (stageObjects.playerObjects.Count > 0)
                stageObjects.playerObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のプレイヤーオブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<PlayerStageObject> GetMyPlayerObjects()
        {
            return stageObjects.playerObjects;
        }

        /// <summary>
        /// 指定位置の自身のプレイヤーオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<PlayerStageObject> GetAtMyPlayerObjects(Vector2Int position)
        {
            var targets = new List<PlayerStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as PlayerStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.playerObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のプレイヤーオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<PlayerStageObject> GetRangeMyPlayerObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<PlayerStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as PlayerStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.playerObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// プレイヤーオブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasPlayerObjects()
        {
            return GetMyPlayerObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のプレイヤーオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtPlayerObjects(Vector2Int position)
        {
            return GetAtMyPlayerObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のプレイヤーオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangePlayerObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyPlayerObjects(position, size).Count > 0;
        }

#endregion

#region ゴールオブジェクト

        /// <summary>
        /// ゴールオブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public GoalStageObject AddGoalObject(GoalData data)
        {
            if (!objectsLayer)
                return null;

            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateGoalObject(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyGoalObject(newItem);

                return null;
            }

            if (!stageObjects.goalObjects.Contains(newItem))
                stageObjects.goalObjects.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// ゴールオブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveGoalObject(GoalStageObject item)
        {
            var objectFactory = objectsLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.goalObjects.Contains(item))
                stageObjects.goalObjects.Remove(item);

            objectFactory.DestroyGoalObject(item);
        }

        /// <summary>
        /// 全てのゴールオブジェクトを取り除く
        /// </summary>
        public void RemoveAllGoalObject()
        {
            while (stageObjects.goalObjects.Count > 0)
                stageObjects.goalObjects[0].DestroyThis();
        }

        /// <summary>
        /// 自身のゴールオブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<GoalStageObject> GetMyGoalObjects()
        {
            return stageObjects.goalObjects;
        }

        /// <summary>
        /// 指定位置の自身のゴールオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<GoalStageObject> GetAtMyGoalObjects(Vector2Int position)
        {
            var targets = new List<GoalStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as GoalStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.goalObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身のゴールオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<GoalStageObject> GetRangeMyGoalObjects(Vector2Int position, Vector2Int size)
        {
            var targets = new List<GoalStageObject>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as GoalStageObject;

                if (!target)
                    continue;

                if (!this.stageObjects.goalObjects.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// ゴールオブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasGoalObjects()
        {
            return GetMyGoalObjects().Count > 0;
        }

        /// <summary>
        /// 指定位置のゴールオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtGoalObjects(Vector2Int position)
        {
            return GetAtMyGoalObjects(position).Count > 0;
        }

        /// <summary>
        /// 指定位置のゴールオブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeGoalObjects(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyGoalObjects(position, size).Count > 0;
        }

#endregion

#region 敵 1 オブジェクト

        /// <summary>
        /// 敵 1 オブジェクト追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 追加したオブジェクト </returns>
        public EnemyStageObject_1 AddEnemyObject_1(EnemyData_1 data)
        {
            if (!objectsLayer)
                return null;

            var stageManager = objectsLayer.stageManager;
            var objectFactory = objectsLayer.objectFactory;

            if (!stageManager || !objectFactory)
                return null;

            var newItem = objectFactory.InstantiateEnemyObject_1(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data, objectsLayer))
            {
                objectFactory.DestroyEnemyObject_1(newItem);

                return null;
            }

            if (!stageObjects.enemyObjects_1.Contains(newItem))
                stageObjects.enemyObjects_1.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 敵 1 オブジェクトを取り除く
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveEnemyObject_1(EnemyStageObject_1 item)
        {
            var objectFactory = objectsLayer?.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.enemyObjects_1.Contains(item))
                stageObjects.enemyObjects_1.Remove(item);

            objectFactory.DestroyEnemyObject_1(item);
        }

        /// <summary>
        /// 全ての敵 1 オブジェクトを取り除く
        /// </summary>
        public void RemoveAllEnemyObject_1()
        {
            while (stageObjects.enemyObjects_1.Count > 0)
                stageObjects.enemyObjects_1[0].DestroyThis();
        }

        /// <summary>
        /// 自身の敵 1 オブジェクト群を取得
        /// </summary>
        /// <returns> オブジェクト群 </returns>
        public List<EnemyStageObject_1> GetMyEnemyObjects_1()
        {
            return stageObjects.enemyObjects_1;
        }

        /// <summary>
        /// 指定位置の自身の敵 1 オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> オブジェクト群 </returns>
        public List<EnemyStageObject_1> GetAtMyEnemyObjects_1(Vector2Int position)
        {
            var targets = new List<EnemyStageObject_1>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetAt(position);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as EnemyStageObject_1;

                if (!target)
                    continue;

                if (!this.stageObjects.enemyObjects_1.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 指定位置の自身の敵 1 オブジェクト群を取得
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> オブジェクト群 </returns>
        public List<EnemyStageObject_1> GetRangeMyEnemyObjects_1(Vector2Int position, Vector2Int size)
        {
            var targets = new List<EnemyStageObject_1>();
            var stageObjects = objectsLayer?.stageManager?.GridHasObjects?.GetRange(position, size);

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var target = stageObjects[i] as EnemyStageObject_1;

                if (!target)
                    continue;

                if (!this.stageObjects.enemyObjects_1.Contains(target))
                    continue;

                if (targets.Contains(target))
                    continue;

                targets.Add(target);
            }

            return targets;
        }

        /// <summary>
        /// 敵 1 オブジェクトを所持しているか
        /// </summary>
        /// <returns> 所持しているか </returns>
        public bool CheckHasEnemyObjects_1()
        {
            return GetMyEnemyObjects_1().Count > 0;
        }

        /// <summary>
        /// 指定位置の敵 1 オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasAtEnemyObjects_1(Vector2Int position)
        {
            return GetAtMyEnemyObjects_1(position).Count > 0;
        }

        /// <summary>
        /// 指定位置の敵 1 オブジェクトを所持しているか
        /// </summary>
        /// <param name="position"> 指定位置 </param>
        /// <param name="size"> 指定サイズ </param>
        /// <returns> 所持しているか </returns>
        public bool CheckHasRangeEnemyObjects_1(Vector2Int position, Vector2Int size)
        {
            return GetRangeMyEnemyObjects_1(position, size).Count > 0;
        }

#endregion
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StageObjectsLayer : StageObject
{
    /// <summary> 背景オブジェクト </summary>
    List<BackStageObject> backObjects = new List<BackStageObject>();

    /// <summary> 処理領域オブジェクト群 </summary>
    List<ProcessAreaStageObject> processAreaObjects = new List<ProcessAreaStageObject>();

    /// <summary> プレイヤーオブジェクト群 </summary>
    List<PlayerStageObject> playerObjects = new List<PlayerStageObject>();

    /// <summary> ゴールオブジェクト群 </summary>
    List<GoalStageObject> goalObjects = new List<GoalStageObject>();

    /// <summary> 敵 1 オブジェクト群 </summary>
    List<EnemyStageObject_1> enemyObjects_1 = new List<EnemyStageObject_1>();

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        RemoveAllBackObject();
        RemoveAllProcessAreaObject();
        RemoveAllPlayerObject();
        RemoveAllGoalObject();
        RemoveAllEnemyObject_1();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        stageManager.RemoveObjectsLayer(this);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="data"> データ </param>
    public void SetData(ObjectsLayerData data)
    {
        UnInitialize();

        systemBitFlag.Pop(bitFlagEnable);

        transform.position = CalculatePosition(Vector2Int.zero);
        transform.parent = stageManager.transform;

        for (int i = 0; i < data.BackDatas.Count; ++i)
            AddBackObject(data.BackDatas[i]);

        for (int i = 0; i < data.ProcessAreaDatas.Count; ++i)
            AddProcessAreaObject(data.ProcessAreaDatas[i]);

        for (int i = 0; i < data.PlayerDatas.Count; ++i)
            AddPlayerObject(data.PlayerDatas[i]);

        for (int i = 0; i < data.GoalDatas.Count; ++i)
            AddGoalObject(data.GoalDatas[i]);

        for (int i = 0; i < data.EnemyDatas_1.Count; ++i)
            AddEnemyObject_1(data.EnemyDatas_1[i]);
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < processAreaObjects.Count; ++i)
            processAreaObjects[i].UpdateProcess(managerCondition);

        for (int i = 0; i < backObjects.Count; ++i)
            backObjects[i].UpdateProcess(managerCondition);

        for (int i = 0; i < playerObjects.Count; ++i)
            playerObjects[i].UpdateProcess(managerCondition);

        for (int i = 0; i < goalObjects.Count; ++i)
            goalObjects[i].UpdateProcess(managerCondition);

        for (int i = 0; i < enemyObjects_1.Count; ++i)
            enemyObjects_1[i].UpdateProcess(managerCondition);
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < processAreaObjects.Count; ++i)
            processAreaObjects[i].FixedUpdateProcess(managerCondition);

        for (int i = 0; i < backObjects.Count; ++i)
            backObjects[i].FixedUpdateProcess(managerCondition);

        for (int i = 0; i < playerObjects.Count; ++i)
            playerObjects[i].FixedUpdateProcess(managerCondition);

        for (int i = 0; i < goalObjects.Count; ++i)
            goalObjects[i].FixedUpdateProcess(managerCondition);

        for (int i = 0; i < enemyObjects_1.Count; ++i)
            enemyObjects_1[i].FixedUpdateProcess(managerCondition);
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        for (int i = 0; i < processAreaObjects.Count; ++i)
            processAreaObjects[i].LateUpdateProcess(managerCondition);

        for (int i = 0; i < backObjects.Count; ++i)
            backObjects[i].LateUpdateProcess(managerCondition);

        for (int i = 0; i < playerObjects.Count; ++i)
            playerObjects[i].LateUpdateProcess(managerCondition);

        for (int i = 0; i < goalObjects.Count; ++i)
            goalObjects[i].LateUpdateProcess(managerCondition);

        for (int i = 0; i < enemyObjects_1.Count; ++i)
            enemyObjects_1[i].LateUpdateProcess(managerCondition);
    }

#endregion

#region グリッド関係

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        return new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
    }

#endregion

#region 背景オブジェクト

    /// <summary>
    /// 背景オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public BackStageObject AddBackObject(BackData data)
    {
        var newItem = objectFactory.InstantiateBackObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, this);

        if (!backObjects.Contains(newItem))
            backObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 背景オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBackObject(BackStageObject item)
    {
        if (!item)
            return;

        if (backObjects.Contains(item))
            backObjects.Remove(item);

        objectFactory.DestroyBackObject(item);
    }

    /// <summary>
    /// 全ての背景オブジェクトを取り除く
    /// </summary>
    public void RemoveAllBackObject()
    {
        while (backObjects.Count > 0)
            backObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の背景オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<BackStageObject> GetMyBackObjects()
    {
        return backObjects;
    }

    /// <summary>
    /// 背景オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasBackObjects()
    {
        return GetMyBackObjects().Count > 0;
    }

#endregion

#region 処理領域オブジェクト

    /// <summary>
    /// 処理領域オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public ProcessAreaStageObject AddProcessAreaObject(ProcessAreaData data)
    {
        var newItem = objectFactory.InstantiateProcessAreaObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, this);

        if (!processAreaObjects.Contains(newItem))
            processAreaObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 処理領域オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveProcessAreaObject(ProcessAreaStageObject item)
    {
        if (!item)
            return;

        if (processAreaObjects.Contains(item))
            processAreaObjects.Remove(item);

        objectFactory.DestroyProcessAreaObject(item);
    }

    /// <summary>
    /// 全ての処理領域オブジェクトを取り除く
    /// </summary>
    public void RemoveAllProcessAreaObject()
    {
        while (processAreaObjects.Count > 0)
            processAreaObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身の処理領域オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<ProcessAreaStageObject> GetMyProcessAreaObjects()
    {
        return processAreaObjects;
    }

    /// <summary>
    /// 処理領域オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasProcessAreaObjects()
    {
        return GetMyProcessAreaObjects().Count > 0;
    }

#endregion

#region プレイヤーオブジェクト

    /// <summary>
    /// プレイヤーオブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public PlayerStageObject AddPlayerObject(PlayerData data)
    {
        var newItem = objectFactory.InstantiatePlayerObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, this);

        if (!playerObjects.Contains(newItem))
            playerObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// プレイヤーオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemovePlayerObject(PlayerStageObject item)
    {
        if (!item)
            return;

        if (playerObjects.Contains(item))
            playerObjects.Remove(item);

        objectFactory.DestroyPlayerObject(item);
    }

    /// <summary>
    /// 全てのプレイヤーオブジェクトを取り除く
    /// </summary>
    public void RemoveAllPlayerObject()
    {
        while (playerObjects.Count > 0)
            playerObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身のプレイヤーオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<PlayerStageObject> GetMyPlayerObjects()
    {
        return playerObjects;
    }

    /// <summary>
    /// プレイヤーオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasPlayerObjects()
    {
        return GetMyPlayerObjects().Count > 0;
    }

#endregion

#region ゴールオブジェクト

    /// <summary>
    /// ゴールオブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public GoalStageObject AddGoalObject(GoalData data)
    {
        var newItem = objectFactory.InstantiateGoalObject(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, this);

        if (!goalObjects.Contains(newItem))
            goalObjects.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// ゴールオブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveGoalObject(GoalStageObject item)
    {
        if (!item)
            return;

        if (goalObjects.Contains(item))
            goalObjects.Remove(item);

        objectFactory.DestroyGoalObject(item);
    }

    /// <summary>
    /// 全てのゴールオブジェクトを取り除く
    /// </summary>
    public void RemoveAllGoalObject()
    {
        while (goalObjects.Count > 0)
            goalObjects[0].DestroyThis();
    }

    /// <summary>
    /// 自身のゴールオブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<GoalStageObject> GetMyGoalObjects()
    {
        return goalObjects;
    }

    /// <summary>
    /// ゴールオブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasGoalObjects()
    {
        return GetMyGoalObjects().Count > 0;
    }

#endregion

#region 敵 1 オブジェクト

    /// <summary>
    /// 敵 1 オブジェクト追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 追加したオブジェクト </returns>
    public EnemyStageObject_1 AddEnemyObject_1(EnemyData_1 data)
    {
        var newItem = objectFactory.InstantiateEnemyObject_1(stageManager);

        if (!newItem)
            return null;

        newItem.SetData(data, this);

        if (!enemyObjects_1.Contains(newItem))
            enemyObjects_1.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 敵 1 オブジェクトを取り除く
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveEnemyObject_1(EnemyStageObject_1 item)
    {
        if (!item)
            return;

        if (enemyObjects_1.Contains(item))
            enemyObjects_1.Remove(item);

        objectFactory.DestroyEnemyObject_1(item);
    }

    /// <summary>
    /// 全ての敵 1 オブジェクトを取り除く
    /// </summary>
    public void RemoveAllEnemyObject_1()
    {
        while (enemyObjects_1.Count > 0)
            enemyObjects_1[0].DestroyThis();
    }

    /// <summary>
    /// 自身の敵 1 オブジェクト群を取得
    /// </summary>
    /// <returns> オブジェクト群 </returns>
    public List<EnemyStageObject_1> GetMyEnemyObject_1s()
    {
        return enemyObjects_1;
    }

    /// <summary>
    /// 敵 1 オブジェクトを所持しているか
    /// </summary>
    /// <returns> 所持しているか </returns>
    public bool CheckHasEnemyObject_1s()
    {
        return GetMyEnemyObject_1s().Count > 0;
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.ObjectsLayer;
    }

#endregion
}

#endif