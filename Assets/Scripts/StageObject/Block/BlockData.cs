using System;
using System.Collections.Generic;
using UnityEngine;

public class BlockData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        GridPosition = -1,
    }

    /// <summary>
    /// ブロックの種類
    /// </summary>
    public enum BlockType
    {
        Fixed,
        Red,
        Green,
        Blue,
    }

    /// <summary>
    /// ブロックの種類の数
    /// </summary>
    static public int BlockTypeNum { get => Enum.GetNames(typeof(BlockType)).Length; }

    #endregion

    #region static変数

    /// <summary>
    /// グリッド上の大きさ
    /// </summary>
    static Vector2Int defaultGridSize = new Vector2Int(1, 1);

    /// <summary>
    /// マス目判定用コライダーのずれ
    /// </summary>
    static Vector3 defaultGridColliderOffset = new Vector3(0.0f, 0.0f, -1.0f);

    /// <summary>
    /// マス目判定用コライダーの大きさ
    /// </summary>
    static Vector3 defaultGridColliderSize = new Vector3(0.95f, 0.95f, 2.0f);

    /// <summary>
    /// 位置のずれ
    /// </summary>
    static Vector3 defaultPositionOffset = new Vector3(0.5f, 0.5f, 0.0f);

    /// <summary>
    /// グリッド上の大きさ取得用
    /// </summary>
    public static Vector2Int DEFAULT_GRID_SIZE { get => defaultGridSize; }

    /// <summary>
    /// マス目判定用コライダーのずれ取得用
    /// </summary>
    public static Vector3 DEFAULT_GRID_COLLIDER_OFFSET { get => defaultGridColliderOffset; }

    /// <summary>
    /// マス目判定用コライダーの大きさ取得用
    /// </summary>
    public static Vector3 DEFAULT_GRID_COLLIDER_SIZE { get => defaultGridColliderSize; }

    /// <summary>
    /// 位置のずれ取得用
    /// </summary>
    public static Vector3 DEFAULT_POSITION_OFFSET { get => defaultPositionOffset; }

    #endregion

    /// <summary> グリッド上の位置 </summary>
    Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> グリッド上の位置設定・取得用 </summary>
    public Vector2Int GridPosition { get => gridPosition; set => gridPosition = value; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public BlockData()
    {
        gridPosition = Vector2Int.zero;
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public BlockData(BlockEditorObject target)
    {
        gridPosition = target.GetGridPosition();
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(BlockEditorObject target)
    {
        target.SetGridPosition(gridPosition);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        for (int indexX = 0; indexX < csvDatas[index].Length;)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][indexX], out dataID))
                break;

            switch (dataID)
            {
                case DataID.GridPosition:
                    gridPosition.x = int.Parse(csvDatas[index][indexX + 1]);
                    gridPosition.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                default:
                    ++indexX;
                    break;
            }
        }

        ++index;
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.Block.ToString() });

        csvDatas.Add(new string[] {
            DataID.GridPosition.ToString(),
            gridPosition.x.ToString(),
            gridPosition.y.ToString(),
        });
    }
}
