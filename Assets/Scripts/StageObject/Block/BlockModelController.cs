using UnityEngine;

/// <summary>
/// ブロックモデル操作用
/// </summary>
public class BlockModelController : MonoBehaviour
{
    /// <summary>
    /// 光の種類
    /// </summary>
    enum BrightType
    {
        None,
        Once,
        Loop,
    }

    /// <summary>
    /// マテリアルの種類
    /// </summary>
    enum MaterialType
    {
        Normal,
        Shield
    }

    /// <summary> 自身のメッシュレンダラー </summary>
    [SerializeField] MeshRenderer[] myMeshRenderers = new MeshRenderer[0];

    /// <summary> 光らせる色 </summary>
    [SerializeField] Color emissionColor = Color.white;

    /// <summary> 通常時のマテリアル </summary>
    [SerializeField] Material[] normalMaterials = new Material[0];

    /// <summary> シールド時のマテリアル </summary>
    [SerializeField] Material[] shieldMaterials = new Material[0];

    /// <summary> 光の種類 </summary>
    BrightType brightType = BrightType.None;

    /// <summary> 光の種類 </summary>
    MaterialType materialType = MaterialType.Normal;

    /// <summary> 点灯時間 </summary>
    [SerializeField] float brightTime = 1.0f;

    /// <summary> 光の割合 </summary>
    float brightRate = 0.0f;

    /// <summary> 現在のマテリアルインデックス番号 </summary>
    int materialIndex = 0;

    /// <summary> 光の割合取得用 </summary>
    public float BrightRate { get { return brightRate; } }

    /// <summary>
    /// マテリアル番号を設定
    /// </summary>
    /// <param name="index"> マテリアル番号 </param>
    public void SetMaterialIndex(int index)
    {
        switch (materialType)
        {
            case MaterialType.Normal:
                if (normalMaterials.Length < 1)
                    break;

                materialIndex = Mathf.Clamp(index, 0, normalMaterials.Length - 1);

                for (int i = 0, iLength = myMeshRenderers.Length; i < iLength; ++i)
                    myMeshRenderers[i].material = normalMaterials[materialIndex];
                break;
            case MaterialType.Shield:
                if (shieldMaterials.Length < 1)
                    break;

                materialIndex = Mathf.Clamp(index, 0, shieldMaterials.Length - 1);

                for (int i = 0, iLength = myMeshRenderers.Length; i < iLength; ++i)
                    myMeshRenderers[i].material = shieldMaterials[materialIndex];
                break;
        }
    }

    /// <summary>
    /// 通常マテリアルを設定する
    /// </summary>
    public void SetNormalMaterial()
    {
        if (normalMaterials.Length < 1)
            return;

        materialType = MaterialType.Normal;

        materialIndex = Mathf.Clamp(materialIndex, 0, normalMaterials.Length - 1);

        for (int i = 0, iLength = myMeshRenderers.Length; i < iLength; ++i)
            myMeshRenderers[i].material = normalMaterials[materialIndex];
    }

    /// <summary>
    /// シールドマテリアルを設定する
    /// </summary>
    public void SetShieldMaterial()
    {
        if (shieldMaterials.Length < 1)
            return;

        materialType = MaterialType.Shield;

        materialIndex = Mathf.Clamp(materialIndex, 0, shieldMaterials.Length - 1);

        for (int i = 0, iLength = myMeshRenderers.Length; i < iLength; ++i)
            myMeshRenderers[i].material = shieldMaterials[materialIndex];
    }

    /// <summary>
    /// 強制的に光らせるのをやめる
    /// </summary>
    public void ForcedFinishBright()
    {
        brightType = BrightType.None;
        brightRate = 0.0f;

        if (materialType != MaterialType.Normal)
            return;

        SetEmmisionColor(Color.black);
    }

    /// <summary>
    /// 光らせるのをやめる
    /// </summary>
    public void FinishBright()
    {
        switch (brightType)
        {
            case BrightType.Loop:
                break;
            default:
                return;
        }

        brightType = BrightType.Once;
    }

    /// <summary>
    /// 光らせる
    /// </summary>
    public void StartBright()
    {
        brightRate = 0.0f;
        brightType = BrightType.Loop;
    }

    /// <summary>
    /// 一度だけ光らせる
    /// </summary>
    public void StartBrightOnce()
    {
        brightRate = 0.0f;
        brightType = BrightType.Once;
    }

    /// <summary>
    /// 点灯時間設定
    /// </summary>
    /// <param name="time"> 点灯時間 </param>
    public void SetBrightTime(float time)
    {
        brightTime = time;
    }

    /// <summary>
    /// 光の更新
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    public void BrightUpdate(float deltaTime)
    {
        if (brightType == BrightType.None)
            return;

        if (myMeshRenderers.Length < 1 || brightTime <= 0.0f)
        {
            brightType = BrightType.None;
            return;
        }

        brightRate += deltaTime / brightTime;

        if (brightRate >= 1.0f)
        {
            if (brightType == BrightType.Loop)
                brightRate = brightRate % 1.0f;
            else
            {
                brightType = BrightType.None;
                brightRate = 1.0f;
            }
        }

        var rate = 1.0f - Mathf.Sin(brightRate * 180.0f * Mathf.Deg2Rad);
        var color = (Color.black * rate) + (emissionColor * (1.0f - rate));

        if (materialType != MaterialType.Normal)
            return;

        SetEmmisionColor(color);
    }

    /// <summary>
    /// 光の色設定
    /// </summary>
    /// <param name="color"> 光の色 </param>
    public void SetBrightColor(Color color)
    {
        emissionColor = color;
    }


    /// <summary>
    /// エミッションカラー設定
    /// </summary>
    /// <param name="color"> エミッションカラー </param>
    void SetEmmisionColor(Color color)
    {
        for (int i = 0; i < myMeshRenderers.Length; ++i)
        {
            var material = myMeshRenderers[i].material;
            material.EnableKeyword("_EMISSION");
            material.SetColor("_EmissionColor", color);
            myMeshRenderers[i].material = material;
        }
    }
}
