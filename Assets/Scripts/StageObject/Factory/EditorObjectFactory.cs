using System.Collections.Generic;
using UnityEngine;

public class EditorObjectFactory : MonoBehaviour
{
    #region プレハブ

    /// <summary> オブジェクトレイヤー Prefab </summary>
    [SerializeField] GameObject objectsLayer = null;

    /// <summary> ブロックレイヤー Prefab </summary>
    [SerializeField] GameObject blockLayer = null;

    /// <summary> 領域レイヤー Prefab </summary>
    [SerializeField] GameObject areaLayer = null;

    /// <summary> 背景 Prefab </summary>
    [SerializeField] GameObject backObject = null;

    /// <summary> 処理領域 Prefab </summary>
    [SerializeField] GameObject processAreaObject = null;

    /// <summary> プレイヤー Prefab </summary>
    [SerializeField] GameObject playerObject = null;

    /// <summary> 取っ手 Prefab </summary>
    [SerializeField] GameObject handleObject = null;

    /// <summary> ゴール Prefab </summary>
    [SerializeField] GameObject goalObject = null;

    /// <summary> 敵 1 Prefab </summary>
    [SerializeField] GameObject enemyObject_1 = null;

    /// <summary> ブロック Prefab </summary>
    [SerializeField] GameObject blockObject = null;

    /// <summary> 領域 Prefab </summary>
    [SerializeField] GameObject areaObject = null;

    #endregion

    #region プール

    /// <summary> オブジェクトレイヤー Pool </summary>
    [SerializeField] List<EditorObjectsLayer> objectsLayerPool = new List<EditorObjectsLayer>();

    /// <summary> ブロックレイヤー Pool </summary>
    [SerializeField] List<EditorBlockLayer> blockLayerPool = new List<EditorBlockLayer>();

    /// <summary> 領域レイヤー Pool </summary>
    [SerializeField] List<EditorAreaLayer> areaLayerPool = new List<EditorAreaLayer>();

    /// <summary> 背景 Pool </summary>
    [SerializeField] List<BackEditorObject> backObjectPool = new List<BackEditorObject>();

    /// <summary> 処理領域 Pool </summary>
    [SerializeField] List<CameraAreaEditorObject> processAreaObjectPool = new List<CameraAreaEditorObject>();

    /// <summary> プレイヤー Pool </summary>
    [SerializeField] List<PlayerEditorObject> playerObjectPool = new List<PlayerEditorObject>();

    /// <summary> 取っ手 Pool </summary>
    [SerializeField] List<HandleEditorObject> handleObjectPool = new List<HandleEditorObject>();

    /// <summary> ゴール Pool </summary>
    [SerializeField] List<GoalEditorObject> goalObjectPool = new List<GoalEditorObject>();

    /// <summary> 敵 1 Pool </summary>
    [SerializeField] List<EnemyEditorObject_1> enemyObject_1_Pool = new List<EnemyEditorObject_1>();

    /// <summary> ブロック Pool </summary>
    [SerializeField] List<BlockEditorObject> blockObjectPool = new List<BlockEditorObject>();

    /// <summary> 領域 Pool </summary>
    [SerializeField] List<AreaEditorObject> areaObjectPool = new List<AreaEditorObject>();

    #endregion

    #region オブジェクトレイヤー

    /// <summary>
    /// オブジェクトレイヤープールの開放
    /// </summary>
    public void ReleaseObjectsLayerPool()
    {
        GameObjectUtility.DestroyGameObject(ref objectsLayerPool);
    }

    /// <summary>
    /// オブジェクトレイヤーの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> オブジェクトレイヤー </returns>
    public EditorObjectsLayer InstantiateObjectsLayer(StageEditor_Manager manager)
    {
        if (objectsLayerPool.Count > 0)
        {
            var poolItem = objectsLayerPool[0];
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            objectsLayerPool.RemoveAt(0);

            poolItem.transform.parent = manager.transform;
            poolItem.transform.transform.localPosition = Vector3.zero;

            return poolItem;
        }

        if (!objectsLayer)
            return null;

        var newObject = Instantiate(objectsLayer);
        var newItem = newObject.GetComponent<EditorObjectsLayer>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// オブジェクトレイヤーの削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void DestroyObjectsLayer(EditorObjectsLayer item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!objectsLayerPool.Contains(item))
            objectsLayerPool.Add(item);
    }

    #endregion

    #region ブロックレイヤー

    /// <summary>
    /// ブロックレイヤープールの開放
    /// </summary>
    public void ReleaseBlockLayerPool()
    {
        GameObjectUtility.DestroyGameObject(ref blockLayerPool);
    }

    /// <summary>
    /// ブロックレイヤーの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> ブロックレイヤー </returns>
    public EditorBlockLayer InstantiateBlockLayer(StageEditor_Manager manager)
    {
        if (blockLayerPool.Count > 0)
        {
            var poolItem = blockLayerPool[0];
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            blockLayerPool.RemoveAt(0);

            poolItem.transform.parent = manager.transform;
            poolItem.transform.transform.localPosition = Vector3.zero;

            return poolItem;
        }

        if (!blockLayer)
            return null;

        var newObject = Instantiate(blockLayer);
        var newItem = newObject.GetComponent<EditorBlockLayer>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// ブロックレイヤーの削除
    /// </summary>
    /// <param name="item"> 対象ブロック </param>
    public void DestroyBlockLayer(EditorBlockLayer item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!blockLayerPool.Contains(item))
            blockLayerPool.Add(item);
    }

    #endregion

    #region 領域レイヤー

    /// <summary>
    /// 領域レイヤープールの開放
    /// </summary>
    public void ReleaseAreaLayerPool()
    {
        GameObjectUtility.DestroyGameObject(ref areaLayerPool);
    }

    /// <summary>
    /// 領域レイヤーの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 領域レイヤー </returns>
    public EditorAreaLayer InstantiateAreaLayer(StageEditor_Manager manager)
    {
        if (areaLayerPool.Count > 0)
        {
            var poolItem = areaLayerPool[0];
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            areaLayerPool.RemoveAt(0);

            poolItem.transform.parent = manager.transform;
            poolItem.transform.transform.localPosition = Vector3.zero;

            return poolItem;
        }

        if (!areaLayer)
            return null;

        var newObject = Instantiate(areaLayer);
        var newItem = newObject.GetComponent<EditorAreaLayer>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 領域レイヤーの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyAreaLayer(EditorAreaLayer item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!areaLayerPool.Contains(item))
            areaLayerPool.Add(item);
    }

    #endregion

    #region 背景オブジェクト

    /// <summary>
    /// 背景オブジェクトプールの開放
    /// </summary>
    public void ReleaseBackObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref backObjectPool);
    }

    /// <summary>
    /// 背景オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 背景オブジェクト </returns>
    public BackEditorObject InstantiateBackObject(StageEditor_Manager manager, EditorObjectsLayer objectsLayer)
    {
        if (backObjectPool.Count > 0)
        {
            var poolItem = backObjectPool[0];

            poolItem.ObjectsLayer = objectsLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            backObjectPool.RemoveAt(0);

            poolItem.transform.parent = objectsLayer.transform;

            return poolItem;
        }

        if (!backObject)
            return null;

        var newObject = Instantiate(backObject);
        var newItem = newObject.GetComponent<BackEditorObject>();

        if (!newItem)
            return null;

        newItem.ObjectsLayer = objectsLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = objectsLayer.transform;

        return newItem;
    }

    /// <summary>
    /// 背景オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyBackObject(BackEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!backObjectPool.Contains(item))
            backObjectPool.Add(item);
    }

    #endregion

    #region 処理領域オブジェクト

    /// <summary>
    /// 処理領域オブジェクトプールの開放
    /// </summary>
    public void ReleaseProcessAreaObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref processAreaObjectPool);
    }

    /// <summary>
    /// 処理領域オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 処理領域オブジェクト </returns>
    public CameraAreaEditorObject InstantiateProcessAreaObject(StageEditor_Manager manager, EditorObjectsLayer objectsLayer)
    {
        if (processAreaObjectPool.Count > 0)
        {
            var poolItem = processAreaObjectPool[0];

            poolItem.ObjectsLayer = objectsLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            processAreaObjectPool.RemoveAt(0);

            poolItem.transform.parent = objectsLayer.transform;

            return poolItem;
        }

        if (!processAreaObject)
            return null;

        var newObject = Instantiate(processAreaObject);
        var newItem = newObject.GetComponent<CameraAreaEditorObject>();

        if (!newItem)
            return null;

        newItem.ObjectsLayer = objectsLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = objectsLayer.transform;

        return newItem;
    }

    /// <summary>
    /// 処理領域オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyProcessAreaObject(CameraAreaEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!processAreaObjectPool.Contains(item))
            processAreaObjectPool.Add(item);
    }

    #endregion

    #region プレイヤーオブジェクト

    /// <summary>
    /// プレイヤーオブジェクトプールの開放
    /// </summary>
    public void ReleasePlayerObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref playerObjectPool);
    }

    /// <summary>
    /// プレイヤーオブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> プレイヤーオブジェクト </returns>
    public PlayerEditorObject InstantiatePlayerObject(StageEditor_Manager manager, EditorObjectsLayer objectsLayer)
    {
        if (playerObjectPool.Count > 0)
        {
            var poolItem = playerObjectPool[0];

            poolItem.ObjectsLayer = objectsLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            playerObjectPool.RemoveAt(0);

            poolItem.transform.parent = objectsLayer.transform;

            return poolItem;
        }

        if (!playerObject)
            return null;

        var newObject = Instantiate(playerObject);
        var newItem = newObject.GetComponent<PlayerEditorObject>();

        if (!newItem)
            return null;

        newItem.ObjectsLayer = objectsLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = objectsLayer.transform;

        return newItem;
    }

    /// <summary>
    /// プレイヤーオブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyPlayerObject(PlayerEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!playerObjectPool.Contains(item))
            playerObjectPool.Add(item);
    }

    #endregion

    #region 取っ手オブジェクト

    /// <summary>
    /// 取っ手オブジェクトプールの開放
    /// </summary>
    public void ReleaseHandleObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref handleObjectPool);
    }

    /// <summary>
    /// 取っ手オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="areaLayer"> 領域レイヤー </param>
    /// <returns> 取っ手オブジェクト </returns>
    public HandleEditorObject InstantiateHandleObject(StageEditor_Manager manager, EditorAreaLayer areaLayer)
    {
        if (handleObjectPool.Count > 0)
        {
            var poolItem = handleObjectPool[0];

            poolItem.AreaLayer = areaLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            handleObjectPool.RemoveAt(0);

            poolItem.transform.parent = areaLayer.transform;

            return poolItem;
        }

        if (!handleObject)
            return null;

        var newObject = Instantiate(handleObject);
        var newItem = newObject.GetComponent<HandleEditorObject>();

        if (!newItem)
            return null;

        newItem.AreaLayer = areaLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = areaLayer.transform;

        return newItem;
    }

    /// <summary>
    /// 取っ手オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象取っ手 </param>
    public void DestroyHandleObject(HandleEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!handleObjectPool.Contains(item))
            handleObjectPool.Add(item);
    }

    #endregion

    #region ゴールオブジェクト

    /// <summary>
    /// ゴールオブジェクトプールの開放
    /// </summary>
    public void ReleaseGoalObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref goalObjectPool);
    }

    /// <summary>
    /// ゴールオブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> ゴールオブジェクト </returns>
    public GoalEditorObject InstantiateGoalObject(StageEditor_Manager manager, EditorObjectsLayer objectsLayer)
    {
        if (goalObjectPool.Count > 0)
        {
            var poolItem = goalObjectPool[0];

            poolItem.ObjectsLayer = objectsLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            goalObjectPool.RemoveAt(0);

            poolItem.transform.parent = objectsLayer.transform;

            return poolItem;
        }

        if (!goalObject)
            return null;

        var newObject = Instantiate(goalObject);
        var newItem = newObject.GetComponent<GoalEditorObject>();

        if (!newItem)
            return null;

        newItem.ObjectsLayer = objectsLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = objectsLayer.transform;

        return newItem;
    }

    /// <summary>
    /// ゴールオブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyGoalObject(GoalEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!goalObjectPool.Contains(item))
            goalObjectPool.Add(item);
    }

    #endregion

    #region 敵 1 オブジェクト

    /// <summary>
    /// 敵 1 オブジェクトプールの開放
    /// </summary>
    public void ReleaseEnemyObjectPool_1()
    {
        GameObjectUtility.DestroyGameObject(ref enemyObject_1_Pool);
    }

    /// <summary>
    /// 敵 1 オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    /// <returns> 敵 1 オブジェクト </returns>
    public EnemyEditorObject_1 InstantiateEnemyObject_1(StageEditor_Manager manager, EditorObjectsLayer objectsLayer)
    {
        if (enemyObject_1_Pool.Count > 0)
        {
            var poolItem = enemyObject_1_Pool[0];

            poolItem.ObjectsLayer = objectsLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            enemyObject_1_Pool.RemoveAt(0);

            poolItem.transform.parent = objectsLayer.transform;

            return poolItem;
        }

        if (!enemyObject_1)
            return null;

        var newObject = Instantiate(enemyObject_1);
        var newItem = newObject.GetComponent<EnemyEditorObject_1>();

        if (!newItem)
            return null;

        newItem.ObjectsLayer = objectsLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = objectsLayer.transform;

        return newItem;
    }

    /// <summary>
    /// 敵 1 オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyEnemyObject_1(EnemyEditorObject_1 item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!enemyObject_1_Pool.Contains(item))
            enemyObject_1_Pool.Add(item);
    }

    #endregion

    #region ブロックオブジェクト

    /// <summary>
    /// ブロックオブジェクトプールの開放
    /// </summary>
    public void ReleaseBlockObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref blockObjectPool);
    }

    /// <summary>
    /// ブロックオブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="blockLayer"> ブロックレイヤー </param>
    /// <returns> ブロックオブジェクト </returns>
    public BlockEditorObject InstantiateBlockObject(StageEditor_Manager manager, EditorBlockLayer blockLayer)
    {
        if (blockObjectPool.Count > 0)
        {
            var poolItem = blockObjectPool[0];

            poolItem.BlockLayer = blockLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            blockObjectPool.RemoveAt(0);

            poolItem.transform.parent = blockLayer.transform;

            return poolItem;
        }

        if (!blockObject)
            return null;

        var newObject = Instantiate(blockObject);
        var newItem = newObject.GetComponent<BlockEditorObject>();

        if (!newItem)
            return null;

        newItem.BlockLayer = blockLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = blockLayer.transform;

        return newItem;
    }

    /// <summary>
    /// ブロックオブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyBlockObject(BlockEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!blockObjectPool.Contains(item))
            blockObjectPool.Add(item);
    }

    #endregion

    #region 領域オブジェクト

    /// <summary>
    /// 領域オブジェクトプールの開放
    /// </summary>
    public void ReleaseAreaObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref areaObjectPool);
    }

    /// <summary>
    /// 領域オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <param name="areaLayer"> 領域レイヤー </param>
    /// <returns> 領域オブジェクト </returns>
    public AreaEditorObject InstantiateAreaObject(StageEditor_Manager manager, EditorAreaLayer areaLayer)
    {
        if (areaObjectPool.Count > 0)
        {
            var poolItem = areaObjectPool[0];

            poolItem.AreaLayer = areaLayer;
            if (!poolItem.Initialize(manager, this))
                return null;

            poolItem.gameObject.SetActive(true);
            areaObjectPool.RemoveAt(0);

            poolItem.transform.parent = areaLayer.transform;

            return poolItem;
        }

        if (!areaObject)
            return null;

        var newObject = Instantiate(areaObject);
        var newItem = newObject.GetComponent<AreaEditorObject>();

        if (!newItem)
            return null;

        newItem.AreaLayer = areaLayer;
        if (!newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = areaLayer.transform;

        return newItem;
    }

    /// <summary>
    /// 領域オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyAreaObject(AreaEditorObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!areaObjectPool.Contains(item))
            areaObjectPool.Add(item);
    }

    #endregion
}
