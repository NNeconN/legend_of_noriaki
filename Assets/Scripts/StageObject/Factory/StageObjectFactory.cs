using System.Collections.Generic;
using UnityEngine;

public class StageObjectFactory : MonoBehaviour
{
    #region プレハブ

    /// <summary> オブジェクトレイヤー Prefab </summary>
    [SerializeField] GameObject objectsLayer = null;

    /// <summary> ブロックレイヤー Prefab </summary>
    [SerializeField] GameObject blockLayer = null;

    /// <summary> 領域レイヤー Prefab </summary>
    [SerializeField] GameObject areaLayer = null;

    /// <summary> 背景 Prefab </summary>
    [SerializeField] GameObject backObject = null;

    /// <summary> 処理領域 Prefab </summary>
    [SerializeField] GameObject processAreaObject = null;

    /// <summary> プレイヤー Prefab </summary>
    [SerializeField] GameObject playerObject = null;

    /// <summary> 取っ手 Prefab </summary>
    [SerializeField] GameObject handleObject = null;

    /// <summary> ゴール Prefab </summary>
    [SerializeField] GameObject goalObject = null;

    /// <summary> 敵 1 Prefab </summary>
    [SerializeField] GameObject enemyObject_1 = null;

    /// <summary> ブロック Prefab </summary>
    [SerializeField] GameObject blockObject = null;

    /// <summary> 領域 Prefab </summary>
    [SerializeField] GameObject areaObject = null;

    #endregion

    #region プール

    /// <summary> オブジェクトレイヤー Pool </summary>
    [SerializeField] List<StageObjectsLayer> objectsLayerPool = new List<StageObjectsLayer>();

    /// <summary> ブロックレイヤー Pool </summary>
    [SerializeField] List<StageBlockLayer> blockLayerPool = new List<StageBlockLayer>();

    /// <summary> 領域レイヤー Pool </summary>
    [SerializeField] List<StageAreaLayer> areaLayerPool = new List<StageAreaLayer>();

    /// <summary> 背景 Pool </summary>
    [SerializeField] List<BackStageObject> backObjectPool = new List<BackStageObject>();

    /// <summary> 処理領域 Pool </summary>
    [SerializeField] List<CameraAreaStageObject> processAreaObjectPool = new List<CameraAreaStageObject>();

    /// <summary> プレイヤー Pool </summary>
    [SerializeField] List<PlayerStageObject> playerObjectPool = new List<PlayerStageObject>();

    /// <summary> 取っ手 Pool </summary>
    [SerializeField] List<HandleStageObject> handleObjectPool = new List<HandleStageObject>();

    /// <summary> ゴール Pool </summary>
    [SerializeField] List<GoalStageObject> goalObjectPool = new List<GoalStageObject>();

    /// <summary> 敵 1 Pool </summary>
    [SerializeField] List<EnemyStageObject_1> enemyObject_1_Pool = new List<EnemyStageObject_1>();

    /// <summary> ブロック Pool </summary>
    [SerializeField] List<BlockStageObject> blockObjectPool = new List<BlockStageObject>();

    /// <summary> 領域 Pool </summary>
    [SerializeField] List<AreaStageObject> areaObjectPool = new List<AreaStageObject>();

    #endregion

    #region オブジェクトレイヤー

    /// <summary>
    /// オブジェクトレイヤープールの開放
    /// </summary>
    public void ReleaseObjectsLayerPool()
    {
        GameObjectUtility.DestroyGameObject(ref objectsLayerPool);
    }

    /// <summary>
    /// オブジェクトレイヤーの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> オブジェクトレイヤー </returns>
    public StageObjectsLayer InstantiateObjectsLayer(Stage_Manager manager)
    {
        StageObjectsLayer newItem = null;

        if (objectsLayerPool.Count > 0)
        {
            newItem = objectsLayerPool[0];
            newItem.gameObject.SetActive(true);
            objectsLayerPool.RemoveAt(0);
        }
        else if (objectsLayer)
            newItem = Instantiate(objectsLayer)?.GetComponent<StageObjectsLayer>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// オブジェクトレイヤーの削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void DestroyObjectsLayer(StageObjectsLayer item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!objectsLayerPool.Contains(item))
            objectsLayerPool.Add(item);
    }

    #endregion

    #region ブロックレイヤー

    /// <summary>
    /// ブロックレイヤープールの開放
    /// </summary>
    public void ReleaseBlockLayerPool()
    {
        GameObjectUtility.DestroyGameObject(ref blockLayerPool);
    }

    /// <summary>
    /// ブロックレイヤーの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> ブロックレイヤー </returns>
    public StageBlockLayer InstantiateBlockLayer(Stage_Manager manager)
    {
        StageBlockLayer newItem = null;

        if (blockLayerPool.Count > 0)
        {
            newItem = blockLayerPool[0];
            newItem.gameObject.SetActive(true);
            blockLayerPool.RemoveAt(0);
        }
        else if (blockLayer)
            newItem = Instantiate(blockLayer)?.GetComponent<StageBlockLayer>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// ブロックレイヤーの削除
    /// </summary>
    /// <param name="item"> 対象ブロック </param>
    public void DestroyBlockLayer(StageBlockLayer item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!blockLayerPool.Contains(item))
            blockLayerPool.Add(item);
    }

    #endregion

    #region 領域レイヤー

    /// <summary>
    /// 領域レイヤープールの開放
    /// </summary>
    public void ReleaseAreaLayerPool()
    {
        GameObjectUtility.DestroyGameObject(ref areaLayerPool);
    }

    /// <summary>
    /// 領域レイヤーの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 領域レイヤー </returns>
    public StageAreaLayer InstantiateAreaLayer(Stage_Manager manager)
    {
        StageAreaLayer newItem = null;

        if (areaLayerPool.Count > 0)
        {
            newItem = areaLayerPool[0];
            newItem.gameObject.SetActive(true);
            areaLayerPool.RemoveAt(0);
        }
        else if (areaLayer)
            newItem = Instantiate(areaLayer)?.GetComponent<StageAreaLayer>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 領域レイヤーの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyAreaLayer(StageAreaLayer item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!areaLayerPool.Contains(item))
            areaLayerPool.Add(item);
    }

    #endregion

    #region 背景オブジェクト

    /// <summary>
    /// 背景オブジェクトプールの開放
    /// </summary>
    public void ReleaseBackObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref backObjectPool);
    }

    /// <summary>
    /// 背景オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 背景オブジェクト </returns>
    public BackStageObject InstantiateBackObject(Stage_Manager manager)
    {
        BackStageObject newItem = null;

        if (backObjectPool.Count > 0)
        {
            newItem = backObjectPool[0];
            newItem.gameObject.SetActive(true);
            backObjectPool.RemoveAt(0);
        }
        else if (backObject)
            newItem = Instantiate(backObject)?.GetComponent<BackStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 背景オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象背景 </param>
    public void DestroyBackObject(BackStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!backObjectPool.Contains(item))
            backObjectPool.Add(item);
    }

    #endregion

    #region 処理領域オブジェクト

    /// <summary>
    /// 処理領域オブジェクトプールの開放
    /// </summary>
    public void ReleaseProcessAreaObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref processAreaObjectPool);
    }

    /// <summary>
    /// 処理領域オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 処理領域オブジェクト </returns>
    public CameraAreaStageObject InstantiateProcessAreaObject(Stage_Manager manager)
    {
        CameraAreaStageObject newItem = null;

        if (processAreaObjectPool.Count > 0)
        {
            newItem = processAreaObjectPool[0];
            newItem.gameObject.SetActive(true);
            processAreaObjectPool.RemoveAt(0);
        }
        else if (processAreaObject)
            newItem = Instantiate(processAreaObject)?.GetComponent<CameraAreaStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 処理領域オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象処理領域 </param>
    public void DestroyProcessAreaObject(CameraAreaStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!processAreaObjectPool.Contains(item))
            processAreaObjectPool.Add(item);
    }

    #endregion

    #region プレイヤーオブジェクト

    /// <summary>
    /// プレイヤーオブジェクトプールの開放
    /// </summary>
    public void ReleasePlayerObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref playerObjectPool);
    }

    /// <summary>
    /// プレイヤーオブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> プレイヤーオブジェクト </returns>
    public PlayerStageObject InstantiatePlayerObject(Stage_Manager manager)
    {
        PlayerStageObject newItem = null;

        if (playerObjectPool.Count > 0)
        {
            newItem = playerObjectPool[0];
            newItem.gameObject.SetActive(true);
            playerObjectPool.RemoveAt(0);
        }
        else if (playerObject)
            newItem = Instantiate(playerObject)?.GetComponent<PlayerStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// プレイヤーオブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象プレイヤー </param>
    public void DestroyPlayerObject(PlayerStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!playerObjectPool.Contains(item))
            playerObjectPool.Add(item);
    }

    #endregion

    #region ゴールオブジェクト

    /// <summary>
    /// ゴールオブジェクトプールの開放
    /// </summary>
    public void ReleaseGoalObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref goalObjectPool);
    }

    /// <summary>
    /// ゴールオブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> ゴールオブジェクト </returns>
    public GoalStageObject InstantiateGoalObject(Stage_Manager manager)
    {
        GoalStageObject newItem = null;

        if (goalObjectPool.Count > 0)
        {
            newItem = goalObjectPool[0];
            newItem.gameObject.SetActive(true);
            goalObjectPool.RemoveAt(0);
        }
        else if (goalObject)
            newItem = Instantiate(goalObject)?.GetComponent<GoalStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// ゴールオブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象ゴール </param>
    public void DestroyGoalObject(GoalStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!goalObjectPool.Contains(item))
            goalObjectPool.Add(item);
    }

    #endregion

    #region 取っ手オブジェクト

    /// <summary>
    /// 取っ手オブジェクトプールの開放
    /// </summary>
    public void ReleaseHandleObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref handleObjectPool);
    }

    /// <summary>
    /// 取っ手オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 取っ手オブジェクト </returns>
    public HandleStageObject InstantiateHandleObject(Stage_Manager manager)
    {
        HandleStageObject newItem = null;

        if (handleObjectPool.Count > 0)
        {
            newItem = handleObjectPool[0];
            newItem.gameObject.SetActive(true);
            handleObjectPool.RemoveAt(0);
        }
        else if (handleObject)
            newItem = Instantiate(handleObject)?.GetComponent<HandleStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 取っ手オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象取っ手 </param>
    public void DestroyHandleObject(HandleStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!handleObjectPool.Contains(item))
            handleObjectPool.Add(item);
    }

    #endregion

    #region 敵 1 オブジェクト

    /// <summary>
    /// 敵 1 オブジェクトプールの開放
    /// </summary>
    public void ReleaseEnemyObjectPool_1()
    {
        GameObjectUtility.DestroyGameObject(ref enemyObject_1_Pool);
    }

    /// <summary>
    /// 敵 1 オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 敵 1 オブジェクト </returns>
    public EnemyStageObject_1 InstantiateEnemyObject_1(Stage_Manager manager)
    {
        EnemyStageObject_1 newItem = null;

        if (enemyObject_1_Pool.Count > 0)
        {
            newItem = enemyObject_1_Pool[0];
            newItem.gameObject.SetActive(true);
            enemyObject_1_Pool.RemoveAt(0);
        }
        else if (enemyObject_1)
            newItem = Instantiate(enemyObject_1)?.GetComponent<EnemyStageObject_1>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 敵 1 オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象敵 1  </param>
    public void DestroyEnemyObject_1(EnemyStageObject_1 item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!enemyObject_1_Pool.Contains(item))
            enemyObject_1_Pool.Add(item);
    }

    #endregion

    #region ブロックオブジェクト

    /// <summary>
    /// ブロックオブジェクトプールの開放
    /// </summary>
    public void ReleaseBlockObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref blockObjectPool);
    }

    /// <summary>
    /// ブロックオブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> ブロックオブジェクト </returns>
    public BlockStageObject InstantiateBlockObject(Stage_Manager manager)
    {
        BlockStageObject newItem = null;

        if (blockObjectPool.Count > 0)
        {
            newItem = blockObjectPool[0];
            newItem.gameObject.SetActive(true);
            blockObjectPool.RemoveAt(0);
        }
        else if (blockObject)
            newItem = Instantiate(blockObject)?.GetComponent<BlockStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// ブロックオブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象ブロック </param>
    public void DestroyBlockObject(BlockStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!blockObjectPool.Contains(item))
            blockObjectPool.Add(item);
    }

    #endregion

    #region 領域オブジェクト

    /// <summary>
    /// 領域オブジェクトプールの開放
    /// </summary>
    public void ReleaseAreaObjectPool()
    {
        GameObjectUtility.DestroyGameObject(ref areaObjectPool);
    }

    /// <summary>
    /// 領域オブジェクトの生成
    /// </summary>
    /// <param name="manager"> エディターマネージャー </param>
    /// <returns> 領域オブジェクト </returns>
    public AreaStageObject InstantiateAreaObject(Stage_Manager manager)
    {
        AreaStageObject newItem = null;

        if (areaObjectPool.Count > 0)
        {
            newItem = areaObjectPool[0];
            newItem.gameObject.SetActive(true);
            areaObjectPool.RemoveAt(0);
        }
        else if (areaObject)
            newItem = Instantiate(areaObject)?.GetComponent<AreaStageObject>();

        if (!newItem || !newItem.Initialize(manager, this))
            return null;

        newItem.transform.parent = manager.transform;
        newItem.transform.transform.localPosition = Vector3.zero;

        return newItem;
    }

    /// <summary>
    /// 領域オブジェクトの削除
    /// </summary>
    /// <param name="item"> 対象領域 </param>
    public void DestroyAreaObject(AreaStageObject item)
    {
        if (!item)
            return;

        item.UnInitialize();
        item.transform.position = new Vector3(0.0f, -1000.0f, 0.0f);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);

        if (!areaObjectPool.Contains(item))
            areaObjectPool.Add(item);
    }

    #endregion
}
