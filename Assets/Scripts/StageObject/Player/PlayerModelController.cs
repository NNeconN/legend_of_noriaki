using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーモデル操作用
/// </summary>
public class PlayerModelController : MonoBehaviour
{
    /// <summary> 左手位置 </summary>
    [SerializeField] GameObject leftPoint = null;

    /// <summary> 右手位置 </summary>
    [SerializeField] GameObject rightPoint = null;

    /// <summary> 着地エフェクト </summary>
    [SerializeField] GameObject smokeEffect = null;

    /// <summary> アニメーター </summary>
    [SerializeField] Animator myAnimator = null;

    /// <summary> ノックバックアニメーションのクリップ </summary>
    [SerializeField] AnimationClip knockBackAnimationClip = null;

    /// <summary> ゴールアニメーションのクリップ </summary>
    [SerializeField] AnimationClip goalAnimationClip = null;

    /// <summary> ドア開けアニメーションのクリップ </summary>
    [SerializeField] AnimationClip doorOpenAnimationClip = null;

    /// <summary> 空中アニメーションのクリップ </summary>
    [SerializeField] AnimationClip[] airAnimationClips = null;

    /// <summary> 掴み待機アニメーションのクリップ </summary>
    [SerializeField] AnimationClip[] grabIdleAnimationClips = null;

    /// <summary> ゴールコントローラー </summary>
    [SerializeField] GoalModelController goalModelController = null;

    /// <summary> 溜めエフェクト </summary>
    [SerializeField] ChargeEffect chargeEffect = null;

    /// <summary> 解放エフェクト </summary>
    [SerializeField] ReleaseEffect releaseEffect = null;

    /// <summary> 着地エフェクト群 </summary>
    List<SmokeEffect> smokeEffects = new List<SmokeEffect>();

    /// <summary> 向き </summary>
    Vector3 direction = Vector3.right;

    /// <summary> ドアを開ける処理中か </summary>
    bool isDoorOpenProcess = false;

    /// <summary> 左手位置 取得用 </summary>
    public GameObject LeftPoint { get => leftPoint; }

    /// <summary> 右手位置 取得用 </summary>
    public GameObject RightPoint { get => rightPoint; }

    /// <summary> 溜めエフェクト 取得用 </summary>
    public ChargeEffect ChargeEffect { get => chargeEffect; }

    /// <summary> エフェクト 取得用 </summary>
    public ReleaseEffect ReleaseEffect { get => releaseEffect; }

    private void Update()
    {
        if (isDoorOpenProcess)
            DoorOpenProcess();
    }

    /// <summary>
    /// 全てのエフェクトを削除
    /// </summary>
    public void DestroyEffects()
    {
        while (smokeEffects.Count > 0)
        {
            if (smokeEffects[0])
                Destroy(smokeEffects[0].gameObject);

            smokeEffects.RemoveAt(0);
        }
    }

    #region ドアを開ける

    /// <summary>
    /// ドアを開ける
    /// </summary>
    public void SetIsDoorOpen()
    {
        isDoorOpenProcess = true;
    }

    /// <summary>
    /// ドアを開けない
    /// </summary>
    public void SetIsNotDoorOpen()
    {
        isDoorOpenProcess = false;
    }

    /// <summary>
    /// ドアを開ける処理
    /// </summary>
    public void DoorOpenProcess()
    {
        if (!goalModelController || !leftPoint || !rightPoint)
            return;

        goalModelController.DoorRotate(leftPoint.transform.position, rightPoint.transform.position);
    }

    /// <summary>
    /// ゴールのモデルコントローラーを設定
    /// </summary>
    /// <param name="controller"> コントローラー </param>
    public void SetGoalModelController(GoalModelController controller)
    {
        goalModelController = controller;
    }

    #endregion

    #region 向き

    /// <summary>
    /// 向きを強制的に変更
    /// </summary>
    /// <param name="direction"> 向き </param>
    public void ForcedChangeDirection(Vector3 direction)
    {
        this.direction = direction;
        transform.rotation = Quaternion.LookRotation(direction);
    }

    /// <summary>
    /// 向きを変更
    /// </summary>
    /// <param name="direction"> 向き </param>
    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    /// <summary>
    /// 向き更新
    /// </summary>
    /// <param name="speed"> 移動スピード </param>
    public void DirectionUpdate(float speed)
    {
        Vector3 direction = Vector3.Slerp(transform.forward, this.direction, speed);
        transform.rotation = Quaternion.LookRotation(direction);
    }

    /// <summary>
    /// 向き回転終了か
    /// </summary>
    /// <returns> 向き回転終了か </returns>
    public bool CheckFinishDirectionRotate()
    {
        return direction == transform.forward;
    }

    #endregion

    #region エフェクト関係

    /// <summary>
    /// 着地エフェクトを再生
    /// </summary>
    public void PlaySmokeEffect()
    {
        var position = transform.position;

        var ray = new Ray(transform.position + Vector3.down * 0.001f, Vector3.down);
        var distance = 3.0f;
        var hits = Physics.BoxCastAll(ray.origin, new Vector3(0.3f, 0.0f, 0.1f), ray.direction, Quaternion.identity, distance);

        for (int i = 0; i < hits.Length; ++i)
        {
            if (hits[i].distance > distance)
                continue;

            distance = hits[i].distance;

            position.y = hits[i].transform.position.y + 0.5f;
        }

        position.y += 0.975f;

        for (int i = 0; i < smokeEffects.Count; ++i)
        {
            if (!smokeEffects[i])
                continue;

            if (!smokeEffects[i].CheckStop())
                continue;

            smokeEffects[i].transform.position = position;

            smokeEffects[i].Play();
            return;
        }

        {
            var effect = Instantiate(smokeEffect)?.GetComponent<SmokeEffect>();
            if (!effect)
                return;

            effect.transform.position = position;

            if (!smokeEffects.Contains(effect))
                smokeEffects.Add(effect);

            effect.Play();
        }
    }

    #endregion

    #region アニメーションパラメーター設定

    /// <summary>
    /// アニメーターのisGroudに設定
    /// </summary>
    /// <param name="isGroud"> 設定値 </param>
    public void SetIsGround(bool isGroud)
    {
        myAnimator.SetBool("isGround", isGroud);
    }

    /// <summary>
    /// アニメーターのisLandingに設定
    /// </summary>
    /// <param name="isLanding"> 設定値 </param>
    public void SetIsLanding(bool isLanding)
    {
        myAnimator.SetBool("isLanding", isLanding);
    }

    /// <summary>
    /// アニメーターのwalkAnimationSpeedに設定
    /// </summary>
    /// <param name="speed"> 設定値 </param>
    public void SetWalkAnimationSpeed(float speed)
    {
        myAnimator.SetFloat("walkAnimationSpeed", speed);
    }

    /// <summary>
    /// アニメーターのverticalSpeedに設定
    /// </summary>
    /// <param name="speed"> 設定値 </param>
    public void SetVerticalSpeed(float speed)
    {
        myAnimator.SetFloat("verticalSpeed", speed);
    }

    #endregion

    #region アニメーション再生

    /// <summary>
    /// KnockBackアニメーション再生
    /// </summary>
    public void PlayKnockBackAniamtion()
    {
        myAnimator.Play("KnockBack");
    }

    /// <summary>
    /// Idleアニメーション再生
    /// </summary>
    public void PlayIdleAniamtion()
    {
        myAnimator.Play("Idle");
    }

    /// <summary>
    /// Jumpアニメーション再生
    /// </summary>
    public void PlayJumpAniamtion()
    {
        myAnimator.Play("Jump");
    }

    /// <summary>
    /// ClearIdleアニメーション再生
    /// </summary>
    public void PlayClearIdleAniamtion()
    {
        myAnimator.Play("ClearIdle");
    }

    /// <summary>
    /// ClearWalkアニメーション再生
    /// </summary>
    public void PlayClearWalkAniamtion()
    {
        myAnimator.Play("ClearWalk");
    }

    /// <summary>
    /// Goalアニメーション再生
    /// </summary>
    public void PlayGoalAniamtion()
    {
        myAnimator.Play("Goal");
    }

    /// <summary>
    /// GrabUpアニメーション再生
    /// </summary>
    public void PlayGrabUpAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabUp", 0, rate);
    }

    /// <summary>
    /// GrabDownアニメーション再生
    /// </summary>
    public void PlayGrabDownAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabDown", 0, rate);
    }

    /// <summary>
    /// GrabLeftアニメーション再生
    /// </summary>
    public void PlayGrabLeftAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabLeft", 0, rate);
    }

    /// <summary>
    /// GrabRightアニメーション再生
    /// </summary>
    public void PlayGrabRightAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabRight", 0, rate);
    }

    /// <summary>
    /// GrabUpToDownアニメーション再生
    /// </summary>
    public void PlayGrabUpToDownAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabUpToDown", 0, rate);
    }

    /// <summary>
    /// GrabDownToUpアニメーション再生
    /// </summary>
    public void PlayGrabDownToUpAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabDownToUp", 0, rate);
    }

    /// <summary>
    /// GrabWalkLeftアニメーション再生
    /// </summary>
    public void PlayGrabWalkLeftAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabWalkLeft", 0, rate);
    }

    /// <summary>
    /// GrabWalkRightアニメーション再生
    /// </summary>
    public void PlayGrabWalkRightAniamtion(float rate = 0.0f)
    {
        myAnimator.Play("GrabWalkRight", 0, rate);
    }

    /// <summary>
    /// ChargeUpアニメーション再生
    /// </summary>
    public void PlayChargeUpAniamtion()
    {
        myAnimator.Play("ChargeUp");
    }

    /// <summary>
    /// ChargeLeftアニメーション再生
    /// </summary>
    public void PlayChargeLeftAniamtion()
    {
        myAnimator.Play("ChargeLeft");
    }

    /// <summary>
    /// ChargeRightアニメーション再生
    /// </summary>
    public void PlayChargeRightAniamtion()
    {
        myAnimator.Play("ChargeRight");
    }

    /// <summary>
    /// ThrowUpアニメーション再生
    /// </summary>
    public void PlayThrowUpAniamtion()
    {
        myAnimator.Play("ThrowUp");
    }

    /// <summary>
    /// ThrowLeftアニメーション再生
    /// </summary>
    public void PlayThrowLeftAniamtion()
    {
        myAnimator.Play("ThrowLeft");
    }

    /// <summary>
    /// ThrowRightアニメーション再生
    /// </summary>
    public void PlayThrowRightAniamtion()
    {
        myAnimator.Play("ThrowRight");
    }

    /// <summary>
    /// DoorOpenアニメーション再生
    /// </summary>
    public void PlayDoorOpenAniamtion(float rate)
    {
        myAnimator.Play("DoorOpen", 0, rate);
    }

    #endregion

    #region アニメーションチェック

    /// <summary>
    /// 現在のアニメーションの名前取得
    /// </summary>
    /// <returns> 名前 </returns>
    public string GetCurrentAnimationName()
    {
        if (myAnimator.GetCurrentAnimatorClipInfo(0).Length < 1)
            return "";

        return myAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
    }

    /// <summary>
    /// 現在AirAnimationステートかどうか
    /// </summary>
    /// <returns> 現在AirAnimationステートかどうか </returns>
    public bool IsCurrentAirAnimation()
    {
        var clipName = GetCurrentAnimationName();

        for (int i = 0, iLength = airAnimationClips.Length; i < iLength; ++i)
        {
            if (airAnimationClips[i].name == clipName)
                return true;
        }

        return false;
    }

    /// <summary>
    /// 現在GrabIdleAnimationステートかどうか
    /// </summary>
    /// <returns> 現在GrabIdleAnimationステートかどうか </returns>
    public bool IsCurrentGrabIdleAnimation()
    {
        var clipName = GetCurrentAnimationName();

        for (int i = 0, iLength = grabIdleAnimationClips.Length; i < iLength; ++i)
        {
            if (grabIdleAnimationClips[i].name == clipName)
                return true;
        }

        return false;
    }

    /// <summary>
    /// 現在Goalアニメーションかどうか
    /// </summary>
    /// <returns> 現在Goalアニメーションかどうか </returns>
    public bool IsCurrentGoalAnimation()
    {
        return goalAnimationClip.name == GetCurrentAnimationName();
    }

    /// <summary>
    /// 現在DoorOpenアニメーションかどうか
    /// </summary>
    /// <returns> 現在DoorOpenアニメーションかどうか </returns>
    public bool IsCurrentDoorOpenAnimation()
    {
        return doorOpenAnimationClip.name == GetCurrentAnimationName();
    }

    /// <summary>
    /// 現在KnockBackアニメーションかどうか
    /// </summary>
    /// <returns> 現在KnockBackアニメーションかどうか </returns>
    public bool IsCurrentKnockBackAnimation()
    {
        return knockBackAnimationClip.name == GetCurrentAnimationName();
    }

    #endregion
}
