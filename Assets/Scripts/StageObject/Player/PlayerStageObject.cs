#if true

using UnityEngine;

public class PlayerStageObject : StageObject
{
    #region enum

    /// <summary>
    /// プレイヤーのメイン状態
    /// </summary>
    enum MainCondition
    {
        Normal,
        InAir,
        Grab,
        Clear,
        FallOut
    }

    /// <summary>
    /// プレイヤーのサブ状態
    /// </summary>
    enum SubCondition
    {
        None,
        MoveHandle,
        GrabAnimation,
        GrabIdle,
        ReleaseHandle,
        GrabMove,
        GrabCharge,
        GrabThrow,
        MoveGoal,
        RotateBack,
        GoalAnimation,
        RotateFront,
        DoorOpen,
    }

    #endregion

    /// <summary> 自身のコライダー群 </summary>
    Collider[] myColliders;

    /// <summary> 自身のリジッドボディ </summary>
    Rigidbody myRigidbody = null;

    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> 掴み対象取っ手 </summary>
    HandleStageObject targetHandle = null;

    /// <summary> モデルコントローラー </summary>
    PlayerModelController controller = null;

    /// <summary> 移動方向 </summary>
    PlayerData.MoveDirection myMoveDirection = PlayerData.MoveDirection.Right;

    /// <summary> プレイヤーのメイン状態 </summary>
    MainCondition mainCondition = MainCondition.Normal;

    /// <summary> プレイヤーのサブ状態 </summary>
    SubCondition subCondition = SubCondition.None;

    /// <summary> 掴み方向 </summary>
    StageData.GridDirection grabDirection = StageData.GridDirection.None;

    /// <summary> 実際の移動方向 </summary>
    StageData.GridDirection grabMoveDirection = StageData.GridDirection.None;

    /// <summary> 地面レイヤー </summary>
    [SerializeField] LayerMask groundLayers = 0;

    /// <summary> 前回の位置 </summary>
    Vector3 prevPosition = Vector3.zero;

    /// <summary> 次回の位置 </summary>
    Vector3 nextPosition = Vector3.zero;

    /// <summary> ドア開け時の移動推移 </summary>
    [SerializeField] AnimationCurve doorOpenMoveRate;

    /// <summary> 横方向掴み歩きスピード </summary>
    [SerializeField] AnimationCurve horizontalGrabMoveCurve = new AnimationCurve();

    /// <summary> 縦方向掴み歩きスピード </summary>
    [SerializeField] AnimationCurve verticalGrabMoveCurve = new AnimationCurve();

    /// <summary> 投げスピード </summary>
    [SerializeField] AnimationCurve throwGrabMoveCurve = new AnimationCurve();

    /// <summary> 落下時間 </summary>
    [SerializeField] float fallOutTime = 0.5f;

    /// <summary> 横方向掴み歩き時間 </summary>
    [SerializeField] float horizontalGrabMoveTime = 0.5f;

    /// <summary> 縦方向掴み歩き時間 </summary>
    [SerializeField] float verticalGrabMoveTime = 0.5f;

    /// <summary> 投げ時間 </summary>
    [SerializeField] float throwGrabMoveTime = 0.5f;

    /// <summary> 溜め時間 </summary>
    [SerializeField] float chargeTime = 0.5f;

    /// <summary> 溜めの力 </summary>
    [SerializeField] float chargePower = 0.5f;

    /// <summary> 現在の溜めの力 </summary>
    float currentChargePower = 0.0f;

    /// <summary> ゴールへの移動時間 </summary>
    [SerializeField] float goalMoveTime = 0.5f;

    /// <summary> 歩くスピード </summary>
    [SerializeField] float walkSpeed = 1.0f;

    /// <summary> 回転スピード </summary>
    [SerializeField] float rotateSpeed = 1.0f;

    /// <summary> ジャンプ力 </summary>
    [SerializeField] float jumpPower = 1.0f;

    /// <summary> 通常の重力の大きさ </summary>
    [SerializeField] float defaultGravityScale = 1.0f;

    /// <summary> 下降時の重力の大きさ </summary>
    [SerializeField] float downGravityScale = 2.0f;

    /// <summary> 重力の大きさ </summary>
    float gravityScale = 1.0f;

    /// <summary> 処理の経過時間 </summary>
    float processDeltaTime = 1.0f;

    /// <summary> ノックバック待ち時間 </summary>
    float knockBackWait = 0.0f;

    /// <summary> 接地判定 </summary>
    bool isGround = true;

    /// <summary> 着地判定 </summary>
    bool isLanding = true;

    /// <summary> SE関数呼び出し </summary>
    public SetSE setSE;


    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        myRigidbody = GetComponent<Rigidbody>();
        if (!myRigidbody)
            myRigidbody = gameObject.AddComponent<Rigidbody>();

        myColliders = GetComponents<Collider>();

        setSE = GetComponent<SetSE>();　　　　//SE

        var isFailed = !base.Initialize(stageManager, objectFactory);
        isFailed = isFailed || !controller;
        isFailed = isFailed || !myRigidbody;
        isFailed = isFailed || !setSE;    //SE

        return !isFailed;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGrid(this, gridPosition, gridSize);

        controller.DestroyEffects();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemovePlayerObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    public bool SetData(PlayerData data, StageObjectsLayer objectsLayer)
    {
        if (!stageManager)
            return false;

        this.objectsLayer = objectsLayer;

        myMoveDirection = data.MyMoveDirection;

        myRigidbody.isKinematic = false;
        myRigidbody.useGravity = false;
        myRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

        CalculatePosition(data.GridPosition);
        transform.parent = this.objectsLayer.transform;

        if (myMoveDirection == PlayerData.MoveDirection.Right)
            controller.ForcedChangeDirection(Vector3.right);
        else
            controller.ForcedChangeDirection(Vector3.left);

        controller.SetGoalModelController(null);

        SetNormalCondition();

        targetHandle = null;

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        CalculateGridPosition(xMin, yMin);
        CalculateGridSize(xMin, xMax, yMin, yMax);

        stageManager.AddRangeGrid(this, gridPosition, gridSize);

        return true;
    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        var deltaTime = Time.deltaTime;

        if (knockBackWait > 0.0f)
            knockBackWait -= deltaTime;

        controller.DirectionUpdate(rotateSpeed * deltaTime);
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        var deltaTime = Time.deltaTime;

        switch (mainCondition)
        {
            case MainCondition.Normal:
                if (transform.position.z != PlayerData.DEFAULT_POSITION_OFFSET.z)
                {
                    nextPosition = transform.position;
                    nextPosition.z = PlayerData.DEFAULT_POSITION_OFFSET.z;
                    MoveTowards(deltaTime, 2.0f, false);
                }

                NormalFixedUpdateProcess(deltaTime);
                AddGravity(myRigidbody, deltaTime * gravityScale);
                break;
            case MainCondition.InAir:
                if (transform.position.z != PlayerData.DEFAULT_POSITION_OFFSET.z) 
                {
                    nextPosition = transform.position;
                    nextPosition.z = PlayerData.DEFAULT_POSITION_OFFSET.z;
                    MoveTowards(deltaTime, 2.0f, false);
                }

                AirFixedUpdateProcess(deltaTime);
                AddGravity(myRigidbody, deltaTime * gravityScale);
                break;
            case MainCondition.Grab:
                GrabFixedUpdateProcess(deltaTime);
                break;
            case MainCondition.Clear:
                ClearFixedUpdateProcess(deltaTime);
                break;
            case MainCondition.FallOut:
                FallOutFixedUpdateProcess(deltaTime);
                break;
        }
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        UpdateGridParameter(stageManager.MyMainCondition);

        if (stageManager.CheckScaleChangeLock())
            if (CheckIsStuckGround(groundLayers))
                stageManager.SetFailedObject(this);

        CheckIsGround();

        SetMoveAnimationSpeed();

        SetFallOutState();
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// グリッド上の位置・大きさの更新
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    void UpdateGridParameter(Stage_Manager.MainCondition managerCondition)
    {
        switch (managerCondition)
        {
            case Stage_Manager.MainCondition.PlayNormal:
                UpdateGridParameter();
                break;
        }
    }

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + PlayerData.DEFAULT_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + PlayerData.DEFAULT_GRID_COLLIDER_OFFSET.y;

        rect.width = PlayerData.DEFAULT_GRID_COLLIDER_SIZE.x;
        rect.height = PlayerData.DEFAULT_GRID_COLLIDER_SIZE.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = PlayerData.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

    #endregion

    #region 移動関係

    /// <summary>
    /// 移動方向の取得
    /// </summary>
    /// <returns> 移動方向 </returns>
    public PlayerData.MoveDirection GetMoveDirection()
    {
        return myMoveDirection;
    }

    /// <summary>
    /// 移動方向の設定
    /// </summary>
    /// <param name="moveDirection"> 移動方向 </param>
    public void SetMoveDirection(PlayerData.MoveDirection moveDirection)
    {
        myMoveDirection = moveDirection;

        if (myMoveDirection == PlayerData.MoveDirection.Left)
            controller.SetDirection(Vector3.left);
        else if (myMoveDirection == PlayerData.MoveDirection.Right)
            controller.SetDirection(Vector3.right);
    }

    /// <summary>
    /// 目的地(nextPosition)へ移動
    /// </summary>
    /// <param name="deltaTime"> フレーム間の経過時間 </param>
    /// <param name="speedScale"> 歩く速さの大きさ </param>
    /// <param name="isSetAnimationSpeed"> アニメーションの速さを変更するか </param>
    /// <returns> 目的地へ到達したか </returns>
    bool MoveTowards(float deltaTime, float speedScale, bool isSetAnimationSpeed)
    {
        var walkSpeed = this.walkSpeed * deltaTime * speedScale * (1.0f / 60.0f);

        transform.position = Vector3.MoveTowards(transform.position, nextPosition, walkSpeed);

        if (transform.position == nextPosition)
            return true;
        else if (isSetAnimationSpeed)
            controller.SetWalkAnimationSpeed(walkSpeed * 0.001f);

        return false;
    }

    /// <summary>
    /// 移動アニメーションのスピード入力
    /// </summary>
    void SetMoveAnimationSpeed()
    {
        var velocity = myRigidbody.velocity;

        switch (mainCondition)
        {
            case MainCondition.Normal:
            case MainCondition.InAir:
                if (myMoveDirection == PlayerData.MoveDirection.Left)
                    controller.SetWalkAnimationSpeed(-velocity.x);
                else if (myMoveDirection == PlayerData.MoveDirection.Right)
                    controller.SetWalkAnimationSpeed(velocity.x);
                else
                    controller.SetWalkAnimationSpeed(0.0f);

                controller.SetVerticalSpeed(velocity.y);
                break;
        }
    }

    #endregion

    #region 接地判定

    /// <summary>
    /// 接地判定
    /// </summary>
    void CheckIsGround()
    {
        var hits = Physics.OverlapBox(transform.position + PlayerData.GROUND_COLLIDER_OFFSET, PlayerData.GROUND_COLLIDER_SIZE, Quaternion.identity, groundLayers);

        isGround = hits.Length > 0;

        controller.SetIsGround(isGround);
    }

    /// <summary>
    /// 着地判定
    /// </summary>
    void CheckIsLanding()
    {
        var position = transform.position + PlayerData.GROUND_COLLIDER_OFFSET;
        position.y -= 0.34293f;
        var hits = Physics.OverlapBox(position, PlayerData.GROUND_COLLIDER_SIZE, Quaternion.identity, groundLayers);

        isLanding = hits.Length > 0;
        controller.SetIsLanding(isLanding);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Player;
    }

    #endregion

    #region 拡大縮小関係

    /// <summary>
    /// 拡大縮小失敗か確認
    /// </summary>
    public override void CheckFailedScaleChange()
    {
        if (!isGround)
        {
            myRigidbody.isKinematic = false;
            controller.PlayIdleAniamtion();
            SetAirCondition();
            stageManager.SetFailedObject(this);
            controller.ChargeEffect?.Stop();
            return;
        }

        var hitObjects = stageManager.HitCheckGridActive(this);

        for (int i = 0; i < hitObjects.Count; ++i)
        {
            var objectType = hitObjects[i].GetObjectType();

            switch (objectType)
            {
                case StageData.ObjectType.Block:
                    stageManager.SetFailedObject(this);
                    return;
            }
        }
    }

    #endregion

    #region ノックバック関係

    /// <summary>
    /// ノックバックさせる
    /// </summary>
    /// <param name="knockBackPower"> ノックバックの力 </param>
    public void KnockBack(Vector2 knockBackPower)
    {
        if (knockBackWait > 0.0f)
            return;

        knockBackWait = 0.5f;
        myRigidbody.isKinematic = false;
        SetNormalCondition();
        controller.PlayKnockBackAniamtion();
        stageManager.SetFailedObject(this);
        controller.ChargeEffect?.Stop();

        gravityScale = defaultGravityScale;
        myRigidbody.velocity = Vector3.zero;
        myRigidbody.AddForce(knockBackPower * myRigidbody.mass, ForceMode.Impulse);
        setSE.KnockBack();
    }

    #endregion

    #region Normal Condition

    /// <summary>
    /// Normal状態にする
    /// </summary>
    void SetNormalCondition()
    {
        mainCondition = MainCondition.Normal;
        subCondition = SubCondition.None;
        controller.SetIsLanding(false);
    }

    /// <summary>
    /// 通常時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void NormalFixedUpdateProcess(float deltaTime)
    {
        if (controller.IsCurrentKnockBackAnimation())
            return;

        if (!isGround)
        {
            SetAirCondition();
            AirFixedUpdateProcess(deltaTime);
            return;
        }

        var horizontal = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        if (horizontal.x < 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Left);
        else if (horizontal.x > 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Right);

        var velocity = myRigidbody.velocity;
        velocity.x = horizontal.x * walkSpeed * deltaTime;
        myRigidbody.velocity = velocity;

        gravityScale = defaultGravityScale;

        if (Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.A))
        {
            
            myRigidbody.AddForce(Vector3.up * jumpPower * myRigidbody.mass, ForceMode.Impulse);
            controller.PlayJumpAniamtion();
            SetAirCondition();
            setSE.JumpSe();         //SEジャンプ音
            return;
        }

        if (SetClearState())
            return;

        SearchHandleProcess();

        if (SetGrabState())
            return;
    }

    #endregion

    #region InAir Condition

    /// <summary>
    /// Air状態にする
    /// </summary>
    void SetAirCondition()
    {
        isGround = false;
        mainCondition = MainCondition.InAir;
        subCondition = SubCondition.None;
        controller.SetIsLanding(false);
    }

    /// <summary>
    /// 空中時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void AirFixedUpdateProcess(float deltaTime)
    {
        if (!controller.IsCurrentAirAnimation() && isGround)
        {
            SetNormalCondition();
            return;
        }

        var horizontal = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        if (horizontal.x < 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Left);
        else if (horizontal.x > 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Right);

        var velocity = myRigidbody.velocity;
        velocity.x = horizontal.x * walkSpeed * deltaTime;
        myRigidbody.velocity = velocity;

        if (velocity.y < 0.0f)
        {
            gravityScale = downGravityScale;

            CheckIsLanding();

            if (isLanding)
            {
                velocity.x *= 0.75f;
                myRigidbody.velocity = velocity;
            }
        }
        else
            gravityScale = defaultGravityScale;
    }

    #endregion

    #region 取っ手掴み状態

    /// <summary>
    /// 取っ手の探索処理
    /// </summary>
    void SearchHandleProcess()
    {
        HandleStageObject currentSearchHandle = null;

        if (!stageManager.CheckScaleChangeLock())
            currentSearchHandle = SearchAndGetHandle();

        if (targetHandle == currentSearchHandle)
            return;

        stageManager.Select(currentSearchHandle);

        targetHandle = currentSearchHandle;
    }

    /// <summary>
    /// 取っ手の探索
    /// </summary>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetHandle()
    {
        var inputAxis = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        HandleStageObject[] targetHandles = new HandleStageObject[4] { null, null, null, null };

        if (myMoveDirection == PlayerData.MoveDirection.Left)
        {
            if (inputAxis.x < 0.0f)
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 0);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 0);
                }
            }
            else
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, -1);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, -1);
                }
            }
        }
        else if (myMoveDirection == PlayerData.MoveDirection.Right)
        {
            if (inputAxis.x > 0.0f)
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 0);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 0);
                }
            }
            else
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 1);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 1);
                }
            }
        }
        else
            return null;

        for (int i = 0; i < targetHandles.Length; ++i)
        {
            if (!targetHandles[i])
                continue;

            return targetHandles[i];
        }

        return null;
    }

    /// <summary>
    /// 下側取っ手の探索
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="direction"> 左右ずらし </param>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetBottomHandle(Vector2Int rootPosition, int direction)
    {
        rootPosition.x += direction;
        var up = rootPosition;
        ++up.y;

        var gridHasObjects = stageManager.GetAtGrid(up);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return null;
                default:
                    break;
            }
        }

        var down = rootPosition;
        --down.y;
        bool isHit = false;

        gridHasObjects = stageManager.GetAtGrid(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
                default:
                    break;
            }
        }

        if (!isHit)
            return null;

        gridHasObjects = stageManager.GetAtGrid(rootPosition);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            var handleObject = gridHasObjects[i] as HandleStageObject;

            if (!handleObject)
                continue;

            if (HandleData.CheckHorizontalHandleType(handleObject.GetHandleType()))
                continue;

            return handleObject;
        }

        return null;
    }

    /// <summary>
    /// 上側取っ手の探索
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="direction"> 左右ずらし </param>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetTopHandle(Vector2Int rootPosition, int direction)
    {
        rootPosition.x += direction;
        ++rootPosition.y;
        var down = rootPosition;
        --down.y;

        var gridHasObjects = stageManager.GetAtGrid(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return null;
                default:
                    break;
            }
        }

        --down.y;
        bool isHit = false;

        gridHasObjects = stageManager.GetAtGrid(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
                default:
                    break;
            }
        }

        if (!isHit)
            return null;

        gridHasObjects = stageManager.GetAtGrid(rootPosition);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            var handleObject = gridHasObjects[i] as HandleStageObject;
            if (handleObject)
                return handleObject;
        }

        return null;
    }

    /// <summary>
    /// Grabステートにする
    /// </summary>
    /// <returns> 変更したか </returns>
    bool SetGrabState()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.B))
            return false;

        if (!targetHandle)
            return false;

        if (targetHandle.GetGridPosition().y == gridPosition.y)
            grabDirection = StageData.GridDirection.Up;
        else
        {
            if (HandleData.CheckHorizontalHandleType(targetHandle.GetHandleType()))
            {
                if (myMoveDirection == PlayerData.MoveDirection.Left)
                    grabDirection = StageData.GridDirection.Left;
                else
                    grabDirection = StageData.GridDirection.Right;
            }
            else
                grabDirection = StageData.GridDirection.Down;
        }

        var prevPosition = transform.position;
        nextPosition = targetHandle.transform.position;
        nextPosition.y = transform.position.y;

        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                nextPosition.x += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                break;
            case StageData.GridDirection.Right:
                nextPosition.x -= PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                break;
            case StageData.GridDirection.Up:
            case StageData.GridDirection.Down:
                nextPosition.x += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.z;
                break;
        }

        var direction = nextPosition - prevPosition;
        direction.y = 0.0f;
        if (direction.magnitude == 0)
            controller.SetDirection(Vector3.forward);
        else
            controller.SetDirection(direction.normalized);

        controller.PlayClearWalkAniamtion();

        myRigidbody.velocity = Vector3.zero;
        myRigidbody.isKinematic = true;

        mainCondition = MainCondition.Grab;
        subCondition = SubCondition.MoveHandle;

        return true;
    }

    /// <summary>
    /// 取っ手掴み時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void GrabFixedUpdateProcess(float deltaTime)
    {
        switch (subCondition)
        {
            case SubCondition.MoveHandle:
                if (MoveTowards(deltaTime, 2.0f, true))
                {
                    setSE.Grab();                                  //SE取っ手掴む音

                    SetConditionGrabAnimation();
                }
                break;
            case SubCondition.GrabAnimation:
                if (controller.CheckFinishDirectionRotate() && controller.IsCurrentGrabIdleAnimation())
                {
                    subCondition = SubCondition.GrabIdle;
                }
                break;
            case SubCondition.GrabIdle:
                if (SetConditionReleaseHandle())
                    return;

                if (SetConditionGrabMove())
                    return;

                if (SetConditionGrabCharge())
                    return;
                break;
            case SubCondition.ReleaseHandle:
                if (MoveTowards(deltaTime, 2.0f, true))
                {
                    myRigidbody.isKinematic = false;
                    SetNormalCondition();
                    controller.PlayIdleAniamtion();
                    NormalFixedUpdateProcess(deltaTime);
                    return;
                }
                break;
            case SubCondition.GrabMove:
                {
                    var processRate = stageManager.ScaleChangeGetProcessRate();
                    var position = targetHandle.transform.position;

                    switch (grabDirection)
                    {
                        case StageData.GridDirection.Left:
                            position.x += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                            position.y -= 1.5f;
                            position.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                            if (grabDirection == grabMoveDirection)
                                controller.PlayGrabWalkLeftAniamtion(processRate);
                            else
                                controller.PlayGrabWalkLeftAniamtion(1.0f - processRate);
                            break;
                        case StageData.GridDirection.Right:
                            position.x -= PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                            position.y -= 1.5f;
                            position.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                            if (grabDirection == grabMoveDirection)
                                controller.PlayGrabWalkRightAniamtion(processRate);
                            else
                                controller.PlayGrabWalkRightAniamtion(1.0f - processRate);
                            break;
                        case StageData.GridDirection.Up:
                            position.x += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.x;
                            if (GetFlagMoveLock())
                                position.y -= 0.5f;
                            else
                            {
                                position.y = transform.position.y;
                                controller.PlayGrabDownToUpAniamtion(processRate);
                            }
                            position.z += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.z;
                            break;
                        case StageData.GridDirection.Down:
                            position.x += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.x;
                            if (GetFlagMoveLock())
                                position.y -= 1.5f;
                            else
                            {
                                position.y = transform.position.y;
                                controller.PlayGrabUpToDownAniamtion(processRate);
                            }
                            position.z += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.z;
                            break;
                    }

                    transform.position = position;
                    UpdateGridParameter();

                    CheckFailedScaleChange();

                    if (stageManager.CheckScaleChangeLock())
                        break;

                    stageManager.Select(targetHandle);

                    if (targetHandle.GetGridPosition().y == gridPosition.y)
                        grabDirection = StageData.GridDirection.Up;
                    else
                    {
                        if (HandleData.CheckHorizontalHandleType(targetHandle.GetHandleType()))
                        {
                            if (myMoveDirection == PlayerData.MoveDirection.Left)
                                grabDirection = StageData.GridDirection.Left;
                            else
                                grabDirection = StageData.GridDirection.Right;
                        }
                        else
                            grabDirection = StageData.GridDirection.Down;
                    }

                    switch (grabDirection)
                    {
                        case StageData.GridDirection.Left:
                            controller.PlayGrabLeftAniamtion(1.0f);
                            break;
                        case StageData.GridDirection.Right:
                            controller.PlayGrabRightAniamtion(1.0f);
                            break;
                        case StageData.GridDirection.Up:
                            controller.PlayGrabDownAniamtion(1.0f);
                            break;
                        case StageData.GridDirection.Down:
                            controller.PlayGrabUpAniamtion(1.0f);
                            break;
                    }

                    subCondition = SubCondition.GrabIdle;
                }
                break;
            case SubCondition.GrabCharge:
                {
                    currentChargePower += deltaTime / chargeTime;
                    currentChargePower = Mathf.Min(currentChargePower, chargePower);

                    var handlePosition = targetHandle.transform.position;

                    switch (grabMoveDirection)
                    {
                        case StageData.GridDirection.Left:
                        case StageData.GridDirection.Right:
                            handlePosition.x = controller.LeftPoint ? controller.LeftPoint.transform.position.x : controller.RightPoint.transform.position.x;
                            break;
                        case StageData.GridDirection.Up:
                            handlePosition.y = controller.LeftPoint ? controller.LeftPoint.transform.position.y : controller.RightPoint.transform.position.y;
                            break;
                    }

                    targetHandle.transform.position = handlePosition;

                    if (!Input_Manager.Instance.GetButtonFixedPress(Input_Manager.ButtonIndex.RB))
                    {
                        switch (grabMoveDirection)
                        {
                            case StageData.GridDirection.Left:
                                controller.PlayThrowLeftAniamtion();
                                break;
                            case StageData.GridDirection.Right:
                                controller.PlayThrowRightAniamtion();
                                break;
                            case StageData.GridDirection.Up:
                                controller.PlayThrowUpAniamtion();
                                break;
                        }

                        subCondition = SubCondition.GrabThrow;
                        controller.ChargeEffect?.Stop();
                        controller.ReleaseEffect?.Play();
                    }
                }
                break;
            case SubCondition.GrabThrow:
                {
                    var realPosition = targetHandle.GetCalculatePosition(targetHandle.GetGridPosition());
                    var handlePosition = targetHandle.transform.position;
                    var isFinished = controller.GetCurrentAnimationName() == "PlayerIdle";

                    switch (grabMoveDirection)
                    {
                        case StageData.GridDirection.Left:
                            handlePosition.x = controller.LeftPoint ? controller.LeftPoint.transform.position.x : controller.RightPoint.transform.position.x;
                            if (!isFinished)
                                isFinished = handlePosition.x <= realPosition.x;
                            break;
                        case StageData.GridDirection.Right:
                            handlePosition.x = controller.LeftPoint ? controller.LeftPoint.transform.position.x : controller.RightPoint.transform.position.x;
                            if (!isFinished)
                                isFinished = handlePosition.x >= realPosition.x;
                            break;
                        case StageData.GridDirection.Up:
                            handlePosition.y = controller.LeftPoint ? controller.LeftPoint.transform.position.y : controller.RightPoint.transform.position.y;
                            if (!isFinished)
                                isFinished = handlePosition.y >= realPosition.y;
                            break;
                    }

                    targetHandle.transform.position = handlePosition;

                    if (isFinished)
                    {
                        var moveDistance = Mathf.FloorToInt(currentChargePower);
                        stageManager.ScaleChangeSetParameter(throwGrabMoveCurve, throwGrabMoveTime, moveDistance);
                        moveDistance = stageManager.CheckCanScaleDown(moveDistance);
                        stageManager.ScaleChangeSetParameter(throwGrabMoveCurve, throwGrabMoveTime, moveDistance);

                        stageManager.ScaleChangeStartProcess();

                        myRigidbody.isKinematic = false;
                        SetNormalCondition();
                    }
                }
                break;
        }
    }

    /// <summary>
    /// 状態をGrabAnimationに変更する
    /// </summary>
    void SetConditionGrabAnimation()
    {
        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                controller.SetDirection(Vector3.left);
                controller.PlayGrabLeftAniamtion();
                break;
            case StageData.GridDirection.Right:
                controller.SetDirection(Vector3.right);
                controller.PlayGrabRightAniamtion();
                break;
            case StageData.GridDirection.Up:
                controller.SetDirection(Vector3.forward);
                controller.PlayGrabDownAniamtion();
                break;
            case StageData.GridDirection.Down:
                controller.SetDirection(Vector3.forward);
                controller.PlayGrabUpAniamtion();
                break;
        }

        subCondition = SubCondition.GrabAnimation;
    }

    /// <summary>
    /// 状態をReleaseHandleに変更する
    /// </summary>
    /// <returns> 変更出来たか </returns>
    bool SetConditionReleaseHandle()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.B))
            return false;

        var prevPosition = transform.position;
        nextPosition = prevPosition;
        nextPosition.z = PlayerData.DEFAULT_POSITION_OFFSET.z;

        var direction = nextPosition - prevPosition;
        direction.y = 0.0f;
        if (direction.magnitude == 0)
            controller.SetDirection(Vector3.back);
        else
            controller.SetDirection(direction.normalized);

        controller.PlayClearWalkAniamtion();

        subCondition = SubCondition.ReleaseHandle;

        return true;
    }

    /// <summary>
    /// 状態をGrabMoveに変更する
    /// </summary>
    /// <returns> 変更出来たか </returns>
    bool SetConditionGrabMove()
    {
        var input = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        SetFlagMoveLock(false);

        switch (grabDirection)
        {
            default:
                return false;
            case StageData.GridDirection.Left:
            case StageData.GridDirection.Right:
                if (input.x == 0)
                    return false;

                grabMoveDirection = input.x < 0 ? StageData.GridDirection.Left : StageData.GridDirection.Right;

                if (!stageManager.MoveLock(grabMoveDirection))
                    return false;

                if (!stageManager.ScaleChangeSetParameter(horizontalGrabMoveCurve, horizontalGrabMoveTime, 1))
                {
                    stageManager.UnLock();
                    return false;
                }
                break;
            case StageData.GridDirection.Up:
                {
                    if (input.y == 0)
                        return false;

                    grabMoveDirection = input.y < 0 ? StageData.GridDirection.Down : StageData.GridDirection.Up;

                    if (!stageManager.MoveLock(grabMoveDirection))
                        return false;

                    var groundPocition = gridPosition;
                    --groundPocition.y;

                    var stageObjects = stageManager.GetAtGrid(groundPocition);
                    var isHit = false;

                    for (int i = 0; i < stageObjects.Count; ++i)
                    {
                        if (stageObjects[i].GetObjectType() != StageData.ObjectType.Block)
                            continue;

                        if (!stageObjects[i].GetFlagMoveLock())
                            continue;

                        isHit = true;
                        SetFlagMoveLock(true);

                        break;
                    }

                    if (grabMoveDirection == StageData.GridDirection.Down)
                    {
                        if (!isHit)
                        {
                            stageManager.UnLock();
                            return false;
                        }
                    }

                    if (!stageManager.ScaleChangeSetParameter(verticalGrabMoveCurve, verticalGrabMoveTime, 1))
                    {
                        stageManager.UnLock();
                        return false;
                    }
                }
                break;
            case StageData.GridDirection.Down:
                {
                    if (input.y == 0)
                        return false;

                    grabMoveDirection = input.y < 0 ? StageData.GridDirection.Down : StageData.GridDirection.Up;

                    if (!stageManager.MoveLock(grabMoveDirection))
                        return false;

                    var groundPocition = gridPosition;
                    --groundPocition.y;

                    var stageObjects = stageManager.GetAtGrid(groundPocition);
                    var isHit = false;

                    for (int i = 0; i < stageObjects.Count; ++i)
                    {
                        if (stageObjects[i].GetObjectType() != StageData.ObjectType.Block)
                            continue;

                        if (!stageObjects[i].GetFlagMoveLock())
                            continue;

                        isHit = true;
                        SetFlagMoveLock(true);

                        break;
                    }

                    if (grabMoveDirection == StageData.GridDirection.Up)
                    {
                        if (!isHit)
                        {
                            stageManager.UnLock();
                            return false;
                        }
                    }

                    if (!stageManager.ScaleChangeSetParameter(verticalGrabMoveCurve, verticalGrabMoveTime, 1))
                    {
                        stageManager.UnLock();
                        return false;
                    }
                }
                break;
        }

        if (stageManager.CheckCanScaleDown(1) < 1)
        {
            stageManager.UnLock();
            return false;
        }

        subCondition = SubCondition.GrabMove;
        stageManager.ScaleChangeStartProcess();

        return true;
    }

    /// <summary>
    /// 状態をGrabChargeに変更する
    /// </summary>
    /// <returns> 変更出来たか </returns>
    bool SetConditionGrabCharge()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.RB))
            return false;

        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                grabMoveDirection = StageData.GridDirection.Left;

                if (!stageManager.MoveLock(grabMoveDirection))
                    return false;

                if (!stageManager.ScaleChangeSetParameter(throwGrabMoveCurve, throwGrabMoveTime, 1) || stageManager.CheckCanScaleDown(1) < 1)
                {
                    stageManager.UnLock();
                    return false;
                }

                controller.PlayChargeLeftAniamtion();
                break;
            case StageData.GridDirection.Right:
                grabMoveDirection = StageData.GridDirection.Right;

                if (!stageManager.MoveLock(grabMoveDirection))
                    return false;

                if (!stageManager.ScaleChangeSetParameter(throwGrabMoveCurve, throwGrabMoveTime, 1) || stageManager.CheckCanScaleDown(1) < 1)
                {
                    stageManager.UnLock();
                    return false;
                }

                controller.PlayChargeRightAniamtion();
                break;
            case StageData.GridDirection.Down:
                grabMoveDirection = StageData.GridDirection.Up;

                if (!stageManager.MoveLock(grabMoveDirection))
                    return false;

                if (!stageManager.ScaleChangeSetParameter(throwGrabMoveCurve, throwGrabMoveTime, 1) || stageManager.CheckCanScaleDown(1) < 1 )
                {
                    stageManager.UnLock();
                    return false;
                }

                controller.PlayChargeUpAniamtion();
                break;
            default:
                return false;
        }

        currentChargePower = 0.0f;
        subCondition = SubCondition.GrabCharge;
        controller.ChargeEffect?.Play();

        return true;
    }

    #endregion

    #region ゲームクリア状態

    /// <summary>
    /// Clearステートにする
    /// </summary>
    /// <returns> 変更したか </returns>
    bool SetClearState()
    {
        var goalObjects = stageManager.HitCheckGridActive<GoalStageObject>(this);

        if (goalObjects.Count <= 0)
            return false;

        processDeltaTime = 0.0f;

        controller.SetGoalModelController(goalObjects[0].Controller);

        nextPosition = goalObjects[0].transform.position;
        nextPosition.z = PlayerData.DEFAULT_POSITION_OFFSET.z;

        myRigidbody.velocity = Vector3.zero;
        myRigidbody.isKinematic = true;

        controller.PlayClearIdleAniamtion();

        SetMoveDirection(transform.position.x < nextPosition.x ? PlayerData.MoveDirection.Right : PlayerData.MoveDirection.Left);

        mainCondition = MainCondition.Clear;
        subCondition = SubCondition.MoveGoal;

        stageManager.SetGameClear(nextPosition, 1.0f, StageCamera.CalculateType.Lerp);

        return true;
    }

    /// <summary>
    /// ゲームクリア時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void ClearFixedUpdateProcess(float deltaTime)
    {
        switch (subCondition)
        {
            case SubCondition.MoveGoal:
                if (MoveTowards(deltaTime, 2.0f, true))
                {
                    controller.SetDirection(Vector3.back);
                    subCondition = SubCondition.RotateBack;
                    controller.SetWalkAnimationSpeed(0.0f);
                    controller.PlayClearIdleAniamtion();
                }
                break;
            case SubCondition.RotateBack:
                if (controller.CheckFinishDirectionRotate())
                {
                    controller.PlayGoalAniamtion();
                    subCondition = SubCondition.GoalAnimation;
                }
                break;
            case SubCondition.GoalAnimation:
                if (!controller.IsCurrentGoalAnimation())
                {
                    controller.PlayClearIdleAniamtion();
                    controller.SetDirection(Vector3.forward);
                    subCondition = SubCondition.RotateFront;
                }
                break;
            case SubCondition.RotateFront:
                if (controller.CheckFinishDirectionRotate())
                {
                    processDeltaTime = 0.0f;
                    prevPosition = transform.position;
                    nextPosition = prevPosition + transform.forward * 1.20f;
                    controller.PlayDoorOpenAniamtion(0.0f);
                    subCondition = SubCondition.DoorOpen;
                    stageManager.StartNextStage();
                }
                break;
            case SubCondition.DoorOpen:
                {
                    processDeltaTime += deltaTime;

                    var moveRate = processDeltaTime / 1.0f;

                    if (0.2825f < moveRate && moveRate < 0.825f)
                        controller.SetIsDoorOpen();
                    else
                        controller.SetIsNotDoorOpen();

                    if (moveRate >= 1.0f)
                    {
                        moveRate = 1.0f;
                        controller.PlayClearIdleAniamtion();
                        subCondition = SubCondition.None;
                    }
                    else
                        controller.PlayDoorOpenAniamtion(moveRate);

                    transform.position = Vector3.Lerp(prevPosition, nextPosition, doorOpenMoveRate.Evaluate(moveRate));
                }
                break;
            default:
                break;
        }
    }

    #endregion

    #region 落下状態

    /// <summary>
    /// FallOutステートにする
    /// </summary>
    /// <returns> 変更したか </returns>
    bool SetFallOutState()
    {
        switch (mainCondition)
        {
            case MainCondition.Normal:
            case MainCondition.InAir:
            case MainCondition.Grab:
                break;
            default:
                return false;
        }

        var positionY = transform.position.y + GetGridColliderRect().height;
        var borderBottom = stageManager.GetBottomPivotY();

        if (positionY > borderBottom)
            return false;

        mainCondition = MainCondition.FallOut;
        processDeltaTime = 0.0f;

        return true;
    }

    /// <summary>
    /// ゲームクリア時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void FallOutFixedUpdateProcess(float deltaTime)
    {
        processDeltaTime += deltaTime;

        if (processDeltaTime < fallOutTime)
            return;

        DestroyThis();
    }

    #endregion
}

#elif false

using UnityEngine;

public class PlayerStageObject : StageObject
{
#region enum

    /// <summary>
    /// プレイヤーのメイン状態
    /// </summary>
    enum MainCondition
    {
        Normal,
        InAir,
        Grab,
    }

    /// <summary>
    /// プレイヤーのサブ状態
    /// </summary>
    enum SubCondition
    {
        None,
        MoveHandle,
        GrabAnimation,
        GrabIdle,
        ReleaseHandle,
    }

#endregion

    /// <summary> 自身のコライダー群 </summary>
    Collider[] myColliders;

    /// <summary> 自身のリジッドボディ </summary>
    Rigidbody myRigidbody = null;

    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> 掴み対象取っ手 </summary>
    HandleStageObject targetHandle = null;

    /// <summary> モデルコントローラー </summary>
    PlayerModelController controller = null;

    /// <summary> 移動方向 </summary>
    PlayerData.MoveDirection myMoveDirection = PlayerData.MoveDirection.Right;

    /// <summary> プレイヤーのメイン状態 </summary>
    MainCondition mainCondition = MainCondition.Normal;

    /// <summary> プレイヤーのサブ状態 </summary>
    SubCondition subCondition = SubCondition.None;

    /// <summary> 掴み方向 </summary>
    StageData.GridDirection grabDirection = StageData.GridDirection.None;

    /// <summary> 地面レイヤー </summary>
    [SerializeField] LayerMask gorundLayers = 0;

    /// <summary> 次回の位置 </summary>
    Vector3 nextPosition = Vector3.zero;

    /// <summary> 歩くスピード </summary>
    [SerializeField] float walkSpeed = 1.0f;

    /// <summary> 回転スピード </summary>
    [SerializeField] float rotateSpeed = 1.0f;

    /// <summary> ジャンプ力 </summary>
    [SerializeField] float jumpPower = 1.0f;

    /// <summary> 通常の重力の大きさ </summary>
    [SerializeField] float defaultGravityScale = 1.0f;

    /// <summary> 下降時の重力の大きさ </summary>
    [SerializeField] float downGravityScale = 2.0f;

    /// <summary> 重力の大きさ </summary>
    float gravityScale = 1.0f;

    /// <summary> 接地判定 </summary>
    bool isGround = true;

    /// <summary> 着地判定 </summary>
    bool isLanding = true;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        base.Initialize(stageManager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        myRigidbody = GetComponent<Rigidbody>();
        if (!myRigidbody)
            myRigidbody = gameObject.AddComponent<Rigidbody>();

        myColliders = GetComponents<Collider>();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager?.GridHasObjects?.RemoveRange(this, gridPosition, gridSize);

        controller.DestroyEffects();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer?.ObjectManager?.RemovePlayerObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    public bool SetData(PlayerData data, StageObjectsLayer objectsLayer)
    {
        if (!stageManager)
            return false;

        this.objectsLayer = objectsLayer;

        myMoveDirection = data.MyMoveDirection;

        myRigidbody.useGravity = false;
        myRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

        transform.position = CalculatePosition(data.GridPosition);
        transform.parent = this.objectsLayer.transform;

        if (myMoveDirection == PlayerData.MoveDirection.Right)
            controller.ForcedChangeDirection(Vector3.right);
        else
            controller.ForcedChangeDirection(Vector3.left);

        SetNormalCondition();

        targetHandle = null;

        var rect = GetGridColliderRect();

        CalculateGridPosition(rect.xMin, rect.yMin);
        CalculateGridSize(rect.xMin, rect.xMax, rect.yMin, rect.yMax);

        stageManager.GridHasObjects?.AddRange(this, gridPosition, gridSize);

        return true;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public override void UpdateProcess()
    {
        var deltaTime = Time.deltaTime;

        controller.DirectionUpdate(rotateSpeed * deltaTime);
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public override void FixedUpdateProcess()
    {
        var deltaTime = Time.deltaTime;

        switch (mainCondition)
        {
            case MainCondition.Normal:
                NormalFixedUpdateProcess(deltaTime);
                AddGravity(myRigidbody, deltaTime * gravityScale);
                break;
            case MainCondition.InAir:
                AirFixedUpdateProcess(deltaTime);
                AddGravity(myRigidbody, deltaTime * gravityScale);
                break;
            case MainCondition.Grab:
                GrabFixedUpdateProcess(deltaTime);
                break;
        }
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public override void LateUpdateProcess()
    {
        if (stageManager)
            UpdateGridParameter(stageManager.MyMainCondition);

        CheckIsGround();

        SetMoveAnimationSpeed();
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド上の位置・大きさの更新
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    void UpdateGridParameter(Stage_Manager.MainCondition managerCondition)
    {
        switch (managerCondition)
        {
            case Stage_Manager.MainCondition.PlayNormal:
                UpdateGridParameter();
                break;
        }
    }

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + PlayerData.DEFAULT_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + PlayerData.DEFAULT_GRID_COLLIDER_OFFSET.y;

        rect.width = PlayerData.DEFAULT_GRID_COLLIDER_SIZE.x;
        rect.height = PlayerData.DEFAULT_GRID_COLLIDER_SIZE.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = PlayerData.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

#endregion

#region 移動関係

    /// <summary>
    /// 移動方向の取得
    /// </summary>
    /// <returns> 移動方向 </returns>
    public PlayerData.MoveDirection GetMoveDirection()
    {
        return myMoveDirection;
    }

    /// <summary>
    /// 移動方向の設定
    /// </summary>
    /// <param name="moveDirection"> 移動方向 </param>
    public void SetMoveDirection(PlayerData.MoveDirection moveDirection)
    {
        myMoveDirection = moveDirection;

        if (myMoveDirection == PlayerData.MoveDirection.Left)
            controller.SetDirection(Vector3.left);
        else if (myMoveDirection == PlayerData.MoveDirection.Right)
            controller.SetDirection(Vector3.right);
    }

    /// <summary>
    /// 目的地(nextPosition)へ移動
    /// </summary>
    /// <param name="deltaTime"> フレーム間の経過時間 </param>
    /// <param name="speedScale"> 歩く速さの大きさ </param>
    /// <param name="isSetAnimationSpeed"> アニメーションの速さを変更するか </param>
    /// <returns> 目的地へ到達したか </returns>
    bool MoveTowards(float deltaTime, float speedScale, bool isSetAnimationSpeed)
    {
        var walkSpeed = this.walkSpeed * deltaTime * speedScale * (1.0f / 60.0f);

        transform.position = Vector3.MoveTowards(transform.position, nextPosition, walkSpeed);

        if (transform.position == nextPosition)
            return true;
        else if (isSetAnimationSpeed)
            controller.SetWalkAnimationSpeed(walkSpeed * 0.001f);

        return false;
    }

    /// <summary>
    /// 移動アニメーションのスピード入力
    /// </summary>
    void SetMoveAnimationSpeed()
    {
        var velocity = myRigidbody.velocity;

        switch (mainCondition)
        {
            case MainCondition.Normal:
            case MainCondition.InAir:
                if (myMoveDirection == PlayerData.MoveDirection.Left)
                    controller.SetWalkAnimationSpeed(-velocity.x);
                else if (myMoveDirection == PlayerData.MoveDirection.Right)
                    controller.SetWalkAnimationSpeed(velocity.x);
                else
                    controller.SetWalkAnimationSpeed(0.0f);

                controller.SetVerticalSpeed(velocity.y);
                break;
        }
    }

#endregion

#region 接地判定

    /// <summary>
    /// 接地判定
    /// </summary>
    void CheckIsGround()
    {
        var hits = Physics.OverlapBox(transform.position + PlayerData.GROUND_COLLIDER_OFFSET, PlayerData.GROUND_COLLIDER_SIZE, Quaternion.identity, gorundLayers);

        isGround = hits.Length > 0;

        controller.SetIsGround(isGround);
    }

    /// <summary>
    /// 着地判定
    /// </summary>
    void CheckIsLanding()
    {
        var position = transform.position + PlayerData.GROUND_COLLIDER_OFFSET;
        position.y -= 0.34293f;
        var hits = Physics.OverlapBox(position, PlayerData.GROUND_COLLIDER_SIZE, Quaternion.identity, gorundLayers);

        isLanding = hits.Length > 0;
        controller.SetIsLanding(isLanding);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Player;
    }

#endregion

#region Normal Condition

    /// <summary>
    /// Normal状態にする
    /// </summary>
    void SetNormalCondition()
    {
        mainCondition = MainCondition.Normal;
        subCondition = SubCondition.None;
        controller.SetIsLanding(false);
    }

    /// <summary>
    /// 通常時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void NormalFixedUpdateProcess(float deltaTime)
    {
        if (!isGround)
        {
            SetAirCondition();
            AirFixedUpdateProcess(deltaTime);
            return;
        }

        if (Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.Menu))
            stageManager?.ReSetStage();

        var horizontal = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        if (horizontal.x < 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Left);
        else if (horizontal.x > 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Right);

        var velocity = myRigidbody.velocity;
        velocity.x = horizontal.x * walkSpeed * deltaTime;
        myRigidbody.velocity = velocity;

        gravityScale = defaultGravityScale;

        if (Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.A))
        {
            myRigidbody.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
            controller.PlayJumpAniamtion();
            SetAirCondition();
            return;
        }

        //if (SetClearState())
        //    return;

        SearchHandleProcess();

        if (SetGrabState())
            return;
    }

#endregion

#region InAir Condition

    /// <summary>
    /// Air状態にする
    /// </summary>
    void SetAirCondition()
    {
        isGround = false;
        mainCondition = MainCondition.InAir;
        subCondition = SubCondition.None;
        controller.SetIsLanding(false);
    }

    /// <summary>
    /// 空中時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void AirFixedUpdateProcess(float deltaTime)
    {
        if (!controller.IsCurrentAirAnimation() && isGround)
        {
            SetNormalCondition();
            return;
        }

        var horizontal = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        if (horizontal.x < 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Left);
        else if (horizontal.x > 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Right);

        var velocity = myRigidbody.velocity;
        velocity.x = horizontal.x * walkSpeed * deltaTime;
        myRigidbody.velocity = velocity;

        if (velocity.y < 0.0f)
        {
            gravityScale = downGravityScale;

            CheckIsLanding();

            if (isLanding)
            {
                velocity.x *= 0.75f;
                myRigidbody.velocity = velocity;
            }
        }
        else
            gravityScale = defaultGravityScale;
    }

#endregion

#region 取っ手掴み状態

    /// <summary>
    /// 取っ手の探索処理
    /// </summary>
    void SearchHandleProcess()
    {
        var currentSearchHandle = SearchAndGetHandle();

        if (targetHandle == currentSearchHandle)
            return;

        //stageManager.SetUnTarget(targetHandle);
        //stageManager.SetTarget(currentSearchHandle);
        //currentSearchHandle?.SetTarget(stageManager.SelectColor);

        targetHandle = currentSearchHandle;

        Debug.Log(targetHandle);
    }

    /// <summary>
    /// 取っ手の探索
    /// </summary>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetHandle()
    {
        var inputAxis = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        HandleStageObject[] targetHandles = new HandleStageObject[4] { null, null, null, null };

        if (myMoveDirection == PlayerData.MoveDirection.Left)
        {
            if (inputAxis.x < 0.0f)
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 0);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 0);
                }
            }
            else
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, -1);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, -1);
                }
            }
        }
        else if (myMoveDirection == PlayerData.MoveDirection.Right)
        {
            if (inputAxis.x > 0.0f)
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 0);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 0);
                }
            }
            else
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 1);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 1);
                }
            }
        }
        else
            return null;

        for (int i = 0; i < targetHandles.Length; ++i)
        {
            if (!targetHandles[i])
                continue;

            return targetHandles[i];
        }

        return null;
    }

    /// <summary>
    /// 下側取っ手の探索
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="direction"> 左右ずらし </param>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetBottomHandle(Vector2Int rootPosition, int direction)
    {
        var grid = stageManager?.GridHasObjects;

        if (grid == null)
            return null;

        rootPosition.x += direction;
        var up = rootPosition;
        ++up.y;

        var gridHasObjects = grid.GetAt(up);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return null;
                default:
                    break;
            }
        }

        var down = rootPosition;
        --down.y;
        bool isHit = false;

        gridHasObjects = grid.GetAt(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
                default:
                    break;
            }
        }

        if (!isHit)
            return null;

        gridHasObjects = grid.GetAt(rootPosition);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            var handleObject = gridHasObjects[i] as HandleStageObject;

            if (!handleObject)
                continue;

            if (HandleData.CheckHorizontalHandleType(handleObject.GetHandleType()))
                continue;

            return handleObject;
        }

        return null;
    }

    /// <summary>
    /// 上側取っ手の探索
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="direction"> 左右ずらし </param>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetTopHandle(Vector2Int rootPosition, int direction)
    {
        var grid = stageManager?.GridHasObjects;

        if (grid == null)
            return null;

        rootPosition.x += direction;
        ++rootPosition.y;
        var down = rootPosition;
        --down.y;

        var gridHasObjects = grid.GetAt(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return null;
                default:
                    break;
            }
        }

        --down.y;
        bool isHit = false;

        gridHasObjects = grid.GetAt(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
                default:
                    break;
            }
        }

        if (!isHit)
            return null;

        gridHasObjects = grid.GetAt(rootPosition);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            var handleObject = gridHasObjects[i] as HandleStageObject;
            if (handleObject)
                return handleObject;
        }

        return null;
    }

    /// <summary>
    /// Grabステートにする
    /// </summary>
    /// <returns> 変更したか </returns>
    bool SetGrabState()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.B))
            return false;

        if (!targetHandle)
            return false;

        if (targetHandle.GetGridPosition().y == gridPosition.y)
            grabDirection = StageData.GridDirection.Up;
        else
        {
            if (HandleData.CheckHorizontalHandleType(targetHandle.GetHandleType()))
            {
                if (myMoveDirection == PlayerData.MoveDirection.Left)
                    grabDirection = StageData.GridDirection.Left;
                else
                    grabDirection = StageData.GridDirection.Right;
            }
            else
                grabDirection = StageData.GridDirection.Down;
        }

        var prevPosition = transform.position;
        nextPosition = targetHandle.transform.position;
        nextPosition.y = transform.position.y;

        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                nextPosition.x += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                break;
            case StageData.GridDirection.Right:
                nextPosition.x -= PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                break;
            case StageData.GridDirection.Up:
            case StageData.GridDirection.Down:
                nextPosition.x += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.z;
                break;
        }

        var direction = nextPosition - prevPosition;
        direction.y = 0.0f;
        if (direction.magnitude == 0)
            controller.SetDirection(Vector3.forward);
        else
            controller.SetDirection(direction.normalized);

        controller.PlayClearWalkAniamtion();

        myRigidbody.velocity = Vector3.zero;
        myRigidbody.isKinematic = true;

        mainCondition = MainCondition.Grab;
        subCondition = SubCondition.MoveHandle;

        return true;
    }

    /// <summary>
    /// 取っ手掴み時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void GrabFixedUpdateProcess(float deltaTime)
    {
        switch (subCondition)
        {
            case SubCondition.MoveHandle:
                if (MoveTowards(deltaTime, 2.0f, true))
                    SetConditionGrabAnimation();
                break;
            case SubCondition.GrabAnimation:
                if (controller.CheckFinishDirectionRotate() && controller.IsCurrentGrabIdleAnimation())
                    subCondition = SubCondition.GrabIdle;
                break;
            case SubCondition.GrabIdle:
                if (SetConditionReleaseHandle())
                    return;
                break;
            case SubCondition.ReleaseHandle:
                if (MoveTowards(deltaTime, 2.0f, true))
                {
                    myRigidbody.isKinematic = false;
                    SetNormalCondition();
                    controller.PlayIdleAniamtion();
                    NormalFixedUpdateProcess(deltaTime);
                    return;
                }
                break;
        }
    }

    /// <summary>
    /// 状態をGrabAnimationに変更する
    /// </summary>
    void SetConditionGrabAnimation()
    {
        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                controller.SetDirection(Vector3.left);
                controller.PlayGrabLeftAniamtion();
                break;
            case StageData.GridDirection.Right:
                controller.SetDirection(Vector3.right);
                controller.PlayGrabRightAniamtion();
                break;
            case StageData.GridDirection.Up:
                controller.SetDirection(Vector3.forward);
                controller.PlayGrabDownAniamtion();
                break;
            case StageData.GridDirection.Down:
                controller.SetDirection(Vector3.forward);
                controller.PlayGrabUpAniamtion();
                break;
        }

        subCondition = SubCondition.GrabAnimation;
    }

    /// <summary>
    /// 状態をReleaseHandleに変更する
    /// </summary>
    /// <returns> 変更出来たか </returns>
    bool SetConditionReleaseHandle()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.B))
            return false;

        var prevPosition = transform.position;
        nextPosition = prevPosition;
        nextPosition.z = PlayerData.DEFAULT_POSITION_OFFSET.z;

        var direction = nextPosition - prevPosition;
        direction.y = 0.0f;
        if (direction.magnitude == 0)
            controller.SetDirection(Vector3.back);
        else
            controller.SetDirection(direction.normalized);

        controller.PlayClearWalkAniamtion();

        subCondition = SubCondition.ReleaseHandle;

        return true;
    }

    /// <summary>
    /// 横方向にプレイヤーが動けるか
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="moveDirection"> 移動方向 </param>
    /// <returns> プレイヤーが動けるか </returns>
    bool CheckCanGrabMoveHorizontal(Vector2Int rootPosition, StageData.GridDirection moveDirection)
    {
        if (!targetHandle)
            return false;

        int direction = 1;

        switch (moveDirection)
        {
            case StageData.GridDirection.Left:
                direction = -1;
                break;
            case StageData.GridDirection.Right:
                direction = 1;
                break;
            default:
                return false;
        }

        var bottomPosition = rootPosition;
        --bottomPosition.y;

        var sidePosition = rootPosition;
        sidePosition.x += direction;

        var bottomSidePosition = sidePosition;
        bottomSidePosition.y = bottomPosition.y;

        var grid = stageManager?.GridHasObjects;

        if (grid == null)
            return false;
       
        if (!grid.CheckInPosition(bottomPosition) || !grid.CheckInPosition(sidePosition) || !grid.CheckInPosition(bottomSidePosition))
            return false;

        var bottomStageObjects = grid.GetAt(bottomPosition);
        var sideStageObjects = grid.GetRange(bottomPosition, new Vector2Int(1, 2));
        var bottomSideStageObjects = grid.GetAt(bottomSidePosition);

        for (int i = 0; i < sideStageObjects.Count; ++i)
        {
            switch (sideStageObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return false;
            }
        }

        bool isHit = false;
        for (int i = 0; i < bottomStageObjects.Count; ++i)
        {
            switch (bottomStageObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
            }
        }
        if (!isHit)
            return false;

        isHit = false;
        for (int i = 0; i < bottomSideStageObjects.Count; ++i)
        {
            switch (bottomSideStageObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
            }
        }
        if (!isHit)
            return false;

        return true;
    }

#endregion
}

#else

using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;

public class PlayerStageObject : StageObject
{
#region enum

    /// <summary>
    /// プレイヤーのメイン状態
    /// </summary>
    enum MainCondition
    {
        Normal,
        InAir,
        Grab,
    }

    /// <summary>
    /// プレイヤーのサブ状態
    /// </summary>
    enum SubCondition
    {
        None,
        MoveHandle,
        GrabAnimation,
        GrabIdle,
        ReleaseHandle,
    }

#endregion

    /// <summary> 自身のコライダー群 </summary>
    Collider[] myColliders;

    /// <summary> 自身のリジッドボディ </summary>
    Rigidbody myRigidbody = null;

    /// <summary> オブジェクトレイヤー </summary>
    StageObjectsLayer objectsLayer = null;

    /// <summary> 掴み対象取っ手 </summary>
    HandleStageObject targetHandle = null;

    /// <summary> モデルコントローラー </summary>
    PlayerModelController controller = null;

    /// <summary> 移動方向 </summary>
    PlayerData.MoveDirection myMoveDirection = PlayerData.MoveDirection.Right;

    /// <summary> プレイヤーのメイン状態 </summary>
    MainCondition mainCondition = MainCondition.Normal;

    /// <summary> プレイヤーのサブ状態 </summary>
    SubCondition subCondition = SubCondition.None;

    /// <summary> 掴み方向 </summary>
    StageData.GridDirection grabDirection = StageData.GridDirection.None;

    /// <summary> 地面レイヤー </summary>
    [SerializeField] LayerMask gorundLayers = 0;

    /// <summary> 次回の位置 </summary>
    Vector3 nextPosition = Vector3.zero;

    /// <summary> 歩くスピード </summary>
    [SerializeField] float walkSpeed = 1.0f;

    /// <summary> 回転スピード </summary>
    [SerializeField] float rotateSpeed = 1.0f;

    /// <summary> ジャンプ力 </summary>
    [SerializeField] float jumpPower = 1.0f;

    /// <summary> 通常の重力の大きさ </summary>
    [SerializeField] float defaultGravityScale = 1.0f;

    /// <summary> 下降時の重力の大きさ </summary>
    [SerializeField] float downGravityScale = 2.0f;

    /// <summary> 重力の大きさ </summary>
    float gravityScale = 1.0f;

    /// <summary> 接地判定 </summary>
    bool isGround = true;

    /// <summary> 着地判定 </summary>
    bool isLanding = true;

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public override bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        base.Initialize(manager, objectFactory);

        GameObjectUtility.NullCheckAndGet(ref controller, transform);

        myRigidbody = GetComponent<Rigidbody>();
        if (!myRigidbody)
            myRigidbody = gameObject.AddComponent<Rigidbody>();

        myColliders = GetComponents<Collider>();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public override void UnInitialize()
    {
        base.UnInitialize();

        stageManager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        controller.DestroyEffects();
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public override void DestroyThis()
    {
        objectsLayer.RemovePlayerObject(this);
    }

    /// <summary>
    /// データを設定
    /// </summary>
    /// <param name="data"> データ </param>
    /// <param name="objectsLayer"> オブジェクトレイヤー </param>
    public void SetData(PlayerData data, StageObjectsLayer objectsLayer)
    {
        this.objectsLayer = objectsLayer;

        myMoveDirection = data.MyMoveDirection;

        myRigidbody.useGravity = false;
        myRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

        transform.position = CalculatePosition(data.GridPosition);
        transform.parent = this.objectsLayer.transform;

        if (myMoveDirection == PlayerData.MoveDirection.Right)
            controller.ForcedChangeDirection(Vector3.right);
        else
            controller.ForcedChangeDirection(Vector3.left);

        SetNormalCondition();

        targetHandle = null;

        var rect = GetGridColliderRect();

        CalculateGridPosition(rect.xMin, rect.yMin);
        CalculateGridSize(rect.xMin, rect.xMax, rect.yMin, rect.yMax);

        stageManager.AddRangeGridHasObject(this, gridPosition, gridSize);
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        var deltaTime = Time.deltaTime;

        controller.DirectionUpdate(rotateSpeed * deltaTime);
    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        var deltaTime = Time.deltaTime;

        switch (mainCondition)
        {
            case MainCondition.Normal:
                NormalFixedUpdateProcess(deltaTime);
                AddGravity(myRigidbody, deltaTime * gravityScale);
                break;
            case MainCondition.InAir:
                AirFixedUpdateProcess(deltaTime);
                AddGravity(myRigidbody, deltaTime * gravityScale);
                break;
            case MainCondition.Grab:
                GrabFixedUpdateProcess(deltaTime);
                break;
        }
    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public override void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {
        UpdateGridParameter(managerCondition);

        CheckIsGround();

        SetMoveAnimationSpeed();
    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド上の位置・大きさの更新
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    void UpdateGridParameter(Stage_Manager.MainCondition managerCondition)
    {
        switch (managerCondition)
        {
            case Stage_Manager.MainCondition.PlayNormal:
                UpdateGridParameter();
                break;
        }
    }

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public override Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x + PlayerData.DEFAULT_GRID_COLLIDER_OFFSET.x;
        rect.y = transform.position.y + PlayerData.DEFAULT_GRID_COLLIDER_OFFSET.y;

        rect.width = PlayerData.DEFAULT_GRID_COLLIDER_SIZE.x;
        rect.height = PlayerData.DEFAULT_GRID_COLLIDER_SIZE.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public override Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = PlayerData.DEFAULT_POSITION_OFFSET;

        position.x += stageManager.GetLeftPivotX() + gridPosition.x;
        position.y += stageManager.GetBottomPivotY() + gridPosition.y;
        position.z += stageManager.transform.position.z;

        return position;
    }

#endregion

#region 移動関係

    /// <summary>
    /// 移動方向の取得
    /// </summary>
    /// <returns> 移動方向 </returns>
    public PlayerData.MoveDirection GetMoveDirection()
    {
        return myMoveDirection;
    }

    /// <summary>
    /// 移動方向の設定
    /// </summary>
    /// <param name="moveDirection"> 移動方向 </param>
    public void SetMoveDirection(PlayerData.MoveDirection moveDirection)
    {
        myMoveDirection = moveDirection;

        if (myMoveDirection == PlayerData.MoveDirection.Left)
            controller.SetDirection(Vector3.left);
        else if (myMoveDirection == PlayerData.MoveDirection.Right)
            controller.SetDirection(Vector3.right);
    }

    /// <summary>
    /// 目的地(nextPosition)へ移動
    /// </summary>
    /// <param name="deltaTime"> フレーム間の経過時間 </param>
    /// <param name="speedScale"> 歩く速さの大きさ </param>
    /// <param name="isSetAnimationSpeed"> アニメーションの速さを変更するか </param>
    /// <returns> 目的地へ到達したか </returns>
    bool MoveTowards(float deltaTime, float speedScale, bool isSetAnimationSpeed)
    {
        var walkSpeed = this.walkSpeed * deltaTime * speedScale * (1.0f / 60.0f);

        transform.position = Vector3.MoveTowards(transform.position, nextPosition, walkSpeed);

        if (transform.position == nextPosition)
            return true;
        else if (isSetAnimationSpeed)
            controller.SetWalkAnimationSpeed(walkSpeed * 0.001f);

        return false;
    }

    /// <summary>
    /// 移動アニメーションのスピード入力
    /// </summary>
    void SetMoveAnimationSpeed()
    {
        var velocity = myRigidbody.velocity;

        switch (mainCondition)
        {
            case MainCondition.Normal:
            case MainCondition.InAir:
                if (myMoveDirection == PlayerData.MoveDirection.Left)
                    controller.SetWalkAnimationSpeed(-velocity.x);
                else if (myMoveDirection == PlayerData.MoveDirection.Right)
                    controller.SetWalkAnimationSpeed(velocity.x);
                else
                    controller.SetWalkAnimationSpeed(0.0f);

                controller.SetVerticalSpeed(velocity.y);
                break;
        }
    }

#endregion

#region 接地判定

    /// <summary>
    /// 接地判定
    /// </summary>
    void CheckIsGround()
    {
        var hits = Physics.OverlapBox(transform.position + PlayerData.GROUND_COLLIDER_OFFSET, PlayerData.GROUND_COLLIDER_SIZE, Quaternion.identity, gorundLayers);

        isGround = hits.Length > 0;

        controller.SetIsGround(isGround);
    }

    /// <summary>
    /// 着地判定
    /// </summary>
    void CheckIsLanding()
    {
        var position = transform.position + PlayerData.GROUND_COLLIDER_OFFSET;
        position.y -= 0.34293f;
        var hits = Physics.OverlapBox(position, PlayerData.GROUND_COLLIDER_SIZE, Quaternion.identity, gorundLayers);

        isLanding = hits.Length > 0;
        controller.SetIsLanding(isLanding);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public override StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.Player;
    }

#endregion

#region Normal Condition

    /// <summary>
    /// Normal状態にする
    /// </summary>
    void SetNormalCondition()
    {
        mainCondition = MainCondition.Normal;
        subCondition = SubCondition.None;
        controller.SetIsLanding(false);
    }

    /// <summary>
    /// 通常時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void NormalFixedUpdateProcess(float deltaTime)
    {
        if (!isGround)
        {
            SetAirCondition();
            AirFixedUpdateProcess(deltaTime);
            return;
        }

        var horizontal = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        if (horizontal.x < 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Left);
        else if (horizontal.x > 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Right);

        var velocity = myRigidbody.velocity;
        velocity.x = horizontal.x * walkSpeed * deltaTime;
        myRigidbody.velocity = velocity;

        gravityScale = defaultGravityScale;

        if (Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.A))
        {
            myRigidbody.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
            controller.PlayJumpAniamtion();
            SetAirCondition();
            return;
        }

        //if (SetClearState())
        //    return;

        SearchHandleProcess();

        if (SetGrabState())
            return;
    }

#endregion

#region InAir Condition

    /// <summary>
    /// Air状態にする
    /// </summary>
    void SetAirCondition()
    {
        isGround = false;
        mainCondition = MainCondition.InAir;
        subCondition = SubCondition.None;
        controller.SetIsLanding(false);
    }

    /// <summary>
    /// 空中時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void AirFixedUpdateProcess(float deltaTime)
    {
        if (!controller.IsCurrentAirAnimation() && isGround)
        {
            SetNormalCondition();
            return;
        }

        var horizontal = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        if (horizontal.x < 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Left);
        else if (horizontal.x > 0.0f)
            SetMoveDirection(PlayerData.MoveDirection.Right);

        var velocity = myRigidbody.velocity;
        velocity.x = horizontal.x * walkSpeed * deltaTime;
        myRigidbody.velocity = velocity;

        if (velocity.y < 0.0f)
        {
            gravityScale = downGravityScale;

            CheckIsLanding();

            if (isLanding)
            {
                velocity.x *= 0.75f;
                myRigidbody.velocity = velocity;
            }
        }
        else
            gravityScale = defaultGravityScale;
    }

#endregion

#region 取っ手掴み状態

    /// <summary>
    /// 取っ手の探索処理
    /// </summary>
    void SearchHandleProcess()
    {
        var currentSearchHandle = SearchAndGetHandle();

        if (targetHandle == currentSearchHandle)
            return;

        //stageManager.SetUnTarget(targetHandle);
        //stageManager.SetTarget(currentSearchHandle);
        //currentSearchHandle?.SetTarget(stageManager.SelectColor);

        targetHandle = currentSearchHandle;

        Debug.Log(targetHandle);
    }

    /// <summary>
    /// 取っ手の探索
    /// </summary>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetHandle()
    {
        var inputAxis = Input_Manager.Instance.GetAxisRaw(Input_Manager.AxisIndex.Left);

        HandleStageObject[] targetHandles = new HandleStageObject[4] { null, null, null, null };

        if (myMoveDirection == PlayerData.MoveDirection.Left)
        {
            if (inputAxis.x < 0.0f)
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 0);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 0);
                }
            }
            else
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, -1);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, -1);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, -1);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, -1);
                }
            }
        }
        else if (myMoveDirection == PlayerData.MoveDirection.Right)
        {
            if (inputAxis.x > 0.0f)
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 0);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 0);
                }
            }
            else
            {
                if (inputAxis.y < 0.0f)
                {
                    targetHandles[0] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetBottomHandle(gridPosition, 1);
                    targetHandles[3] = SearchAndGetTopHandle(gridPosition, 1);
                }
                else
                {
                    targetHandles[0] = SearchAndGetTopHandle(gridPosition, 0);
                    targetHandles[1] = SearchAndGetBottomHandle(gridPosition, 0);
                    targetHandles[2] = SearchAndGetTopHandle(gridPosition, 1);
                    targetHandles[3] = SearchAndGetBottomHandle(gridPosition, 1);
                }
            }
        }
        else
            return null;

        for (int i = 0; i < targetHandles.Length; ++i)
        {
            if (!targetHandles[i])
                continue;

            return targetHandles[i];
        }

        return null;
    }

    /// <summary>
    /// 下側取っ手の探索
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="direction"> 左右ずらし </param>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetBottomHandle(Vector2Int rootPosition, int direction)
    {
        rootPosition.x += direction;
        var up = rootPosition;
        ++up.y;

        var gridHasObjects = stageManager.GetStageObjects(up);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return null;
                default:
                    break;
            }
        }

        var down = rootPosition;
        --down.y;
        bool isHit = false;

        gridHasObjects = stageManager.GetStageObjects(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
                default:
                    break;
            }
        }

        if (!isHit)
            return null;

        gridHasObjects = stageManager.GetStageObjects(rootPosition);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            var handleObject = gridHasObjects[i] as HandleStageObject;

            if (!handleObject)
                continue;

            if (HandleData.CheckHorizontalHandleType(handleObject.GetHandleType()))
                continue;

            return handleObject;
        }

        return null;
    }

    /// <summary>
    /// 上側取っ手の探索
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="direction"> 左右ずらし </param>
    /// <returns> 取っ手 </returns>
    HandleStageObject SearchAndGetTopHandle(Vector2Int rootPosition, int direction)
    {
        rootPosition.x += direction;
        ++rootPosition.y;
        var down = rootPosition;
        --down.y;

        var gridHasObjects = stageManager.GetStageObjects(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return null;
                default:
                    break;
            }
        }

        --down.y;
        bool isHit = false;

        gridHasObjects = stageManager.GetStageObjects(down);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            switch (gridHasObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
                default:
                    break;
            }
        }

        if (!isHit)
            return null;

        gridHasObjects = stageManager.GetStageObjects(rootPosition);

        for (int i = 0; i < gridHasObjects.Count; ++i)
        {
            var handleObject = gridHasObjects[i] as HandleStageObject;
            if (handleObject)
                return handleObject;
        }

        return null;
    }

    /// <summary>
    /// Grabステートにする
    /// </summary>
    /// <returns> 変更したか </returns>
    bool SetGrabState()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.B))
            return false;

        if (!targetHandle)
            return false;

        if (targetHandle.GetGridPosition().y == gridPosition.y)
            grabDirection = StageData.GridDirection.Up;
        else
        {
            if (HandleData.CheckHorizontalHandleType(targetHandle.GetHandleType()))
            {
                if (myMoveDirection == PlayerData.MoveDirection.Left)
                    grabDirection = StageData.GridDirection.Left;
                else
                    grabDirection = StageData.GridDirection.Right;
            }
            else
                grabDirection = StageData.GridDirection.Down;
        }

        var prevPosition = transform.position;
        nextPosition = targetHandle.transform.position;
        nextPosition.y = transform.position.y;

        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                nextPosition.x += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                break;
            case StageData.GridDirection.Right:
                nextPosition.x -= PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.HORIZONTAL_HANDLE_POSITION_OFFSET.z;
                break;
            case StageData.GridDirection.Up:
            case StageData.GridDirection.Down:
                nextPosition.x += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.x;
                nextPosition.z += PlayerData.VERTICAL_HANDLE_POSITION_OFFSET.z;
                break;
        }

        var direction = nextPosition - prevPosition;
        direction.y = 0.0f;
        if (direction.magnitude == 0)
            controller.SetDirection(Vector3.forward);
        else
            controller.SetDirection(direction.normalized);

        controller.PlayClearWalkAniamtion();

        myRigidbody.velocity = Vector3.zero;
        myRigidbody.isKinematic = true;

        mainCondition = MainCondition.Grab;
        subCondition = SubCondition.MoveHandle;

        return true;
    }

    /// <summary>
    /// 取っ手掴み時FixedUpdateで呼び出す処理
    /// </summary>
    /// <param name="deltaTime"> 経過時間 </param>
    void GrabFixedUpdateProcess(float deltaTime)
    {
        switch (subCondition)
        {
            case SubCondition.MoveHandle:
                if (MoveTowards(deltaTime, 2.0f, true))
                    SetConditionGrabAnimation();
                break;
            case SubCondition.GrabAnimation:
                if (controller.CheckFinishDirectionRotate() && controller.IsCurrentGrabIdleAnimation())
                    subCondition = SubCondition.GrabIdle;
                break;
            case SubCondition.GrabIdle:
                if (SetConditionReleaseHandle())
                    return;
                break;
            case SubCondition.ReleaseHandle:
                if (MoveTowards(deltaTime, 2.0f, true))
                {
                    myRigidbody.isKinematic = false;
                    SetNormalCondition();
                    controller.PlayIdleAniamtion();
                    NormalFixedUpdateProcess(deltaTime);
                    return;
                }
                break;
        }
    }

    /// <summary>
    /// 状態をGrabAnimationに変更する
    /// </summary>
    void SetConditionGrabAnimation()
    {
        switch (grabDirection)
        {
            case StageData.GridDirection.Left:
                controller.SetDirection(Vector3.left);
                controller.PlayGrabLeftAniamtion();
                break;
            case StageData.GridDirection.Right:
                controller.SetDirection(Vector3.right);
                controller.PlayGrabRightAniamtion();
                break;
            case StageData.GridDirection.Up:
                controller.SetDirection(Vector3.forward);
                controller.PlayGrabDownAniamtion();
                break;
            case StageData.GridDirection.Down:
                controller.SetDirection(Vector3.forward);
                controller.PlayGrabUpAniamtion();
                break;
        }

        subCondition = SubCondition.GrabAnimation;
    }

    /// <summary>
    /// 状態をReleaseHandleに変更する
    /// </summary>
    /// <returns> 変更出来たか </returns>
    bool SetConditionReleaseHandle()
    {
        if (!Input_Manager.Instance.GetButtonFixedTrigger(Input_Manager.ButtonIndex.B))
            return false;

        var prevPosition = transform.position;
        nextPosition = prevPosition;
        nextPosition.z = PlayerData.DEFAULT_POSITION_OFFSET.z;

        var direction = nextPosition - prevPosition;
        direction.y = 0.0f;
        if (direction.magnitude == 0)
            controller.SetDirection(Vector3.back);
        else
            controller.SetDirection(direction.normalized);

        controller.PlayClearWalkAniamtion();

        subCondition = SubCondition.ReleaseHandle;

        return true;
    }

    /// <summary>
    /// 横方向にプレイヤーが動けるか
    /// </summary>
    /// <param name="rootPosition"> 探索位置 </param>
    /// <param name="moveDirection"> 移動方向 </param>
    /// <returns> プレイヤーが動けるか </returns>
    bool CheckCanGrabMoveHorizontal(Vector2Int rootPosition, StageData.GridDirection moveDirection)
    {
        if (!targetHandle)
            return false;

        int direction = 1;

        switch (moveDirection)
        {
            case StageData.GridDirection.Left:
                direction = -1;
                break;
            case StageData.GridDirection.Right:
                direction = 1;
                break;
            default:
                return false;
        }

        var bottomPosition = rootPosition;
        --bottomPosition.y;

        var sidePosition = rootPosition;
        sidePosition.x += direction;

        var bottomSidePosition = sidePosition;
        bottomSidePosition.y = bottomPosition.y;

        if (!stageManager.CheckPositionInGrid(bottomPosition) || !stageManager.CheckPositionInGrid(sidePosition) || !stageManager.CheckPositionInGrid(bottomSidePosition))
            return false;

        var bottomStageObjects = stageManager.GetStageObjects(bottomPosition);
        var sideStageObjects = stageManager.GetRangeStageObjects(bottomPosition, new Vector2Int(1, 2));
        var bottomSideStageObjects = stageManager.GetStageObjects(bottomSidePosition);

        for (int i = 0; i < sideStageObjects.Count; ++i)
        {
            switch (sideStageObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    return false;
            }
        }

        bool isHit = false;
        for (int i = 0; i < bottomStageObjects.Count; ++i)
        {
            switch (bottomStageObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
            }
        }
        if (!isHit)
            return false;

        isHit = false;
        for (int i = 0; i < bottomSideStageObjects.Count; ++i)
        {
            switch (bottomSideStageObjects[i].GetObjectType())
            {
                case StageData.ObjectType.Block:
                    isHit = true;
                    break;
            }
        }
        if (!isHit)
            return false;

        return true;
    }

#endregion
}

#endif