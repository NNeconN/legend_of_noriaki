using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        GridPosition = -1,
        MyMoveDirection = -2,
    }

    /// <summary> 移動方向 </summary>
    public enum MoveDirection
    {
        Left,
        Right,
    }

    #endregion

    #region static変数

    /// <summary> グリッド上の大きさ </summary>
    static Vector2Int defaultGridSize = new Vector2Int(1, 2);

    /// <summary> マス目判定用コライダーのずれ </summary>
    static Vector3 defaultGridColliderOffset = new Vector3(0.0f, 0.95f, 0.0f);

    /// <summary> マス目判定用コライダーの大きさ </summary>
    static Vector3 defaultGridColliderSize = new Vector3(0.15f, 1.85f, 0.15f);

    /// <summary> 地面判定コライダーのずれ </summary>
    static Vector3 groundColliderOffset = new Vector3(0.0f, -0.05f, 0.0f);

    /// <summary> 地面判定コライダーの大きさ </summary>
    static Vector3 groundColliderSize = new Vector3(defaultGridColliderSize.x, 0.1f, defaultGridColliderSize.z);

    /// <summary> 位置のずれ </summary>
    static Vector3 defaultPositionOffset = new Vector3(0.5f, 0.0f, -1.0f);

    /// <summary> 横方向取っ手の位置のずれ </summary>
    static Vector3 horizontalHandlePositionOffset = new Vector3(0.6f, 0.0f, -0.35f);

    /// <summary> 縦方向取っ手の位置のずれ </summary>
    static Vector3 verticalHandlePositionOffset = new Vector3(0.0f, 0.0f, -0.55f);

    /// <summary> グリッド上の大きさ取得用 </summary>
    public static Vector2Int DEFAULT_GRID_SIZE { get => defaultGridSize; }

    /// <summary> マス目判定用コライダーのずれ取得用 </summary>
    public static Vector3 DEFAULT_GRID_COLLIDER_OFFSET { get => defaultGridColliderOffset; }

    /// <summary> マス目判定用コライダーの大きさ取得用 </summary>
    public static Vector3 DEFAULT_GRID_COLLIDER_SIZE { get => defaultGridColliderSize; }

    /// <summary> 地面判定コライダーのずれ取得用 </summary>
    public static Vector3 GROUND_COLLIDER_OFFSET { get => groundColliderOffset; }

    /// <summary> 地面判定コライダーの大きさ取得用 </summary>
    public static Vector3 GROUND_COLLIDER_SIZE { get => groundColliderSize; }

    /// <summary> 位置のずれ取得用 </summary>
    public static Vector3 DEFAULT_POSITION_OFFSET { get => defaultPositionOffset; }

    /// <summary> 横方向取っ手の位置のずれ取得用 </summary>
    public static Vector3 HORIZONTAL_HANDLE_POSITION_OFFSET { get => horizontalHandlePositionOffset; }

    /// <summary> 縦方向取っ手の位置のずれ取得用 </summary>
    public static Vector3 VERTICAL_HANDLE_POSITION_OFFSET { get => verticalHandlePositionOffset; }

    #endregion

    /// <summary> グリッド上の位置 </summary>
    Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> 移動方向 </summary>
    MoveDirection myMoveDirection = MoveDirection.Right;

    /// <summary> グリッド上の位置設定・取得用 </summary>
    public Vector2Int GridPosition { get => gridPosition; set => gridPosition = value; }

    /// <summary> 移動方向設定・取得用 </summary>
    public MoveDirection MyMoveDirection { get => myMoveDirection; set => myMoveDirection = value; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public PlayerData()
    {
        gridPosition = Vector2Int.zero;
        myMoveDirection = MoveDirection.Right;
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public PlayerData(PlayerEditorObject target)
    {
        gridPosition = target.GetGridPosition();
        myMoveDirection = target.GetMoveDirection();
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(PlayerEditorObject target)
    {
        target.SetGridPosition(gridPosition);
        target.SetMoveDirection(myMoveDirection);
    }

    /// <summary>
    /// データ設定
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    /// <param name="index"> 要素位置 </param>
    public void SetData(List<string[]> csvDatas, ref int index)
    {
        for (int indexX = 0; indexX < csvDatas[index].Length;)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][indexX], out dataID))
                break;

            switch (dataID)
            {
                case DataID.GridPosition:
                    gridPosition.x = int.Parse(csvDatas[index][indexX + 1]);
                    gridPosition.y = int.Parse(csvDatas[index][indexX + 2]);
                    indexX += 3;
                    break;
                case DataID.MyMoveDirection:
                    myMoveDirection = Enum.Parse<MoveDirection>(csvDatas[index][indexX + 1]);
                    indexX += 2;
                    break;
                default:
                    ++indexX;
                    break;
            }
        }

        ++index;
    }

    /// <summary>
    /// データ取得
    /// </summary>
    /// <param name="csvDatas"> csvのデータ </param>
    public void GetData(ref List<string[]> csvDatas)
    {
        csvDatas.Add(new string[1] { StageData.ObjectType.Player.ToString() });

        csvDatas.Add(new string[] { 
            DataID.GridPosition.ToString(),
            gridPosition.x.ToString(), 
            gridPosition.y.ToString(), 
            DataID.MyMoveDirection.ToString(),
            myMoveDirection.ToString() 
        });
    }
}
