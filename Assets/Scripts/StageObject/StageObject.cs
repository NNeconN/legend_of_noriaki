#if true

using UnityEngine;

public class StageObject : MonoBehaviour
{
    #region static

    /// <summary> 存在フラグ </summary>
    protected static BitFlag bitFlagEnable = new BitFlag(31);

    /// <summary> 移動予定フラグ </summary>
    protected static BitFlag bitFlagMoveLock = new BitFlag(30);

    /// <summary> 掴みロックフラグ </summary>
    protected static BitFlag bitFlagGrabLock = new BitFlag(29);

    /// <summary> 拡大フラグ </summary>
    protected static BitFlag bitFlagScaleUp = new BitFlag(28);

    /// <summary> 縮小フラグ </summary>
    protected static BitFlag bitFlagScaleDown = new BitFlag(27);

    #endregion

    /// <summary> ステージマネージャー </summary>
    protected Stage_Manager stageManager = null;

    /// <summary> オブジェクトファクトリー </summary>
    protected StageObjectFactory objectFactory = null;

    /// <summary> グリッド上の位置 </summary>
    [SerializeField] protected Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> グリッド上の大きさ </summary>
    [SerializeField] protected Vector2Int gridSize = Vector2Int.zero;

    /// <summary> 制御用BitFlag </summary>
    protected BitFlag systemBitFlag = new BitFlag();

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public virtual bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        this.stageManager = stageManager;
        this.objectFactory = objectFactory;

        UnInitialize();

        systemBitFlag.Clear();
        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public virtual void UnInitialize()
    {
        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public virtual void DestroyThis()
    {

    }

    #endregion

    #region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public virtual void UpdateProcess()
    {

    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public virtual void FixedUpdateProcess()
    {

    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public virtual void LateUpdateProcess()
    {

    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// グリッド上の位置・大きさの更新
    /// </summary>
    protected void UpdateGridParameter()
    {
        stageManager.RemoveRangeGrid(this, gridPosition, gridSize);

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        CalculateGridPosition(xMin, yMin);
        CalculateGridSize(xMin, xMax, yMin, yMax);

        stageManager.AddRangeGrid(this, gridPosition, gridSize);
    }

    /// <summary>
    /// グリッド上の位置取得
    /// </summary>
    /// <returns> グリッド上の位置 </returns>
    public Vector2Int GetGridPosition()
    {
        return gridPosition;
    }

    /// <summary>
    /// グリッド上の大きさ取得
    /// </summary>
    /// <returns> グリッド上の大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return gridSize;
    }

    /// <summary>
    /// グリッド上の位置計算
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="bottom"> y下位置 </param>
    public void CalculateGridPosition(float left, float bottom)
    {
        gridPosition = GetCalculateGridPosition(left, bottom);
    }

    /// <summary>
    /// グリッド上の大きさ計算
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="right"> x右位置 </param>
    /// <param name="bottom"> y下位置 </param>
    /// <param name="top"> y上位置 </param>
    public void CalculateGridSize(float left, float right, float bottom, float top)
    {
        gridSize = GetCalculateGridSize(left, right, bottom, top);
    }

    /// <summary>
    /// グリッド上の位置計算をし取得
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="bottom"> y下位置 </param>
    public Vector2Int GetCalculateGridPosition(float left, float bottom)
    {
        left -= stageManager.GetLeftPivotX();
        bottom -= stageManager.GetBottomPivotY();

        return new Vector2Int(Mathf.FloorToInt(left), Mathf.FloorToInt(bottom));
    }

    /// <summary>
    /// グリッド上の大きさ計算をし取得
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="right"> x右位置 </param>
    /// <param name="bottom"> y下位置 </param>
    /// <param name="top"> y上位置 </param>
    public Vector2Int GetCalculateGridSize(float left, float right, float bottom, float top)
    {
        left -= stageManager.GetLeftPivotX();
        right -= stageManager.GetLeftPivotX();
        bottom -= stageManager.GetBottomPivotY();
        top -= stageManager.GetBottomPivotY();

        var leftInt = Mathf.FloorToInt(left);
        var rightInt = Mathf.FloorToInt(right);
        var bottomInt = Mathf.FloorToInt(bottom);
        var topInt = Mathf.FloorToInt(top);

        return new Vector2Int(rightInt - leftInt + 1, topInt - bottomInt + 1);
    }

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public virtual Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x;
        rect.y = transform.position.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    public virtual void CalculatePosition(Vector2Int gridPosition)
    {
        transform.position = GetCalculatePosition(gridPosition);
    }

    /// <summary>
    /// マス目上の位置からワールド座標をし取得
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public virtual Vector3 GetCalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);

        position.x += gridPosition.x;
        position.y += gridPosition.y;

        return position;
    }

    #endregion

    #region Rigidbody関係

    /// <summary>
    /// 重力を与える
    /// </summary>
    /// <param name="rigidbody"> 対象 </param>
    /// <param name="gravityScale"> 重力の大きさ </param>
    protected void AddGravity(Rigidbody rigidbody, float gravityScale)
    {
        rigidbody.AddForce(Vector3.down * 9.8f * gravityScale * rigidbody.mass, ForceMode.Impulse);
    }

    #endregion

    #region フラグ関係

    /// <summary>
    /// 存在しているか
    /// </summary>
    /// <returns> 存在しているか </returns>
    public bool GetFlagEnable()
    {
        return systemBitFlag.CheckAnyPop(bitFlagEnable);
    }

    /// <summary>
    /// 移動予定か
    /// </summary>
    /// <returns> 移動予定か </returns>
    public bool GetFlagMoveLock()
    {
        return systemBitFlag.CheckAnyPop(bitFlagMoveLock);
    }

    /// <summary>
    /// 移動予定か設定
    /// </summary>
    /// <param name="isPop"> 移動予定か </param>
    public void SetFlagMoveLock(bool isPop)
    {
        systemBitFlag.PopOrUnPop(bitFlagMoveLock, isPop);
    }

    /// <summary>
    /// 掴みロック状態か
    /// </summary>
    /// <returns> 掴みロック状態か </returns>
    public bool GetFlagGrabLock()
    {
        return systemBitFlag.CheckAnyPop(bitFlagGrabLock);
    }

    /// <summary>
    /// 掴みロック状態か設定
    /// </summary>
    /// <param name="isPop"> 掴みロック状態か </param>
    public void SetFlagGrabLock(bool isPop)
    {
        if (isPop)
        {
            UpdateGridParameter();
            stageManager.RemoveRangeGrid(this, gridPosition, gridSize);
            systemBitFlag.Pop(bitFlagGrabLock);
        }
        else
        {
            UpdateGridParameter();
            stageManager.AddRangeGrid(this, gridPosition, gridSize);
            systemBitFlag.UnPop(bitFlagGrabLock);
        }
    }

    /// <summary>
    /// 拡大対象か
    /// </summary>
    /// <returns> 拡大対象か </returns>
    public bool GetFlagScaleUp()
    {
        return systemBitFlag.CheckAnyPop(bitFlagScaleUp);
    }

    /// <summary>
    /// 拡大対象か設定
    /// </summary>
    /// <param name="isPop"> 拡大対象か </param>
    public void SetFlagScaleUp(bool isPop)
    {
        systemBitFlag.PopOrUnPop(bitFlagScaleUp, isPop);
    }

    /// <summary>
    /// 縮小対象か
    /// </summary>
    /// <returns> 縮小対象か </returns>
    public bool GetFlagScaleDown()
    {
        return systemBitFlag.CheckAnyPop(bitFlagScaleDown);
    }

    /// <summary>
    /// 縮小対象か設定
    /// </summary>
    /// <param name="isPop"> 縮小対象か </param>
    public void SetFlagScaleDown(bool isPop)
    {
        systemBitFlag.PopOrUnPop(bitFlagScaleDown, isPop);
    }

    #endregion

    #region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public virtual StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.None;
    }

    #endregion

    #region 選択関係

    /// <summary>
    /// 選択する
    /// </summary>
    public virtual void Select()
    {
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    public virtual void UnSelect()
    {
    }

    #endregion

    #region 拡大縮小関係

    /// <summary>
    /// 拡大縮小失敗か確認
    /// </summary>
    public virtual void CheckFailedScaleChange()
    {
    }

    /// <summary>
    /// 失敗演出
    /// </summary>
    public virtual void FailedEffect()
    {

    }

    /// <summary>
    /// 挟まれているか確認
    /// </summary>
    /// <param name="groundLayers"> 地面レイヤー </param>
    /// <returns> 挟まれているか </returns>
    public bool CheckIsStuckGround(LayerMask groundLayers)
    {
        var rect = GetGridColliderRect();
        var halfSize = new Vector2(rect.width * 0.5f, rect.height * 0.5f);
        var quarterSize = halfSize * 0.5f;

        var leftHit = Physics.OverlapBox(new Vector3(rect.x - quarterSize.x, rect.y, transform.position.z), quarterSize, Quaternion.identity, groundLayers).Length > 0;
        var rightHit = Physics.OverlapBox(new Vector3(rect.x + quarterSize.x, rect.y, transform.position.z), quarterSize, Quaternion.identity, groundLayers).Length > 0;
        var bottomHit = Physics.OverlapBox(new Vector3(rect.x, rect.y - quarterSize.y, transform.position.z), quarterSize, Quaternion.identity, groundLayers).Length > 0;
        var topHit = Physics.OverlapBox(new Vector3(rect.x, rect.y + quarterSize.y, transform.position.z), quarterSize, Quaternion.identity, groundLayers).Length > 0;

        return (leftHit && rightHit) || (bottomHit && topHit);
    }

    #endregion
}


#elif false

using UnityEngine;

/// <summary>
/// ステージ上のオブジェクト
/// </summary>
public class StageObject : MonoBehaviour
{
#region static

    /// <summary> 存在フラグ </summary>
    protected static BitFlag bitFlagEnable = new BitFlag(31);

#endregion

    /// <summary> ステージマネージャー </summary>
    protected Stage_Manager stageManager = null;

    /// <summary> オブジェクトファクトリー </summary>
    protected StageObjectFactory objectFactory = null;

    /// <summary> グリッド上の位置 </summary>
    [SerializeField] protected Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> グリッド上の大きさ </summary>
    [SerializeField] protected Vector2Int gridSize = Vector2Int.zero;

    /// <summary> 制御用BitFlag </summary>
    protected BitFlag systemBitFlag = new BitFlag();

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="stageManager"> ステージマネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public virtual bool Initialize(Stage_Manager stageManager, StageObjectFactory objectFactory)
    {
        this.stageManager = stageManager;
        this.objectFactory = objectFactory;

        systemBitFlag.Clear();
        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public virtual void UnInitialize()
    {
        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public virtual void DestroyThis()
    {

    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public virtual void ChangeObjectName()
    {
        gameObject.name = GetType().Name;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    public virtual void UpdateProcess()
    {

    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    public virtual void FixedUpdateProcess()
    {

    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    public virtual void LateUpdateProcess()
    {

    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド上の位置・大きさの更新
    /// </summary>
    protected void UpdateGridParameter()
    {
        var grid = stageManager.GridHasObjects;

        grid?.RemoveRange(this, gridPosition, gridSize);

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        CalculateGridPosition(xMin, yMin);
        CalculateGridSize(xMin, xMax, yMin, yMax);

        grid?.AddRange(this, gridPosition, gridSize);
    }

    /// <summary>
    /// グリッド上の位置取得
    /// </summary>
    /// <returns> グリッド上の位置 </returns>
    public Vector2Int GetGridPosition()
    {
        return gridPosition;
    }

    /// <summary>
    /// グリッド上の大きさ取得
    /// </summary>
    /// <returns> グリッド上の大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return gridSize;
    }

    /// <summary>
    /// グリッド上の位置計算
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="bottom"> y下位置 </param>
    public void CalculateGridPosition(float left, float bottom)
    {
        gridPosition = GetCalculateGridPosition(left, bottom);
    }

    /// <summary>
    /// グリッド上の大きさ計算
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="right"> x右位置 </param>
    /// <param name="bottom"> y下位置 </param>
    /// <param name="top"> y上位置 </param>
    public void CalculateGridSize(float left, float right, float bottom, float top)
    {
        gridSize = GetCalculateGridSize(left, right, bottom, top);
    }

    /// <summary>
    /// グリッド上の位置計算をし取得
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="bottom"> y下位置 </param>
    public Vector2Int GetCalculateGridPosition(float left, float bottom)
    {
        left -= stageManager.GetLeftPivotX();
        bottom -= stageManager.GetBottomPivotY();

        return new Vector2Int(Mathf.FloorToInt(left), Mathf.FloorToInt(bottom));
    }

    /// <summary>
    /// グリッド上の大きさ計算をし取得
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="right"> x右位置 </param>
    /// <param name="bottom"> y下位置 </param>
    /// <param name="top"> y上位置 </param>
    public Vector2Int GetCalculateGridSize(float left, float right, float bottom, float top)
    {
        left -= stageManager.GetLeftPivotX();
        right -= stageManager.GetLeftPivotX();
        bottom -= stageManager.GetBottomPivotY();
        top -= stageManager.GetBottomPivotY();

        var leftInt = Mathf.FloorToInt(left);
        var rightInt = Mathf.FloorToInt(right);
        var bottomInt = Mathf.FloorToInt(bottom);
        var topInt = Mathf.FloorToInt(top);

        return new Vector2Int(rightInt - leftInt + 1, topInt - bottomInt + 1);
    }

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public virtual Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x;
        rect.y = transform.position.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public virtual Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!stageManager)
            return Vector3.zero;

        var position = new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);

        position.x += gridPosition.x;
        position.y += gridPosition.y;

        return position;
    }

#endregion

#region Rigidbody関係

    /// <summary>
    /// 重力を与える
    /// </summary>
    /// <param name="rigidbody"> 対象 </param>
    /// <param name="gravityScale"> 重力の大きさ </param>
    protected void AddGravity(Rigidbody rigidbody, float gravityScale)
    {
        rigidbody.AddForce(Vector3.down * 9.8f * gravityScale * rigidbody.mass, ForceMode.Impulse);
    }

#endregion

#region フラグ関係

    /// <summary>
    /// 存在しているか
    /// </summary>
    /// <returns> 存在しているか </returns>
    public bool GetFlagEnable()
    {
        return systemBitFlag.CheckAnyPop(bitFlagEnable);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public virtual StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.None;
    }

#endregion
}

#else

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ステージ上のオブジェクト
/// </summary>
public class StageObject : MonoBehaviour
{
#region static

    /// <summary> 存在フラグ </summary>
    protected static BitFlag bitFlagEnable = new BitFlag(31);

    /// <summary> 選択フラグ </summary>
    protected static BitFlag bitFlagSelected = new BitFlag(30);

    /// <summary> グラブロックフラグ </summary>
    protected static BitFlag bitFlagGrabLock = new BitFlag(29);

#endregion

    /// <summary> ステージマネージャー </summary>
    protected Stage_Manager manager = null;

    /// <summary> オブジェクトファクトリー </summary>
    protected StageObjectFactory objectFactory = null;

    /// <summary> グリッド上の位置 </summary>
    [SerializeField] protected Vector2Int gridPosition = Vector2Int.zero;

    /// <summary> グリッド上の大きさ </summary>
    [SerializeField] protected Vector2Int gridSize = Vector2Int.zero;

    /// <summary> 制御用BitFlag </summary>
    protected BitFlag systemBitFlag = new BitFlag();

    /// <summary> エディターマネージャー取得用 </summary>
    public Stage_Manager Manager { get => manager; set => manager = value; }

    /// <summary> オブジェクトファクトリー取得用 </summary>
    public StageObjectFactory Factory { get => objectFactory; set => objectFactory = value; }

#region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <param name="manager"> マネージャー </param>
    /// <param name="objectFactory"> オブジェクトファクトリー </param>
    /// <returns> 成功したか </returns>
    public virtual bool Initialize(Stage_Manager manager, StageObjectFactory objectFactory)
    {
        this.manager = manager;
        this.objectFactory = objectFactory;

        systemBitFlag.Clear();
        systemBitFlag.Pop(bitFlagEnable);

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public virtual void UnInitialize()
    {
        systemBitFlag.UnPop(bitFlagEnable);
    }

    /// <summary>
    /// 破壊する
    /// </summary>
    public virtual void DestroyThis()
    {

    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public virtual void ChangeObjectName()
    {
        gameObject.name = GetType().Name;
    }

#endregion

#region 更新処理関係

    /// <summary>
    /// Updateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public virtual void UpdateProcess(Stage_Manager.MainCondition managerCondition)
    {

    }

    /// <summary>
    /// FixedUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public virtual void FixedUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {

    }

    /// <summary>
    /// LateUpdateで呼ばれる
    /// </summary>
    /// <param name="managerCondition"> マネージャーの状態 </param>
    public virtual void LateUpdateProcess(Stage_Manager.MainCondition managerCondition)
    {

    }

#endregion

#region グリッド関係

    /// <summary>
    /// グリッド上の位置・大きさの更新
    /// </summary>
    protected void UpdateGridParameter()
    {
        manager.RemoveRangeGridHasObject(this, gridPosition, gridSize);

        var rect = GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        CalculateGridPosition(xMin, yMin);
        CalculateGridSize(xMin, xMax, yMin, yMax);

        manager.AddRangeGridHasObject(this, gridPosition, gridSize);
    }

    /// <summary>
    /// グリッド上の位置取得
    /// </summary>
    /// <returns> グリッド上の位置 </returns>
    public Vector2Int GetGridPosition()
    {
        return gridPosition;
    }

    /// <summary>
    /// グリッド上の大きさ取得
    /// </summary>
    /// <returns> グリッド上の大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return gridSize;
    }

    /// <summary>
    /// グリッド上の位置計算
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="bottom"> y下位置 </param>
    public void CalculateGridPosition(float left, float bottom)
    {
        left -= manager.GetLeftPivotX();
        bottom -= manager.GetBottomPivotY();

        gridPosition = new Vector2Int(Mathf.FloorToInt(left), Mathf.FloorToInt(bottom));
    }

    /// <summary>
    /// グリッド上の大きさ計算
    /// </summary>
    /// <param name="left"> x左位置 </param>
    /// <param name="right"> x右位置 </param>
    /// <param name="bottom"> y下位置 </param>
    /// <param name="top"> y上位置 </param>
    public void CalculateGridSize(float left, float right, float bottom, float top)
    {
        left -= manager.GetLeftPivotX();
        right -= manager.GetLeftPivotX();
        bottom -= manager.GetBottomPivotY();
        top -= manager.GetBottomPivotY();

        var leftInt = Mathf.FloorToInt(left);
        var rightInt = Mathf.FloorToInt(right);
        var bottomInt = Mathf.FloorToInt(bottom);
        var topInt = Mathf.FloorToInt(top);

        gridSize = new Vector2Int(rightInt - leftInt + 1, topInt - bottomInt + 1);
    }

    /// <summary>
    /// グリッド位置特定用コライダー取得
    /// </summary>
    /// <returns> グリッド位置特定用コライダー </returns>
    public virtual Rect GetGridColliderRect()
    {
        Rect rect = new Rect();

        rect.x = transform.position.x;
        rect.y = transform.position.y;

        return rect;
    }

    /// <summary>
    /// マス目上の位置からワールド座標を計算する
    /// </summary>
    /// <param name="gridPosition"> マス目上の位置 </param>
    /// <returns> ワールド座標 </returns>
    public virtual Vector3 CalculatePosition(Vector2Int gridPosition)
    {
        if (!manager)
            return Vector3.zero;

        var position = new Vector3(manager.GetLeftPivotX(), manager.GetBottomPivotY(), manager.transform.position.z);

        position.x += gridPosition.x;
        position.y += gridPosition.y;

        return position;
    }

#endregion

#region Rigidbody関係

    /// <summary>
    /// 重力を与える
    /// </summary>
    /// <param name="rigidbody"> 対象 </param>
    /// <param name="gravityScale"> 重力の大きさ </param>
    protected void AddGravity(Rigidbody rigidbody, float gravityScale)
    {
        rigidbody.AddForce(Vector3.down * 9.8f * gravityScale * rigidbody.mass, ForceMode.Impulse);
    }

#endregion

#region フラグ関係

    /// <summary>
    /// 存在しているか
    /// </summary>
    /// <returns> 存在しているか </returns>
    public bool GetFlagEnable()
    {
        return systemBitFlag.CheckAnyPop(bitFlagEnable);
    }

    /// <summary>
    /// 選択されているか
    /// </summary>
    /// <returns> 選択されているか </returns>
    public bool GetFlagSelect()
    {
        return systemBitFlag.CheckAnyPop(bitFlagSelected);
    }

    /// <summary>
    /// 選択する
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    public virtual void Select(HandleData.HandleType handleType)
    {
        systemBitFlag.Pop(bitFlagSelected);
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    /// <param name="handleType"> 取っ手の種類 </param>
    public virtual void UnSelect(HandleData.HandleType handleType)
    {
        systemBitFlag.UnPop(bitFlagSelected);
    }

    /// <summary>
    /// 選択されているか
    /// </summary>
    /// <returns> 選択されているか </returns>
    public bool GetFlagGrabLock()
    {
        return systemBitFlag.CheckAnyPop(bitFlagGrabLock);
    }

    /// <summary>
    /// 掴みロック状態にする
    /// </summary>
    public virtual void GrabLock()
    {
        systemBitFlag.Pop(bitFlagGrabLock);
    }

    /// <summary>
    /// 掴みロック状態にしない
    /// </summary>
    public virtual void GrabUnLock()
    {
        systemBitFlag.UnPop(bitFlagGrabLock);
    }

#endregion

#region 種類関係

    /// <summary>
    /// オブジェクトの種類取得
    /// </summary>
    /// <returns> オブジェクトの種類 </returns>
    public virtual StageData.ObjectType GetObjectType()
    {
        return StageData.ObjectType.None;
    }

#endregion
}

#endif