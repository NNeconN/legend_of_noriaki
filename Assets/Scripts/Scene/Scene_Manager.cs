using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// シーンマネージャー
/// </summary>
public class Scene_Manager : MonoSingleton<Scene_Manager>
{
    /// <summary> 読み込みステート </summary>
    enum LoadState
    {
        Idle,
        BeforeLoad,
        AfterLoad,
    }

    /// <summary> 非同期で読み込むかどうか </summary>
    static BitFlag flagLoadAsync = new BitFlag(0);

    /// <summary> インデックス番号で読み込むかどうか </summary>
    static BitFlag flagLoadIndex = new BitFlag(1);

    /// <summary> 読み込み時フェードインを行うかどうか </summary>
    static BitFlag flagLoadFadeIn = new BitFlag(2);

    /// <summary> 読み込み時フェードアウトを行うかどうか </summary>
    static BitFlag flagLoadFadeOut = new BitFlag(3);

    /// <summary> 操作を制限するかどうか </summary>
    static BitFlag flagLock = new BitFlag(4);

    /// <summary> フラグ群 </summary>
    [SerializeField] BitFlag bitFlag = new BitFlag(new BitFlag[3] { flagLoadAsync, flagLoadFadeIn, flagLoadFadeOut });

    /// <summary> 読み込みステート </summary>
    LoadState loadState = LoadState.Idle;

    /// <summary> シーン読み込み監視用 </summary>
    AsyncOperation asyncOperation = null;

    /// <summary> 読み込むシーンのインデックス番号 </summary>
    int nextSceneIndex = 0;

    /// <summary> 読み込むシーンの名前 </summary>
    string nextSceneName = "";

    #region 更新

    void Update()
    {
        switch (loadState)
        {
            default:
            case LoadState.Idle:
                break;
            case LoadState.BeforeLoad:
                BeforeLoadUpdate();
                break;
            case LoadState.AfterLoad:
                AfterLoadUpdate();
                break;
        }
    }

    /// <summary>
    /// 読み込み前更新
    /// </summary>
    void BeforeLoadUpdate()
    {
        if (bitFlag.CheckAnyPop(flagLoadFadeOut))
            if (Fade_Manager.Instance.CheckCurrentFadeOut())
                return;

        if (bitFlag.CheckAnyPop(flagLoadAsync))
        {
            if (asyncOperation.progress >= 0.9f)
                asyncOperation.allowSceneActivation = true;

            if (!asyncOperation.isDone)
                return;

            asyncOperation = null;
        }
        else
        {
            if (bitFlag.CheckAnyPop(flagLoadIndex))
                SceneManager.LoadScene(nextSceneIndex);
            else
                SceneManager.LoadScene(nextSceneName);
        }

        if (bitFlag.CheckAnyPop(flagLoadFadeIn))
            Fade_Manager.Instance.StartFadeIn();

        loadState = LoadState.AfterLoad;
    }

    /// <summary>
    /// 読み込み後更新
    /// </summary>
    void AfterLoadUpdate()
    {
        if (bitFlag.CheckAnyPop(flagLoadFadeIn))
            if (Fade_Manager.Instance.CheckCurrentFadeIn())
                return;

        bitFlag.UnPop(flagLock);
        Time.timeScale = 1.0f;

        loadState = LoadState.Idle;
    }

    #endregion

    #region シーン読み込み

    /// <summary>
    /// インデックス番号からシーンを読み込む
    /// </summary>
    /// <param name="index"> インデックス番号 </param>
    public void LoadScene(int index)
    {
        if (bitFlag.CheckAnyPop(flagLock))
            return;

        bitFlag.Pop(flagLock);

        if (bitFlag.CheckAnyPop(flagLoadAsync))
        {
            asyncOperation = SceneManager.LoadSceneAsync(index);
            asyncOperation.allowSceneActivation = false;
        }
        else
            nextSceneIndex = index;

        if (bitFlag.CheckAllUnPop(flagLoadFadeOut))
            return;

        loadState = LoadState.BeforeLoad;
        Time.timeScale = 0.0f;
        Fade_Manager.Instance.StartFadeOut();
    }

    /// <summary>
    /// 名前からシーンを読み込む
    /// </summary>
    /// <param name="name"> 名前 </param>
    public void LoadScene(string name)
    {
        if (bitFlag.CheckAnyPop(flagLock))
            return;

        bitFlag.Pop(flagLock);

        if (bitFlag.CheckAnyPop(flagLoadAsync))
        {
            asyncOperation = SceneManager.LoadSceneAsync(name);
            asyncOperation.allowSceneActivation = false;
        }
        else
            nextSceneName = name;

        if (bitFlag.CheckAllUnPop(flagLoadFadeOut))
            return;

        loadState = LoadState.BeforeLoad;
        Time.timeScale = 0.0f;
        Fade_Manager.Instance.StartFadeOut();
    }

    #endregion
}
