using UnityEngine;

/// <summary>
/// エフェクト : ChargeEffect 操作関係
/// </summary>
public class ChargeEffect : MonoBehaviour
{
    /// <summary> 自身の親 ParticleSystem </summary>
    ParticleSystem[] myParticleSystems = null;

    void Awake()
    {
        myParticleSystems = GetComponentsInChildren<ParticleSystem>();
    }

    /// <summary>
    /// エフェクトを再生する
    /// </summary>
    public void Play()
    {
        for (int i = 0; i < myParticleSystems.Length; ++i)
            myParticleSystems[i]?.Play();
    }

    /// <summary>
    /// エフェクトを停止する
    /// </summary>
    public void Stop()
    {
        for (int i = 0; i < myParticleSystems.Length; ++i)
            myParticleSystems[i]?.Stop();
    }

    /// <summary>
    /// エフェクトが止まっているか
    /// </summary>
    /// <returns> 止まっているか </returns>
    public bool CheckStop()
    {
        for (int i = 0; i < myParticleSystems.Length; ++i)
            if (myParticleSystems[i])
                if (!myParticleSystems[i].isStopped)
                    return false;

        return true;
    }
}
