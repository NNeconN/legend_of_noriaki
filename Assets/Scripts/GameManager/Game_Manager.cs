using UnityEngine;

public class Game_Manager : MonoSingleton<Game_Manager>
{
    /// <summary> ワールド数 </summary>
    [SerializeField] int worldNum = 1;

    /// <summary> ステージ数 </summary>
    [SerializeField] int stageNum = 1;

    /// <summary>
    /// ワールド数取得用
    /// </summary>
    public int WorldNum { get => worldNum; set => worldNum = value; }

    /// <summary>
    /// ステージ数取得用
    /// </summary>
    public int StageNum { get => stageNum; set => stageNum = value; }
}
