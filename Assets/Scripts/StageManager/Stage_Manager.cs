#if true

using System.Collections.Generic;
using System;
using UnityEngine;

public class Stage_Manager : MonoBehaviour
{
    #region enum

    /// <summary>
    /// 状態
    /// </summary>
    public enum MainCondition
    {
        Failed,
        Reset,
        StartWait,
        PlayNormal,
        GameClear,
        GameOver,
        PauseMenu,
    }

    #endregion

    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = new StageObject_Manager();

    /// <summary> 拡大縮小マネージャー </summary>
    ScaleChange_Manager scaleChangeManager = new ScaleChange_Manager();

    /// <summary> カメラマネージャー </summary>
    Camera_Manager cameraManager = new Camera_Manager();

    /// <summary> ポーズメニュー </summary>
    PauseMenu pauseMenu = null;

    /// <summary> ゲームオーバーメニュー </summary>
    GameOverMenu gameOverMenu = null;

    /// <summary> マス目 </summary>
    GridList gridList = new GridList();

    ///<summary> ステージのBGM </summary>
    StageBGM stageBGM = new StageBGM();

    /// <summary> ステージのデータ </summary>
    StageData myStageData = new StageData();

    /// <summary> オブジェクトファクトリー </summary>
    StageObjectFactory objectFactory = null;

    /// <summary> 状態 </summary>
    MainCondition myMainCondition = MainCondition.Failed;

    /// <summary> 選択色 </summary>
    [SerializeField] Color selectColor = Color.white;

    /// <summary> 失敗色 </summary>
    [SerializeField] Color failedColor = Color.white;

    /// <summary> クリア時カメラのずれ </summary>
    [SerializeField] Vector3 clearCameraOffset = Vector3.zero;

    /// <summary> クリア時カメラの横幅 </summary>
    [SerializeField] float clearCameraHorizontalViewSize = 0.5f;

    /// <summary> 失敗待ち時間 </summary>
    [SerializeField] float failedTime = 0.5f;

    /// <summary> 選択色点滅時間 </summary>
    [SerializeField] float selectBrightTime = 1.0f;

    /// <summary> 失敗色点滅時間 </summary>
    [SerializeField] float failedBrightTime = 0.5f;

    /// <summary> 状態 取得用 </summary>
    public MainCondition MyMainCondition { get => myMainCondition; }

    /// <summary> 選択色 取得用 </summary>
    public Color SelectColor { get => selectColor; }

    /// <summary> 失敗色 取得用 </summary>
    public Color FailedColor { get => failedColor; }

    /// <summary> 選択色点滅時間 取得用 </summary>
    public float SelectBrightTime { get => selectBrightTime; }

    /// <summary> 失敗色点滅時間 取得用 </summary>
    public float FailedBrightTime { get => failedBrightTime; }

    SE_kakusyuku sorce;
    SEtest seTest;

    #region Unity標準

    private void Awake()
    {
        ChangeObjectName();

        var gameManager = Game_Manager.Instance;

        bool isFailed = false;

        objectFactory = FindObjectOfType<StageObjectFactory>();
        pauseMenu = FindObjectOfType<PauseMenu>();
        gameOverMenu = FindObjectOfType<GameOverMenu>();
        stageBGM = FindObjectOfType<StageBGM>();

        isFailed = isFailed || !objectFactory;
        isFailed = isFailed || !pauseMenu;
        isFailed = isFailed || !gameOverMenu;
        isFailed = isFailed || !gridList.OnCreate();
        isFailed = isFailed || !scaleChangeManager.OnCreate(this);
        isFailed = isFailed || !objectManager.OnCreate(this);
        isFailed = isFailed || !cameraManager.OnCreate(this);
        isFailed = isFailed || !myStageData.ReadData(gameManager.WorldNum, gameManager.StageNum);

        if (isFailed)
        {
            myMainCondition = MainCondition.Failed;

            Scene_Manager.Instance.LoadScene("Title");

            return;
        }

        Initialize();
    }

    private void Start()
    {

    }

    private void Update()
    {
        ChangeConditionPauseMenu();

        objectManager.UpdateProcess();
    }

    private void FixedUpdate()
    {
        scaleChangeManager.UpdateProcess(Time.fixedDeltaTime);

        objectManager.FixedUpdateProcess();
    }

    private void LateUpdate()
    {
        objectManager.LateUpdateProcess();

        ChangeConditionGameOver();

        switch (myMainCondition)
        {
            case MainCondition.Reset:
                if (Fade_Manager.Instance.CheckCurrentIdleAfterFadeOut())
                {
                    CreateStage();
                    Fade_Manager.Instance.StartFadeIn();
                }
                break;
            case MainCondition.StartWait:
                if (Fade_Manager.Instance.CheckCurrentIdleAfterFadeIn())
                    myMainCondition = MainCondition.PlayNormal;
                break;
            case MainCondition.PlayNormal:
                cameraManager.UpdateProcess(false);
                break;
            case MainCondition.GameClear:
                if (cameraManager.GetWipeRadius() <= 0.0f)
                {
                    Fade_Manager.Instance.ForcedSetFadeOut();
                    var gameManager = Game_Manager.Instance;
                    var worldNum = gameManager.WorldNum;
                    var stageNum = gameManager.StageNum + 1;

                    if (!myStageData.ReadData(worldNum, stageNum))
                    {
                        ++worldNum;
                        stageNum = 1;

                        if (!myStageData.ReadData(worldNum, stageNum))
                        {
                            Scene_Manager.Instance.LoadScene("Title");
                            myMainCondition = MainCondition.Failed;
                            return;
                        }
                    }

                    gameManager.WorldNum = worldNum;
                    gameManager.StageNum = stageNum;

                    CreateStage();
                    cameraManager.ForcedSetWipeParameter(1.0f);
                    Fade_Manager.Instance.StartFadeIn();
                }
                break;
        }
    }

    #endregion

    #region 初期化・解放関係

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <returns> 成功したか </returns>
    public bool Initialize()
    {
        CreateStage();
        sorce = GetComponent<SE_kakusyuku>();
        seTest = GetComponent<SEtest>();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public void UnInitialize()
    {
        gridList.RemoveAll();
        cameraManager.UnInitialize();
        scaleChangeManager.UnInitialize();
        objectManager.UnInitialize();
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public void ChangeObjectName()
    {
        gameObject.name = "StageManager";
    }

    #endregion

    #region ステージ生成

    /// <summary>
    /// ステージ作成
    /// </summary>
    void CreateStage()
    {
        UnInitialize();

        pauseMenu.ForcedClose();
        gameOverMenu.ForcedClose();
        Time.timeScale = 1.0f;
        
        if(stageBGM)
        {
            stageBGM.SetVolume(1.0f);
            stageBGM.SetBGM(Game_Manager.Instance.WorldNum - 1);
        }

        gridList.SetGridSize(myStageData.GridSize);
        scaleChangeManager.Initialize();
        objectManager.Initialize();
        cameraManager.Initialize();

        for (int i = 0; i < myStageData.BlockLayerDatas.Count; ++i)
            objectManager.AddBlockLayer(myStageData.BlockLayerDatas[i]);

        for (int i = 0; i < myStageData.AreaLayerDatas.Count; ++i)
            objectManager.AddAreaLayer(myStageData.AreaLayerDatas[i]);

        for (int i = 0; i < myStageData.ObjectsLayerDatas.Count; ++i)
            objectManager.AddObjectsLayer(myStageData.ObjectsLayerDatas[i]);

        cameraManager.UpdateProcess(true);

        myMainCondition = MainCondition.StartWait;
    }

    /// <summary>
    /// ステージをもう一度生成する
    /// </summary>
    public void ReSetStage()
    {
        switch (myMainCondition)
        {
            case MainCondition.PlayNormal:
            case MainCondition.PauseMenu:
            case MainCondition.GameOver:
                break;
            default:
                return;
        }

        Fade_Manager.Instance.StartFadeOut();
        myMainCondition = MainCondition.Reset;
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// x位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> x位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckInPositionX(int position)
    {
        return gridList.CheckInPositionX(position);
    }

    /// <summary>
    /// y位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> y位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckInPositionY(int position)
    {
        return gridList.CheckInPositionY(position);
    }

    /// <summary>
    /// 位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> 位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckInPosition(Vector2Int position)
    {
        return gridList.CheckInPosition(position);
    }

    /// <summary>
    /// グリッドの大きさを取得
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return gridList.GetGridSize();
    }

    /// <summary>
    /// グリッドから取り除く
    /// </summary>
    /// <param name="stageObject"> 取り除く対象 </param>
    /// <param name="position"> 取り除く位置 </param>
    /// <param name="size"> 取り除く範囲 </param>
    public void RemoveRangeGrid(StageObject stageObject, Vector2Int position, Vector2Int size)
    {
        gridList.RemoveRange(stageObject, position, size);
    }

    /// <summary>
    /// グリッドから取り除く
    /// </summary>
    /// <param name="stageObject"> 取り除く対象 </param>
    /// <param name="position"> 取り除く位置 </param>
    public void RemoveAtGrid(StageObject stageObject, Vector2Int position)
    {
        gridList.RemoveAt(stageObject, position);
    }

    /// <summary>
    /// グリッドに追加する
    /// </summary>
    /// <param name="stageObject"> 追加する対象 </param>
    /// <param name="position"> 追加する位置 </param>
    /// <param name="size"> 追加する範囲 </param>
    public void AddRangeGrid(StageObject stageObject, Vector2Int position, Vector2Int size)
    {
        gridList.AddRange(stageObject, position, size);
    }

    /// <summary>
    /// グリッドに追加する
    /// </summary>
    /// <param name="stageObject"> 追加する対象 </param>
    /// <param name="position"> 追加する位置 </param>
    public void AddAtGrid(StageObject stageObject, Vector2Int position)
    {
        gridList.AddAt(stageObject, position);
    }

    /// <summary>
    /// グリッド指定位置に所属しているオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 取得する位置 </param>
    /// <param name="size"> 取得する範囲 </param>
    /// <returns> 所属しているオブジェクト群 </returns>
    public List<StageObject> GetRangeGrid(Vector2Int position, Vector2Int size)
    {
        return gridList.GetRange(position, size);
    }

    /// <summary>
    /// グリッド指定位置に所属しているオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 取得する位置 </param>
    /// <returns> 所属しているオブジェクト群 </returns>
    public List<StageObject> GetAtGrid(Vector2Int position)
    {
        return gridList.GetAt(position);
    }

    /// <summary>
    /// 当たり判定を用いてオブジェクト群を取得
    /// </summary>
    /// <param name="stageObject"> 用いる当たり判定 </param>
    /// <returns> 当たったオブジェクト群 </returns>
    public List<StageObject> HitCheckGrid(StageObject stageObject)
    {
        var rect = stageObject.GetGridColliderRect();

        var position = stageObject.GetGridPosition();
        var size = stageObject.GetGridSize();

        List<StageObject> result = new List<StageObject>();
        var stageObjects = GetRangeGrid(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            if (!stageObjects[i])
                continue;

            Vector2 distance = Vector2.zero;

            if (!MathUtility.CheckHitRectToRect(rect, stageObjects[i].GetGridColliderRect(), out distance))
                continue;

            if (result.Contains(stageObjects[i]))
                continue;

            result.Add(stageObjects[i]);
        }

        return result;
    }

    /// <summary>
    /// 当たり判定を用いてオブジェクト群を取得
    /// </summary>
    /// <typeparam name="T"> 調べるオブジェクト </typeparam>
    /// <param name="stageObject"> 用いる当たり判定 </param>
    /// <returns> 当たったオブジェクト群 </returns>
    public List<T> HitCheckGrid<T>(StageObject stageObject) where T : StageObject
    {
        var rect = stageObject.GetGridColliderRect();

        var position = stageObject.GetGridPosition();
        var size = stageObject.GetGridSize();

        List<T> result = new List<T>();
        var stageObjects = GetRangeGrid(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            T target = stageObjects[i] as T;

            if (!target)
                continue;

            Vector2 distance = Vector2.zero;

            if (!MathUtility.CheckHitRectToRect(rect, target.GetGridColliderRect(), out distance))
                continue;

            if (result.Contains(target))
                continue;

            result.Add(target);
        }

        return result;
    }

    /// <summary>
    /// 当たり判定を用いてオブジェクト群を取得
    /// </summary>
    /// <param name="stageObject"> 用いる当たり判定 </param>
    /// <returns> 当たったオブジェクト群 </returns>
    public List<StageObject> HitCheckGridActive(StageObject stageObject)
    {
        var rect = stageObject.GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        var position = stageObject.GetCalculateGridPosition(xMin, yMin);
        var size = stageObject.GetCalculateGridSize(xMin, xMax, yMin, yMax);

        List<StageObject> result = new List<StageObject>();
        var stageObjects = GetRangeGrid(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            if (!stageObjects[i])
                continue;

            Vector2 distance = Vector2.zero;

            if (!MathUtility.CheckHitRectToRect(rect, stageObjects[i].GetGridColliderRect(), out distance))
                continue;

            if (result.Contains(stageObjects[i]))
                continue;

            result.Add(stageObjects[i]);
        }

        return result;
    }

    /// <summary>
    /// 当たり判定を用いてオブジェクト群を取得
    /// </summary>
    /// <typeparam name="T"> 調べるオブジェクト </typeparam>
    /// <param name="stageObject"> 用いる当たり判定 </param>
    /// <returns> 当たったオブジェクト群 </returns>
    public List<T> HitCheckGridActive<T>(StageObject stageObject) where T : StageObject
    {
        var rect = stageObject.GetGridColliderRect();

        var xMin = rect.x - (rect.width * 0.5f);
        var yMin = rect.y - (rect.height * 0.5f);
        var xMax = xMin + rect.width;
        var yMax = yMin + rect.height;

        var position = stageObject.GetCalculateGridPosition(xMin, yMin);
        var size = stageObject.GetCalculateGridSize(xMin, xMax, yMin, yMax);

        List<T> result = new List<T>();
        var stageObjects = GetRangeGrid(position, size);

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            T target = stageObjects[i] as T;

            if (!target)
                continue;

            Vector2 distance = Vector2.zero;

            if (!MathUtility.CheckHitRectToRect(rect, target.GetGridColliderRect(), out distance))
                continue;

            if (result.Contains(target))
                continue;

            result.Add(target);
        }

        return result;
    }

    #endregion

    #region オブジェクトレイヤー関係

    /// <summary>
    /// オブジェクトレイヤー追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> オブジェクトレイヤー </returns>
    public StageObjectsLayer AddObjectsLayer(ObjectsLayerData data)
    {
        return objectManager.AddObjectsLayer(data);
    }

    /// <summary>
    /// オブジェクトレイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveObjectsLayer(StageObjectsLayer item)
    {
        objectManager.RemoveObjectsLayer(item);
    }

    #endregion

    #region 領域レイヤー関係

    /// <summary>
    /// 領域レイヤー追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 領域レイヤー </returns>
    public StageAreaLayer AddAreaLayer(AreaLayerData data)
    {
        return objectManager.AddAreaLayer(data);
    }

    /// <summary>
    /// 領域レイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveAreaLayer(StageAreaLayer item)
    {
        objectManager.RemoveAreaLayer(item);
    }

    #endregion

    #region ブロックレイヤー関係

    /// <summary>
    /// ブロックレイヤー追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> ブロックレイヤー </returns>
    public StageBlockLayer AddBlockLayer(BlockLayerData data)
    {
        return objectManager.AddBlockLayer(data);
    }

    /// <summary>
    /// ブロックレイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBlockLayer(StageBlockLayer item)
    {
        objectManager.RemoveBlockLayer(item);
    }

    #endregion

    #region 選択関係

    public void Select(HandleStageObject target)
    {
        scaleChangeManager.Select(target);
    }

    public void UnSelect()
    {
        scaleChangeManager.UnSelect();
    }

    #endregion

    #region 拡大縮小チェック

    /// <summary>
    /// 移動予定状態にする
    /// </summary>
    /// <param name="gridDirection"> 移動方向 </param>
    /// <returns> 成功したか </returns>
    public bool MoveLock(StageData.GridDirection gridDirection)
    {
        return scaleChangeManager.MoveLock(gridDirection);
    }

    /// <summary>
    /// 掴みロック状態にする
    /// </summary>
    public void GrabLock()
    {
        scaleChangeManager.GrabLock();
    }

    /// <summary>
    /// ロックを外す
    /// </summary>
    public void UnLock()
    {
        scaleChangeManager.UnLock();
    }

    /// <summary>
    /// 縮小可能か
    /// </summary>
    /// <param name="checkDistance"> 縮小距離 </param>
    /// <returns> 縮小可能か </returns>
    public int CheckCanScaleDown(int checkDistance)
    {
        return scaleChangeManager.CheckCanScaleDown(checkDistance);
    }

    /// <summary>
    /// 失敗の原因を登録
    /// </summary>
    /// <param name="target"> 失敗の原因 </param>
    public void SetFailedObject(StageObject target)
    {
        scaleChangeManager.SetFailedObject(target);
    }

    #endregion

    #region 拡大縮小関係

    /// <summary>
    /// 処理割合を取得
    /// </summary>
    /// <returns></returns>
    public float ScaleChangeGetProcessRate()
    {
        return scaleChangeManager.GetProcessRate();
    }

    /// <summary>
    /// グラブロック情報を設定する
    /// </summary>
    /// <param name="animationCurve"> 拡大縮小スピード </param>
    /// <param name="time"> 拡大縮小時間 </param>
    /// <param name="distance"> 拡大縮小距離 </param>
    /// <returns> 成功したか </returns>
    public bool ScaleChangeSetParameter(AnimationCurve animationCurve, float time, int distance)
    {
        return scaleChangeManager.SetParameter(animationCurve, time, distance);
    }

    /// <summary>
    /// 拡大縮小を始める
    /// </summary>
    public void ScaleChangeStartProcess()
    {
        scaleChangeManager.StartProcess();
    }

    /// <summary>
    /// 拡大縮小ロック中か
    /// </summary>
    /// <returns> ロック中 </returns>
    public bool CheckScaleChangeLock()
    {
        return scaleChangeManager.CheckScaleChangeLock();
    }

    #endregion

    #region ゲームクリア

    /// <summary>
    /// ゲームクリア状態にします
    /// </summary>
    /// <param name="goalPosition"> ゴール位置 </param>
    /// <param name="cameraTime"> カメラ移動時間 </param>
    /// <param name="calculateType"> カメラ移動方法 </param>
    public void SetGameClear(Vector3 goalPosition, float cameraTime, StageCamera.CalculateType calculateType)
    {
        if (myMainCondition != MainCondition.PlayNormal)
            return;

        myMainCondition = MainCondition.GameClear;
        cameraManager.SetWipeParameter(cameraTime * 1.3f, 0.25f);
        cameraManager.SetGameClearCamera(goalPosition, cameraTime, calculateType);
    }

    /// <summary>
    /// 次のステージを読み込む
    /// </summary>
    public void StartNextStage()
    {
        if (myMainCondition != MainCondition.GameClear)
            return;

        cameraManager.SetWipeParameter(4.0f, 0.0f);
    }

    #endregion

    #region ゲームオーバー

    /// <summary>
    /// ゲームオーバーステートに変更
    /// </summary>
    void ChangeConditionGameOver()
    {
        switch (myMainCondition)
        {
            case MainCondition.PlayNormal:
                break;
            default:
                return;
        }

        var playerActive = false;

        for (int i = 0; i < objectManager.ObjectsLayers.Count; ++i)
        {
            if (!objectManager.ObjectsLayers[i] || !objectManager.ObjectsLayers[i].GetFlagEnable())
                continue;

            var players = objectManager.ObjectsLayers[i].GetMyPlayerObjects();

            for (int j = 0; j < players.Count; ++j)
            {
                if (!players[i] || !players[i].GetFlagEnable())
                    continue;

                playerActive = true;
                break;
            }

            if (playerActive)
                break;
        }

        if (playerActive)
            return;

        myMainCondition = MainCondition.GameOver;
        gameOverMenu.Open();
    }

    #endregion

    #region ポーズメニュー関係

    /// <summary>
    /// ポーズメニューステートに変更
    /// </summary>
    void ChangeConditionPauseMenu()
    {
        if (myMainCondition == MainCondition.PlayNormal)
        {
            if (!Input_Manager.Instance.GetButtonTrigger(Input_Manager.ButtonIndex.Menu))
                return;

            pauseMenu.Open();
            Time.timeScale = 0.0f;
            myMainCondition = MainCondition.PauseMenu;
        }
        else if (myMainCondition == MainCondition.PauseMenu)
        {
            if (Input_Manager.Instance.GetButtonTrigger(Input_Manager.ButtonIndex.Menu))
                pauseMenu.Close();

            if (!pauseMenu.CheckIsClose())
                return;

            Time.timeScale = 1.0f;
            myMainCondition = MainCondition.PlayNormal;
        }
    }

    #endregion

    #region 原点位置関係

    /// <summary>
    /// 左X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetLeftPivotX()
    {
        return transform.position.x;
    }

    /// <summary>
    /// 中X原点取得
    /// </summary>
    /// <returns>中X原点</returns>
    public float GetMiddlePivotX()
    {
        return transform.position.x + (gridList.GetGridSize().x * 0.5f);
    }

    /// <summary>
    /// 右X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetRightPivotX()
    {
        return transform.position.x + gridList.GetGridSize().x;
    }

    /// <summary>
    /// 下Y原点取得
    /// </summary>
    /// <returns>下Y原点</returns>
    public float GetBottomPivotY()
    {
        return transform.position.y;
    }

    /// <summary>
    /// 中Y原点取得
    /// </summary>
    /// <returns>中Y原点</returns>
    public float GetMiddlePivotY()
    {
        return transform.position.y + (gridList.GetGridSize().y * 0.5f);
    }

    /// <summary>
    /// 上Y原点取得
    /// </summary>
    /// <returns>上Y原点</returns>
    public float GetTopPivotY()
    {
        return transform.position.y + gridList.GetGridSize().y;
    }

    #endregion

    #region 子クラス関係

    /// <summary>
    /// グリッド
    /// </summary>
    public class GridList
    {
        /// <summary> グリッド </summary>
        GridListX gridList = new GridListX();

        /// <summary> グリッドの大きさ </summary>
        Vector2Int gridSize = Vector2Int.zero;

        /// <summary> 横方向グリッド数取得用 </summary>
        int XCount { get => gridList.gridX.Count; }

        /// <summary> 縦方向グリッド数取得用 </summary>
        int YCount { get => XCount > 0 ? gridList.gridX[0].gridY.Count : 0; }

        GridListY this[int index]
        {
            get => gridList[index];
            set => gridList[index] = value;
        }

        ObjectList this[int indexX, int indexY]
        {
            get => gridList[indexX][indexY];
            set => gridList[indexX][indexY] = value;
        }

        StageObject this[int indexX, int indexY, int indexObject]
        {
            get => gridList[indexX][indexY][indexObject];
            set => gridList[indexX][indexY][indexObject] = value;
        }

        #region 初期化・解放関係

        /// <summary>
        /// 一番最初に呼び出す
        /// </summary>
        /// <returns> 成功したか </returns>
        public bool OnCreate()
        {
            return true;
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <returns> 成功したか </returns>
        public bool Initialize()
        {
            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAll();
        }

        #endregion

        #region グリッド関係

        /// <summary>
        /// x位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> x位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPositionX(int position)
        {
            var xMax = XCount;

            if (position < 0 || position >= xMax)
                return false;

            return true;
        }

        /// <summary>
        /// y位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> y位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPositionY(int position)
        {
            if (XCount < 1)
                return false;

            var yMax = YCount;

            if (position < 0 || position >= yMax)
                return false;

            return true;
        }

        /// <summary>
        /// 位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> 位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPosition(Vector2Int position)
        {
            var xMax = XCount;

            if (position.x < 0 || position.x >= xMax)
                return false;

            var yMax = gridList.gridX[position.x].gridY.Count;

            if (position.y < 0 || position.y >= yMax)
                return false;

            return true;
        }

        /// <summary>
        /// グリッドの大きさを取得
        /// </summary>
        /// <returns> グリッドの大きさ </returns>
        public Vector2Int GetGridSize()
        {
            return gridSize;
        }

        /// <summary>
        /// グリッドの大きさを設定
        /// </summary>
        /// <param name="gridSize"> グリッドの大きさ </param>
        public void SetGridSize(Vector2Int gridSize)
        {
            this.gridSize = gridSize;

            while (this.gridSize.x < gridList.gridX.Count)
                gridList.gridX.RemoveAt(gridList.gridX.Count - 1);

            for (int x = gridList.gridX.Count; x < this.gridSize.x; ++x)
                gridList.gridX.Add(new GridListY());

            for (int x = 0; x < gridList.gridX.Count; ++x)
            {
                while (this.gridSize.y < gridList.gridX[x].gridY.Count)
                    gridList.gridX[x].gridY.RemoveAt(gridList.gridX[x].gridY.Count - 1);

                for (int y = gridList.gridX[x].gridY.Count; y < this.gridSize.y; ++y)
                    gridList.gridX[x].gridY.Add(new ObjectList());
            }
        }

        /// <summary>
        /// グリッドから全て取り除く
        /// </summary>
        public void RemoveAll()
        {
            for (int x = 0, xLength = gridList.gridX.Count; x < xLength; ++x)
                for (int y = 0, yLength = gridList.gridX[x].gridY.Count; y < yLength; ++y)
                    gridList[x][y].objects.Clear();
        }

        /// <summary>
        /// グリッドから取り除く
        /// </summary>
        /// <param name="stageObject"> 取り除く対象 </param>
        /// <param name="position"> 取り除く位置 </param>
        /// <param name="size"> 取り除く範囲 </param>
        public void RemoveRange(StageObject stageObject, Vector2Int position, Vector2Int size)
        {
            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    if (!gridList[indexX][indexY].objects.Contains(stageObject))
                        continue;

                    gridList[indexX][indexY].objects.Remove(stageObject);
                }
            }
        }

        /// <summary>
        /// グリッドから取り除く
        /// </summary>
        /// <param name="stageObject"> 取り除く対象 </param>
        /// <param name="position"> 取り除く位置 </param>
        public void RemoveAt(StageObject stageObject, Vector2Int position)
        {
            if (!CheckInPosition(position))
                return;

            if (!gridList[position.x][position.y].objects.Contains(stageObject))
                return;

            gridList[position.x][position.y].objects.Remove(stageObject);
        }

        /// <summary>
        /// グリッドに追加する
        /// </summary>
        /// <param name="stageObject"> 追加する対象 </param>
        /// <param name="position"> 追加する位置 </param>
        /// <param name="size"> 追加する範囲 </param>
        public void AddRange(StageObject stageObject, Vector2Int position, Vector2Int size)
        {
            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    if (gridList[indexX][indexY].objects.Contains(stageObject))
                        return;

                    gridList[indexX][indexY].objects.Add(stageObject);
                }
            }
        }

        /// <summary>
        /// グリッドに追加する
        /// </summary>
        /// <param name="stageObject"> 追加する対象 </param>
        /// <param name="position"> 追加する位置 </param>
        public void AddAt(StageObject stageObject, Vector2Int position)
        {
            if (!CheckInPosition(position))
                return;

            if (gridList[position.x][position.y].objects.Contains(stageObject))
                return;

            gridList[position.x][position.y].objects.Add(stageObject);
        }

        /// <summary>
        /// グリッド指定位置に所属しているオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 取得する位置 </param>
        /// <param name="size"> 取得する範囲 </param>
        /// <returns> 所属しているオブジェクト群 </returns>
        public List<StageObject> GetRange(Vector2Int position, Vector2Int size)
        {
            var result = new List<StageObject>();

            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    for (int i = 0; i < gridList[indexX][indexY].objects.Count; ++i)
                    {
                        if (result.Contains(gridList[indexX][indexY][i]))
                            continue;

                        result.Add(gridList[indexX][indexY][i]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// グリッド指定位置に所属しているオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 取得する位置 </param>
        /// <returns> 所属しているオブジェクト群 </returns>
        public List<StageObject> GetAt(Vector2Int position)
        {
            if (!CheckInPosition(position))
                return new List<StageObject>();

            return gridList[position.x][position.y].objects;
        }

        #endregion

        #region 子クラス群

        /// <summary>
        /// マス目に所属するオブジェクト群
        /// </summary>
        class ObjectList
        {
            /// <summary> マス目に所属するオブジェクト群 </summary>
            public List<StageObject> objects = new List<StageObject>();

            public StageObject this[int index]
            {
                get { return CheckIndexInRange(index) ? objects[index] : null; }
                set { if (CheckIndexInRange(index)) objects[index] = value; }
            }

            /// <summary>
            /// インデックス番号が要素範囲内か
            /// </summary>
            /// <param name="index"> インデックス番号 </param>
            /// <returns> 範囲内か </returns>
            public bool CheckIndexInRange(int index)
            {
                return 0 <= index && index < objects.Count;
            }
        }

        /// <summary>
        /// 横方向グリッドリスト
        /// </summary>
        class GridListX
        {
            /// <summary> 横方向グリッドリスト </summary>
            public List<GridListY> gridX = new List<GridListY>();

            public GridListY this[int index]
            {
                get { return CheckIndexInRange(index) ? gridX[index] : null; }
                set { if (CheckIndexInRange(index)) gridX[index] = value; }
            }

            public ObjectList this[int indexX, int indexY]
            {
                get { return gridX[indexX] != null ? gridX[indexX][indexY] : null; }
                set { if (gridX[indexX] != null) gridX[indexX][indexY] = value; }
            }

            public StageObject this[int indexX, int indexY, int indexObject]
            {
                get { return gridX[indexX][indexY] != null ? gridX[indexX][indexY][indexObject] : null; }
                set { if (gridX[indexX][indexY] != null) gridX[indexX][indexY][indexObject] = value; }
            }

            /// <summary>
            /// インデックス番号が要素範囲内か
            /// </summary>
            /// <param name="index"> インデックス番号 </param>
            /// <returns> 範囲内か </returns>
            public bool CheckIndexInRange(int index)
            {
                return 0 <= index && index < gridX.Count;
            }
        }

        /// <summary>
        /// 縦方向グリッドリスト
        /// </summary>
        class GridListY
        {
            /// <summary> 縦方向グリッドリスト </summary>
            public List<ObjectList> gridY = new List<ObjectList>();

            public ObjectList this[int index]
            {
                get { return CheckIndexInRange(index) ? gridY[index] : null; }
                set { if (CheckIndexInRange(index)) gridY[index] = value; }
            }

            public StageObject this[int indexY, int indexObject]
            {
                get { return gridY[indexY] != null ? gridY[indexY][indexObject] : null; }
                set { if (gridY[indexY] != null) gridY[indexY][indexObject] = value; }
            }

            /// <summary>
            /// インデックス番号が要素範囲内か
            /// </summary>
            /// <param name="index"> インデックス番号 </param>
            /// <returns> 範囲内か </returns>
            public bool CheckIndexInRange(int index)
            {
                return 0 <= index && index < gridY.Count;
            }
        }

        #endregion
    }

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> オブジェクトレイヤー </summary>
        public List<StageObjectsLayer> objectsLayers = new List<StageObjectsLayer>();

        /// <summary> ブロックレイヤー </summary>
        public List<StageBlockLayer> blockLayers = new List<StageBlockLayer>();

        /// <summary> 領域レイヤー </summary>
        public List<StageAreaLayer> areaLayers = new List<StageAreaLayer>();
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> ステージマネージャー </summary>
        Stage_Manager stageManager = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = new StageObjects();

        /// <summary> オブジェクトレイヤー取得用 </summary>
        public List<StageObjectsLayer> ObjectsLayers { get => stageObjects != null ? stageObjects.objectsLayers : null; }

        /// <summary> ブロックレイヤー取得用 </summary>
        public List<StageBlockLayer> BlockLayers { get => stageObjects != null ? stageObjects.blockLayers : null; }

        /// <summary> 領域レイヤー取得用 </summary>
        public List<StageAreaLayer> AreaLayers { get => stageObjects != null ? stageObjects.areaLayers : null; }

        #region 初期化・解放関係

        /// <summary>
        /// 一番最初に呼び出す
        /// </summary>
        /// <param name="stageManager"> ステージマネージャー </param>
        /// <returns> 成功したか </returns>
        public bool OnCreate(Stage_Manager stageManager)
        {
            if (stageManager)
                this.stageManager = stageManager;

            if (!this.stageManager)
                return false;

            return true;
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <returns> 成功したか </returns>
        public bool Initialize()
        {
            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            var objectFactory = stageManager.objectFactory;

            if (!objectFactory)
                return;

            while (stageObjects.objectsLayers.Count > 0)
                RemoveObjectsLayer(stageObjects.objectsLayers[0]);

            while (stageObjects.blockLayers.Count > 0)
                RemoveBlockLayer(stageObjects.blockLayers[0]);

            while (stageObjects.areaLayers.Count > 0)
                RemoveAreaLayer(stageObjects.areaLayers[0]);
        }

        #endregion

        #region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.objectsLayers.Count; ++i)
                stageObjects.objectsLayers[i].UpdateProcess();

            for (int i = 0; i < stageObjects.blockLayers.Count; ++i)
                stageObjects.blockLayers[i].UpdateProcess();

            for (int i = 0; i < stageObjects.areaLayers.Count; ++i)
                stageObjects.areaLayers[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.objectsLayers.Count; ++i)
                stageObjects.objectsLayers[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.blockLayers.Count; ++i)
                stageObjects.blockLayers[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.areaLayers.Count; ++i)
                stageObjects.areaLayers[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.objectsLayers.Count; ++i)
                stageObjects.objectsLayers[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.blockLayers.Count; ++i)
                stageObjects.blockLayers[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.areaLayers.Count; ++i)
                stageObjects.areaLayers[i].LateUpdateProcess();
        }

        #endregion

        #region オブジェクトレイヤー関係

        /// <summary>
        /// オブジェクトレイヤー追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> オブジェクトレイヤー </returns>
        public StageObjectsLayer AddObjectsLayer(ObjectsLayerData data)
        {
            var objectFactory = stageManager.objectFactory;

            var newItem = objectFactory ? objectFactory.InstantiateObjectsLayer(stageManager) : null;

            if (!newItem)
                return null;

            if (!newItem.SetData(data))
            {
                objectFactory.DestroyObjectsLayer(newItem);

                return null;
            }

            if (!stageObjects.objectsLayers.Contains(newItem))
                stageObjects.objectsLayers.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// オブジェクトレイヤー削除
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveObjectsLayer(StageObjectsLayer item)
        {
            var objectFactory = stageManager.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.objectsLayers.Contains(item))
                stageObjects.objectsLayers.Remove(item);

            objectFactory.DestroyObjectsLayer(item);
        }

        #endregion

        #region 領域レイヤー関係

        /// <summary>
        /// 領域レイヤー追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 領域レイヤー </returns>
        public StageAreaLayer AddAreaLayer(AreaLayerData data)
        {
            var objectFactory = stageManager.objectFactory;

            var newItem = objectFactory ? objectFactory.InstantiateAreaLayer(stageManager) : null;

            if (!newItem)
                return null;

            if (!newItem.SetData(data))
            {
                objectFactory.DestroyAreaLayer(newItem);

                return null;
            }

            if (!stageObjects.areaLayers.Contains(newItem))
                stageObjects.areaLayers.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 領域レイヤー削除
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveAreaLayer(StageAreaLayer item)
        {
            var objectFactory = stageManager.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.areaLayers.Contains(item))
                stageObjects.areaLayers.Remove(item);

            objectFactory.DestroyAreaLayer(item);
        }

        #endregion

        #region ブロックレイヤー関係

        /// <summary>
        /// ブロックレイヤー追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> ブロックレイヤー </returns>
        public StageBlockLayer AddBlockLayer(BlockLayerData data)
        {
            var objectFactory = stageManager.objectFactory;

            var newItem = objectFactory ? objectFactory.InstantiateBlockLayer(stageManager) : null;

            if (!newItem)
                return null;

            if (!newItem.SetData(data))
            {
                objectFactory.DestroyBlockLayer(newItem);

                return null;
            }

            if (!stageObjects.blockLayers.Contains(newItem))
                stageObjects.blockLayers.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// ブロックレイヤー削除
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveBlockLayer(StageBlockLayer item)
        {
            var objectFactory = stageManager.objectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.blockLayers.Contains(item))
                stageObjects.blockLayers.Remove(item);

            objectFactory.DestroyBlockLayer(item);
        }

        #endregion
    }

    /// <summary>
    /// 拡大縮小マネージャー
    /// </summary>
    public class ScaleChange_Manager
    {
        /// <summary> ステージマネージャー </summary>
        Stage_Manager stageManager = null;

        /// <summary> 選択情報 </summary>
        SelectInformation selectInformation = new SelectInformation();

        /// <summary> > 拡大縮小情報 </summary>
        ScaleChangeInformation scaleChangeInformation = new ScaleChangeInformation();

        #region 初期化・解放処理

        /// <summary>
        /// 一番最初に呼び出す
        /// </summary>
        /// <param name="stageManager"> ステージマネージャー </param>
        /// <returns> 成功したか </returns>
        public bool OnCreate(Stage_Manager stageManager)
        {
            if (stageManager)
                this.stageManager = stageManager;

            if (!this.stageManager)
                return false;

            return true;
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <returns> 成功した </returns>
        public bool Initialize()
        {
            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            selectInformation.selectHandle = null;
            selectInformation.redHandleType.Clear();
            selectInformation.greenHandleType.Clear();
            selectInformation.blueHandleType.Clear();

            scaleChangeInformation.scaleChangeLock = false;
        }

        #endregion

        #region 選択関係

        public void Select(HandleStageObject target)
        {
            UnSelect();

            if (!target)
                return;

            selectInformation.selectHandle = target;

            var targetLinkGroupNumber = target.GetLinkGroupNumber();
            var targetHandleType = target.GetHandleType();
            var targetIsHorizontal = HandleData.CheckHorizontalHandleType(targetHandleType);
            var areaLayers = stageManager.objectManager.AreaLayers;

            if (targetLinkGroupNumber < 0)
            {
                var areaType = target.GetAreaType();

                switch (areaType)
                {
                    case BlockData.BlockType.Red:
                        selectInformation.redHandleType.Add(target);
                        break;
                    case BlockData.BlockType.Green:
                        selectInformation.greenHandleType.Add(target);
                        break;
                    case BlockData.BlockType.Blue:
                        selectInformation.blueHandleType.Add(target);
                        break;
                }
            }
            else
            {
                for (int i = 0; i < areaLayers.Count; ++i)
                {
                    var handles = areaLayers[i].GetMyHandleObjects();

                    for (int j = 0; j < handles.Count; ++j)
                    {
                        var handleType = handles[j].GetHandleType();

                        if (targetIsHorizontal != HandleData.CheckHorizontalHandleType(handleType))
                            continue;

                        if (targetLinkGroupNumber != handles[j].GetLinkGroupNumber())
                            continue;

                        var areaType = handles[j].GetAreaType();

                        switch (areaType)
                        {
                            case BlockData.BlockType.Red:
                                if (!selectInformation.redHandleType.Contains(handles[j])) 
                                    selectInformation.redHandleType.Add(handles[j]);
                                break;
                            case BlockData.BlockType.Green:
                                if (!selectInformation.greenHandleType.Contains(handles[j])) 
                                    selectInformation.greenHandleType.Add(handles[j]);
                                break;
                            case BlockData.BlockType.Blue:
                                if (!selectInformation.blueHandleType.Contains(handles[j])) 
                                    selectInformation.blueHandleType.Add(handles[j]);
                                break;
                        }
                    }
                }
            }

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].Select(selectInformation);

            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].Select(selectInformation);
        }

        public void UnSelect()
        {
            if (!selectInformation.selectHandle)
                return;

            var areaLayers = stageManager.objectManager.AreaLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].UnSelect(selectInformation);

            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].UnSelect(selectInformation);

            selectInformation.selectHandle = null;
            selectInformation.redHandleType.Clear();
            selectInformation.greenHandleType.Clear();
            selectInformation.blueHandleType.Clear();
        }

        #endregion

        #region 拡大縮小チェック

        /// <summary>
        /// 拡大縮小ロック中か
        /// </summary>
        /// <returns> ロック中 </returns>
        public bool CheckScaleChangeLock()
        {
            return scaleChangeInformation.scaleChangeLock;
        }

        /// <summary>
        /// 移動予定状態にする
        /// </summary>
        /// <param name="gridDirection"> 移動方向 </param>
        /// <returns> 成功したか </returns>
        public bool MoveLock(StageData.GridDirection gridDirection)
        {
            if (!selectInformation.selectHandle)
                return false;

            var isHorizontal = HandleData.CheckHorizontalHandleType(selectInformation.selectHandle.GetHandleType());

            switch (gridDirection)
            {
                case StageData.GridDirection.Left:
                case StageData.GridDirection.Right:
                    if (isHorizontal)
                        break;
                    return false;
                case StageData.GridDirection.Up:
                case StageData.GridDirection.Down:
                    if (!isHorizontal)
                        break;
                    return false;
                default:
                    return false;
            }

            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].MoveLock(gridDirection, selectInformation);

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].MoveLock(gridDirection, selectInformation);

            return true;
        }

        /// <summary>
        /// 掴みロック状態にする
        /// </summary>
        public void GrabLock()
        {
            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].GrabLock();

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].GrabLock();
        }

        /// <summary>
        /// ロックを外す
        /// </summary>
        public void UnLock()
        {
            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].UnLock();

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].UnLock();
        }

        /// <summary>
        /// 縮小可能か
        /// </summary>
        /// <param name="checkDistance"> 縮小距離 </param>
        /// <returns> 縮小可能か </returns>
        public int CheckCanScaleDown(int checkDistance)
        {
            int distance = checkDistance;

            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                distance = Math.Min(distance, areaLayers[i].CheckCanScaleDown(distance));

            for (int i = 0; i < blockLayers.Count; ++i)
                distance = Math.Min(distance, blockLayers[i].CheckCanScaleDown(distance));

            return distance;
        }

        /// <summary>
        /// 失敗の原因を登録
        /// </summary>
        /// <param name="target"> 失敗の原因 </param>
        public void SetFailedObject(StageObject target)
        {
            if (!scaleChangeInformation.scaleChangeLock)
                return;

            if (scaleChangeInformation.isFailed)
                return;

            if (scaleChangeInformation.failedObjects.Contains(target))
                return;

            scaleChangeInformation.failedObjects.Add(target);
        }

        #endregion

        #region 拡大縮小関係

        /// <summary>
        /// 処理割合を取得
        /// </summary>
        /// <returns></returns>
        public float GetProcessRate()
        {
            var realTime = scaleChangeInformation.time * scaleChangeInformation.distance;

            return scaleChangeInformation.animationCurve.Evaluate(scaleChangeInformation.deltaTime / realTime);
        }

        /// <summary>
        /// グラブロック情報を設定する
        /// </summary>
        /// <param name="animationCurve"> 拡大縮小スピード </param>
        /// <param name="time"> 拡大縮小時間 </param>
        /// <param name="distance"> 拡大縮小距離 </param>
        /// <returns> 成功したか </returns>
        public bool SetParameter(AnimationCurve animationCurve, float time, int distance)
        {
            if (scaleChangeInformation.scaleChangeLock)
                return false;

            if (distance < 1)
                return false;

            if (time <= 0.0f)
                return false;

            scaleChangeInformation.animationCurve = animationCurve;
            scaleChangeInformation.time = time;
            scaleChangeInformation.distance = distance;

            return true;
        }

        /// <summary>
        /// 拡大縮小を始める
        /// </summary>
        public void StartProcess()
        {
            if (scaleChangeInformation.scaleChangeLock)
                return;

            GrabLock();
            stageManager.sorce.OnceSE();// SEを鳴らす

            scaleChangeInformation.failedObjects.Clear();
            scaleChangeInformation.scaleChangeLock = true;
            scaleChangeInformation.isFailed = false;
            scaleChangeInformation.failedDeltaTime = 0.0f;
            scaleChangeInformation.deltaTime = 0.0f;
            scaleChangeInformation.currentDistance = 0;

            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].ScaleChangeStartProcess();

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].ScaleChangeStartProcess();

            UnSelect();
        }

        /// <summary>
        /// 拡大終了の終了
        /// </summary>
        public void FinishProcess()
        {
            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].ScaleChangeFinishProcess();

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].ScaleChangeFinishProcess();

            stageManager.sorce.StopSE();
            scaleChangeInformation.scaleChangeLock = false;

            UnLock();

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].CreateSelectInformationAll();

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].CreateSelectInformationAll();
        }

        /// <summary>
        /// 拡大縮小更新
        /// </summary>
        /// <param name="deltaTime"></param>
        public void UpdateProcess(float deltaTime)
        {
            if (!scaleChangeInformation.scaleChangeLock)
                return;

            var isFailed = !scaleChangeInformation.isFailed && scaleChangeInformation.failedObjects.Count > 0;

            if (scaleChangeInformation.isFailed)
                FailedUpdateProcess(deltaTime);
            else
                SuccessUpdateProcess(deltaTime);

            if (isFailed)
            {
                scaleChangeInformation.isFailed = true;

                for (int i = 0; i < scaleChangeInformation.failedObjects.Count; ++i)
                    scaleChangeInformation.failedObjects[i].FailedEffect();

                scaleChangeInformation.failedObjects.Clear();

                stageManager.seTest.CancelSE();
            }
        }

        /// <summary>
        /// 成功時の拡大縮小更新
        /// </summary>
        /// <param name="deltaTime"> 経過時間 </param>
        void SuccessUpdateProcess(float deltaTime)
        {
            var realTime = scaleChangeInformation.time * scaleChangeInformation.distance;

            scaleChangeInformation.deltaTime += deltaTime;

            if (scaleChangeInformation.deltaTime >= realTime)
            {
                scaleChangeInformation.deltaTime = realTime;
                scaleChangeInformation.scaleChangeLock = false;
            }

            var processRate = scaleChangeInformation.animationCurve.Evaluate(scaleChangeInformation.deltaTime / realTime);
            var currentDistance = Math.Clamp(Mathf.FloorToInt(processRate * scaleChangeInformation.distance), 0, scaleChangeInformation.distance);
            scaleChangeInformation.currentDistance = currentDistance;

            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].ScaleChangeUpdateProcess(scaleChangeInformation.isFailed, processRate, currentDistance, scaleChangeInformation.distance);

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].ScaleChangeUpdateProcess(scaleChangeInformation.isFailed, processRate, currentDistance, scaleChangeInformation.distance);

            if (!scaleChangeInformation.scaleChangeLock)
                FinishProcess();
        }

        /// <summary>
        /// 失敗時の拡大縮小更新
        /// </summary>
        /// <param name="deltaTime"> 経過時間 </param>
        void FailedUpdateProcess(float deltaTime)
        {
            var realTime = scaleChangeInformation.time * scaleChangeInformation.distance;
            var failedTime = scaleChangeInformation.time * scaleChangeInformation.currentDistance;

            scaleChangeInformation.deltaTime -= deltaTime;
            scaleChangeInformation.failedDeltaTime += deltaTime;

            if (scaleChangeInformation.deltaTime <= failedTime)
            {
                scaleChangeInformation.deltaTime = failedTime;
                if (scaleChangeInformation.failedDeltaTime > stageManager.failedTime)
                    scaleChangeInformation.scaleChangeLock = false;
            }

            var processRate = scaleChangeInformation.deltaTime / realTime;
            var currentDistance = Math.Clamp(Mathf.CeilToInt(processRate * scaleChangeInformation.distance), 0, scaleChangeInformation.distance);

            var areaLayers = stageManager.objectManager.AreaLayers;
            var blockLayers = stageManager.objectManager.BlockLayers;

            for (int i = 0; i < areaLayers.Count; ++i)
                areaLayers[i].ScaleChangeUpdateProcess(scaleChangeInformation.isFailed, processRate, currentDistance, scaleChangeInformation.distance);

            for (int i = 0; i < blockLayers.Count; ++i)
                blockLayers[i].ScaleChangeUpdateProcess(scaleChangeInformation.isFailed, processRate, currentDistance, scaleChangeInformation.distance);

            if (!scaleChangeInformation.scaleChangeLock)
                FinishProcess();
        }

        #endregion

        #region 子クラス

        /// <summary>
        /// 選択情報
        /// </summary>
        public class SelectInformation
        {
            /// <summary> 選択中の取っ手 </summary>
            public HandleStageObject selectHandle = null;

            /// <summary> 選択中の赤取っ手 </summary>
            public List<HandleStageObject> redHandleType = new List<HandleStageObject>();

            /// <summary> 選択中の赤取っ手 </summary>
            public List<HandleStageObject> greenHandleType = new List<HandleStageObject>();

            /// <summary> 選択中の赤取っ手 </summary>
            public List<HandleStageObject> blueHandleType = new List<HandleStageObject>();
        }

        /// <summary>
        /// 拡大縮小情報
        /// </summary>
        public class ScaleChangeInformation
        {
            /// <summary> 失敗の原因 </summary>
            public List<StageObject> failedObjects = new List<StageObject>();

            /// <summary> 拡大縮小スピード </summary>
            public AnimationCurve animationCurve;

            /// <summary> 拡大縮小経過時間 </summary>
            public float deltaTime;

            /// <summary> 失敗経過時間 </summary>
            public float failedDeltaTime;

            /// <summary> 拡大縮小時間 </summary>
            public float time;

            /// <summary> 拡大縮小距離 </summary>
            public int distance;

            /// <summary> 現在の拡大縮小距離 </summary>
            public int currentDistance;

            /// <summary> 拡大縮小ロック状態か </summary>
            public bool scaleChangeLock;

            /// <summary> 失敗状態か </summary>
            public bool isFailed;
        }

        #endregion
    }

    /// <summary>
    /// カメラマネージャー
    /// </summary>
    public class Camera_Manager
    {
        /// <summary> ステージマネージャー </summary>
        Stage_Manager stageManager = null;

        /// <summary> ステージ用カメラ </summary>
        StageCamera stageCamera = null;

        /// <summary> 主要なプレイヤー </summary>
        PlayerStageObject mainPlayer = null;

        /// <summary> 主要なカメラ領域 </summary>
        CameraAreaStageObject mainCameraArea = null;

        /// <summary> カメラ位置 </summary>
        Vector2Int cameraAreaOffset = Vector2Int.zero;

        /// <summary> カメラの大きさ </summary>
        Vector2Int cameraAreaSize = Vector2Int.one;

        /// <summary> カメラの初期Z位置 </summary>
        float cameraDefaultZOrder = -11.0f;

        #region 初期化・解放処理

        /// <summary>
        /// 一番最初に呼び出す
        /// </summary>
        /// <param name="stageManager"> ステージマネージャー </param>
        /// <returns> 成功したか </returns>
        public bool OnCreate(Stage_Manager stageManager)
        {
            if (stageManager)
                this.stageManager = stageManager;

            if (!stageCamera)
            {
                stageCamera = FindObjectOfType<StageCamera>();
                stageCamera?.Initialize();
            }

            if (!this.stageManager || !stageCamera)
                return false;

            cameraDefaultZOrder = stageCamera.transform.position.z;

            return true;
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <returns> 成功したか </returns>
        public bool Initialize()
        {
            UnInitialize();

            UpdateProcess(true);

            stageCamera.ForcedChangePositionZ(cameraDefaultZOrder);

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            mainPlayer = null;
            mainCameraArea = null;
            cameraAreaOffset = Vector2Int.zero;
            cameraAreaSize = Vector2Int.one;
        }

        #endregion

        #region 更新関係

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="isForcedChange"> 強制的に変更するか </param>
        public void UpdateProcess(bool isForcedChange)
        {
            CheckMainPlayer();
            CheckMainCameraArea(isForcedChange);
        }

        #endregion

        #region 主要プレイヤー関係

        /// <summary>
        /// 主要なプレイヤーを探索
        /// </summary>
        void CheckMainPlayer()
        {
            var objectManager = stageManager.objectManager;

            if (objectManager == null)
                return;

            if (mainPlayer && mainPlayer.GetFlagEnable())
            {
                var gridPosition = mainPlayer.GetGridPosition();
                var gridSize = mainPlayer.GetGridSize();

                if (stageManager.CheckInPosition(gridPosition) && stageManager.CheckInPosition(gridPosition + gridSize - Vector2Int.one))
                    return;
            }

            mainPlayer = null;

            for (int i = 0; i < objectManager.ObjectsLayers.Count; ++i)
            {
                if (!objectManager.ObjectsLayers[i] || !objectManager.ObjectsLayers[i].GetFlagEnable())
                    continue;

                var players = objectManager.ObjectsLayers[i].GetMyPlayerObjects();

                for (int j = 0; j < players.Count; ++j)
                {
                    if (!players[i] || !players[i].GetFlagEnable())
                        continue;

                    var gridPosition = players[i].GetGridPosition();
                    var gridSize = players[i].GetGridSize();

                    if (!stageManager.CheckInPosition(gridPosition) || !stageManager.CheckInPosition(gridPosition + gridSize - Vector2Int.one))
                        continue;

                    mainPlayer = players[i];
                    break;
                }

                if (mainPlayer)
                    return;
            }
        }

        #endregion

        #region 主要カメラ領域関係

        void CheckMainCameraArea(bool isForcedChange)
        {
            if (!mainPlayer)
                return;

            var gridList = stageManager.gridList;

            if (gridList == null)
                return;

            var stageObjects = gridList.GetRange(mainPlayer.GetGridPosition(), mainPlayer.GetGridSize());
            CameraAreaStageObject next = null;

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var currentHit = stageObjects[i] as CameraAreaStageObject;

                if (!currentHit || !currentHit.GetFlagEnable())
                    continue;

                if (currentHit == mainCameraArea)
                    return;

                next = currentHit;
            }

            mainCameraArea = next;

            if (mainCameraArea)
            {
                cameraAreaOffset = mainCameraArea.CameraAreaOffset;
                cameraAreaSize = mainCameraArea.CameraAreaSize;
            }
            else
            {
                cameraAreaOffset = Vector2Int.zero;
                cameraAreaSize = gridList.GetGridSize();
            }

            SetCameraParameter(isForcedChange);
        }

        void SetCameraParameter(bool isForcedChange)
        {
            if (!stageCamera)
                return;

            var targetCamera = stageCamera.MyCamera;

            if (!targetCamera)
                return;

            var distance = MathF.Abs(targetCamera.transform.position.z);

            var cameraCenter = new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
            cameraCenter.x += cameraAreaOffset.x + (cameraAreaSize.x * 0.5f);
            cameraCenter.y += cameraAreaOffset.y + (cameraAreaSize.y * 0.5f);

            var widthFieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumWidth(targetCamera, cameraAreaSize.x, distance);
            var heightFieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumHeight(targetCamera, cameraAreaSize.y, distance);
            var fieldOfView = widthFieldOfView > heightFieldOfView ? widthFieldOfView : heightFieldOfView;

            if (isForcedChange)
            {
                stageCamera.ForcedChangePositionXY(cameraCenter);
                stageCamera.ForcedChangeFieldOfView(fieldOfView);
            }
            else
            {
                stageCamera.SetPositionXY(cameraCenter, 0.5f, StageCamera.CalculateType.Lerp);
                stageCamera.SetFieldOfView(fieldOfView, 0.5f, StageCamera.CalculateType.Lerp);
            }
        }

        #endregion

        #region ゲームクリアカメラ

        /// <summary>
        /// 現在の半径を取得
        /// </summary>
        /// <returns> 半径 </returns>
        public float GetWipeRadius()
        {
            return stageCamera.GetRadius();
        }

        /// <summary>
        /// 強制的にワイプ情報設定
        /// </summary>
        /// <param name="wipeRadius"> ワイプの半径 </param>
        public void ForcedSetWipeParameter(float wipeRadius)
        {
            stageCamera.ForcedSetRadius(wipeRadius);
        }

        /// <summary>
        /// ワイプ情報設定
        /// </summary>
        /// <param name="wipeTime"> ワイプにかかる時間 </param>
        /// <param name="wipeRadius"> ワイプの半径 </param>
        public void SetWipeParameter(float wipeTime, float wipeRadius)
        {
            stageCamera.SetWiprTime(wipeTime);
            stageCamera.SetRadius(wipeRadius);
        }

        /// <summary>
        /// ゲームクリア時のカメラ設定
        /// </summary>
        /// <param name="goalPosition"> ゴール位置 </param>
        /// <param name="cameraTime"> カメラ移動時間 </param>
        /// <param name="calculateType"> カメラ移動方法 </param>
        public void SetGameClearCamera(Vector3 goalPosition, float cameraTime, StageCamera.CalculateType calculateType)
        {
            var position = goalPosition + stageManager.clearCameraOffset;
            var fieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumWidth(stageCamera.MyCamera, stageManager.clearCameraHorizontalViewSize, Mathf.Max(0.1f, Mathf.Abs(position.z)));

            stageCamera.DestroyNextFieldOfView();
            stageCamera.DestroyNextPositionXY();
            stageCamera.DestroyNextPositionZ();

            stageCamera.SetFieldOfView(fieldOfView, cameraTime, calculateType);
            stageCamera.SetPositionXY(new Vector2(position.x, position.y), cameraTime, calculateType);
            stageCamera.SetPositionZ(position.z, cameraTime, calculateType);
        }

        #endregion
    }

    #endregion
}

#elif false

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityEditor;
using UnityEditorInternal.Profiling.Memory.Experimental;
using UnityEngine;
using static Stage_Manager;

public class Stage_Manager : MonoBehaviour
{
#region enum

    /// <summary>
    /// 状態
    /// </summary>
    public enum MainCondition
    {
        Reset,
        StartWait,
        PlayNormal,
    }

#endregion

    /// <summary> オブジェクトマネージャー </summary>
    StageObject_Manager objectManager = null;

    /// <summary> 拡大縮小マネージャー </summary>
    ScaleChange_Manager scaleChangeManager = null;

    /// <summary> カメラマネージャー </summary>
    Camera_Manager cameraManager = null;

    /// <summary> マス目に所属するオブジェクト群 </summary>
    GridHasStageObjects gridHasObjects = null;

    /// <summary> ステージのデータ </summary>
    StageData myStageData = null;

    /// <summary> オブジェクトファクトリー </summary>
    StageObjectFactory objectFactory = null;

    /// <summary> 状態 </summary>
    MainCondition myMainCondition = MainCondition.StartWait;

    /// <summary> オブジェクトマネージャー </summary>
    public StageObject_Manager ObjectManager { get => objectManager; }

    /// <summary> 拡大縮小マネージャー </summary>
    public ScaleChange_Manager ScaleChangeManager { get => scaleChangeManager; }

    /// <summary> カメラマネージャー </summary>
    public Camera_Manager CameraManager { get => cameraManager; }

    /// <summary> マス目に所属するオブジェクト群 </summary>
    public GridHasStageObjects GridHasObjects { get => gridHasObjects; }

    /// <summary> ステージのデータ取得用 </summary>
    public StageData MyStageData { get => myStageData; }

    /// <summary> オブジェクトファクトリー 取得用 </summary>
    public StageObjectFactory ObjectFactory { get => objectFactory; }

    /// <summary> 状態 取得用 </summary>
    public MainCondition MyMainCondition { get => myMainCondition; }

    /// <summary> グリッドの大きさ 取得用 </summary>
    public Vector2Int GridSize { get => myStageData == null ? Vector2Int.one : myStageData.GridSize; }

#region Unity標準

    private void Awake()
    {
        Initialize();
    }

    private void Start()
    {

    }

    private void Update()
    {
        objectManager.UpdateProcess();
    }

    private void FixedUpdate()
    {
        objectManager.FixedUpdateProcess();
    }

    private void LateUpdate()
    {
        objectManager.LateUpdateProcess();

        switch (myMainCondition)
        {
            case MainCondition.Reset:
                if (Fade_Manager.Instance.CheckCurrentIdleAfterFadeOut())
                    CreateStage();
                break;
            case MainCondition.StartWait:
                if (Fade_Manager.Instance.CheckCurrentIdleAfterFadeIn())
                    myMainCondition = MainCondition.PlayNormal;
                break;
            case MainCondition.PlayNormal:
                cameraManager.UpdateProcess(false);
                break;
        }

        Debug.Log(myMainCondition);
    }

#endregion

#region 初期化・解放

    /// <summary>
    /// 初期化処理
    /// </summary>
    public void Initialize()
    {
        objectFactory = FindObjectOfType<StageObjectFactory>();
        objectManager = new StageObject_Manager(this);
        scaleChangeManager = new ScaleChange_Manager(this);
        cameraManager = new Camera_Manager(this);
        gridHasObjects = new GridHasStageObjects();
        myStageData = new StageData();

        ChangeObjectName();

        objectManager.Initialize();
        scaleChangeManager.Initialize();
        cameraManager.Initialize();

        var gameManager = Game_Manager.Instance;
        myStageData.ReadData(gameManager.WorldNum, gameManager.StageNum);

        CreateStage();
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public void UnInitialize()
    {
        objectManager.UnInitialize();
        scaleChangeManager.UnInitialize();
        cameraManager.UnInitialize();
        gridHasObjects.RemoveAll();
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public void ChangeObjectName()
    {
        gameObject.name = "StageManager";
    }

    /// <summary>
    /// ステージ作成
    /// </summary>
    void CreateStage()
    {
        UnInitialize();

        objectManager.Initialize();
        scaleChangeManager.Initialize();
        cameraManager.Initialize();

        gridHasObjects.SetGridSize(myStageData.GridSize);

        for (int i = 0; i < myStageData.BlockLayerDatas.Count; ++i)
            objectManager.AddBlockLayer(myStageData.BlockLayerDatas[i]);

        for (int i = 0; i < myStageData.AreaLayerDatas.Count; ++i)
            objectManager.AddAreaLayer(myStageData.AreaLayerDatas[i]);

        for (int i = 0; i < myStageData.ObjectsLayerDatas.Count; ++i)
            objectManager.AddObjectsLayer(myStageData.ObjectsLayerDatas[i]);

        cameraManager.UpdateProcess(true);

        Fade_Manager.Instance.StartFadeIn();

        myMainCondition = MainCondition.StartWait;
    }

    /// <summary>
    /// ステージをもう一度生成する
    /// </summary>
    public void ReSetStage()
    {
        Fade_Manager.Instance.StartFadeOut();
        myMainCondition = MainCondition.Reset;
    }

#endregion

#region 原点位置関係

    /// <summary>
    /// 左X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetLeftPivotX()
    {
        return transform.position.x;
    }

    /// <summary>
    /// 中X原点取得
    /// </summary>
    /// <returns>中X原点</returns>
    public float GetMiddlePivotX()
    {
        return transform.position.x + (myStageData.GridSize.x * 0.5f);
    }

    /// <summary>
    /// 右X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetRightPivotX()
    {
        return transform.position.x + myStageData.GridSize.x;
    }

    /// <summary>
    /// 下Y原点取得
    /// </summary>
    /// <returns>下Y原点</returns>
    public float GetBottomPivotY()
    {
        return transform.position.y;
    }

    /// <summary>
    /// 中Y原点取得
    /// </summary>
    /// <returns>中Y原点</returns>
    public float GetMiddlePivotY()
    {
        return transform.position.y + (myStageData.GridSize.y * 0.5f);
    }

    /// <summary>
    /// 上Y原点取得
    /// </summary>
    /// <returns>上Y原点</returns>
    public float GetTopPivotY()
    {
        return transform.position.y + myStageData.GridSize.y;
    }

#endregion

#region 子クラス

    /// <summary>
    /// グリッドに所属するオブジェクト
    /// </summary>
    public class GridHasStageObjects
    {
        /// <summary> グリッド </summary>
        GridListX gridList = new GridListX();

        /// <summary> 横方向グリッド数取得用 </summary>
        public int XCount { get => gridList.gridX.Count; }

        /// <summary> 縦方向グリッド数取得用 </summary>
        public int YCount { get => XCount > 0 ? gridList.gridX[0].gridY.Count : 0; }

#region グリッド関係

        /// <summary>
        /// x位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> x位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPositionX(int position)
        {
            var xMax = XCount;

            if (position < 0 || position >= xMax)
                return false;

            return true;
        }

        /// <summary>
        /// y位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> y位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPositionY(int position)
        {
            if (XCount < 1)
                return false;

            var yMax = YCount;

            if (position < 0 || position >= yMax)
                return false;

            return true;
        }

        /// <summary>
        /// 位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> 位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPosition(Vector2Int position)
        {
            var xMax = XCount;

            if (position.x < 0 || position.x >= xMax)
                return false;

            var yMax = gridList.gridX[position.x].gridY.Count;

            if (position.y < 0 || position.y >= yMax)
                return false;

            return true;
        }

        /// <summary>
        /// グリッドの大きさを設定
        /// </summary>
        /// <param name="gridSize"> グリッドの大きさ </param>
        public void SetGridSize(Vector2Int gridSize)
        {
            while (gridSize.x < gridList.gridX.Count)
                gridList.gridX.RemoveAt(gridList.gridX.Count - 1);

            for (int x = gridList.gridX.Count; x < gridSize.x; ++x)
                gridList.gridX.Add(new GridListY());

            for (int x = 0; x < gridList.gridX.Count; ++x)
            {
                while (gridSize.y < gridList.gridX[x].gridY.Count)
                    gridList.gridX[x].gridY.RemoveAt(gridList.gridX[x].gridY.Count - 1);

                for (int y = gridList.gridX[x].gridY.Count; y < gridSize.y; ++y)
                    gridList.gridX[x].gridY.Add(new ObjectList());
            }
        }

        /// <summary>
        /// グリッドから全て取り除く
        /// </summary>
        public void RemoveAll()
        {
            for (int x = 0, xLength = gridList.gridX.Count; x < xLength; ++x)
                for (int y = 0, yLength = gridList.gridX[x].gridY.Count; y < yLength; ++y)
                    gridList.gridX[x].gridY[y].objects.Clear();
        }

        /// <summary>
        /// グリッドから取り除く
        /// </summary>
        /// <param name="stageObject"> 取り除く対象 </param>
        /// <param name="position"> 取り除く位置 </param>
        /// <param name="size"> 取り除く範囲 </param>
        public void RemoveRange(StageObject stageObject, Vector2Int position, Vector2Int size)
        {
            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    if (!gridList.gridX[indexX].gridY[indexY].objects.Contains(stageObject))
                        continue;

                    gridList.gridX[indexX].gridY[indexY].objects.Remove(stageObject);
                }
            }
        }

        /// <summary>
        /// グリッドから取り除く
        /// </summary>
        /// <param name="stageObject"> 取り除く対象 </param>
        /// <param name="position"> 取り除く位置 </param>
        public void RemoveAt(StageObject stageObject, Vector2Int position)
        {
            if (!CheckInPosition(position))
                return;

            if (!gridList.gridX[position.x].gridY[position.y].objects.Contains(stageObject))
                return;

            gridList.gridX[position.x].gridY[position.y].objects.Remove(stageObject);
        }

        /// <summary>
        /// グリッドに追加する
        /// </summary>
        /// <param name="stageObject"> 追加する対象 </param>
        /// <param name="position"> 追加する位置 </param>
        /// <param name="size"> 追加する範囲 </param>
        public void AddRange(StageObject stageObject, Vector2Int position, Vector2Int size)
        {
            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    if (gridList.gridX[indexX].gridY[indexY].objects.Contains(stageObject))
                        return;

                    gridList.gridX[indexX].gridY[indexY].objects.Add(stageObject);
                }
            }
        }

        /// <summary>
        /// グリッドに追加する
        /// </summary>
        /// <param name="stageObject"> 追加する対象 </param>
        /// <param name="position"> 追加する位置 </param>
        public void AddAt(StageObject stageObject, Vector2Int position)
        {
            if (!CheckInPosition(position))
                return;

            if (gridList.gridX[position.x].gridY[position.y].objects.Contains(stageObject))
                return;

            gridList.gridX[position.x].gridY[position.y].objects.Add(stageObject);
        }

        /// <summary>
        /// グリッド指定位置に所属しているオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 取得する位置 </param>
        /// <param name="size"> 取得する範囲 </param>
        /// <returns> 所属しているオブジェクト群 </returns>
        public List<StageObject> GetRange(Vector2Int position, Vector2Int size)
        {
            var result = new List<StageObject>();

            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    for (int i = 0; i < gridList.gridX[indexX].gridY[indexY].objects.Count; ++i)
                    {
                        if (result.Contains(gridList.gridX[indexX].gridY[indexY].objects[i]))
                            continue;

                        result.Add(gridList.gridX[indexX].gridY[indexY].objects[i]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// グリッド指定位置に所属しているオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 取得する位置 </param>
        /// <returns> 所属しているオブジェクト群 </returns>
        public List<StageObject> GetAt(Vector2Int position)
        {
            if (!CheckInPosition(position))
                return new List<StageObject>();

            return gridList.gridX[position.x].gridY[position.y].objects;
        }

#endregion

#region 子クラス群

        /// <summary>
        /// マス目に所属するオブジェクト群
        /// </summary>
        class ObjectList
        {
            /// <summary> マス目に所属するオブジェクト群 </summary>
            public List<StageObject> objects = new List<StageObject>();
        }

        /// <summary>
        /// 横方向グリッドリスト
        /// </summary>
        class GridListX
        {
            /// <summary> 横方向グリッドリスト </summary>
            public List<GridListY> gridX = new List<GridListY>();
        }

        /// <summary>
        /// 縦方向グリッドリスト
        /// </summary>
        class GridListY
        {
            /// <summary> 縦方向グリッドリスト </summary>
            public List<ObjectList> gridY = new List<ObjectList>();
        }

#endregion
    }

    /// <summary>
    /// オブジェクト群
    /// </summary>
    public class StageObjects
    {
        /// <summary> オブジェクトレイヤー </summary>
        public List<StageObjectsLayer> objectsLayers = new List<StageObjectsLayer>();

        /// <summary> ブロックレイヤー </summary>
        public List<StageBlockLayer> blockLayers = new List<StageBlockLayer>();

        /// <summary> 領域レイヤー </summary>
        public List<StageAreaLayer> areaLayers = new List<StageAreaLayer>();
    }

    /// <summary>
    /// オブジェクトマネージャー
    /// </summary>
    public class StageObject_Manager
    {
        /// <summary> ステージマネージャー </summary>
        Stage_Manager stageManager = null;

        /// <summary> オブジェクト群 </summary>
        StageObjects stageObjects = null;

        /// <summary> オブジェクトレイヤー取得用 </summary>
        public List<StageObjectsLayer> ObjectsLayers { get => stageObjects != null ? stageObjects.objectsLayers : null; }

        /// <summary> ブロックレイヤー取得用 </summary>
        public List<StageBlockLayer> BlockLayers { get => stageObjects != null ? stageObjects.blockLayers : null; }

        /// <summary> 領域レイヤー取得用 </summary>
        public List<StageAreaLayer> AreaLayers { get => stageObjects != null ? stageObjects.areaLayers : null; }

#region 初期化・解放処理関係

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="stageManager"> ステージマネージャー </param>
        public StageObject_Manager(Stage_Manager stageManager)
        {
            this.stageManager = stageManager;
            stageObjects = new StageObjects();
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Initialize()
        {
            UnInitialize();
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!objectFactory)
                return;

            while (stageObjects.objectsLayers.Count > 0)
                RemoveObjectsLayer(stageObjects.objectsLayers[0]);

            while (stageObjects.blockLayers.Count > 0)
                RemoveBlockLayer(stageObjects.blockLayers[0]);

            while (stageObjects.areaLayers.Count > 0)
                RemoveAreaLayer(stageObjects.areaLayers[0]);
        }

#endregion

#region 更新処理関係

        /// <summary>
        /// Updateで呼ばれる
        /// </summary>
        public void UpdateProcess()
        {
            for (int i = 0; i < stageObjects.objectsLayers.Count; ++i)
                stageObjects.objectsLayers[i].UpdateProcess();

            for (int i = 0; i < stageObjects.blockLayers.Count; ++i)
                stageObjects.blockLayers[i].UpdateProcess();

            for (int i = 0; i < stageObjects.areaLayers.Count; ++i)
                stageObjects.areaLayers[i].UpdateProcess();
        }

        /// <summary>
        /// FixedUpdateで呼ばれる
        /// </summary>
        public void FixedUpdateProcess()
        {
            for (int i = 0; i < stageObjects.objectsLayers.Count; ++i)
                stageObjects.objectsLayers[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.blockLayers.Count; ++i)
                stageObjects.blockLayers[i].FixedUpdateProcess();

            for (int i = 0; i < stageObjects.areaLayers.Count; ++i)
                stageObjects.areaLayers[i].FixedUpdateProcess();
        }

        /// <summary>
        /// LateUpdateで呼ばれる
        /// </summary>
        public void LateUpdateProcess()
        {
            for (int i = 0; i < stageObjects.objectsLayers.Count; ++i)
                stageObjects.objectsLayers[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.blockLayers.Count; ++i)
                stageObjects.blockLayers[i].LateUpdateProcess();

            for (int i = 0; i < stageObjects.areaLayers.Count; ++i)
                stageObjects.areaLayers[i].LateUpdateProcess();
        }

#endregion

#region オブジェクトレイヤー関係

        /// <summary>
        /// オブジェクトレイヤー追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> オブジェクトレイヤー </returns>
        public StageObjectsLayer AddObjectsLayer(ObjectsLayerData data)
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!objectFactory || !stageManager)
                return null;

            var newItem = objectFactory.InstantiateObjectsLayer(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data))
            {
                objectFactory.DestroyObjectsLayer(newItem);

                return null;
            }

            if (!stageObjects.objectsLayers.Contains(newItem))
                stageObjects.objectsLayers.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// オブジェクトレイヤー削除
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveObjectsLayer(StageObjectsLayer item)
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.objectsLayers.Contains(item))
                stageObjects.objectsLayers.Remove(item);

            objectFactory.DestroyObjectsLayer(item);
        }

#endregion

#region 領域レイヤー関係

        /// <summary>
        /// 領域レイヤー追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> 領域レイヤー </returns>
        public StageAreaLayer AddAreaLayer(AreaLayerData data)
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!objectFactory || !stageManager)
                return null;

            var newItem = objectFactory.InstantiateAreaLayer(stageManager);

            if (!newItem)
                return null;

            newItem.SetData(data);

            if (!stageObjects.areaLayers.Contains(newItem))
                stageObjects.areaLayers.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// 領域レイヤー削除
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveAreaLayer(StageAreaLayer item)
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.areaLayers.Contains(item))
                stageObjects.areaLayers.Remove(item);

            objectFactory.DestroyAreaLayer(item);
        }

#endregion

#region ブロックレイヤー関係

        /// <summary>
        /// ブロックレイヤー追加
        /// </summary>
        /// <param name="data"> データ </param>
        /// <returns> ブロックレイヤー </returns>
        public StageBlockLayer AddBlockLayer(BlockLayerData data)
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!objectFactory || !stageManager)
                return null;

            var newItem = objectFactory.InstantiateBlockLayer(stageManager);

            if (!newItem)
                return null;

            if (!newItem.SetData(data))
            {
                objectFactory.DestroyBlockLayer(newItem);

                return null;
            }

            if (!stageObjects.blockLayers.Contains(newItem))
                stageObjects.blockLayers.Add(newItem);

            return newItem;
        }

        /// <summary>
        /// ブロックレイヤー削除
        /// </summary>
        /// <param name="item"> 対象オブジェクト </param>
        public void RemoveBlockLayer(StageBlockLayer item)
        {
            var objectFactory = stageManager?.ObjectFactory;

            if (!item || !objectFactory)
                return;

            if (stageObjects.blockLayers.Contains(item))
                stageObjects.blockLayers.Remove(item);

            objectFactory.DestroyBlockLayer(item);
        }

#endregion
    }

    /// <summary>
    /// 拡大縮小マネージャー
    /// </summary>
    public class ScaleChange_Manager
    {
        /// <summary> ステージマネージャー </summary>
        Stage_Manager stageManager = null;

        /// <summary> 選択中の主要な取っ手 </summary>
        HandleStageObject mainSelectedHandle = null;

        /// <summary> 選択中の取っ手群 </summary>
        List<HandleStageObject> selectedHandles = null;

        /// <summary> 選択中のブロック群 </summary>
        List<BlockStageObject> selectedBlocks = null;

        /// <summary> 選択中の領域群 </summary>
        List<AreaStageObject> selectedAreas = null;

#region 初期化・解放処理

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="stageManager"> ステージマネージャー </param>
        public ScaleChange_Manager(Stage_Manager stageManager)
        {
            this.stageManager = stageManager;
            mainSelectedHandle = null;
            selectedHandles = new List<HandleStageObject>();
            selectedBlocks = new List<BlockStageObject>();
            selectedAreas = new List<AreaStageObject>();
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Initialize()
        {
            UnInitialize();
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            mainSelectedHandle = null;
            selectedHandles.Clear();
            selectedBlocks.Clear();
            selectedAreas.Clear();
        }

#endregion

        //public void Select(HandleStageObject handleObject, StageHas_StageObject_Manager objectManager)
        //{
        //    UnSelect();

        //    if (!handleObject)
        //        return;

        //    mainSelectedHandle = handleObject;

        //    List<BlockData.BlockType> targetAreaTypes = new List<BlockData.BlockType>();
        //    List<HandleData.HandleType> targetHandleTypes = new List<HandleData.HandleType>();

        //    GetLinkTypes(objectManager, ref targetAreaTypes, ref targetHandleTypes);
        //    SetLinkHandles(objectManager, ref targetAreaTypes, ref targetHandleTypes);
        //    SetLinkBlocks(objectManager, ref targetAreaTypes, ref targetHandleTypes);
        //    SetLinkAreas(objectManager, ref targetAreaTypes, ref targetHandleTypes);
        //}

        //public void UnSelect()
        //{

        //}

        ///// <summary>
        ///// リンクするオブジェクトの種類を取得
        ///// </summary>
        ///// <param name="objectManager"> オブジェクトマネージャー </param>
        //void GetLinkTypes(StageHas_StageObject_Manager objectManager, ref List<BlockData.BlockType> targetAreaTypes, ref List<HandleData.HandleType> targetHandleTypes)
        //{
        //    var linkNumber = mainSelectedHandle.GetLinkGroupNumber();

        //    if (linkNumber < 0)
        //        return;

        //    var linkHandles = GetLinkHandle(objectManager, linkNumber);

        //    for (int i = 0; i < linkHandles.Count; ++i)
        //    {
        //        if (!linkHandles[i])
        //            continue;

        //        var areaType = linkHandles[i].GetAreaType();
        //        var handleType = linkHandles[i].GetHandleType();

        //        if (!targetAreaTypes.Contains(areaType))
        //            targetAreaTypes.Add(areaType);

        //        if (!targetHandleTypes.Contains(handleType))
        //            targetHandleTypes.Add(handleType);
        //    }
        //}

        ///// <summary>
        ///// リンク取っ手の取得
        ///// </summary>
        ///// <param name="objectManager"> オブジェクトマネージャー </param>
        ///// <param name="linkNumber"> リンク番号 </param>
        ///// <returns> リンク取っ手 </returns>
        //List<HandleStageObject> GetLinkHandle(StageHas_StageObject_Manager objectManager, int linkNumber)
        //{
        //    var areaLayers = objectManager.AreaLayers;
        //    var targetIsHorizontal = HandleData.CheckHorizontalHandleType(mainSelectedHandle.GetHandleType());

        //    List<HandleStageObject> linkHandles = new List<HandleStageObject>();

        //    for (int i = 0; i < areaLayers.Count; ++i)
        //    {
        //        if (!areaLayers[i])
        //            continue;

        //        var handleObjects = areaLayers[i].GetMyHandleObjects();
        //        for (int j = 0; j < handleObjects.Count; ++j)
        //        {
        //            if (!handleObjects[j])
        //                continue;

        //            if (linkNumber != handleObjects[j].GetLinkGroupNumber())
        //                continue;

        //            if (handleObjects[j].GetAreaType() == BlockData.BlockType.Fixed)
        //                continue;

        //            var isHorizontal = HandleData.CheckHorizontalHandleType(handleObjects[j].GetHandleType());

        //            if ((!targetIsHorizontal && isHorizontal) || (targetIsHorizontal && !isHorizontal))
        //                continue;

        //            if (linkHandles.Contains(handleObjects[j]))
        //                continue;

        //            linkHandles.Add(handleObjects[j]);
        //        }
        //    }

        //    return linkHandles;
        //}

        ///// <summary>
        ///// リンク取っ手の設定
        ///// </summary>
        ///// <param name="objectManager"> オブジェクトマネージャー </param>
        //void SetLinkHandles(StageHas_StageObject_Manager objectManager, ref List<BlockData.BlockType> targetAreaTypes, ref List<HandleData.HandleType> targetHandleTypes)
        //{
        //    selectedHandles.Clear();
        //}

        ///// <summary>
        ///// リンクブロックの設定
        ///// </summary>
        ///// <param name="objectManager"> オブジェクトマネージャー </param>
        //void SetLinkBlocks(StageHas_StageObject_Manager objectManager, ref List<BlockData.BlockType> targetAreaTypes, ref List<HandleData.HandleType> targetHandleTypes)
        //{
        //    selectedBlocks.Clear();
        //}

        ///// <summary>
        ///// リンク領域の設定
        ///// </summary>
        ///// <param name="objectManager"> オブジェクトマネージャー </param>
        //void SetLinkAreas(StageHas_StageObject_Manager objectManager, ref List<BlockData.BlockType> targetAreaTypes, ref List<HandleData.HandleType> targetHandleTypes)
        //{
        //    selectedAreas.Clear();
        //}
    }

    /// <summary>
    /// カメラマネージャー
    /// </summary>
    public class Camera_Manager
    {
        /// <summary> ステージマネージャー </summary>
        Stage_Manager stageManager = null;

        /// <summary> ステージ用カメラ </summary>
        StageCamera stageCamera = null;

        /// <summary> 主要なプレイヤー </summary>
        PlayerStageObject mainPlayer = null;

        /// <summary> 主要なカメラ領域 </summary>
        CameraAreaStageObject mainCameraArea = null;

        /// <summary> カメラ位置 </summary>
        Vector2Int cameraAreaOffset = Vector2Int.zero;

        /// <summary> カメラの大きさ </summary>
        Vector2Int cameraAreaSize = Vector2Int.one;

#region 初期化・解放処理

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="stageManager"> ステージマネージャー </param>
        public Camera_Manager(Stage_Manager stageManager)
        {
            this.stageManager = stageManager;
            stageCamera = FindObjectOfType<StageCamera>();
            stageCamera?.Initialize();
            mainPlayer = null;
            mainCameraArea = null;
            cameraAreaOffset = Vector2Int.zero;
            cameraAreaSize = Vector2Int.one;
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Initialize()
        {
            UnInitialize();
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            mainPlayer = null;
            mainCameraArea = null;
            cameraAreaOffset = Vector2Int.zero;
            cameraAreaSize = Vector2Int.one;
        }

#endregion

#region 更新関係

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="isForcedChange"> 強制的に変更するか </param>
        public void UpdateProcess(bool isForcedChange)
        {
            CheckMainPlayer();
            CheckMainCameraArea(isForcedChange);
            //SetCameraParameter(isForcedChange);

            Debug.Log(stageCamera);
            Debug.Log(mainPlayer);
            Debug.Log(mainCameraArea);
        }

#endregion

#region 主要プレイヤー関係

        /// <summary>
        /// 主要なプレイヤーを探索
        /// </summary>
        void CheckMainPlayer()
        {
            var objectManager = stageManager?.ObjectManager;

            if (objectManager == null)
                return;

            if (!mainPlayer || !mainPlayer.GetFlagEnable())
            {
                mainPlayer = null;

                for (int i = 0; i < objectManager.ObjectsLayers.Count; ++i)
                {
                    if (!objectManager.ObjectsLayers[i] || !objectManager.ObjectsLayers[i].GetFlagEnable())
                        continue;

                    var players = objectManager.ObjectsLayers[i].ObjectManager.GetMyPlayerObjects();

                    for (int j = 0; j < players.Count; ++j)
                    {
                        if (!players[i] || !players[i].GetFlagEnable())
                            continue;

                        mainPlayer = players[i];
                        break;
                    }

                    if (mainPlayer)
                        return;
                }
            }
        }

#endregion

#region 主要カメラ領域関係

        void CheckMainCameraArea(bool isForcedChange)
        {
            if (!mainPlayer)
                return;

            var gridHasObjects = stageManager?.GridHasObjects;

            if (gridHasObjects == null)
                return;

            var stageObjects = gridHasObjects.GetRange(mainPlayer.GetGridPosition(), mainPlayer.GetGridSize());
            CameraAreaStageObject next = null;

            for (int i = 0; i < stageObjects.Count; ++i)
            {
                var currentHit = stageObjects[i] as CameraAreaStageObject;
                if (!currentHit || !currentHit.GetFlagEnable())
                    continue;

                if (currentHit == mainCameraArea)
                    return;

                next = currentHit;
            }

            mainCameraArea = next;

            if (mainCameraArea)
            {
                cameraAreaOffset = mainCameraArea.CameraAreaOffset;
                cameraAreaSize = mainCameraArea.CameraAreaSize;
            }
            else
            {
                cameraAreaOffset = Vector2Int.zero;
                cameraAreaSize.x = gridHasObjects.XCount;
                cameraAreaSize.y = gridHasObjects.YCount;
            }

            SetCameraParameter(isForcedChange);
        }

        void SetCameraParameter(bool isForcedChange)
        {
            if (!stageCamera || stageManager == null)
                return;

            var targetCamera = stageCamera.MyCamera;

            if (!targetCamera)
                return;

            var distance = MathF.Abs(targetCamera.transform.position.z);

            var cameraCenter = new Vector3(stageManager.GetLeftPivotX(), stageManager.GetBottomPivotY(), stageManager.transform.position.z);
            cameraCenter.x += cameraAreaOffset.x + (cameraAreaSize.x * 0.5f);
            cameraCenter.y += cameraAreaOffset.y + (cameraAreaSize.y * 0.5f);

            var widthFieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumWidth(targetCamera, cameraAreaSize.x, distance);
            var heightFieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumHeight(targetCamera, cameraAreaSize.y, distance);

            if (isForcedChange)
            {
                stageCamera.ForcedChangePositionXY(cameraCenter);
                stageCamera.ForcedChangeFieldOfView(widthFieldOfView > heightFieldOfView ? widthFieldOfView : heightFieldOfView);
            }
            else
            {
                stageCamera.SetPositionXY(cameraCenter, 0.5f, StageCamera.CalculateType.Lerp);
                stageCamera.SetFieldOfView(widthFieldOfView > heightFieldOfView ? widthFieldOfView : heightFieldOfView, 0.5f, StageCamera.CalculateType.Lerp);
            }
        }

#endregion
    }

#endregion
}

#else

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal.Profiling.Memory.Experimental;
using UnityEngine;

public class Stage_Manager : MonoBehaviour
{
#region enum

    /// <summary>
    /// 状態
    /// </summary>
    public enum MainCondition
    {
        Reset,
        StartWait,
        PlayNormal,
    }

#endregion

    /// <summary> オブジェクトレイヤー </summary>
    List<StageObjectsLayer> objectsLayers = new List<StageObjectsLayer>();

    /// <summary> ブロックレイヤー </summary>
    List<StageBlockLayer> blockLayers = new List<StageBlockLayer>();

    /// <summary> 領域レイヤー </summary>
    List<StageAreaLayer> areaLayers = new List<StageAreaLayer>();

    /// <summary> マス目に所属するオブジェクト群 </summary>
    List<List<List<StageObject>>> gridHasObjects = new List<List<List<StageObject>>>();

    /// <summary> オブジェクトファクトリー </summary>
    StageObjectFactory objectFactory = null;

    /// <summary> ステージ用カメラ </summary>
    StageCamera stageCamera = null;

    /// <summary> 主要なプレイヤー </summary>
    PlayerStageObject mainPlayer = null;

    /// <summary> 主要な処理領域 </summary>
    ProcessAreaStageObject mainProcessArea = null;

    /// <summary> ステージのデータ </summary>
    StageData stageData = new StageData();

    /// <summary> 選択中のオブジェクト群 </summary>
    //SelectItems selectedItems = new SelectItems();

    /// <summary> 状態 </summary>
    MainCondition mainCondition = MainCondition.StartWait;

    /// <summary> 処理領域の位置 </summary>
    Vector2Int processGridAreaOffset = Vector2Int.zero;

    /// <summary> 処理領域の大きさ </summary>
    Vector2Int processGridAreaSize = Vector2Int.one;

#region Unity標準

    private void Awake()
    {
        Initialize();
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        for (int i = 0; i < objectsLayers.Count; ++i)
            objectsLayers[i].UpdateProcess(mainCondition);

        for (int i = 0; i < blockLayers.Count; ++i)
            blockLayers[i].UpdateProcess(mainCondition);

        for (int i = 0; i < areaLayers.Count; ++i)
            areaLayers[i].UpdateProcess(mainCondition);
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < objectsLayers.Count; ++i)
            objectsLayers[i].FixedUpdateProcess(mainCondition);

        for (int i = 0; i < blockLayers.Count; ++i)
            blockLayers[i].FixedUpdateProcess(mainCondition);

        for (int i = 0; i < areaLayers.Count; ++i)
            areaLayers[i].FixedUpdateProcess(mainCondition);
    }

    private void LateUpdate()
    {
        for (int i = 0; i < objectsLayers.Count; ++i)
            objectsLayers[i].LateUpdateProcess(mainCondition);

        for (int i = 0; i < blockLayers.Count; ++i)
            blockLayers[i].LateUpdateProcess(mainCondition);

        for (int i = 0; i < areaLayers.Count; ++i)
            areaLayers[i].LateUpdateProcess(mainCondition);

        var fadeManager = Fade_Manager.Instance;

        switch (mainCondition)
        {
            case MainCondition.Reset:
                if (fadeManager.CheckCurrentIdleAfterFadeOut())
                    CreateStage();
                break;
            case MainCondition.StartWait:
                if (fadeManager.CheckCurrentIdleAfterFadeIn())
                    mainCondition = MainCondition.PlayNormal;
                break;
            case MainCondition.PlayNormal:
                break;
            default:
                break;
        }

        Debug.Log("current" + mainCondition.ToString());

        CheckMainPlayer();
        CheckMainProcessArea(false);
    }

#endregion

#region 初期化・解放

    /// <summary>
    /// 初期化処理
    /// </summary>
    public void Initialize()
    {
        GameObjectUtility.NullCheckAndFind(ref objectFactory);

        GameObjectUtility.NullCheckAndFind(ref stageCamera);

        var gameManager = Game_Manager.Instance;

        stageData.ReadData(gameManager.WorldNum, gameManager.StageNum);

        stageCamera.Initialize();

        ChangeObjectName();

        CreateStage();
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public void UnInitialize()
    {
        while (objectsLayers.Count > 0)
            RemoveObjectsLayer(objectsLayers[0]);

        while (blockLayers.Count > 0)
            RemoveBlockLayer(blockLayers[0]);

        while(areaLayers.Count > 0)
            RemoveAreaLayer(areaLayers[0]);

        mainPlayer = null;
        mainProcessArea = null;

        RemoveAllGridHasObject();
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public void ChangeObjectName()
    {
        gameObject.name = "StageManager";
    }

    /// <summary>
    /// ステージ作成
    /// </summary>
    void CreateStage()
    {
        UnInitialize();

        ReSizeGrid();

        for (int i = 0; i < stageData.BlockLayerDatas.Count; ++i)
            AddBlockLayer(stageData.BlockLayerDatas[i]);

        for (int i = 0; i < stageData.AreaLayerDatas.Count; ++i)
            AddAreaLayer(stageData.AreaLayerDatas[i]);

        for (int i = 0; i < stageData.ObjectsLayerDatas.Count; ++i)
            AddObjectsLayer(stageData.ObjectsLayerDatas[i]);

        CheckMainPlayer();
        CheckMainProcessArea(true);

        mainCondition = MainCondition.StartWait;
    }

    public void ReSetStage()
    {
        Fade_Manager.Instance.StartFadeOut();
        mainCondition = MainCondition.Reset;
    }

#endregion

#region グリッド関係

    /// <summary>
    /// x位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> x位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckPositionXInGrid(int position)
    {
        var xMax = gridHasObjects.Count;

        if (position < 0 || position >= xMax)
            return false;

        return true;
    }

    /// <summary>
    /// y位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> y位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckPositionYInGrid(int position)
    {
        if (gridHasObjects.Count < 1)
            return false;

        var yMax = gridHasObjects[0].Count;

        if (position < 0 || position >= yMax)
            return false;

        return true;
    }

    /// <summary>
    /// 位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> 位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckPositionInGrid(Vector2Int position)
    {
        var xMax = gridHasObjects.Count;

        if (position.x < 0 || position.x >= xMax)
            return false;

        var yMax = gridHasObjects[position.x].Count;

        if (position.y < 0 || position.y >= yMax)
            return false;

        return true;
    }

    /// <summary>
    /// グリッドの大きさ取得
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return stageData.GridSize;
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    void ReSizeGrid()
    {
        while (stageData.GridSize.x < gridHasObjects.Count)
            gridHasObjects.RemoveAt(gridHasObjects.Count - 1);

        for (int x = gridHasObjects.Count; x < stageData.GridSize.x; ++x)
            gridHasObjects.Add(new List<List<StageObject>>());

        for (int x = 0; x < gridHasObjects.Count; ++x)
        {
            while (stageData.GridSize.y < gridHasObjects[x].Count)
                gridHasObjects[x].RemoveAt(gridHasObjects[x].Count - 1);

            for (int y = gridHasObjects[x].Count; y < stageData.GridSize.y; ++y)
                gridHasObjects[x].Add(new List<StageObject>());
        }
    }

    /// <summary>
    /// グリッドから全て取り除く
    /// </summary>
    public void RemoveAllGridHasObject()
    {
        for (int x = 0, xLength = gridHasObjects.Count; x < xLength; ++x)
            for (int y = 0, yLength = gridHasObjects[x].Count; y < yLength; ++y)
                gridHasObjects[x][y].Clear();
    }

    /// <summary>
    /// グリッドから取り除く
    /// </summary>
    /// <param name="stageObject"> 取り除く対象 </param>
    /// <param name="position"> 取り除く位置 </param>
    /// <param name="size"> 取り除く範囲 </param>
    public void RemoveRangeGridHasObject(StageObject stageObject, Vector2Int position, Vector2Int size)
    {
        for (int x = 0; x < size.x; ++x)
        {
            var indexX = position.x + x;

            if (!CheckPositionXInGrid(indexX))
                break;

            for (int y = 0; y < size.y; ++y)
            {
                var indexY = position.y + y;

                if (!CheckPositionYInGrid(indexY))
                {
                    size.y = y;
                    break;
                }

                if (!gridHasObjects[indexX][indexY].Contains(stageObject))
                    continue;

                gridHasObjects[indexX][indexY].Remove(stageObject);
            }
        }
    }

    /// <summary>
    /// グリッドから取り除く
    /// </summary>
    /// <param name="stageObject"> 取り除く対象 </param>
    /// <param name="position"> 取り除く位置 </param>
    public void RemoveGridHasObject(StageObject stageObject, Vector2Int position)
    {
        if (!CheckPositionInGrid(position))
            return;

        if (!gridHasObjects[position.x][position.y].Contains(stageObject))
            return;

        gridHasObjects[position.x][position.y].Remove(stageObject);
    }

    /// <summary>
    /// グリッドに追加する
    /// </summary>
    /// <param name="stageObject"> 追加する対象 </param>
    /// <param name="position"> 追加する位置 </param>
    /// <param name="size"> 追加する範囲 </param>
    public void AddRangeGridHasObject(StageObject stageObject, Vector2Int position, Vector2Int size)
    {
        for (int x = 0; x < size.x; ++x)
        {
            var indexX = position.x + x;

            if (!CheckPositionXInGrid(indexX))
                break;

            for (int y = 0; y < size.y; ++y)
            {
                var indexY = position.y + y;

                if (!CheckPositionYInGrid(indexY))
                {
                    size.y = y;
                    break;
                }

                if (gridHasObjects[indexX][indexY].Contains(stageObject))
                    return;

                gridHasObjects[indexX][indexY].Add(stageObject);
            }
        }
    }

    /// <summary>
    /// グリッドに追加する
    /// </summary>
    /// <param name="stageObject"> 追加する対象 </param>
    /// <param name="position"> 追加する位置 </param>
    public void AddGridHasObject(StageObject stageObject, Vector2Int position)
    {
        if (!CheckPositionInGrid(position))
            return;

        if (gridHasObjects[position.x][position.y].Contains(stageObject))
            return;

        gridHasObjects[position.x][position.y].Add(stageObject);
    }

    /// <summary>
    /// グリッド指定位置に所属しているオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 取得する位置 </param>
    /// <param name="size"> 取得する範囲 </param>
    /// <returns> 所属しているオブジェクト群 </returns>
    public List<StageObject> GetRangeStageObjects(Vector2Int position, Vector2Int size)
    {
        var result = new List<StageObject>();

        for (int x = 0; x < size.x; ++x)
        {
            var indexX = position.x + x;

            if (!CheckPositionXInGrid(indexX))
                break;

            for (int y = 0; y < size.y; ++y)
            {
                var indexY = position.y + y;

                if (!CheckPositionYInGrid(indexY))
                {
                    size.y = y;
                    break;
                }

                for (int i = 0; i < gridHasObjects[indexX][indexY].Count; ++i)
                {
                    if (result.Contains(gridHasObjects[indexX][indexY][i]))
                        continue;

                    result.Add(gridHasObjects[indexX][indexY][i]);
                }
            }
        }

        return result;
    }

    /// <summary>
    /// グリッド指定位置に所属しているオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 取得する位置 </param>
    /// <returns> 所属しているオブジェクト群 </returns>
    public List<StageObject> GetStageObjects(Vector2Int position)
    {
        if (!CheckPositionInGrid(position))
            return new List<StageObject>();

        return gridHasObjects[position.x][position.y];
    }

#endregion

#region オブジェクトレイヤー関係

    /// <summary>
    /// オブジェクトレイヤー追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> オブジェクトレイヤー </returns>
    public StageObjectsLayer AddObjectsLayer(ObjectsLayerData data)
    {
        var newItem = objectFactory.InstantiateObjectsLayer(this);

        if (!newItem)
            return null;

        newItem.SetData(data);

        if (!objectsLayers.Contains(newItem))
            objectsLayers.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// オブジェクトレイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveObjectsLayer(StageObjectsLayer item)
    {
        if (!item)
            return;

        if (objectsLayers.Contains(item))
            objectsLayers.Remove(item);

        objectFactory.DestroyObjectsLayer(item);
    }

    /// <summary>
    /// 自身の所有するオブジェクトレイヤー取得
    /// </summary>
    /// <returns> オブジェクトレイヤー </returns>
    public List<StageObjectsLayer> GetHasMyObjectsLayer()
    {
        return objectsLayers;
    }

#endregion

#region 領域レイヤー関係

    /// <summary>
    /// 領域レイヤー追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> 領域レイヤー </returns>
    public StageAreaLayer AddAreaLayer(AreaLayerData data)
    {
        var newItem = objectFactory.InstantiateAreaLayer(this);

        if (!newItem)
            return null;

        newItem.SetData(data);

        if (!areaLayers.Contains(newItem))
            areaLayers.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 領域レイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveAreaLayer(StageAreaLayer item)
    {
        if (!item)
            return;

        if (areaLayers.Contains(item))
            areaLayers.Remove(item);

        objectFactory.DestroyAreaLayer(item);
    }

    /// <summary>
    /// 自身の所有する領域レイヤー取得
    /// </summary>
    /// <returns> 領域レイヤー </returns>
    public List<StageAreaLayer> GetHasMyAreaLayers()
    {
        return areaLayers;
    }

#endregion

#region ブロックレイヤー関係

    /// <summary>
    /// ブロックレイヤー追加
    /// </summary>
    /// <param name="data"> データ </param>
    /// <returns> ブロックレイヤー </returns>
    public StageBlockLayer AddBlockLayer(BlockLayerData data)
    {
        var newItem = objectFactory.InstantiateBlockLayer(this);

        if (!newItem)
            return null;

        newItem.SetData(data);

        if (!blockLayers.Contains(newItem))
            blockLayers.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// ブロックレイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBlockLayer(StageBlockLayer item)
    {
        if (!item)
            return;

        if (blockLayers.Contains(item))
            blockLayers.Remove(item);

        objectFactory.DestroyBlockLayer(item);
    }

    /// <summary>
    /// 自身の所有するブロックレイヤー取得
    /// </summary>
    /// <returns> ブロックレイヤー </returns>
    public List<StageBlockLayer> GetHasMyBlockLayers()
    {
        return blockLayers;
    }

#endregion

#region 主要プレイヤー関係

    void CheckMainPlayer()
    {
        if (!mainPlayer || !mainPlayer.GetFlagEnable())
        {
            mainPlayer = null;

            for (int i = 0; i < objectsLayers.Count; ++i)
            {
                if (!objectsLayers[i] || !objectsLayers[i].GetFlagEnable())
                    continue;

                var players = objectsLayers[i].GetMyPlayerObjects();

                for (int j = 0; j < players.Count; ++j)
                {
                    if (!players[i] || !players[i].GetFlagEnable())
                        continue;

                    mainPlayer = players[i];
                    break;
                }

                if (mainPlayer)
                    return;
            }
        }
    }

#endregion

#region 主要処理領域関係

    void CheckMainProcessArea(bool isForcedChange)
    {
        if (!mainPlayer)
            return;

        var stageObjects = GetRangeStageObjects(mainPlayer.GetGridPosition(), mainPlayer.GetGridSize());
        ProcessAreaStageObject next = null;

        for (int i = 0; i < stageObjects.Count; ++i)
        {
            var currentHit = stageObjects[i] as ProcessAreaStageObject;
            if (!currentHit || !currentHit.GetFlagEnable())
                continue;

            if (currentHit == mainProcessArea)
                return;

            next = currentHit;
        }

        mainProcessArea = next;

        if (mainProcessArea)
        {
            processGridAreaOffset = mainProcessArea.GetProcessGridAreaOffset();
            processGridAreaSize = mainProcessArea.GetProcessGridAreaSize();
        }
        else
        {
            processGridAreaOffset = Vector2Int.zero;
            processGridAreaSize = stageData.GridSize;
        }

        if (!stageCamera)
            return;

        var targetCamera = stageCamera.MyCamera;

        if (!targetCamera)
            return;

        var distance = MathF.Abs(targetCamera.transform.position.z);

        var cameraCenter = new Vector3(GetLeftPivotX(), GetBottomPivotY(), transform.position.z);
        cameraCenter.x += processGridAreaOffset.x + (processGridAreaSize.x * 0.5f);
        cameraCenter.y += processGridAreaOffset.y + (processGridAreaSize.y * 0.5f);

        var widthFieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumWidth(targetCamera, processGridAreaSize.x, distance);
        var heightFieldOfView = CameraUtility.CalculateFieldOfViewFromFrustumHeight(targetCamera, processGridAreaSize.y, distance);

        if (isForcedChange)
        {
            stageCamera.ForcedChangePositionXY(cameraCenter);
            stageCamera.ForcedChangeFieldOfView(widthFieldOfView > heightFieldOfView ? widthFieldOfView : heightFieldOfView);
        }
        else
        {
            stageCamera.SetPositionXY(cameraCenter, 0.5f, StageCamera.CalculateType.Lerp);
            stageCamera.SetFieldOfView(widthFieldOfView > heightFieldOfView ? widthFieldOfView : heightFieldOfView, 0.5f, StageCamera.CalculateType.Lerp);
        }
    }

#endregion

#region 掴み関係

    /// <summary>
    /// 選択する
    /// </summary>
    /// <param name="handleObject">  </param>
    public void Select(HandleStageObject handleObject)
    {
    }

    /// <summary>
    /// 選択しない
    /// </summary>
    public void UnSelect()
    {
    }

    /// <summary>
    /// 掴みロック状態にする
    /// </summary>
    public virtual void GrabLock()
    {
    }

    /// <summary>
    /// 掴みロック状態にしない
    /// </summary>
    public virtual void GrabUnLock()
    {
    }

#endregion

#region 原点位置関係

    /// <summary>
    /// 左X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetLeftPivotX()
    {
        return transform.position.x;
    }

    /// <summary>
    /// 中X原点取得
    /// </summary>
    /// <returns>中X原点</returns>
    public float GetMiddlePivotX()
    {
        return transform.position.x + (stageData.GridSize.x * 0.5f);
    }

    /// <summary>
    /// 右X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetRightPivotX()
    {
        return transform.position.x + stageData.GridSize.x;
    }

    /// <summary>
    /// 下Y原点取得
    /// </summary>
    /// <returns>下Y原点</returns>
    public float GetBottomPivotY()
    {
        return transform.position.y;
    }

    /// <summary>
    /// 中Y原点取得
    /// </summary>
    /// <returns>中Y原点</returns>
    public float GetMiddlePivotY()
    {
        return transform.position.y + (stageData.GridSize.y * 0.5f);
    }

    /// <summary>
    /// 上Y原点取得
    /// </summary>
    /// <returns>上Y原点</returns>
    public float GetTopPivotY()
    {
        return transform.position.y + stageData.GridSize.y;
    }

#endregion
}

#endif