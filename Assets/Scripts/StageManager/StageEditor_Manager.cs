using System;
using System.Collections.Generic;
using UnityEngine;

public class StageEditor_Manager : MonoBehaviour
{
    /// <summary> グリッド上の大きさ </summary>
    [SerializeField] Vector2Int gridSize = Vector2Int.one;

    /// <summary> オブジェクトファクトリー </summary>
    [SerializeField] EditorObjectFactory objectFactory = null;

    /// <summary> オブジェクトレイヤー </summary>
    [SerializeField] EditorObjectsLayer objectsLayer = null;

    /// <summary> ブロックレイヤー </summary>
    [SerializeField] EditorBlockLayer[] blockLayers = new EditorBlockLayer[BlockData.BlockTypeNum];

    /// <summary> 領域レイヤー </summary>
    [SerializeField] List<EditorAreaLayer> areaLayers = new List<EditorAreaLayer>();

    /// <summary> ワールド番号 </summary>
    [SerializeField] int worldNum = 1;

    /// <summary> ステージ番号 </summary>
    [SerializeField] int stageNum = 1;

    /// <summary> マス目に所属するオブジェクト群 </summary>
    [SerializeField] GridList gridList = new GridList();
    //[SerializeField] List<List<List<EditorObject>>> gridHasObjects = new List<List<List<EditorObject>>>();

    private void Awake()
    {
        ResetGridAllObjects();

        worldNum = -1;
        stageNum = -1;

        WriteData();

        Game_Manager.Instance.WorldNum = -1;
        Game_Manager.Instance.StageNum = -1;

        Scene_Manager.Instance.LoadScene("StageGame");
    }

    #region 初期化・解放処理

    /// <summary>
    /// 初期化処理
    /// </summary>
    /// <returns> 成功したか </returns>
    public bool Initialize()
    {
        UnInitialize();

        ReSizeGrid();

        AddObjectsLayer();
        AddBlockLayer(BlockData.BlockType.Fixed);
        AddBlockLayer(BlockData.BlockType.Red);
        AddBlockLayer(BlockData.BlockType.Green);
        AddBlockLayer(BlockData.BlockType.Blue);

        ChangeObjectName();

        return true;
    }

    /// <summary>
    /// 解放処理
    /// </summary>
    public void UnInitialize()
    {
        RemoveObjectsLayer(objectsLayer);

        for (int i = 0; i < blockLayers.Length; ++i)
            RemoveBlockLayer(blockLayers[i]);

        while (areaLayers.Count > 0)
            RemoveAreaLayer(areaLayers[0]);

        RemoveAllGridHasObject();
    }

    /// <summary>
    /// 名前の変更
    /// </summary>
    public void ChangeObjectName()
    {
        gameObject.name = "StageEditor";
    }

    #endregion

    #region グリッド関係

    /// <summary>
    /// 再びグリッドに設定しなおす
    /// </summary>
    public void ResetGridAllObjects()
    {
        RemoveAllGridHasObject();
        gridSize = ReTouchEditorObjects();
        ReSizeGrid(false);

        objectsLayer?.ResetGridAllObjects();

        for (int i = 0; i < blockLayers.Length; ++i)
            blockLayers[i]?.ResetGridAllObjects();

        for (int i = 0; i < areaLayers.Count; ++i)
            areaLayers[i]?.ResetGridAllObjects();
    }

    /// <summary>
    /// 保有オブジェクトを再び設定
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int ReTouchEditorObjects()
    {
        Vector2Int maxGridSize = Vector2Int.one;

        objectsLayer = null;

        this.areaLayers.Clear();

        for (int i = 0; i < this.blockLayers.Length; ++i)
            this.blockLayers[i] = null;

        var objectsLayers = GetComponentsInChildren<EditorObjectsLayer>();

        if (objectsLayers.Length > 0)
        {
            objectsLayer = objectsLayers[0];
            maxGridSize = MathUtility.Max(maxGridSize, objectsLayer.ReTouchEditorObjects());
        }

        for (int i = 1; i < objectsLayers.Length; ++i)
            GameObjectUtility.DestroyGameObject(objectsLayers[i]);

        var blockLayers = GetComponentsInChildren<EditorBlockLayer>();

        for (int i = 0; i < blockLayers.Length; ++i)
        {
            var blockType = blockLayers[i].GetBlockType();

            if (!this.blockLayers[(int)blockType])
            {
                this.blockLayers[(int)blockType] = blockLayers[i];
                maxGridSize = MathUtility.Max(maxGridSize, this.blockLayers[(int)blockType].ReTouchEditorObjects());
            }
            else
                GameObjectUtility.DestroyGameObject(blockLayers[i]);
        }

        var areaLayers = GetComponentsInChildren<EditorAreaLayer>();

        for (int i = 0; i < areaLayers.Length; ++i)
        {
            this.areaLayers.Add(areaLayers[i]);
            maxGridSize = MathUtility.Max(maxGridSize, areaLayers[i].ReTouchEditorObjects());
        }

        return maxGridSize;
    }

    /// <summary>
    /// x位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> x位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckPositionXInGrid(int position)
    {
        return gridList.CheckInPositionX(position);

        //var xMax = gridHasObjects.Count;

        //if (position < 0 || position >= xMax)
        //    return false;

        //return true;
    }

    /// <summary>
    /// y位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> y位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckPositionYInGrid(int position)
    {
        return gridList.CheckInPositionY(position);

        //if (gridHasObjects.Count < 1)
        //    return false;

        //var yMax = gridHasObjects[0].Count;

        //if (position < 0 || position >= yMax)
        //    return false;

        //return true;
    }

    /// <summary>
    /// 位置がグリッド内か判定
    /// </summary>
    /// <param name="position"> 位置 </param>
    /// <returns> グリッド内か </returns>
    public bool CheckPositionInGrid(Vector2Int position)
    {
        return gridList.CheckInPosition(position);

        //var xMax = gridHasObjects.Count;

        //if (position.x < 0 || position.x >= xMax)
        //    return false;

        //var yMax = gridHasObjects[position.x].Count;

        //if (position.y < 0 || position.y >= yMax)
        //    return false;

        //return true;
    }

    /// <summary>
    /// グリッドの大きさ取得
    /// </summary>
    /// <returns> グリッドの大きさ </returns>
    public Vector2Int GetGridSize()
    {
        return gridSize;
    }

    /// <summary>
    /// グリッドの大きさ設定
    /// </summary>
    /// <param name="gridSize"> グリッドの大きさ </param>
    public void SetGridSize(Vector2Int gridSize)
    {
        this.gridSize = MathUtility.Clamp(gridSize, Vector2Int.one, new Vector2Int(200, 200));

        ReSizeGrid();
    }

    /// <summary>
    /// グリッドの大きさを更新
    /// </summary>
    /// <param name="resizeChildren"> 子オブジェクトも更新するか </param>
    public void ReSizeGrid(bool resizeChildren = true)
    {
        gridList.SetGridSize(gridSize);

        //while (gridSize.x < gridHasObjects.Count)
        //    gridHasObjects.RemoveAt(gridHasObjects.Count - 1);

        //for (int x = gridHasObjects.Count; x < gridSize.x; ++x)
        //    gridHasObjects.Add(new List<List<EditorObject>>());

        //for (int x = 0; x < gridHasObjects.Count; ++x)
        //{
        //    while (gridSize.y < gridHasObjects[x].Count)
        //        gridHasObjects[x].RemoveAt(gridHasObjects[x].Count - 1);

        //    for (int y = gridHasObjects[x].Count; y < gridSize.y; ++y)
        //        gridHasObjects[x].Add(new List<EditorObject>());
        //}

        if (!resizeChildren)
            return;

        objectsLayer?.ReSizeGrid();

        for (int i = 0; i < blockLayers.Length; ++i)
            blockLayers[i]?.ReSizeGrid();

        for (int i = 0; i < areaLayers.Count; ++i)
            areaLayers[i]?.ReSizeGrid();
    }

    /// <summary>
    /// グリッドから全て取り除く
    /// </summary>
    public void RemoveAllGridHasObject()
    {
        gridList.RemoveAll();

        //for (int x = 0, xLength = gridHasObjects.Count; x < xLength; ++x)
        //    for (int y = 0, yLength = gridHasObjects[x].Count; y < yLength; ++y)
        //        gridHasObjects[x][y].Clear();
    }

    /// <summary>
    /// グリッドから取り除く
    /// </summary>
    /// <param name="stageObject"> 取り除く対象 </param>
    /// <param name="position"> 取り除く位置 </param>
    /// <param name="size"> 取り除く範囲 </param>
    public void RemoveRangeGridHasObject(EditorObject stageObject, Vector2Int position, Vector2Int size)
    {
        gridList.RemoveRange(stageObject, position, size);

        //for (int x = 0; x < size.x; ++x)
        //{
        //    var indexX = position.x + x;

        //    if (!CheckPositionXInGrid(indexX))
        //        break;

        //    for (int y = 0; y < size.y; ++y)
        //    {
        //        var indexY = position.y + y;

        //        if (!CheckPositionYInGrid(indexY))
        //        {
        //            size.y = y;
        //            break;
        //        }

        //        if (!gridHasObjects[indexX][indexY].Contains(stageObject))
        //            continue;

        //        gridHasObjects[indexX][indexY].Remove(stageObject);
        //    }
        //}
    }

    /// <summary>
    /// グリッドから取り除く
    /// </summary>
    /// <param name="stageObject"> 取り除く対象 </param>
    /// <param name="position"> 取り除く位置 </param>
    public void RemoveGridHasObject(EditorObject stageObject, Vector2Int position)
    {
        gridList.RemoveAt(stageObject, position);

        //if (!CheckPositionInGrid(position))
        //    return;

        //if (!gridHasObjects[position.x][position.y].Contains(stageObject))
        //    return;

        //gridHasObjects[position.x][position.y].Remove(stageObject);
    }

    /// <summary>
    /// グリッドに追加する
    /// </summary>
    /// <param name="stageObject"> 追加する対象 </param>
    /// <param name="position"> 追加する位置 </param>
    /// <param name="size"> 追加する範囲 </param>
    public void AddRangeGridHasObject(EditorObject stageObject, Vector2Int position, Vector2Int size)
    {
        gridList.AddRange(stageObject, position, size);

        //for (int x = 0; x < size.x; ++x)
        //{
        //    var indexX = position.x + x;

        //    if (!CheckPositionXInGrid(indexX))
        //        break;

        //    for (int y = 0; y < size.y; ++y)
        //    {
        //        var indexY = position.y + y;

        //        if (!CheckPositionYInGrid(indexY))
        //        {
        //            size.y = y;
        //            break;
        //        }

        //        if (gridHasObjects[indexX][indexY].Contains(stageObject))
        //            return;

        //        gridHasObjects[indexX][indexY].Add(stageObject);
        //    }
        //}
    }

    /// <summary>
    /// グリッドに追加する
    /// </summary>
    /// <param name="stageObject"> 追加する対象 </param>
    /// <param name="position"> 追加する位置 </param>
    public void AddGridHasObject(EditorObject stageObject, Vector2Int position)
    {
        gridList.AddAt(stageObject, position);

        //if (!CheckPositionInGrid(position))
        //    return;

        //if (gridHasObjects[position.x][position.y].Contains(stageObject))
        //    return;

        //gridHasObjects[position.x][position.y].Add(stageObject);
    }

    /// <summary>
    /// グリッド指定位置に所属しているオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 取得する位置 </param>
    /// <param name="size"> 取得する範囲 </param>
    /// <returns> 所属しているオブジェクト群 </returns>
    public List<EditorObject> GetRangeStageObjects(Vector2Int position, Vector2Int size)
    {
        return gridList.GetRange(position, size);

        //var result = new List<EditorObject>();

        //for (int x = 0; x < size.x; ++x)
        //{
        //    var indexX = position.x + x;

        //    if (!CheckPositionXInGrid(indexX))
        //        break;

        //    for (int y = 0; y < size.y; ++y)
        //    {
        //        var indexY = position.y + y;

        //        if (!CheckPositionYInGrid(indexY))
        //        {
        //            size.y = y;
        //            break;
        //        }

        //        for (int i = 0; i < gridHasObjects[indexX][indexY].Count; ++i)
        //        {
        //            if (result.Contains(gridHasObjects[indexX][indexY][i]))
        //                continue;

        //            result.Add(gridHasObjects[indexX][indexY][i]);
        //        }
        //    }
        //}

        //return result;
    }

    /// <summary>
    /// グリッド指定位置に所属しているオブジェクト群を取得
    /// </summary>
    /// <param name="position"> 取得する位置 </param>
    /// <returns> 所属しているオブジェクト群 </returns>
    public List<EditorObject> GetStageObjects(Vector2Int position)
    {
        return gridList.GetAt(position);

        //if (!CheckPositionInGrid(position))
        //    return new List<EditorObject>();

        //return gridHasObjects[position.x][position.y];
    }

    #endregion

    #region オブジェクトレイヤー関係

    /// <summary>
    /// オブジェクトレイヤー追加
    /// </summary>
    /// <returns> オブジェクトレイヤー </returns>
    public EditorObjectsLayer AddObjectsLayer()
    {
        RemoveObjectsLayer(objectsLayer);

        var newItem = objectFactory.InstantiateObjectsLayer(this);
        if (!newItem)
            return null;

        objectsLayer = newItem;

        return newItem;
    }

    /// <summary>
    /// オブジェクトレイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveObjectsLayer(EditorObjectsLayer item)
    {
        if (!item)
            return;

        if (objectsLayer == item)
            objectsLayer = null;

        objectFactory.DestroyObjectsLayer(item);
    }

    /// <summary>
    /// 自身の所有するオブジェクトレイヤー取得
    /// </summary>
    /// <returns> オブジェクトレイヤー </returns>
    public EditorObjectsLayer GetHasMyObjectsLayer()
    {
        return objectsLayer;
    }

    #endregion

    #region 領域レイヤー関係

    /// <summary>
    /// 領域レイヤー追加
    /// </summary>
    /// <param name="areaType"> 領域の種類 </param>
    /// <returns> 領域レイヤー </returns>
    public EditorAreaLayer AddAreaLayer(BlockData.BlockType areaType)
    {
        var newItem = objectFactory.InstantiateAreaLayer(this);
        if (!newItem)
            return null;

        areaLayers.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// 領域レイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveAreaLayer(EditorAreaLayer item)
    {
        if (!item)
            return;

        objectFactory.DestroyAreaLayer(item);

        if (areaLayers.Contains(item))
            areaLayers.Remove(item);
    }

    /// <summary>
    /// 自身の所有する領域レイヤー取得
    /// </summary>
    /// <returns> 領域レイヤー </returns>
    public List<EditorAreaLayer> GetHasMyAreaLayers()
    {
        return areaLayers;
    }

    #endregion

    #region ブロックレイヤー関係

    /// <summary>
    /// ブロックレイヤー数を合わせる
    /// </summary>
    void StretchBlockLayerNum()
    {
        if (blockLayers.Length != BlockData.BlockTypeNum)
            Array.Resize(ref blockLayers, BlockData.BlockTypeNum);
    }

    /// <summary>
    /// ブロックレイヤー追加
    /// </summary>
    /// <param name="blockType"> ブロックの種類 </param>
    /// <returns> ブロックレイヤー </returns>
    public EditorBlockLayer AddBlockLayer(BlockData.BlockType blockType)
    {
        StretchBlockLayerNum();

        var newItem = objectFactory.InstantiateBlockLayer(this);
        if (!newItem)
            return null;

        int index = (int)blockType;

        RemoveBlockLayer(blockLayers[index]);

        newItem.SetBlockType(blockType);
        blockLayers[index] = newItem;

        return newItem;
    }

    /// <summary>
    /// ブロックレイヤー削除
    /// </summary>
    /// <param name="item"> 対象オブジェクト </param>
    public void RemoveBlockLayer(EditorBlockLayer item)
    {
        if (!item)
            return;

        for (int i = 0; i < blockLayers.Length; ++i)
        {
            if (blockLayers[i] != item)
                continue;

            objectFactory.DestroyBlockLayer(blockLayers[i]);
            blockLayers[i] = null;
        }
    }

    /// <summary>
    /// 自身の所有するブロックレイヤー取得
    /// </summary>
    /// <returns> ブロックレイヤー </returns>
    public EditorBlockLayer[] GetHasMyBlockLayers()
    {
        return blockLayers;
    }

    #endregion

    #region セーブ・ロード関係

    /// <summary>
    /// ワールド番号取得
    /// </summary>
    /// <returns> 番号 </returns>
    public int GetWorldNum()
    {
        return worldNum;
    }

    /// <summary>
    /// ステージ番号取得
    /// </summary>
    /// <returns> 番号 </returns>
    public int GetStageNum()
    {
        return stageNum;
    }

    /// <summary>
    /// ワールド番号設定
    /// </summary>
    /// <param name="number"> 番号 </param>
    public void SetWorldNum(int number)
    {
        worldNum = Math.Max(number, -1);
    }

    /// <summary>
    /// ステージ番号設定
    /// </summary>
    /// <param name="number"> 番号 </param>
    public void SetStageNum(int number)
    {
        stageNum = Math.Max(number, -1);
    }

    /// <summary>
    /// データ読み込み
    /// </summary>
    /// <returns> 成功か </returns>
    public bool ReadData()
    {
        StageData stageData = new StageData();

        if (!stageData.ReadData(worldNum, stageNum))
            return false;

        stageData.SetUpEditorObject(this);

        return true;
    }

    /// <summary>
    /// データ書き込み
    /// </summary>
    /// <returns> 成功か </returns>
    public bool WriteData()
    {
        StageData stageData = new StageData(this);

        return stageData.WriteData(worldNum, stageNum);
    }

    #endregion

    #region 原点位置関係

    /// <summary>
    /// 左X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetLeftPivotX()
    {
        return transform.position.x;
    }

    /// <summary>
    /// 中X原点取得
    /// </summary>
    /// <returns>中X原点</returns>
    public float GetMiddlePivotX()
    {
        return transform.position.x + (gridSize.x * 0.5f);
    }

    /// <summary>
    /// 右X原点取得
    /// </summary>
    /// <returns>左X原点</returns>
    public float GetRightPivotX()
    {
        return transform.position.x + gridSize.x;
    }

    /// <summary>
    /// 下Y原点取得
    /// </summary>
    /// <returns>下Y原点</returns>
    public float GetBottomPivotY()
    {
        return transform.position.y;
    }

    /// <summary>
    /// 中Y原点取得
    /// </summary>
    /// <returns>中Y原点</returns>
    public float GetMiddlePivotY()
    {
        return transform.position.y + (gridSize.y * 0.5f);
    }

    /// <summary>
    /// 上Y原点取得
    /// </summary>
    /// <returns>上Y原点</returns>
    public float GetTopPivotY()
    {
        return transform.position.y + gridSize.y;
    }

    #endregion

    /// <summary>
    /// グリッド
    /// </summary>
    public class GridList
    {
        /// <summary> グリッド </summary>
        GridListX gridList = new GridListX();

        /// <summary> グリッドの大きさ </summary>
        Vector2Int gridSize = Vector2Int.zero;

        /// <summary> 横方向グリッド数取得用 </summary>
        int XCount { get => gridList.gridX.Count; }

        /// <summary> 縦方向グリッド数取得用 </summary>
        int YCount { get => XCount > 0 ? gridList.gridX[0].gridY.Count : 0; }

        GridListY this[int index]
        {
            get => gridList[index];
            set => gridList[index] = value;
        }

        ObjectList this[int indexX, int indexY]
        {
            get => gridList[indexX][indexY];
            set => gridList[indexX][indexY] = value;
        }

        EditorObject this[int indexX, int indexY, int indexObject]
        {
            get => gridList[indexX][indexY][indexObject];
            set => gridList[indexX][indexY][indexObject] = value;
        }

        #region 初期化・解放関係

        /// <summary>
        /// 一番最初に呼び出す
        /// </summary>
        /// <returns> 成功したか </returns>
        public bool OnCreate()
        {
            return true;
        }

        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <returns> 成功したか </returns>
        public bool Initialize()
        {
            UnInitialize();

            return true;
        }

        /// <summary>
        /// 解放処理
        /// </summary>
        public void UnInitialize()
        {
            RemoveAll();
        }

        #endregion

        #region グリッド関係

        /// <summary>
        /// x位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> x位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPositionX(int position)
        {
            var xMax = XCount;

            if (position < 0 || position >= xMax)
                return false;

            return true;
        }

        /// <summary>
        /// y位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> y位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPositionY(int position)
        {
            if (XCount < 1)
                return false;

            var yMax = YCount;

            if (position < 0 || position >= yMax)
                return false;

            return true;
        }

        /// <summary>
        /// 位置がグリッド内か判定
        /// </summary>
        /// <param name="position"> 位置 </param>
        /// <returns> グリッド内か </returns>
        public bool CheckInPosition(Vector2Int position)
        {
            var xMax = XCount;

            if (position.x < 0 || position.x >= xMax)
                return false;

            var yMax = gridList.gridX[position.x].gridY.Count;

            if (position.y < 0 || position.y >= yMax)
                return false;

            return true;
        }

        /// <summary>
        /// グリッドの大きさを取得
        /// </summary>
        /// <returns> グリッドの大きさ </returns>
        public Vector2Int GetGridSize()
        {
            return gridSize;
        }

        /// <summary>
        /// グリッドの大きさを設定
        /// </summary>
        /// <param name="gridSize"> グリッドの大きさ </param>
        public void SetGridSize(Vector2Int gridSize)
        {
            this.gridSize = gridSize;

            while (this.gridSize.x < gridList.gridX.Count)
                gridList.gridX.RemoveAt(gridList.gridX.Count - 1);

            for (int x = gridList.gridX.Count; x < this.gridSize.x; ++x)
                gridList.gridX.Add(new GridListY());

            for (int x = 0; x < gridList.gridX.Count; ++x)
            {
                while (this.gridSize.y < gridList.gridX[x].gridY.Count)
                    gridList.gridX[x].gridY.RemoveAt(gridList.gridX[x].gridY.Count - 1);

                for (int y = gridList.gridX[x].gridY.Count; y < this.gridSize.y; ++y)
                    gridList.gridX[x].gridY.Add(new ObjectList());
            }
        }

        /// <summary>
        /// グリッドから全て取り除く
        /// </summary>
        public void RemoveAll()
        {
            for (int x = 0, xLength = gridList.gridX.Count; x < xLength; ++x)
                for (int y = 0, yLength = gridList.gridX[x].gridY.Count; y < yLength; ++y)
                    gridList[x][y].objects.Clear();
        }

        /// <summary>
        /// グリッドから取り除く
        /// </summary>
        /// <param name="stageObject"> 取り除く対象 </param>
        /// <param name="position"> 取り除く位置 </param>
        /// <param name="size"> 取り除く範囲 </param>
        public void RemoveRange(EditorObject stageObject, Vector2Int position, Vector2Int size)
        {
            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    if (!gridList[indexX][indexY].objects.Contains(stageObject))
                        continue;

                    gridList[indexX][indexY].objects.Remove(stageObject);
                }
            }
        }

        /// <summary>
        /// グリッドから取り除く
        /// </summary>
        /// <param name="stageObject"> 取り除く対象 </param>
        /// <param name="position"> 取り除く位置 </param>
        public void RemoveAt(EditorObject stageObject, Vector2Int position)
        {
            if (!CheckInPosition(position))
                return;

            if (!gridList[position.x][position.y].objects.Contains(stageObject))
                return;

            gridList[position.x][position.y].objects.Remove(stageObject);
        }

        /// <summary>
        /// グリッドに追加する
        /// </summary>
        /// <param name="stageObject"> 追加する対象 </param>
        /// <param name="position"> 追加する位置 </param>
        /// <param name="size"> 追加する範囲 </param>
        public void AddRange(EditorObject stageObject, Vector2Int position, Vector2Int size)
        {
            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    if (gridList[indexX][indexY].objects.Contains(stageObject))
                        return;

                    gridList[indexX][indexY].objects.Add(stageObject);
                }
            }
        }

        /// <summary>
        /// グリッドに追加する
        /// </summary>
        /// <param name="stageObject"> 追加する対象 </param>
        /// <param name="position"> 追加する位置 </param>
        public void AddAt(EditorObject stageObject, Vector2Int position)
        {
            if (!CheckInPosition(position))
                return;

            if (gridList[position.x][position.y].objects.Contains(stageObject))
                return;

            gridList[position.x][position.y].objects.Add(stageObject);
        }

        /// <summary>
        /// グリッド指定位置に所属しているオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 取得する位置 </param>
        /// <param name="size"> 取得する範囲 </param>
        /// <returns> 所属しているオブジェクト群 </returns>
        public List<EditorObject> GetRange(Vector2Int position, Vector2Int size)
        {
            var result = new List<EditorObject>();

            for (int x = 0; x < size.x; ++x)
            {
                var indexX = position.x + x;

                if (!CheckInPositionX(indexX))
                    break;

                for (int y = 0; y < size.y; ++y)
                {
                    var indexY = position.y + y;

                    if (!CheckInPositionY(indexY))
                    {
                        size.y = y;
                        break;
                    }

                    for (int i = 0; i < gridList[indexX][indexY].objects.Count; ++i)
                    {
                        if (result.Contains(gridList[indexX][indexY][i]))
                            continue;

                        result.Add(gridList[indexX][indexY][i]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// グリッド指定位置に所属しているオブジェクト群を取得
        /// </summary>
        /// <param name="position"> 取得する位置 </param>
        /// <returns> 所属しているオブジェクト群 </returns>
        public List<EditorObject> GetAt(Vector2Int position)
        {
            if (!CheckInPosition(position))
                return new List<EditorObject>();

            return gridList[position.x][position.y].objects;
        }

        #endregion

        #region 子クラス群

        /// <summary>
        /// マス目に所属するオブジェクト群
        /// </summary>
        class ObjectList
        {
            /// <summary> マス目に所属するオブジェクト群 </summary>
            public List<EditorObject> objects = new List<EditorObject>();

            public EditorObject this[int index]
            {
                get { return CheckIndexInRange(index) ? objects[index] : null; }
                set { if (CheckIndexInRange(index)) objects[index] = value; }
            }

            /// <summary>
            /// インデックス番号が要素範囲内か
            /// </summary>
            /// <param name="index"> インデックス番号 </param>
            /// <returns> 範囲内か </returns>
            public bool CheckIndexInRange(int index)
            {
                return 0 <= index && index < objects.Count;
            }
        }

        /// <summary>
        /// 横方向グリッドリスト
        /// </summary>
        class GridListX
        {
            /// <summary> 横方向グリッドリスト </summary>
            public List<GridListY> gridX = new List<GridListY>();

            public GridListY this[int index]
            {
                get { return CheckIndexInRange(index) ? gridX[index] : null; }
                set { if (CheckIndexInRange(index)) gridX[index] = value; }
            }

            public ObjectList this[int indexX, int indexY]
            {
                get { return gridX[indexX] != null ? gridX[indexX][indexY] : null; }
                set { if (gridX[indexX] != null) gridX[indexX][indexY] = value; }
            }

            public EditorObject this[int indexX, int indexY, int indexObject]
            {
                get { return gridX[indexX][indexY] != null ? gridX[indexX][indexY][indexObject] : null; }
                set { if (gridX[indexX][indexY] != null) gridX[indexX][indexY][indexObject] = value; }
            }

            /// <summary>
            /// インデックス番号が要素範囲内か
            /// </summary>
            /// <param name="index"> インデックス番号 </param>
            /// <returns> 範囲内か </returns>
            public bool CheckIndexInRange(int index)
            {
                return 0 <= index && index < gridX.Count;
            }
        }

        /// <summary>
        /// 縦方向グリッドリスト
        /// </summary>
        class GridListY
        {
            /// <summary> 縦方向グリッドリスト </summary>
            public List<ObjectList> gridY = new List<ObjectList>();

            public ObjectList this[int index]
            {
                get { return CheckIndexInRange(index) ? gridY[index] : null; }
                set { if (CheckIndexInRange(index)) gridY[index] = value; }
            }

            public EditorObject this[int indexY, int indexObject]
            {
                get { return gridY[indexY] != null ? gridY[indexY][indexObject] : null; }
                set { if (gridY[indexY] != null) gridY[indexY][indexObject] = value; }
            }

            /// <summary>
            /// インデックス番号が要素範囲内か
            /// </summary>
            /// <param name="index"> インデックス番号 </param>
            /// <returns> 範囲内か </returns>
            public bool CheckIndexInRange(int index)
            {
                return 0 <= index && index < gridY.Count;
            }
        }

        #endregion
    }
}
