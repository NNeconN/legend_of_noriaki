using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class StageData
{
    #region enum

    /// <summary>
    /// データの種類判別ID
    /// </summary>
    enum DataID
    {
        GridSize,
        ChildDatas,
    }

    /// <summary>
    /// オブジェクトの種類
    /// </summary>
    public enum ObjectType
    {
        None,
        ObjectsLayer,
        BlockLayer,
        AreaLayer,
        Back,
        CameraArea,
        Player,
        Handle,
        Goal,
        Enemy1,
        Block,
        Area,
    }

    /// <summary>
    /// グリッド向き
    /// </summary>
    public enum GridDirection
    {
        None,
        Left,
        Right,
        Up,
        Down
    }

    /// <summary>
    /// オブジェクトの種類の数
    /// </summary>
    public static int ObjectTypeNum { get => Enum.GetNames(typeof(ObjectType)).Length; }

    /// <summary>
    /// enum方向から向きベクトル取得
    /// </summary>
    /// <param name="gridDirection"> enum方向 </param>
    /// <returns> 向きベクトル </returns>
    public static Vector2Int GetDirectionByGridDirection(GridDirection gridDirection)
    {
        switch (gridDirection)
        {
            case GridDirection.Left:
                return Vector2Int.left;
            case GridDirection.Right:
                return Vector2Int.right;
            case GridDirection.Up:
                return Vector2Int.up;
            case GridDirection.Down:
                return Vector2Int.down;
            default:
                return Vector2Int.zero;
        }
    }

    #endregion

    /// <summary> オブジェクトレイヤーデータ群 </summary>
    List<ObjectsLayerData> objectsLayerDatas = new List<ObjectsLayerData>();

    /// <summary> ブロックレイヤーデータ群 </summary>
    List<BlockLayerData> blockLayerDatas = new List<BlockLayerData>();

    /// <summary> 領域レイヤーデータ群 </summary>
    List<AreaLayerData> areaLayerDatas = new List<AreaLayerData>();

    /// <summary> グリッド上の大きさ </summary>
    Vector2Int gridSize = Vector2Int.one;

    /// <summary> オブジェクトレイヤーデータ群取得用 </summary>
    public List<ObjectsLayerData> ObjectsLayerDatas { get => objectsLayerDatas; }

    /// <summary> ブロックレイヤーデータ群取得用 </summary>
    public List<BlockLayerData> BlockLayerDatas { get => blockLayerDatas; }

    /// <summary> 領域レイヤーデータ群取得用 </summary>
    public List<AreaLayerData> AreaLayerDatas { get => areaLayerDatas; }

    /// <summary> グリッド上の大きさ取得用 </summary>
    public Vector2Int GridSize { get => gridSize; }

    /// <summary>
    /// 通常コンストラクタ
    /// </summary>
    public StageData()
    {
        gridSize = Vector2Int.zero;
        objectsLayerDatas.Clear();
        blockLayerDatas.Clear();
        areaLayerDatas.Clear();
    }

    /// <summary>
    /// エディターオブジェクトからデータを生成するコンストラクタ
    /// </summary>
    /// <param name="target"> 対象 </param>
    public StageData(StageEditor_Manager target)
    {
        gridSize = target.GetGridSize();

        var objectsLayer = target.GetHasMyObjectsLayer();
        var blockLayers = target.GetHasMyBlockLayers();
        var areaLayers = target.GetHasMyAreaLayers();

        if (objectsLayer)
            objectsLayerDatas.Add(new ObjectsLayerData(objectsLayer));

        for (int i = 0; i < blockLayers.Length; ++i)
            if (blockLayers[i])
                blockLayerDatas.Add(new BlockLayerData(blockLayers[i]));

        for (int i = 0; i < areaLayers.Count; ++i)
            if (areaLayers[i])
                areaLayerDatas.Add(new AreaLayerData(areaLayers[i]));
    }

    /// <summary>
    /// データを削除する
    /// </summary>
    public void Clear()
    {
        gridSize = Vector2Int.zero;
        objectsLayerDatas.Clear();
        blockLayerDatas.Clear();
        areaLayerDatas.Clear();
    }

    /// <summary>
    /// エディターオブジェクトにデータを設定
    /// </summary>
    /// <param name="target"> 対象 </param>
    public void SetUpEditorObject(StageEditor_Manager target)
    {
        target.Initialize();

        target.SetGridSize(gridSize);

        for (int i = 0; i < objectsLayerDatas.Count; ++i)
        {
            var newItem = target.AddObjectsLayer();

            if (!newItem)
                continue;

            objectsLayerDatas[i].SetUpEditorObject(newItem);
        }

        for (int i = 0; i < blockLayerDatas.Count; ++i)
        {
            var newItem = target.AddBlockLayer(blockLayerDatas[i].MyBlockType);

            if (!newItem)
                continue;

            blockLayerDatas[i].SetUpEditorObject(newItem);
        }

        for (int i = 0; i < areaLayerDatas.Count; ++i)
        {
            var newItem = target.AddAreaLayer(areaLayerDatas[i].MyAreaType);

            if (!newItem)
                continue;

            areaLayerDatas[i].SetUpEditorObject(newItem);
        }
    }

    /// <summary>
    /// ディレクトリ名の取得
    /// </summary>
    /// <returns> ディレクトリ名 </returns>
    public static string GetDirectoryName()
    {
        return Application.streamingAssetsPath + "/StageData";
    }

    /// <summary>
    /// ファイル名の取得
    /// </summary>
    /// <param name="worldNum"> ワールド番号 </param>
    /// <param name="stageNum"> ステージ番号 </param>
    /// <returns> ファイル名 </returns>
    public static string GetFileName(int worldNum, int stageNum)
    {
        if (worldNum < 0 || stageNum < 0)
            return "Data_Editor.csv";
        else
            return "Data_" + worldNum.ToString() + "_" + stageNum.ToString() + ".csv";
    }

    /// <summary>
    /// ファイルパスの取得
    /// </summary>
    /// <param name="worldNum"> ワールド番号 </param>
    /// <param name="stageNum"> ステージ番号 </param>
    /// <returns> ファイルパス </returns>
    public static string GetFilePath(int worldNum, int stageNum)
    {
        return GetDirectoryName() + "/" + GetFileName(worldNum, stageNum);
    }

    /// <summary>
    /// データの読み込み
    /// </summary>
    /// <param name="worldNum"> ワールド番号 </param>
    /// <param name="stageNum"> ステージ番号 </param>
    /// <returns> 成功か </returns>
    public bool ReadData(int worldNum, int stageNum)
    {
        var directoryName = GetDirectoryName();
        var filePath = GetFilePath(worldNum, stageNum);

        if (!Directory.Exists(directoryName))
            return false;

        if (!File.Exists(filePath))
            return false;

        Clear();

        List<string[]> csvDatas = new List<string[]>();

        using (var reader = new StringReader(File.ReadAllText(filePath)))
        {
            while (reader.Peek() > -1)
            {
                string line = reader.ReadLine();
                line.Replace("\n", "");
                csvDatas.Add(line.Split(','));
            }
        }

        int index = 0;

        while (index < csvDatas.Count)
        {
            DataID dataID;
            if (!Enum.TryParse(csvDatas[index][0], out dataID))
                break;

            var dataIsNone = false;

            switch (dataID)
            {
                case DataID.GridSize:
                    gridSize.x = int.Parse(csvDatas[index][1]);
                    gridSize.y = int.Parse(csvDatas[index][2]);
                    ++index;
                    break;
                case DataID.ChildDatas:
                    ++index;
                    while (index < csvDatas.Count)
                    {
                        ObjectType objectType;
                        if (!Enum.TryParse(csvDatas[index][0], out objectType))
                            break;

                        var isFailed = false;

                        switch (objectType)
                        {
                            case ObjectType.ObjectsLayer:
                                ++index;
                                {
                                    var newData = new ObjectsLayerData();
                                    newData.SetData(csvDatas, ref index);
                                    objectsLayerDatas.Add(newData);
                                }
                                break;
                            case ObjectType.BlockLayer:
                                ++index;
                                {
                                    var newData = new BlockLayerData();
                                    newData.SetData(csvDatas, ref index);
                                    blockLayerDatas.Add(newData);
                                }
                                break;
                            case ObjectType.AreaLayer:
                                ++index;
                                {
                                    var newData = new AreaLayerData();
                                    newData.SetData(csvDatas, ref index);
                                    areaLayerDatas.Add(newData);
                                }
                                break;
                            default:
                                isFailed = true;
                                break;
                        }

                        if (isFailed)
                            break;
                    }
                    break;
                default:
                    dataIsNone = true;
                    break;
            }

            if (dataIsNone)
                break;
        }

        return true;
    }

    /// <summary>
    /// データの書き込み
    /// </summary>
    /// <param name="worldNum"> ワールド番号 </param>
    /// <param name="stageNum"> ステージ番号 </param>
    /// <returns> 成功か </returns>
    public bool WriteData(int worldNum, int stageNum)
    {
        var directoryName = GetDirectoryName();
        var filePath = GetFilePath(worldNum, stageNum);

        if (!Directory.Exists(directoryName))
            if (!Directory.CreateDirectory(directoryName).Exists)
                return false;

        List<string[]> csvDatas = new List<string[]>();

        csvDatas.Add(new string[] { DataID.GridSize.ToString(), gridSize.x.ToString(), gridSize.y.ToString() });

        csvDatas.Add(new string[] { DataID.ChildDatas.ToString() });

        for (int i = 0; i < objectsLayerDatas.Count; ++i)
            objectsLayerDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < blockLayerDatas.Count; ++i)
            blockLayerDatas[i].GetData(ref csvDatas);

        for (int i = 0; i < areaLayerDatas.Count; ++i)
            areaLayerDatas[i].GetData(ref csvDatas);

        using (StreamWriter streamWriter = new StreamWriter(filePath))
        {
            for (int i = 0; i < csvDatas.Count; ++i)
            {
                streamWriter.WriteLine(string.Join(",", csvDatas[i]));
            }
        }

        return true;
    }
}
