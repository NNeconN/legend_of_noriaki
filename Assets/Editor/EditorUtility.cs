using UnityEditor;
using UnityEngine;

/// <summary>
/// エディター拡張用便利関数
/// </summary>
public class EditorUtility : MonoBehaviour
{
    #region Script参照

    /// <summary>
    /// 自身のScriptを参照する領域
    /// </summary>
    /// <param name="target"> 自身のScript </param>
    /// <param name="content"> タイトル </param>
    public static void ScriptReferenceField(Object target, GUIContent content)
    {
        using (new EditorGUI.DisabledGroupScope(true))
        {
            EditorGUILayout.ObjectField(content, MonoScript.FromMonoBehaviour((MonoBehaviour)target), typeof(MonoScript), false);
        }
    }

    /// <summary>
    /// 自身のScriptを参照する領域
    /// </summary>
    /// <param name="target"> 自身のScript </param>
    /// <param name="text"> タイトル </param>
    public static void ScriptReferenceField(Object target, string text)
    {
        using (new EditorGUI.DisabledGroupScope(true))
        {
            EditorGUILayout.ObjectField(text, MonoScript.FromMonoBehaviour((MonoBehaviour)target), typeof(MonoScript), false);
        }
    }

    #endregion

    #region Convert

    /// <summary>
    /// targetを対象クラスに変換
    /// </summary>
    /// <typeparam name="T"> 対象クラス </typeparam>
    /// <param name="targetScript"> 対象クラス </param>
    /// <param name="target"> target </param>
    /// <returns> 変換できているか </returns>
    public static bool ConvertTarget<T>(ref T targetScript, Object target) where T : class
    {
        if (targetScript == null)
            targetScript = target as T;

        return targetScript != null;
    }

    /// <summary>
    /// SerializeProperty取得
    /// </summary>
    /// <param name="serializedProperty"> SerializeProperty </param>
    /// <param name="serializedObject"> 取得するオブジェクト </param>
    /// <param name="valueName"> 変数名 </param>
    public static void ConvertSerializeProperty(ref SerializedProperty serializedProperty, SerializedObject serializedObject, string valueName)
    {
        if (serializedProperty != null)
            return;

        serializedProperty = serializedObject.FindProperty(valueName);
    }

    #endregion

    #region HeaderProcess

    /// <summary>
    /// ヘッダー表示
    /// </summary>
    /// <param name="content"> タイトル </param>
    /// <param name="isOpen"> 内容を開いた状態か </param>
    /// <param name="color"> 背景色 </param>
    /// <returns> 内容を開いた状態か </returns>
    public static bool HeaderProcess(GUIContent content, bool isOpen, Color color)
    {
        GUI.backgroundColor = color;
        using (new GUILayout.HorizontalScope(EditorStyles.toolbar))
        {
            GUILayout.Space(15.0f);
            isOpen = EditorGUILayout.BeginFoldoutHeaderGroup(isOpen, content);
        }

        EditorGUILayout.EndFoldoutHeaderGroup();

        return isOpen;
    }

    /// <summary>
    /// ヘッダー表示
    /// </summary>
    /// <param name="text"> タイトル </param>
    /// <param name="isOpen"> 内容を開いた状態か </param>
    /// <param name="color"> 背景色 </param>
    /// <returns> 内容を開いた状態か </returns>
    public static bool HeaderProcess(string text, bool isOpen, Color color)
    {
        return HeaderProcess(new GUIContent(text), isOpen, color);
    }

    /// <summary>
    /// ヘッダー表示
    /// </summary>
    /// <param name="content"> タイトル </param>
    /// <param name="color"> 背景色 </param>
    public static void HeaderProcess(GUIContent content, Color color)
    {
        GUI.backgroundColor = color;
        using (new GUILayout.HorizontalScope(EditorStyles.toolbar))
        {
            EditorGUILayout.LabelField(content);
        }

        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    /// <summary>
    /// ヘッダー表示
    /// </summary>
    /// <param name="text"> タイトル </param>
    /// <param name="color"> 背景色 </param>
    public static void HeaderProcess(string text, Color color)
    {
        HeaderProcess(new GUIContent(text), color);
    }

    #endregion

    #region PropertyField

    /// <summary>
    /// シリアライズプロパティ設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="space"> 空白部分 </param>
    public static void PropertyField(SerializedProperty property, GUIContent content, float space)
    {
        using (new GUILayout.HorizontalScope())
        {
            GUILayout.Space(space);
            EditorGUILayout.PropertyField(property, content);
        }
    }

    #endregion

    #region SerializedObjectField

    /// <summary>
    /// オブジェクト参照設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="type"> オブジェクトの型情報 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool ObjectField(SerializedProperty property, GUIContent content, System.Type type)
    {
        var prev = property.objectReferenceValue;

        property.objectReferenceValue = EditorGUILayout.ObjectField(content, property.objectReferenceValue, type, true);

        return property.objectReferenceValue != prev;
    }

    /// <summary>
    /// オブジェクト参照設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="type"> オブジェクトの型情報 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool ObjectField(SerializedProperty property, string text, System.Type type)
    {
        return ObjectField(property, new GUIContent(text), type);
    }

    /// <summary>
    /// オブジェクト参照設定
    /// </summary>
    /// <typeparam name="T"> 対象オブジェクト </typeparam>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool ObjectField<T>(SerializedProperty property, GUIContent content) where T : UnityEngine.Object
    {
        return ObjectField(property, content, typeof(T));
    }

    /// <summary>
    /// オブジェクト参照設定
    /// </summary>
    /// <typeparam name="T"> 対象オブジェクト </typeparam>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool ObjectField<T>(SerializedProperty property, string text) where T : UnityEngine.Object
    {
        return ObjectField(property, new GUIContent(text), typeof(T));
    }

    #endregion

    #region ObjectField

    /// <summary>
    /// オブジェクト参照設定
    /// </summary>
    /// <typeparam name="T"> 対象オブジェクト </typeparam>
    /// <param name="obj"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool ObjectField<T>(ref T obj, GUIContent content) where T : UnityEngine.Object
    {
        var prev = obj;

        obj = (T)EditorGUILayout.ObjectField(content, (UnityEngine.Object)obj, typeof(T), true);

        return obj != prev;
    }

    /// <summary>
    /// オブジェクト参照設定
    /// </summary>
    /// <typeparam name="T"> 対象オブジェクト </typeparam>
    /// <param name="obj"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool ObjectField<T>(ref T obj, string text) where T : UnityEngine.Object
    {
        return ObjectField<T>(ref obj, new GUIContent(text));
    }

    #endregion

    #region SerializedFloatField

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(SerializedProperty property, GUIContent content)
    {
        var prev = property.floatValue;

        property.floatValue = EditorGUILayout.FloatField(content, property.floatValue);

        return property.floatValue != prev;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(SerializedProperty property, string text)
    {
        return FloatField(property, new GUIContent(text));
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(SerializedProperty property, GUIContent content, float min, float max)
    {
        var prev = property.floatValue;

        property.floatValue = Mathf.Clamp(EditorGUILayout.FloatField(content, property.floatValue), min, max);

        return property.floatValue != prev;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(SerializedProperty property, string text, float min, float max)
    {
        return FloatField(property, new GUIContent(text), min, max);
    }

    #endregion

    #region FloatField

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(ref float value, GUIContent content)
    {
        var prev = value;

        value = EditorGUILayout.FloatField(content, value);

        return value != prev;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(ref float value, string text)
    {
        return FloatField(ref value, new GUIContent(text));
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(ref float value, GUIContent content, float min, float max)
    {
        var prev = value;

        value = Mathf.Clamp(EditorGUILayout.FloatField(content, value), min, max);

        return value != prev;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool FloatField(ref float value, string text, float min, float max)
    {
        return FloatField(ref value, new GUIContent(text), min, max);
    }

    #endregion

    #region SerializedDelayedFloatField

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(SerializedProperty property, GUIContent content)
    {
        var temp = property.floatValue;

        property.floatValue = EditorGUILayout.DelayedFloatField(content, property.floatValue);

        return property.floatValue != temp;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(SerializedProperty property, string text)
    {
        var temp = property.floatValue;

        property.floatValue = EditorGUILayout.DelayedFloatField(text, property.floatValue);

        return property.floatValue != temp;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(SerializedProperty property, float min, float max, GUIContent content)
    {
        var temp = property.floatValue;

        property.floatValue = Mathf.Clamp(EditorGUILayout.DelayedFloatField(content, property.floatValue), min, max);

        return property.floatValue != temp;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(SerializedProperty property, float min, float max, string text)
    {
        var temp = property.floatValue;

        property.floatValue = Mathf.Clamp(EditorGUILayout.DelayedFloatField(text, property.floatValue), min, max);

        return property.floatValue != temp;
    }

    #endregion

    #region DelayedFloatField

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(ref float value, GUIContent content)
    {
        var temp = value;

        value = EditorGUILayout.DelayedFloatField(content, value);

        return value != temp;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(ref float value, string text)
    {
        var temp = value;
        value = EditorGUILayout.DelayedFloatField(text, value);

        return value != temp;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(ref float value, float min, float max, GUIContent content)
    {
        var temp = value;

        value = Mathf.Clamp(EditorGUILayout.DelayedFloatField(content, value), min, max);

        return value != temp;
    }

    /// <summary>
    /// float設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedFloatField(ref float value, float min, float max, string text)
    {
        var temp = value;

        value = Mathf.Clamp(EditorGUILayout.DelayedFloatField(text, value), min, max);

        return value != temp;
    }

    #endregion

    #region SerializedVector2Field

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content)
    {
        var prev = property.vector2Value;

        property.vector2Value = EditorGUILayout.Vector2Field(content, property.vector2Value);

        return property.vector2Value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text)
    {
        return Vector2Field(property, new GUIContent(text));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = property.vector2Value;
        var temp = property.vector2Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
        }

        property.vector2Value = temp;

        return property.vector2Value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent)
    {
        return Vector2Field(property, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent)
    {
        return Vector2Field(property, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText)
    {
        return Vector2Field(property, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, string xText, GUIContent yContent)
    {
        return Vector2Field(property, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, GUIContent xContent, string yText)
    {
        return Vector2Field(property, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, string xText, string yText)
    {
        return Vector2Field(property, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, string xText, string yText)
    {
        return Vector2Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = property.vector2Value;
        var temp = property.vector2Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
        }

        property.vector2Value = temp;

        return property.vector2Value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return Vector2Field(property, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return Vector2Field(property, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return Vector2Field(property, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, string xText, GUIContent yContent, Color color)
    {
        return Vector2Field(property, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, GUIContent xContent, string yText, Color color)
    {
        return Vector2Field(property, new GUIContent(text), xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, GUIContent content, string xText, string yText, Color color)
    {
        return Vector2Field(property, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(SerializedProperty property, string text, string xText, string yText, Color color)
    {
        return Vector2Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region Vector2Field

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content)
    {
        var prev = value;

        value = EditorGUILayout.Vector2Field(content, value);

        return value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text)
    {
        return Vector2Field(ref value, new GUIContent(text));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, GUIContent xContent, GUIContent yContent)
    {
        return Vector2Field(ref value, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, string xText, GUIContent yContent)
    {
        return Vector2Field(ref value, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, string yText)
    {
        return Vector2Field(ref value, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, string xText, GUIContent yContent)
    {
        return Vector2Field(ref value, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, GUIContent xContent, string yText)
    {
        return Vector2Field(ref value, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, string xText, string yText)
    {
        return Vector2Field(ref value, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, string xText, string yText)
    {
        return Vector2Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return Vector2Field(ref value, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return Vector2Field(ref value, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return Vector2Field(ref value, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, string xText, GUIContent yContent, Color color)
    {
        return Vector2Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, GUIContent content, string xText, string yText, Color color)
    {
        return Vector2Field(ref value, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2Field(ref Vector2 value, string text, string xText, string yText, Color color)
    {
        return Vector2Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region SerializedDelayedDelayedVector2Field

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = property.vector2Value;
        var temp = property.vector2Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
        }

        property.vector2Value = temp;

        return property.vector2Value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent)
    {
        return DelayedVector2Field(property, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent)
    {
        return DelayedVector2Field(property, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText)
    {
        return DelayedVector2Field(property, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, string xText, GUIContent yContent)
    {
        return DelayedVector2Field(property, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, GUIContent xContent, string yText)
    {
        return DelayedVector2Field(property, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, string xText, string yText)
    {
        return DelayedVector2Field(property, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, string xText, string yText)
    {
        return DelayedVector2Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = property.vector2Value;
        var temp = property.vector2Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
        }

        property.vector2Value = temp;

        return property.vector2Value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return DelayedVector2Field(property, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2Field(property, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return DelayedVector2Field(property, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2Field(property, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, GUIContent xContent, string yText, Color color)
    {
        return DelayedVector2Field(property, new GUIContent(text), xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, GUIContent content, string xText, string yText, Color color)
    {
        return DelayedVector2Field(property, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(SerializedProperty property, string text, string xText, string yText, Color color)
    {
        return DelayedVector2Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region DelayedVector2Field

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, GUIContent xContent, GUIContent yContent)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, string xText, GUIContent yContent)
    {
        return DelayedVector2Field(ref value, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, string yText)
    {
        return DelayedVector2Field(ref value, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, string xText, GUIContent yContent)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, GUIContent xContent, string yText)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, string xText, string yText)
    {
        return DelayedVector2Field(ref value, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, string xText, string yText)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2Field(ref value, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return DelayedVector2Field(ref value, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, GUIContent content, string xText, string yText, Color color)
    {
        return DelayedVector2Field(ref value, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2Field(ref Vector2 value, string text, string xText, string yText, Color color)
    {
        return DelayedVector2Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region SerializedVector3Field

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content)
    {
        var prev = property.vector3Value;

        property.vector3Value = EditorGUILayout.Vector3Field(content, property.vector3Value);

        return property.vector3Value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text)
    {
        return Vector3Field(property, new GUIContent(text));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = property.vector3Value;
        var temp = property.vector3Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
            temp.z = EditorGUILayout.FloatField(zContent, temp.z);
        }

        property.vector3Value = temp;

        return property.vector3Value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return Vector3Field(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3Field(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3Field(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3Field(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3Field(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return Vector3Field(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return Vector3Field(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return Vector3Field(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, string yText, GUIContent zContent)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, string zText)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, string zText)
    {
        return Vector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, string yText, string zText)
    {
        return Vector3Field(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, string yText, string zText)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = property.vector3Value;
        var temp = property.vector3Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
            temp.z = EditorGUILayout.FloatField(zContent, temp.z);
        }

        property.vector3Value = temp;

        return property.vector3Value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3Field(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3Field(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3Field(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return Vector3Field(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(SerializedProperty property, string text, string xText, string yText, string zText, Color color)
    {
        return Vector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region Vector3Field

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content)
    {
        var prev = value;

        value = EditorGUILayout.Vector3Field(content, value);

        return value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text)
    {
        return Vector3Field(ref value, new GUIContent(text));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
            temp.z = EditorGUILayout.FloatField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3Field(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3Field(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return Vector3Field(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, string yText, GUIContent zContent)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, string zText)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, string zText)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, string yText, string zText)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, string yText, string zText)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.FloatField(xContent, temp.x);
            temp.y = EditorGUILayout.FloatField(yContent, temp.y);
            temp.z = EditorGUILayout.FloatField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3Field(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return Vector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3Field(ref Vector3 value, string text, string xText, string yText, string zText, Color color)
    {
        return Vector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region SerializedDelayedVector3Field

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = property.vector3Value;
        var temp = property.vector3Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedFloatField(zContent, temp.z);
        }

        property.vector3Value = temp;

        return property.vector3Value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3Field(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, string yText, string zText)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, string yText, string zText)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = property.vector3Value;
        var temp = property.vector3Value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedFloatField(zContent, temp.z);
        }

        property.vector3Value = temp;

        return property.vector3Value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3Field(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3Field(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(SerializedProperty property, string text, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3Field(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region DelayedVector3Field

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedFloatField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3Field(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, string yText, string zText)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, string yText, string zText)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedFloatField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedFloatField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedFloatField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3Field(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3Field(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3Field(ref Vector3 value, string text, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3Field(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region SerializedIntField

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(SerializedProperty property, GUIContent content)
    {
        var prev = property.intValue;

        property.intValue = EditorGUILayout.IntField(content, property.intValue);

        return property.intValue != prev;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(SerializedProperty property, string text)
    {
        return IntField(property, new GUIContent(text));
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(SerializedProperty property, GUIContent content, int min, int max)
    {
        var prev = property.intValue;

        property.intValue = Mathf.Clamp(EditorGUILayout.IntField(content, property.intValue), min, max);

        return property.intValue != prev;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(SerializedProperty property, string text, int min, int max)
    {
        return IntField(property, new GUIContent(text), min, max);
    }

    #endregion

    #region IntField

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(ref int value, GUIContent content)
    {
        var prev = value;

        value = EditorGUILayout.IntField(content, value);

        return value != prev;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(ref int value, string text)
    {
        return IntField(ref value, new GUIContent(text));
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(ref int value, GUIContent content, int min, int max)
    {
        var prev = value;

        value = Mathf.Clamp(EditorGUILayout.IntField(content, value), min, max);

        return value != prev;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool IntField(ref int value, string text, int min, int max)
    {
        return IntField(ref value, new GUIContent(text), min, max);
    }

    #endregion

    #region SerializedDelayedIntField

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(SerializedProperty property, GUIContent content)
    {
        var temp = property.intValue;

        property.intValue = EditorGUILayout.DelayedIntField(content, property.intValue);

        return property.intValue != temp;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(SerializedProperty property, string text)
    {
        var temp = property.intValue;

        property.intValue = EditorGUILayout.DelayedIntField(text, property.intValue);

        return property.intValue != temp;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(SerializedProperty property, int min, int max, GUIContent content)
    {
        var temp = property.intValue;

        property.intValue = Mathf.Clamp(EditorGUILayout.DelayedIntField(content, property.intValue), min, max);

        return property.intValue != temp;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(SerializedProperty property, int min, int max, string text)
    {
        var temp = property.intValue;

        property.intValue = Mathf.Clamp(EditorGUILayout.DelayedIntField(text, property.intValue), min, max);

        return property.intValue != temp;
    }

    #endregion

    #region DelayedIntField

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(ref int value, GUIContent content)
    {
        var temp = value;

        value = EditorGUILayout.DelayedIntField(content, value);

        return value != temp;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(ref int value, string text)
    {
        var temp = value;
        value = EditorGUILayout.DelayedIntField(text, value);

        return value != temp;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(ref int value, int min, int max, GUIContent content)
    {
        var temp = value;

        value = Mathf.Clamp(EditorGUILayout.DelayedIntField(content, value), min, max);

        return value != temp;
    }

    /// <summary>
    /// int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="min"> 最小値 </param>
    /// <param name="max"> 最大値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedIntField(ref int value, int min, int max, string text)
    {
        var temp = value;

        value = Mathf.Clamp(EditorGUILayout.DelayedIntField(text, value), min, max);

        return value != temp;
    }

    #endregion

    #region SerializedVector2IntField

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content)
    {
        var prev = property.vector2IntValue;

        property.vector2IntValue = EditorGUILayout.Vector2IntField(content, property.vector2IntValue);

        return property.vector2IntValue != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text)
    {
        return Vector2IntField(property, new GUIContent(text));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = property.vector2IntValue;
        var temp = property.vector2IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
        }

        property.vector2IntValue = temp;

        return property.vector2IntValue != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent)
    {
        return Vector2IntField(property, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent)
    {
        return Vector2IntField(property, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText)
    {
        return Vector2IntField(property, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, string xText, GUIContent yContent)
    {
        return Vector2IntField(property, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, GUIContent xContent, string yText)
    {
        return Vector2IntField(property, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, string xText, string yText)
    {
        return Vector2IntField(property, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, string xText, string yText)
    {
        return Vector2IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = property.vector2IntValue;
        var temp = property.vector2IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
        }

        property.vector2IntValue = temp;

        return property.vector2IntValue != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return Vector2IntField(property, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return Vector2IntField(property, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return Vector2IntField(property, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, string xText, GUIContent yContent, Color color)
    {
        return Vector2IntField(property, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, GUIContent xContent, string yText, Color color)
    {
        return Vector2IntField(property, new GUIContent(text), xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, GUIContent content, string xText, string yText, Color color)
    {
        return Vector2IntField(property, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(SerializedProperty property, string text, string xText, string yText, Color color)
    {
        return Vector2IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region Vector2IntField

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content)
    {
        var prev = value;

        value = EditorGUILayout.Vector2IntField(content, value);

        return value != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text)
    {
        return Vector2IntField(ref value, new GUIContent(text));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, GUIContent xContent, GUIContent yContent)
    {
        return Vector2IntField(ref value, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, string xText, GUIContent yContent)
    {
        return Vector2IntField(ref value, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, string yText)
    {
        return Vector2IntField(ref value, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, string xText, GUIContent yContent)
    {
        return Vector2IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, GUIContent xContent, string yText)
    {
        return Vector2IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, string xText, string yText)
    {
        return Vector2IntField(ref value, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, string xText, string yText)
    {
        return Vector2IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return Vector2IntField(ref value, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return Vector2IntField(ref value, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return Vector2IntField(ref value, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, string xText, GUIContent yContent, Color color)
    {
        return Vector2IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, GUIContent content, string xText, string yText, Color color)
    {
        return Vector2IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector2IntField(ref Vector2Int value, string text, string xText, string yText, Color color)
    {
        return Vector2IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region SerializedDelayedDelayedVector2IntField

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = property.vector2IntValue;
        var temp = property.vector2IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
        }

        property.vector2IntValue = temp;

        return property.vector2IntValue != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent)
    {
        return DelayedVector2IntField(property, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent)
    {
        return DelayedVector2IntField(property, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText)
    {
        return DelayedVector2IntField(property, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, string xText, GUIContent yContent)
    {
        return DelayedVector2IntField(property, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, GUIContent xContent, string yText)
    {
        return DelayedVector2IntField(property, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, string xText, string yText)
    {
        return DelayedVector2IntField(property, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, string xText, string yText)
    {
        return DelayedVector2IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = property.vector2IntValue;
        var temp = property.vector2IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
        }

        property.vector2IntValue = temp;

        return property.vector2IntValue != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return DelayedVector2IntField(property, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2IntField(property, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return DelayedVector2IntField(property, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2IntField(property, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, GUIContent xContent, string yText, Color color)
    {
        return DelayedVector2IntField(property, new GUIContent(text), xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, GUIContent content, string xText, string yText, Color color)
    {
        return DelayedVector2IntField(property, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(SerializedProperty property, string text, string xText, string yText, Color color)
    {
        return DelayedVector2IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region DelayedVector2IntField

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, GUIContent yContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, GUIContent xContent, GUIContent yContent)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), xContent, yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, string xText, GUIContent yContent)
    {
        return DelayedVector2IntField(ref value, content, new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, string yText)
    {
        return DelayedVector2IntField(ref value, content, xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, string xText, GUIContent yContent)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, GUIContent xContent, string yText)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, string xText, string yText)
    {
        return DelayedVector2IntField(ref value, content, new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, string xText, string yText)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText));
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, GUIContent yContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, GUIContent xContent, GUIContent yContent, Color color)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), xContent, yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2IntField(ref value, content, new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, GUIContent xContent, string yText, Color color)
    {
        return DelayedVector2IntField(ref value, content, xContent, new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, string xText, GUIContent yContent, Color color)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, GUIContent content, string xText, string yText, Color color)
    {
        return DelayedVector2IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), color);
    }

    /// <summary>
    /// Vector2Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector2IntField(ref Vector2Int value, string text, string xText, string yText, Color color)
    {
        return DelayedVector2IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), color);
    }

    #endregion

    #region SerializedVector3IntField

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content)
    {
        var prev = property.vector3IntValue;

        property.vector3IntValue = EditorGUILayout.Vector3IntField(content, property.vector3IntValue);

        return property.vector3IntValue != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text)
    {
        return Vector3IntField(property, new GUIContent(text));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = property.vector3IntValue;
        var temp = property.vector3IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
            temp.z = EditorGUILayout.IntField(zContent, temp.z);
        }

        property.vector3IntValue = temp;

        return property.vector3IntValue != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3IntField(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3IntField(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3IntField(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return Vector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return Vector3IntField(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return Vector3IntField(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, string yText, GUIContent zContent)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, string zText)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, string zText)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, string zText)
    {
        return Vector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, string yText, string zText)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = property.vector3IntValue;
        var temp = property.vector3IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
            temp.z = EditorGUILayout.IntField(zContent, temp.z);
        }

        property.vector3IntValue = temp;

        return property.vector3IntValue != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3IntField(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return Vector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(SerializedProperty property, string text, string xText, string yText, string zText, Color color)
    {
        return Vector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region Vector3IntField

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content)
    {
        var prev = value;

        value = EditorGUILayout.Vector3IntField(content, value);

        return value != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text)
    {
        return Vector3IntField(ref value, new GUIContent(text));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
            temp.z = EditorGUILayout.IntField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3IntField(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3IntField(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return Vector3IntField(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, string yText, GUIContent zContent)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, string zText)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, string zText)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, string zText)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, string yText, string zText)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.IntField(xContent, temp.x);
            temp.y = EditorGUILayout.IntField(yContent, temp.y);
            temp.z = EditorGUILayout.IntField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3IntField(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return Vector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool Vector3IntField(ref Vector3Int value, string text, string xText, string yText, string zText, Color color)
    {
        return Vector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region SerializedDelayedVector3IntField

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = property.vector3IntValue;
        var temp = property.vector3IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedIntField(zContent, temp.z);
        }

        property.vector3IntValue = temp;

        return property.vector3IntValue != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3IntField(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, string zText)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, string yText, string zText)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = property.vector3IntValue;
        var temp = property.vector3IntValue;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedIntField(zContent, temp.z);
        }

        property.vector3IntValue = temp;

        return property.vector3IntValue != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(property, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(property, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(property, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(SerializedProperty property, string text, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(property, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region DelayedVector3IntField

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorGUILayout.LabelField(content);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedIntField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3IntField(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, string yText, GUIContent zContent)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, string zText)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, string zText)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, string zText)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, string yText, string zText)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        var prev = value;
        var temp = value;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess(content, color);
            temp.x = EditorGUILayout.DelayedIntField(xContent, temp.x);
            temp.y = EditorGUILayout.DelayedIntField(yContent, temp.y);
            temp.z = EditorGUILayout.DelayedIntField(zContent, temp.z);
        }

        value = temp;

        return value != prev;
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, content, xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, content, xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, content, xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zContent"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, string yText, GUIContent zContent, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), zContent);
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yContent"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, GUIContent yContent, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), yContent, new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xContent"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, GUIContent xContent, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), xContent, new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, GUIContent content, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, content, new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    /// <summary>
    /// Vector3Int設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <param name="xText"> x要素のタイトル </param>
    /// <param name="yText"> y要素のタイトル </param>
    /// <param name="zText"> z要素のタイトル </param>
    /// <param name="color"> ヘッダーのカラー </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedVector3IntField(ref Vector3Int value, string text, string xText, string yText, string zText, Color color)
    {
        return DelayedVector3IntField(ref value, new GUIContent(text), new GUIContent(xText), new GUIContent(yText), new GUIContent(zText));
    }

    #endregion

    #region LayerField

    /// <summary>
    /// レイヤー設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool LayerField(SerializedProperty property, GUIContent content)
    {
        var temp = property.intValue;

        property.intValue = EditorGUILayout.LayerField(content, property.intValue);

        return property.intValue != temp;
    }

    /// <summary>
    /// レイヤー設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool LayerField(SerializedProperty property, string text)
    {
        return LayerField(property, new GUIContent(text));
    }

    #endregion

    #region TextField

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool TextField(SerializedProperty property, GUIContent content)
    {
        var temp = property.stringValue;

        property.stringValue = EditorGUILayout.TextField(content, property.stringValue);

        return property.stringValue != temp;
    }

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool TextField(SerializedProperty property, string text)
    {
        return TextField(property, new GUIContent(text));
    }

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool TextField(ref string value, GUIContent content)
    {
        var temp = value;

        value = EditorGUILayout.TextField(content, value);

        return value != temp;
    }

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool TextField(ref string value, string text)
    {
        return TextField(ref value, new GUIContent(text));
    }

    #endregion

    #region DelayedTextField

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedTextField(SerializedProperty property, GUIContent content)
    {
        var temp = property.stringValue;

        property.stringValue = EditorGUILayout.DelayedTextField(content, property.stringValue);

        return property.stringValue != temp;
    }

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="property"> プロパティ </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedTextField(SerializedProperty property, string text)
    {
        return DelayedTextField(property, new GUIContent(text));
    }

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="content"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedTextField(ref string value, GUIContent content)
    {
        var temp = value;

        value = EditorGUILayout.DelayedTextField(content, value);

        return value != temp;
    }

    /// <summary>
    /// string設定
    /// </summary>
    /// <param name="value"> 入力された値 </param>
    /// <param name="text"> タイトル </param>
    /// <returns> 値が変更されたかどうか </returns>
    public static bool DelayedTextField(ref string value, string text)
    {
        return DelayedTextField(ref value, new GUIContent(text));
    }

    #endregion

    #region LinkField

    /// <summary>
    /// リンク付きラベル
    /// </summary>
    /// <param name="content"> タイトル </param>
    /// <param name="link"> リンク </param>
    public static void LinkField(GUIContent content, string link)
    {
        GUILayout.Label(content, EditorStyles.linkLabel);
        Rect rect = GUILayoutUtility.GetLastRect();
        EditorGUIUtility.AddCursorRect(rect, MouseCursor.Link);
        Event nowEvent = Event.current;
        if (nowEvent.type == EventType.MouseDown && rect.Contains(nowEvent.mousePosition))
        {
            Help.BrowseURL(@link);
        }
    }

    /// <summary>
    /// リンク付きラベル
    /// </summary>
    /// <param name="text"> タイトル </param>
    /// <param name="link"> リンク </param>
    public static void LinkField(string text, string link)
    {
        LinkField(new GUIContent(text), link);
    }

    #endregion
}
