using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Game_Manager))]
public class Game_ManagerES : Editor
{
    /// <summary> 対象クラス </summary>
    Game_Manager targetScript = null;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
                ValueInputFieldOnPlay(defaultColor);
            }
            else
            {
                ValueInputFieldOnEditor(defaultColor);
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// 値入力関係領域 (プレイ時)
    /// </summary>
    /// <param name="defaultColor"> 初期色 </param>
    void ValueInputFieldOnPlay(Color defaultColor)
    {
        EditorUtility.HeaderProcess("値参照関係", Color.gray);
        GUI.backgroundColor = defaultColor;

        Vector2Int worldStageNum = new Vector2Int(targetScript.WorldNum, targetScript.StageNum);

        using (new EditorGUI.DisabledGroupScope(true))
        {
            EditorUtility.Vector2IntField(ref worldStageNum, "選択している番号", "ワールド番号", "ステージ番号", Color.gray);
        }
    }

    /// <summary>
    /// 値入力関係領域 (エディター時)
    /// </summary>
    /// <param name="defaultColor"> 初期色 </param>
    void ValueInputFieldOnEditor(Color defaultColor)
    {
        EditorUtility.HeaderProcess("値入力関係", Color.gray);
        GUI.backgroundColor = defaultColor;

        var worldStageNum = new Vector2Int(targetScript.WorldNum, targetScript.StageNum);
        var isChanged = false;

        isChanged |= EditorUtility.Vector2IntField(ref worldStageNum, "選択している番号", "ワールド番号", "ステージ番号", Color.gray);

        if (isChanged)
        {
            Undo.RecordObject(target, "Update World And Stage Num");
            targetScript.WorldNum = worldStageNum.x;
            targetScript.StageNum = worldStageNum.y;
            UnityEditor.EditorUtility.SetDirty(target);
        }
    }
}
