using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Input_Manager))]
public class Input_ManagerES : Editor
{
    /// <summary> 対象クラス </summary>
    Input_Manager targetScript = null;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
                ValueInputFieldOnPlay(defaultColor);
            }
            else
            {
                ValueInputFieldOnEditor(defaultColor);
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// 値入力関係領域 (プレイ時)
    /// </summary>
    /// <param name="defaultColor"> 初期色 </param>
    void ValueInputFieldOnPlay(Color defaultColor)
    {
        EditorUtility.HeaderProcess("入力名関係", Color.gray);
        GUI.backgroundColor = defaultColor;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            var buttonNames = targetScript.GetButtonNames();
            var horizontalAxisNames = targetScript.GetHorizontalAxisNames();
            var verticalAxisNames = targetScript.GetVerticalAxisNames();
            var triggerName = targetScript.GetTriggerName();

            EditorUtility.HeaderProcess("ボタン入力名関係", Color.gray);
            using (new EditorGUI.DisabledGroupScope(true))
            {
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.A], "Aボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.B], "Bボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.X], "Xボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.Y], "Yボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.LB], "左ボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.RB], "右ボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.View], "Viewボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.Menu], "Menuボタン名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.LStick], "左スティック押し込み名");
                EditorUtility.DelayedTextField(ref buttonNames[(int)Input_Manager.ButtonIndex.RStick], "右スティック押し込み名");
                EditorUtility.DelayedTextField(ref triggerName, "トリガーボタン入力名");
            }

            EditorUtility.HeaderProcess("横軸入力名関係", Color.gray);
            using (new EditorGUI.DisabledGroupScope(true))
            {
                EditorUtility.DelayedTextField(ref horizontalAxisNames[(int)Input_Manager.AxisIndex.Left], "左スティック横入力名");
                EditorUtility.DelayedTextField(ref horizontalAxisNames[(int)Input_Manager.AxisIndex.Right], "右スティック横入力名");
                EditorUtility.DelayedTextField(ref horizontalAxisNames[(int)Input_Manager.AxisIndex.Direction], "方向ボタン横入力名");
            }

            EditorUtility.HeaderProcess("縦軸入力名関係", Color.gray);
            using (new EditorGUI.DisabledGroupScope(true))
            {
                EditorUtility.DelayedTextField(ref verticalAxisNames[(int)Input_Manager.AxisIndex.Left], "左スティック縦入力名");
                EditorUtility.DelayedTextField(ref verticalAxisNames[(int)Input_Manager.AxisIndex.Right], "右スティック縦入力名");
                EditorUtility.DelayedTextField(ref verticalAxisNames[(int)Input_Manager.AxisIndex.Direction], "方向ボタン縦入力名");
            }
        }
    }

    /// <summary>
    /// 値入力関係領域 (エディター時)
    /// </summary>
    /// <param name="defaultColor"> 初期色 </param>
    void ValueInputFieldOnEditor(Color defaultColor)
    {
        EditorUtility.HeaderProcess("入力名関係", Color.gray);
        GUI.backgroundColor = defaultColor;

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            var buttonNames = targetScript.GetButtonNames();
            var triggerName = targetScript.GetTriggerName();
            var isChanged = false;

            EditorUtility.HeaderProcess("ボタン入力名関係", Color.gray);

            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.A], "Aボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.B], "Bボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.X], "Xボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.Y], "Yボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.LB], "左ボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.RB], "右ボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.View], "Viewボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.Menu], "Menuボタン名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.LStick], "左スティック押し込み名");
            isChanged |= EditorUtility.TextField(ref buttonNames[(int)Input_Manager.ButtonIndex.RStick], "右スティック押し込み名");
            isChanged |= EditorUtility.TextField(ref triggerName, "トリガーボタン入力名");

            if (isChanged)
            {
                Undo.RecordObject(target, "Update ButtonNames");
                targetScript.SetButtonNames(buttonNames);
                targetScript.SetTriggerName(triggerName);
                UnityEditor.EditorUtility.SetDirty(target);
            }
        }

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            var horizontalAxisNames = targetScript.GetHorizontalAxisNames();
            var isChanged = false;

            EditorUtility.HeaderProcess("横軸入力名関係", Color.gray);
            isChanged |= EditorUtility.TextField(ref horizontalAxisNames[(int)Input_Manager.AxisIndex.Left], "左スティック横入力名");
            isChanged |= EditorUtility.TextField(ref horizontalAxisNames[(int)Input_Manager.AxisIndex.Right], "右スティック横入力名");
            isChanged |= EditorUtility.TextField(ref horizontalAxisNames[(int)Input_Manager.AxisIndex.Direction], "方向ボタン横入力名");

            if (isChanged)
            {
                Undo.RecordObject(target, "Update HorizontalAxisNames");
                targetScript.SetHorizontalAxisNames(horizontalAxisNames);
                UnityEditor.EditorUtility.SetDirty(target);
            }
        }

        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            var verticalAxisNames = targetScript.GetVerticalAxisNames();
            var isChanged = false;

            EditorUtility.HeaderProcess("縦軸入力名関係", Color.gray);
            isChanged |= EditorUtility.TextField(ref verticalAxisNames[(int)Input_Manager.AxisIndex.Left], "左スティック縦入力名");
            isChanged |= EditorUtility.TextField(ref verticalAxisNames[(int)Input_Manager.AxisIndex.Right], "右スティック縦入力名");
            isChanged |= EditorUtility.TextField(ref verticalAxisNames[(int)Input_Manager.AxisIndex.Direction], "方向ボタン縦入力名");

            if (isChanged)
            {
                Undo.RecordObject(target, "Update VerticalAxisNames");
                targetScript.SetVerticalAxisNames(verticalAxisNames);
                UnityEditor.EditorUtility.SetDirty(target);
            }
        }
    }
}