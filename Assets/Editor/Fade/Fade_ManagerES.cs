using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Fade_Manager))]
public class Fade_ManagerES : Editor
{
    /// <summary> 対象クラス </summary>
    Fade_Manager targetScript = null;

    /// <summary> currentFadeList プロパティ </summary>
    SerializedProperty currentFadeListProperty = null;

    /// <summary> fadeTime プロパティ </summary>
    SerializedProperty fadeTimeProperty = null;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        Undo.RecordObject(target, "Update Fade_Manager");
        serializedObject.Update();

        EditorUtility.ConvertSerializeProperty(ref currentFadeListProperty, serializedObject, "currentFadeList");
        EditorUtility.ConvertSerializeProperty(ref fadeTimeProperty, serializedObject, "fadeTime");

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess("フェード制御関係", Color.gray);
            GUI.backgroundColor = defaultColor;

            EditorUtility.FloatField(fadeTimeProperty, new GUIContent("初期フェード時間", "※途中で変更される可能性あり。"));
            EditorUtility.PropertyField(currentFadeListProperty, new GUIContent("初期フェード群"), 15.0f);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
