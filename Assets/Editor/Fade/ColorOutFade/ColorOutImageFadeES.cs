using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ColorOutImageFade))]
public class ColorOutImageFadeES : Editor
{
    /// <summary> 対象クラス </summary>
    ColorOutImageFade targetScript = null;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            EditorUtility.HeaderProcess("フェード表示関係", Color.gray);
            GUI.backgroundColor = defaultColor;

            if (GUILayout.Button("表す"))
                targetScript.OnAwake(1.0f);

            if (GUILayout.Button("隠す"))
                targetScript.OnSleep(0.0f);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
