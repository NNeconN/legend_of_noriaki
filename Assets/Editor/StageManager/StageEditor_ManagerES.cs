using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

[CustomEditor(typeof(StageEditor_Manager))]
public class StageEditor_ManagerES : Editor
{
    /// <summary> 対象クラス </summary>
    StageEditor_Manager targetScript = null;

    /// <summary> 書き出し注意 </summary>
    bool writeWarning = false;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorUtility.HeaderProcess("データ関係", Color.gray);
                    GUI.backgroundColor = defaultColor;

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        var worldNum = targetScript.GetWorldNum();
                        var stageNum = targetScript.GetStageNum();

                        EditorUtility.IntField(ref worldNum, "対象ワールド番号");
                        EditorUtility.IntField(ref stageNum, "対象ステージ番号");

                        if (worldNum != targetScript.GetWorldNum() || stageNum != targetScript.GetStageNum())
                        {
                            Undo.RecordObject(target, "Update World / Stage Num");
                            targetScript.SetWorldNum(worldNum);
                            targetScript.SetStageNum(stageNum);
                            UnityEditor.EditorUtility.SetDirty(target);

                            worldNum = targetScript.GetWorldNum();
                            stageNum = targetScript.GetStageNum();
                        }

                        using (new GUILayout.HorizontalScope())
                        {
                            if (!writeWarning)
                            {
                                if (GUILayout.Button("書き込み"))
                                {
                                    var filePath = StageData.GetFilePath(worldNum, stageNum);

                                    if (File.Exists(filePath))
                                        writeWarning = true;
                                    else if (!targetScript.WriteData())
                                        Debug.LogError("読み込み失敗！");
                                }
                            }

                            if (GUILayout.Button("読み込み"))
                            {
                                writeWarning = false;
                                if (!targetScript.ReadData())
                                    Debug.LogError("読み込み失敗！");
                            }
                        }

                        if (writeWarning)
                        {
                            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                            {
                                EditorGUILayout.HelpBox("ワールド：" + worldNum.ToString() + "\nステージ：" + stageNum.ToString() + "このデータは既に存在しています！\nそれでも書き出しをしますか？", MessageType.Warning);
                            }

                            using (new GUILayout.HorizontalScope())
                            {
                                if (GUILayout.Button("はい"))
                                {
                                    writeWarning = false;
                                    if (!targetScript.WriteData())
                                        Debug.LogError("読み込み失敗！");
                                }

                                if (GUILayout.Button("いいえ"))
                                    writeWarning = false;
                            }
                        }
                    }
                }

                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorUtility.HeaderProcess("初期化・解放", Color.gray);
                    GUI.backgroundColor = defaultColor;

                    using (new GUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("ステージマネージャー初期化"))
                        {
                            Undo.RecordObject(target, "Initialize");
                            targetScript.Initialize();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (GUILayout.Button("ステージマネージャー破棄"))
                        {
                            Undo.RecordObject(target, "UnInitialize");
                            targetScript.UnInitialize();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }
                    }
                }

                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorUtility.HeaderProcess("追加・編集", Color.gray);
                    GUI.backgroundColor = defaultColor;

                    var gridSize = targetScript.GetGridSize();
                    if (EditorUtility.DelayedVector2IntField(ref gridSize, "グリッドの大きさ", "横幅 : ", "縦幅 : ", Color.gray))
                    {
                        Undo.RecordObject(target, "SetGridSize");
                        targetScript.SetGridSize(gridSize);
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("再びグリッドに設定"))
                    {
                        Undo.RecordObject(target, "ResetGridAllObjects");
                        targetScript.ResetGridAllObjects();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("領域レイヤーを追加"))
                    {
                        Undo.RecordObject(target, "AddAreaLayer");
                        targetScript.AddAreaLayer(BlockData.BlockType.Fixed);
                        UnityEditor.EditorUtility.SetDirty(target);
                    }
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    #region ギズモ描画関連

    /// <summary>
    /// ギズモ描画
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <param name="gizmoType"> ギズモタイプ </param>
    [DrawGizmo(GizmoType.Selected | GizmoType.NonSelected, typeof(StageEditor_Manager))]
    static void OnDrawGizmos(StageEditor_Manager target, GizmoType gizmoType)
    {
        if (EditorApplication.isPlaying)
            return;

        var defaultColor = Gizmos.color;
        Gizmos.color = Color.red;

        var gridNum = target.GetGridSize();

        var lineNumX = gridNum.x - 1;
        var lineNumY = gridNum.y - 1;

        var fromX = new Vector3(target.GetLeftPivotX() + 1, target.GetBottomPivotY(), target.transform.position.z);
        var toX = new Vector3(fromX.x, target.GetTopPivotY(), target.transform.position.z);
        var fromY = new Vector3(target.GetLeftPivotX(), target.GetBottomPivotY() + 1, target.transform.position.z);
        var toY = new Vector3(target.GetRightPivotX(), fromY.y, target.transform.position.z);

        for (int i = 0; i < lineNumX; ++i)
        {
            Gizmos.DrawLine(fromX, toX);
            toX.x = ++fromX.x;
        }

        for (int i = 0; i < lineNumY; ++i)
        {
            Gizmos.DrawLine(fromY, toY);
            toY.y = ++fromY.y;
        }

        Gizmos.color = defaultColor;
    }

    #endregion
}
