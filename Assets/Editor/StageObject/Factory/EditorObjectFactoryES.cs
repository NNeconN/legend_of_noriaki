using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EditorObjectFactory))]
public class EditorObjectFactoryES : Editor
{
    /// <summary> 対象クラス </summary>
    EditorObjectFactory targetScript = null;

    /// <summary> objectsLayer プロパティ </summary>
    SerializedProperty objectsLayerProperty = null;

    /// <summary> blockLayer プロパティ </summary>
    SerializedProperty blockLayerProperty = null;

    /// <summary> areaLayer プロパティ </summary>
    SerializedProperty areaLayerProperty = null;

    /// <summary> backObject プロパティ </summary>
    SerializedProperty backObjectProperty = null;

    /// <summary> processAreaObject プロパティ </summary>
    SerializedProperty processAreaObjectProperty = null;

    /// <summary> playerObject プロパティ </summary>
    SerializedProperty playerObjectProperty = null;

    /// <summary> goalObject プロパティ </summary>
    SerializedProperty goalObjectProperty = null;

    /// <summary> enemyObject_1 プロパティ </summary>
    SerializedProperty enemyObject_1_Property = null;

    /// <summary> blockObject プロパティ </summary>
    SerializedProperty blockObjectProperty = null;

    /// <summary> areaObject プロパティ </summary>
    SerializedProperty areaObjectProperty = null;

    /// <summary> handleObject プロパティ </summary>
    SerializedProperty handleObjectProperty = null;

    /// <summary> objectsLayerPool プロパティ </summary>
    SerializedProperty objectsLayerPoolProperty = null;

    /// <summary> blockLayerPool プロパティ </summary>
    SerializedProperty blockLayerPoolProperty = null;

    /// <summary> areaLayerPool プロパティ </summary>
    SerializedProperty areaLayerPoolProperty = null;

    /// <summary> backObjectPool プロパティ </summary>
    SerializedProperty backObjectPoolProperty = null;

    /// <summary> processAreaObjectPool プロパティ </summary>
    SerializedProperty processAreaObjectPoolProperty = null;

    /// <summary> playerObjectPool プロパティ </summary>
    SerializedProperty playerObjectPoolProperty = null;

    /// <summary> goalObjectPool プロパティ </summary>
    SerializedProperty goalObjectPoolProperty = null;

    /// <summary> enemyObject_1_Pool プロパティ </summary>
    SerializedProperty enemyObject_1_PoolProperty = null;

    /// <summary> blockObjectPool プロパティ </summary>
    SerializedProperty blockObjectPoolProperty = null;

    /// <summary> areaObjectPool プロパティ </summary>
    SerializedProperty areaObjectPoolProperty = null;

    /// <summary> handleObjectPool プロパティ </summary>
    SerializedProperty handleObjectPoolProperty = null;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        EditorUtility.ConvertSerializeProperty(ref objectsLayerProperty, serializedObject, "objectsLayer");
        EditorUtility.ConvertSerializeProperty(ref blockLayerProperty, serializedObject, "blockLayer");
        EditorUtility.ConvertSerializeProperty(ref areaLayerProperty, serializedObject, "areaLayer");
        EditorUtility.ConvertSerializeProperty(ref backObjectProperty, serializedObject, "backObject");
        EditorUtility.ConvertSerializeProperty(ref processAreaObjectProperty, serializedObject, "processAreaObject");
        EditorUtility.ConvertSerializeProperty(ref playerObjectProperty, serializedObject, "playerObject");
        EditorUtility.ConvertSerializeProperty(ref goalObjectProperty, serializedObject, "goalObject");
        EditorUtility.ConvertSerializeProperty(ref enemyObject_1_Property, serializedObject, "enemyObject_1");
        EditorUtility.ConvertSerializeProperty(ref blockObjectProperty, serializedObject, "blockObject");
        EditorUtility.ConvertSerializeProperty(ref areaObjectProperty, serializedObject, "areaObject");
        EditorUtility.ConvertSerializeProperty(ref handleObjectProperty, serializedObject, "handleObject");

        EditorUtility.ConvertSerializeProperty(ref objectsLayerPoolProperty, serializedObject, "objectsLayerPool");
        EditorUtility.ConvertSerializeProperty(ref blockLayerPoolProperty, serializedObject, "blockLayerPool");
        EditorUtility.ConvertSerializeProperty(ref areaLayerPoolProperty, serializedObject, "areaLayerPool");
        EditorUtility.ConvertSerializeProperty(ref backObjectPoolProperty, serializedObject, "backObjectPool");
        EditorUtility.ConvertSerializeProperty(ref processAreaObjectPoolProperty, serializedObject, "processAreaObjectPool");
        EditorUtility.ConvertSerializeProperty(ref playerObjectPoolProperty, serializedObject, "playerObjectPool");
        EditorUtility.ConvertSerializeProperty(ref goalObjectPoolProperty, serializedObject, "goalObjectPool");
        EditorUtility.ConvertSerializeProperty(ref enemyObject_1_PoolProperty, serializedObject, "enemyObject_1_Pool");
        EditorUtility.ConvertSerializeProperty(ref blockObjectPoolProperty, serializedObject, "blockObjectPool");
        EditorUtility.ConvertSerializeProperty(ref areaObjectPoolProperty, serializedObject, "areaObjectPool");
        EditorUtility.ConvertSerializeProperty(ref handleObjectPoolProperty, serializedObject, "handleObjectPool");

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorUtility.HeaderProcess("プレハブ設定", Color.gray);
                    GUI.backgroundColor = defaultColor;

                    EditorUtility.PropertyField(objectsLayerProperty, new GUIContent("オブジェクトレイヤー"), 15.0f);
                    EditorUtility.PropertyField(blockLayerProperty, new GUIContent("ブロックレイヤー"), 15.0f);
                    EditorUtility.PropertyField(areaLayerProperty, new GUIContent("領域レイヤー"), 15.0f);
                    EditorUtility.PropertyField(backObjectProperty, new GUIContent("背景"), 15.0f);
                    EditorUtility.PropertyField(processAreaObjectProperty, new GUIContent("処理領域"), 15.0f);
                    EditorUtility.PropertyField(playerObjectProperty, new GUIContent("プレイヤー"), 15.0f);
                    EditorUtility.PropertyField(goalObjectProperty, new GUIContent("ゴール"), 15.0f);
                    EditorUtility.PropertyField(enemyObject_1_Property, new GUIContent("敵 1"), 15.0f);
                    EditorUtility.PropertyField(blockObjectProperty, new GUIContent("ブロック"), 15.0f);
                    EditorUtility.PropertyField(areaObjectProperty, new GUIContent("領域"), 15.0f);
                    EditorUtility.PropertyField(handleObjectProperty, new GUIContent("取っ手"), 15.0f);
                }

                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorUtility.HeaderProcess("プール解放", Color.gray);
                    GUI.backgroundColor = defaultColor;

                    if (GUILayout.Button("全て"))
                    {
                        Undo.RecordObject(target, "ReleaseAll");
                        targetScript.ReleaseObjectsLayerPool();
                        targetScript.ReleaseBlockLayerPool();
                        targetScript.ReleaseProcessAreaObjectPool();
                        targetScript.ReleaseAreaLayerPool();
                        targetScript.ReleaseBackObjectPool();
                        targetScript.ReleasePlayerObjectPool();
                        targetScript.ReleaseGoalObjectPool();
                        targetScript.ReleaseEnemyObjectPool_1();
                        targetScript.ReleaseBlockObjectPool();
                        targetScript.ReleaseAreaObjectPool();
                        targetScript.ReleaseHandleObjectPool();

                        while (targetScript.transform.childCount > 0)
                            GameObjectUtility.DestroyGameObject(targetScript.transform.GetChild(0).gameObject);

                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("オブジェクトレイヤー"))
                    {
                        Undo.RecordObject(target, "ReleaseObjectsLayerPool");
                        targetScript.ReleaseObjectsLayerPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("ブロックレイヤー"))
                    {
                        Undo.RecordObject(target, "ReleaseAreaLayerPool");
                        targetScript.ReleaseBlockLayerPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("領域レイヤー"))
                    {
                        Undo.RecordObject(target, "ReleaseBlockLayerPool");
                        targetScript.ReleaseAreaLayerPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("背景"))
                    {
                        Undo.RecordObject(target, "ReleaseBackObjectPool");
                        targetScript.ReleaseBackObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("処理領域"))
                    {
                        Undo.RecordObject(target, "ReleaseProcessAreaObjectPool");
                        targetScript.ReleaseProcessAreaObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("プレイヤー"))
                    {
                        Undo.RecordObject(target, "ReleasePlayerObjectPool");
                        targetScript.ReleasePlayerObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("ゴール"))
                    {
                        Undo.RecordObject(target, "ReleaseGoalObjectPool");
                        targetScript.ReleaseGoalObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("敵 1"))
                    {
                        Undo.RecordObject(target, "ReleaseEnemyObjectPool_1");
                        targetScript.ReleaseEnemyObjectPool_1();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("ブロック"))
                    {
                        Undo.RecordObject(target, "ReleaseBlockObjectPool");
                        targetScript.ReleaseBlockObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("領域"))
                    {
                        Undo.RecordObject(target, "ReleaseAreaObjectPool");
                        targetScript.ReleaseAreaObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }

                    if (GUILayout.Button("取っ手"))
                    {
                        Undo.RecordObject(target, "ReleaseHandleObjectPool");
                        targetScript.ReleaseHandleObjectPool();
                        UnityEditor.EditorUtility.SetDirty(target);
                    }
                }

                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorUtility.HeaderProcess("プール参照", Color.gray);
                    GUI.backgroundColor = defaultColor;

                    using (new EditorGUI.DisabledGroupScope(true))
                    {
                        EditorUtility.PropertyField(objectsLayerPoolProperty, new GUIContent("オブジェクトレイヤー"), 15.0f);
                        EditorUtility.PropertyField(blockLayerPoolProperty, new GUIContent("ブロックレイヤー"), 15.0f);
                        EditorUtility.PropertyField(areaLayerPoolProperty, new GUIContent("領域レイヤー"), 15.0f);
                        EditorUtility.PropertyField(backObjectPoolProperty, new GUIContent("背景"), 15.0f);
                        EditorUtility.PropertyField(processAreaObjectPoolProperty, new GUIContent("処理領域"), 15.0f);
                        EditorUtility.PropertyField(playerObjectPoolProperty, new GUIContent("プレイヤー"), 15.0f);
                        EditorUtility.PropertyField(goalObjectPoolProperty, new GUIContent("ゴール"), 15.0f);
                        EditorUtility.PropertyField(enemyObject_1_PoolProperty, new GUIContent("敵 1"), 15.0f);
                        EditorUtility.PropertyField(blockObjectPoolProperty, new GUIContent("ブロック"), 15.0f);
                        EditorUtility.PropertyField(areaObjectPoolProperty, new GUIContent("領域"), 15.0f);
                        EditorUtility.PropertyField(handleObjectPoolProperty, new GUIContent("取っ手"), 15.0f);
                    }
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
