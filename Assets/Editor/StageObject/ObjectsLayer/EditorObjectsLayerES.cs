using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEditor;
using UnityEngine;
using static HandleData;

[CustomEditor(typeof(EditorObjectsLayer))]
public class EditorObjectsLayerES : Editor
{
    /// <summary> 対象クラス </summary>
    EditorObjectsLayer targetScript = null;

    /// <summary> 設置方向 </summary>
    StageData.GridDirection gridDirection = StageData.GridDirection.None;

    /// <summary> プレイヤー移動方向 </summary>
    PlayerData.MoveDirection moveDirection = PlayerData.MoveDirection.Right;

    /// <summary> 背景マテリアルのインデックス番号 </summary>
    int backMaterialIndex = 0;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                if (targetScript.Manager)
                {
                    var gridPosition = targetScript.GetGridPosition();
                    var gridSize = targetScript.GetGridSize();

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("追加・編集位置設定", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        gridDirection = (StageData.GridDirection)EditorGUILayout.EnumPopup("次の設置方向", gridDirection);

                        if (EditorUtility.Vector2IntField(ref gridPosition, "グリッドの位置", "X : ", "Y : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridPosition");
                            targetScript.SetGridPosition(gridPosition);
                            gridPosition = targetScript.GetGridPosition();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (EditorUtility.Vector2IntField(ref gridSize, "グリッドの大きさ", "横幅 : ", "縦幅 : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridSize");
                            targetScript.SetGridSize(gridSize);
                            gridSize = targetScript.GetGridSize();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("全体編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        if (GUILayout.Button("全て削除"))
                        {
                            Undo.RecordObject(target, "RemoveAll");
                            targetScript.RemoveAllBackObject();
                            targetScript.RemoveAllPlayerObject();
                            targetScript.RemoveAllGoalObject();
                            targetScript.RemoveAllEnemyObject_1();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanSlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection))))
                        {
                            if (GUILayout.Button("まとめて移動"))
                            {
                                Undo.RecordObject(target, "SlideGridPosition");
                                targetScript.SlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection));
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("背景追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        EditorUtility.IntField(ref backMaterialIndex, "背景のマテリアル番号");

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddBackObject(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddBackObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddBackObject(gridPosition, gridSize, backMaterialIndex), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtBackObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtBackObject");
                                targetScript.RemoveAtBackObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeBackObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeBackObjects");
                                targetScript.RemoveRangeBackObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasBackObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllBackObject");
                                targetScript.RemoveAllBackObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("処理領域追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddProcessAreaObject(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddProcessAreaObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddProcessAreaObject(gridPosition, gridSize), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtProcessAreaObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtProcessAreaObject");
                                targetScript.RemoveAtProcessAreaObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeProcessAreaObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeProcessAreaObjects");
                                targetScript.RemoveRangeProcessAreaObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasProcessAreaObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllProcessAreaObject");
                                targetScript.RemoveAllProcessAreaObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("プレイヤー追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        moveDirection = (PlayerData.MoveDirection)EditorGUILayout.EnumPopup("移動歩行", moveDirection);

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddPlayerObject(gridPosition)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddPlayerObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddPlayerObject(gridPosition, moveDirection), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtPlayerObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtPlayerObject");
                                targetScript.RemoveAtPlayerObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangePlayerObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangePlayerObjects");
                                targetScript.RemoveRangePlayerObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasPlayerObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllPlayerObject");
                                targetScript.RemoveAllPlayerObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("ゴール追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddGoalObject(gridPosition)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddGoalObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddGoalObject(gridPosition), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtGoalObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtGoalObject");
                                targetScript.RemoveAtGoalObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeGoalObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeGoalObjects");
                                targetScript.RemoveRangeGoalObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasGoalObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllGoalObject");
                                targetScript.RemoveAllGoalObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("敵 1 追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddEnemyObject_1(gridPosition)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddEnemyObject_1");
                                SlideGridPosition(ref gridPosition, targetScript.AddEnemyObject_1(gridPosition), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtEnemyObjects_1(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtEnemyObject_1");
                                targetScript.RemoveAtEnemyObject_1(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeEnemyObjects_1(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeEnemyObject_1");
                                targetScript.RemoveRangeEnemyObject_1(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasEnemyObjects_1()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllEnemyObject_1");
                                targetScript.RemoveAllEnemyObject_1();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("編集不可能！\n意図しない生成が行われている可能性があります！", MessageType.Warning);
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// グリッド位置をずらす
    /// </summary>
    /// <param name="gridPosition"> グリッド位置 </param>
    /// <param name="editorObject"></param>
    /// <param name="gridDirection"></param>
    void SlideGridPosition(ref Vector2Int gridPosition, EditorObject editorObject, StageData.GridDirection gridDirection)
    {
        if (!editorObject)
            return;

        var direction = StageData.GetDirectionByGridDirection(gridDirection);

        Undo.RecordObject(target, "SetGridPosition");
        targetScript.SetGridPosition(gridPosition + (direction * editorObject.GetGridSize()));
        gridPosition = targetScript.GetGridPosition();
        UnityEditor.EditorUtility.SetDirty(target);
    }

    #region ギズモ描画関連

    /// <summary>
    /// ギズモ描画
    /// </summary>
    /// <param name="target"> オブジェクトレイヤー </param>
    /// <param name="gizmoType"> ギズモタイプ </param>
    [DrawGizmo(GizmoType.Selected, typeof(EditorObjectsLayer))]
    static void OnDrawGizmos(EditorObjectsLayer target, GizmoType gizmoType)
    {
        if (EditorApplication.isPlaying)
            return;

        var defaultColor = Gizmos.color;

        var selectedGridPosition = target.GetGridPosition();
        var selectedGridSize = target.GetGridSize();

        var cubePosition = target.transform.position;
        cubePosition.x += selectedGridPosition.x + 0.5f;
        cubePosition.y += selectedGridPosition.y + 0.5f;
        cubePosition.z -= 0.0002f;

        var cubeSize = Vector3.one * 0.8f;
        cubeSize.z = 0.0f;

        Gizmos.color = Color.red;
        Gizmos.DrawCube(cubePosition, cubeSize * 1.1f);
        Gizmos.color = Color.blue;

        cubePosition.z += 0.0001f;

        for (int x = 0; x < selectedGridSize.x; ++x)
        {
            for (int y = 0; y < selectedGridSize.y; ++y)
            {
                Gizmos.DrawCube(cubePosition, cubeSize);

                cubePosition.y += 1.0f;
            }

            cubePosition.x += 1.0f;
            cubePosition.y = target.transform.position.y + selectedGridPosition.y + 0.5f;
        }

        Gizmos.color = defaultColor;
    }

    #endregion
}
