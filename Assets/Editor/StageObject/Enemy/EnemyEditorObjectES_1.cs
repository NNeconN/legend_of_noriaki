using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EnemyEditorObject_1))]
public class EnemyEditorObjectES_1 : Editor
{
    /// <summary> 対象クラス </summary>
    EnemyEditorObject_1 targetScript = null;

    /// <summary> 設置方向 </summary>
    StageData.GridDirection gridDirection = StageData.GridDirection.None;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                if (targetScript.Manager)
                {
                    var gridPosition = targetScript.GetGridPosition();

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("編集位置設定", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        gridDirection = (StageData.GridDirection)EditorGUILayout.EnumPopup("次の設置方向", gridDirection);

                        if (EditorUtility.DelayedVector2IntField(ref gridPosition, "グリッドの位置", "X : ", "Y : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridPosition");
                            targetScript.SetGridPosition(gridPosition);
                            gridPosition = targetScript.GetGridPosition();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanSlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection))))
                        {
                            if (GUILayout.Button("移動"))
                            {
                                Undo.RecordObject(target, "SlideGridPosition");
                                targetScript.SlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection));
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        if (GUILayout.Button("削除"))
                        {
                            Undo.RecordObject(target, "DestroyThis");
                            targetScript.DestroyThis();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("編集不可能！\n意図しない生成が行われている可能性があります！", MessageType.Warning);
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
