using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EditorAreaLayer))]
public class EditorAreaLayerES : Editor
{
    /// <summary> 対象クラス </summary>
    EditorAreaLayer targetScript = null;

    /// <summary> 設置方向 </summary>
    StageData.GridDirection gridDirection = StageData.GridDirection.None;

    /// <summary> 取っ手の種類 </summary>
    HandleData.HandleType handleType = HandleData.HandleType.MiddleHorizontal;

    /// <summary> 領域の種類 </summary>
    BlockData.BlockType areaType = BlockData.BlockType.Red;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                if (targetScript.Manager)
                {
                    var gridPosition = targetScript.GetGridPosition();
                    var gridSize = targetScript.GetGridSize();

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("追加・編集位置設定", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        gridDirection = (StageData.GridDirection)EditorGUILayout.EnumPopup("次の設置方向", gridDirection);

                        if (EditorUtility.Vector2IntField(ref gridPosition, "グリッドの位置", "X : ", "Y : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridPosition");
                            targetScript.SetGridPosition(gridPosition);
                            gridPosition = targetScript.GetGridPosition();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (EditorUtility.Vector2IntField(ref gridSize, "グリッドの大きさ", "横幅 : ", "縦幅 : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridSize");
                            targetScript.SetGridSize(gridSize);
                            gridSize = targetScript.GetGridSize();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("全体編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        var myAreaType = targetScript.GetAreaType();
                        var prevMyAreaType = myAreaType;
                        myAreaType = (BlockData.BlockType)EditorGUILayout.EnumPopup("領域の種類", myAreaType);
                        if (myAreaType != prevMyAreaType)
                        {
                            Undo.RecordObject(target, "SetAreaType");
                            targetScript.SetAreaType(myAreaType);
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (GUILayout.Button("全て削除"))
                        {
                            Undo.RecordObject(target, "RemoveAll");
                            targetScript.RemoveAllAreaObject();
                            targetScript.RemoveAllHandleObject();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanSlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection))))
                        {
                            if (GUILayout.Button("まとめて移動"))
                            {
                                Undo.RecordObject(target, "SlideGridPosition");
                                targetScript.SlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection));
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("領域追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddAreaObject(gridPosition)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddAreaObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddAreaObject(gridPosition), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        if (GUILayout.Button("まとめて追加"))
                        {
                            Undo.RecordObject(target, "AddRangeAreaObject");
                            targetScript.AddRangeAreaObject(gridPosition, gridSize);
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtAreaObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtAreaObject");
                                targetScript.RemoveAtAreaObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeAreaObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeAreaObjects");
                                targetScript.RemoveRangeAreaObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAreaObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllAreaObject");
                                targetScript.RemoveAllAreaObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("取っ手追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;
                        handleType = (HandleData.HandleType)EditorGUILayout.EnumPopup("取っ手の種類", handleType);
                        areaType = (BlockData.BlockType)EditorGUILayout.EnumPopup("領域の種類", areaType);

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddHandleObject(gridPosition)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddHandleObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddHandleObject(gridPosition, handleType, areaType), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtHandleObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtHandleObject");
                                targetScript.RemoveAtHandleObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeHandleObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeHandleObjects");
                                targetScript.RemoveRangeHandleObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasHandleObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllHandleObject");
                                targetScript.RemoveAllHandleObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("編集不可能！\n意図しない生成が行われている可能性があります！", MessageType.Warning);
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// グリッド位置をずらす
    /// </summary>
    /// <param name="gridPosition"> グリッド位置 </param>
    /// <param name="editorObject"></param>
    /// <param name="gridDirection"></param>
    void SlideGridPosition(ref Vector2Int gridPosition, EditorObject editorObject, StageData.GridDirection gridDirection)
    {
        if (!editorObject)
            return;

        var direction = StageData.GetDirectionByGridDirection(gridDirection);

        Undo.RecordObject(target, "SetGridPosition");
        targetScript.SetGridPosition(gridPosition + (direction * editorObject.GetGridSize()));
        gridPosition = targetScript.GetGridPosition();
        UnityEditor.EditorUtility.SetDirty(target);
    }

    #region ギズモ描画関連

    /// <summary>
    /// ギズモ描画
    /// </summary>
    /// <param name="target"> 領域レイヤー </param>
    /// <param name="gizmoType"> ギズモタイプ </param>
    [DrawGizmo(GizmoType.Selected, typeof(EditorAreaLayer))]
    static void OnDrawGizmos(EditorAreaLayer target, GizmoType gizmoType)
    {
        if (EditorApplication.isPlaying)
            return;

        var defaultColor = Gizmos.color;

        var selectedGridPosition = target.GetGridPosition();
        var selectedGridSize = target.GetGridSize();

        var cubePosition = target.transform.position;
        cubePosition.x += selectedGridPosition.x + 0.5f;
        cubePosition.y += selectedGridPosition.y + 0.5f;
        cubePosition.z -= 0.0002f;

        var cubeSize = Vector3.one * 0.8f;
        cubeSize.z = 0.0f;

        Gizmos.color = Color.red;
        Gizmos.DrawCube(cubePosition, cubeSize * 1.1f);
        Gizmos.color = Color.blue;

        cubePosition.z += 0.0001f;

        for (int x = 0; x < selectedGridSize.x; ++x)
        {
            for (int y = 0; y < selectedGridSize.y; ++y)
            {
                Gizmos.DrawCube(cubePosition, cubeSize);

                cubePosition.y += 1.0f;
            }

            cubePosition.x += 1.0f;
            cubePosition.y = target.transform.position.y + selectedGridPosition.y + 0.5f;
        }

        Gizmos.color = defaultColor;
    }

    #endregion
}
