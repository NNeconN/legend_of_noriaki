using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EditorBlockLayer))]
public class EditorBlockLayerES : Editor
{
    /// <summary> 対象クラス </summary>
    EditorBlockLayer targetScript = null;

    /// <summary> 設置方向 </summary>
    StageData.GridDirection gridDirection = StageData.GridDirection.None;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                if (targetScript.Manager)
                {
                    var gridPosition = targetScript.GetGridPosition();
                    var gridSize = targetScript.GetGridSize();

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("追加・編集位置設定", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        if (EditorUtility.Vector2IntField(ref gridPosition, "グリッドの位置", "X : ", "Y : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridPosition");
                            targetScript.SetGridPosition(gridPosition);
                            gridPosition = targetScript.GetGridPosition();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (EditorUtility.Vector2IntField(ref gridSize, "グリッドの大きさ", "横幅 : ", "縦幅 : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridSize");
                            targetScript.SetGridSize(gridSize);
                            gridSize = targetScript.GetGridSize();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        gridDirection = (StageData.GridDirection)EditorGUILayout.EnumPopup("次の設置方向", gridDirection);
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("全体編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        if (GUILayout.Button("全て削除"))
                        {
                            Undo.RecordObject(target, "RemoveAll");
                            targetScript.RemoveAllBlockObject();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanSlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection))))
                        {
                            if (GUILayout.Button("まとめて移動"))
                            {
                                Undo.RecordObject(target, "SlideGridPosition");
                                targetScript.SlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection));
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("ブロック追加・編集", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanAddBlockObject(gridPosition)))
                        {
                            if (GUILayout.Button("追加"))
                            {
                                Undo.RecordObject(target, "AddBlockObject");
                                SlideGridPosition(ref gridPosition, targetScript.AddBlockObject(gridPosition), gridDirection);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        if (GUILayout.Button("まとめて追加"))
                        {
                            Undo.RecordObject(target, "AddRangeBlockObject");
                            targetScript.AddRangeBlockObject(gridPosition, gridSize);
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (GUILayout.Button("周囲に追加"))
                        {
                            Undo.RecordObject(target, "AddFrameBlockObject");
                            targetScript.AddFrameBlockObject(gridPosition, gridSize);
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasAtBlockObjects(gridPosition)))
                        {
                            if (GUILayout.Button("削除"))
                            {
                                Undo.RecordObject(target, "RemoveAtBlockObject");
                                targetScript.RemoveAtBlockObject(gridPosition);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasRangeBlockObjects(gridPosition, gridSize)))
                        {
                            if (GUILayout.Button("まとめて削除"))
                            {
                                Undo.RecordObject(target, "RemoveRangeBlockObjects");
                                targetScript.RemoveRangeBlockObjects(gridPosition, gridSize);
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckHasBlockObjects()))
                        {
                            if (GUILayout.Button("全て削除"))
                            {
                                Undo.RecordObject(target, "RemoveAllBackObject");
                                targetScript.RemoveAllBlockObject();
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("編集不可能！\n意図しない生成が行われている可能性があります！", MessageType.Warning);
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// グリッド位置をずらす
    /// </summary>
    /// <param name="gridPosition"> グリッド位置 </param>
    /// <param name="editorObject"></param>
    /// <param name="gridDirection"></param>
    void SlideGridPosition(ref Vector2Int gridPosition, EditorObject editorObject, StageData.GridDirection gridDirection)
    {
        if (!editorObject)
            return;

        var direction = StageData.GetDirectionByGridDirection(gridDirection);

        Undo.RecordObject(target, "SetGridPosition");
        targetScript.SetGridPosition(gridPosition + (direction * editorObject.GetGridSize()));
        gridPosition = targetScript.GetGridPosition();
        UnityEditor.EditorUtility.SetDirty(target);
    }

    #region ギズモ描画関連

    /// <summary>
    /// ギズモ描画
    /// </summary>
    /// <param name="target"> ブロックレイヤー </param>
    /// <param name="gizmoType"> ギズモタイプ </param>
    [DrawGizmo(GizmoType.Selected, typeof(EditorBlockLayer))]
    static void OnDrawGizmos(EditorBlockLayer target, GizmoType gizmoType)
    {
        if (EditorApplication.isPlaying)
            return;

        var defaultColor = Gizmos.color;

        var selectedGridPosition = target.GetGridPosition();
        var selectedGridSize = target.GetGridSize();

        var cubePosition = target.transform.position;
        cubePosition.x += selectedGridPosition.x + 0.5f;
        cubePosition.y += selectedGridPosition.y + 0.5f;
        cubePosition.z -= 0.0002f;

        var cubeSize = Vector3.one * 0.8f;
        cubeSize.z = 0.0f;

        Gizmos.color = Color.red;
        Gizmos.DrawCube(cubePosition, cubeSize * 1.1f);
        Gizmos.color = Color.blue;

        cubePosition.z += 0.0001f;

        for (int x = 0; x < selectedGridSize.x; ++x)
        {
            for (int y = 0; y < selectedGridSize.y; ++y)
            {
                Gizmos.DrawCube(cubePosition, cubeSize);

                cubePosition.y += 1.0f;
            }

            cubePosition.x += 1.0f;
            cubePosition.y = target.transform.position.y + selectedGridPosition.y + 0.5f;
        }

        Gizmos.color = defaultColor;
    }

    #endregion
}
