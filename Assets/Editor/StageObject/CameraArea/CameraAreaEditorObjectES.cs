using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraAreaEditorObject))]
public class CameraAreaEditorObjectES : Editor
{
    /// <summary> 対象クラス </summary>
    CameraAreaEditorObject targetScript = null;

    /// <summary> 設置方向 </summary>
    StageData.GridDirection gridDirection = StageData.GridDirection.None;

    public override void OnInspectorGUI()
    {
        if (!EditorUtility.ConvertTarget(ref targetScript, target))
        {
            base.OnInspectorGUI();
            return;
        }

        EditorUtility.ScriptReferenceField(target, new GUIContent("Script"));

        serializedObject.Update();

        Color defaultColor = GUI.backgroundColor;
        using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            if (Application.isPlaying)
            {
            }
            else
            {
                if (targetScript.Manager)
                {
                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("編集位置設定", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        gridDirection = (StageData.GridDirection)EditorGUILayout.EnumPopup("次の設置方向", gridDirection);

                        var gridPosition = targetScript.GetGridPosition();
                        var gridSize = targetScript.GetGridSize();

                        if (EditorUtility.DelayedVector2IntField(ref gridPosition, "グリッドの位置", "X : ", "Y : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridPosition");
                            targetScript.SetGridPosition(gridPosition);
                            gridPosition = targetScript.GetGridPosition();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (EditorUtility.DelayedVector2IntField(ref gridSize, "グリッドの大きさ", "横幅 : ", "縦幅 : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetGridSize");
                            targetScript.SetGridSize(gridSize);
                            gridSize = targetScript.GetGridSize();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        using (new EditorGUI.DisabledGroupScope(!targetScript.CheckCanSlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection))))
                        {
                            if (GUILayout.Button("移動"))
                            {
                                Undo.RecordObject(target, "SlideGridPosition");
                                targetScript.SlideGridPosition(StageData.GetDirectionByGridDirection(gridDirection));
                                UnityEditor.EditorUtility.SetDirty(target);
                            }
                        }

                        if (GUILayout.Button("削除"))
                        {
                            Undo.RecordObject(target, "DestroyThis");
                            targetScript.DestroyThis();
                            UnityEditor.EditorUtility.SetDirty(target);
                        }
                    }

                    using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorUtility.HeaderProcess("編集位置設定", Color.gray);
                        GUI.backgroundColor = defaultColor;

                        var processGridAreaOffset = targetScript.GetProcessGridAreaOffset();
                        var processGridAreaSize = targetScript.GetProcessGridAreaSize();

                        if (EditorUtility.DelayedVector2IntField(ref processGridAreaOffset, "処理領域の位置", "X : ", "Y : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetProcessGridAreaOffset");
                            targetScript.SetProcessGridAreaOffset(processGridAreaOffset);
                            UnityEditor.EditorUtility.SetDirty(target);
                        }

                        if (EditorUtility.DelayedVector2IntField(ref processGridAreaSize, "処理領域の大きさ", "横幅 : ", "縦幅 : ", Color.gray))
                        {
                            Undo.RecordObject(target, "SetProcessGridAreaSize");
                            targetScript.SetProcessGridAreaSize(processGridAreaSize);
                            UnityEditor.EditorUtility.SetDirty(target);
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("編集不可能！\n意図しない生成が行われている可能性があります！", MessageType.Warning);
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    #region ギズモ描画関連

    /// <summary>
    /// ギズモ描画
    /// </summary>
    /// <param name="target"> 対象 </param>
    /// <param name="gizmoType"> ギズモタイプ </param>
    [DrawGizmo(GizmoType.Selected, typeof(CameraAreaEditorObject))]
    static void OnDrawGizmos(CameraAreaEditorObject target, GizmoType gizmoType)
    {
        if (EditorApplication.isPlaying)
            return;

        var defaultColor = Gizmos.color;

        var gridPosition = target.GetGridPosition();
        var gridSize = target.GetGridSize();

        var cubePosition = target.transform.position;
        cubePosition.x += gridPosition.x + 0.5f;
        cubePosition.y += gridPosition.y + 0.5f;
        cubePosition.z -= 0.0002f;

        var cubeSize = Vector3.one * 0.8f;
        cubeSize.z = 0.0f;

        Gizmos.color = Color.red;
        for (int x = 0; x < gridSize.x; ++x)
        {
            for (int y = 0; y < gridSize.y; ++y)
            {
                Gizmos.DrawCube(cubePosition, cubeSize);

                cubePosition.y += 1.0f;
            }

            cubePosition.x += 1.0f;
            cubePosition.y = target.transform.position.y + gridPosition.y + 0.5f;
        }

        gridPosition = target.GetProcessGridAreaOffset();
        gridSize = target.GetProcessGridAreaSize();

        cubePosition = target.transform.position;
        cubePosition.x += gridPosition.x + 0.5f;
        cubePosition.y += gridPosition.y + 0.5f;
        cubePosition.z -= 0.0001f;

        cubeSize = Vector3.one * 0.9f;
        cubeSize.z = 0.0f;

        Gizmos.color = Color.blue;
        for (int x = 0; x < gridSize.x; ++x)
        {
            for (int y = 0; y < gridSize.y; ++y)
            {
                Gizmos.DrawWireCube(cubePosition, cubeSize);

                cubePosition.y += 1.0f;
            }

            cubePosition.x += 1.0f;
            cubePosition.y = target.transform.position.y + gridPosition.y + 0.5f;
        }

        Gizmos.color = defaultColor;
    }

    #endregion
}
